//This file is part of the MeMoJC library and is protected by LGPL.

package lrg.memoria.core;

public final class AccessMode {
    public static final int PUBLIC = 1;
    public static final int PROTECTED = 2;
    public static final int PACKAGE = 3;
    public static final int PRIVATE = 4;
}
