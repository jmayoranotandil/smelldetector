package lrg.memoria.hismo.core;

public interface AbstractVersion extends Comparable {

    public String versionName();

    public String getFullName();

    public String getName();    
}
