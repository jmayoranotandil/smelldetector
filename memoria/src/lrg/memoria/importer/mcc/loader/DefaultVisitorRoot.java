package lrg.memoria.importer.mcc.loader;

public class DefaultVisitorRoot {
    final String ERROR = "<ERROR>";
    final String NO_ONE = "<NO_ONE>";
    final String NULL = "NULL";
    final String INIT_NULL_BODY = "<INIT_NULL_BODY>";
    final String ONLY_DECLARED = "<ONLY_DECLARED>";
    final String UNKNOWN = "<UNKNOWN>";
    final String NOT_INIT = "<NOT_INIT>";
    final String NO_NAME = "NO_NAME";

}
