package lrg.memoria.importer.mcc.loader;

public interface Tp2tVisitor {
    public void setId(String id);
    public void setTemplateParamID(String tpi);
    public void setInstantiationTypeID(String iti);
    public void addInstantiation();
}
