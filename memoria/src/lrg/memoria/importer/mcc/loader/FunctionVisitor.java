package lrg.memoria.importer.mcc.loader;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: May 6, 2004
 * Time: 4:36:24 PM
 * To change this template use Options | File Templates.
 */
public interface FunctionVisitor {
    void setId(Integer id);
    void setName(String name);
    void setKind(String kind);
    void setSignature(String signature);
    void setReturnType(String returnType);
    void setScopeId(String scopeId);
    void setNamespaceId(String namespaceId);
    void setAccess(String access);
    void setIsStatic(String isStatic);
    void setIsVirtual(String isVirtual);
    void setBodyId(String bodyId);
    void addFunction();

}
