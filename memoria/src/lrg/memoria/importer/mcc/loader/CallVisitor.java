package lrg.memoria.importer.mcc.loader;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: May 11, 2004
 * Time: 10:04:37 AM
 * To change this template use Options | File Templates.
 */
public interface CallVisitor {
    void setId(Integer id);
    void setBodyId(String bodyId);
    void setFuncId(String functId);
    void setCounter(Integer counter);
    void addCall();
}
