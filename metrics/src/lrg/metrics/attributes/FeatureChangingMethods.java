package lrg.metrics.attributes;

import lrg.metrics.Result;
import lrg.metrics.NumericalResult;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * <b>Name:</b> Feature Changing Methods.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> FCM
 * <br>
 * <b>Description:</b> Counts the number of methods from other classes that depend
 * on a given feature.
 * <br>
 * A feature might be an attribute or a method.
 * A method depends on a feature if:
 * - it invokes that feature, or
 * - it redefines that feature (a method) from a superclass
 * Constructors and destructors are not considered as methods.
 * <br>
 * <b>Source:</b>
 */

public class FeatureChangingMethods extends AttributeMeasure {

    public FeatureChangingMethods() {
        m_name = "FeatureChangingMethods";
        m_fullName = "Feature Changing Methods";
    }

    /**
     * Counts the number of methods from other classes that depend
     * on a given feature.
     * <br>
     * A feature might be an attribute or a method.
     * A method depends on a feature if:
     * - it invokes that feature, or
     * - it redefines that feature (a method) from a superclass
     * Constructors and destructors are not considered as methods.
     */

    public Result measure(lrg.memoria.core.Attribute att) {
        int count = 0, i, j, k, l;
        ArrayList accesses, ml = new ArrayList();
        lrg.memoria.core.Class crtcls;
        lrg.memoria.core.Method crtmeth, rewmeth;
        HashSet cl = new HashSet();
        lrg.memoria.core.Class cls = (lrg.memoria.core.Class) att.getScope();

        accesses = att.getAccessList();
        for (j = 0; j < accesses.size(); j++) {
            try {
                crtmeth = (lrg.memoria.core.Method)
                          ((lrg.memoria.core.Access) accesses.get(j)).getScope().getScope();
            } catch(ClassCastException e) {
                continue;
            }
            ml.add(crtmeth);
        }

        for (i = 0; i < ml.size(); i++) {
            if (!((lrg.memoria.core.Method) ml.get(i)).isConstructor()) {
                crtmeth = (lrg.memoria.core.Method) ml.get(i);
                try {
                    crtcls = (lrg.memoria.core.Class) crtmeth.getScope();
                } catch(ClassCastException e) {
                    continue;
                }
                if (!cl.contains(crtcls) && (!crtcls.isAbstract()) && (crtcls != cls)) {
                    cl.add(crtmeth);
                    count++;
                }
            }
        }

        return new NumericalResult(cls, count);
    }

}
