package lrg.metrics.classes;


/**
 * <b>Name:</b> Number Of Protected Data Members.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> cl_data_protected
 * <br>
 * <b>Description:</b> Number of protected attributes for a class.
 * <br>
 * <b>Source:</b> t?
 */
public class ProtectedAttributes extends AttributeIterator {

    public ProtectedAttributes() {
        m_name = "ProtectedAttributes";
        m_fullName = "Number of Protected Data Members";
    }

    /**
     * Check if the current attribute is protected.
     */
    protected boolean check(lrg.memoria.core.Attribute act_attr) {
        return act_attr.isProtected();
    }
}
