package lrg.metrics.classes;

import lrg.metrics.NumericalResult;
import lrg.metrics.Result;

import java.util.ArrayList;

/**
 * <b>Name:</b> Number Of Children.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> NOC
 * <br>
 * <b>Description:</b> The Number of Children (NOC) represents the
 * number of  immediate sub-classes subordinated to a class in the
 * class hierarchy. The interfaces are also considered here.
 * <br>
 * <b>Source:</b> S.R. Chidamber, C.F. Kemerer. A metrics Suite for
 * Object Oriented Design. IEEE Transactions on Softw. Engineering, Vol.20,
 * No.6, June 1994.
 */

public class NumberOfChildren extends ClassMeasure {

    public NumberOfChildren() {
        m_name = "NumberOfChildren";
        m_fullName = "Number Of Children";
    }

    /**
     * Returns the number of  immediate sub-classes subordinated to a
     * class in the class hierarchy. The interfaces are also considered.
     */
    public Result measure(lrg.memoria.core.Class cls) {
        int i, size, count = 0;
        ArrayList descendants_list = cls.getDescendants();
        lrg.memoria.core.Class act_desc;
        size = descendants_list.size();
        for (i = 0; i < size; i++) {
            act_desc = (lrg.memoria.core.Class) descendants_list.get(i);
            if (!act_desc.isInterface())
                count++;
        }
        return new NumericalResult(cls, count);
    }

}
