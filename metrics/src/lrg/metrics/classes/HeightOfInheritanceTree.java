package lrg.metrics.classes;

import lrg.metrics.Result;
import lrg.metrics.NumericalResult;
import lrg.memoria.core.DataAbstraction;

import java.util.ArrayList;

/**
 * <b>Name:</b> Height Of Inheritance Tree.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> HIT
 * <br>
 * <b>Description:</b> Is the maximum number of levels of derivation for a given class.
 * <br>
 * <b>Source:</b>
 */

public class HeightOfInheritanceTree extends ClassMeasure {

    public HeightOfInheritanceTree() {
        m_name = "HeightOfInheritanceTree";
        m_fullName = "Height Of Inheritance Tree";
    }

    /**
     * Is the maximum number of levels of derivation for a given class.
     */
    private int level(lrg.memoria.core.Class actcls) {
        int i, max = 0, aux;
        ArrayList descs = actcls.getDescendants();
        lrg.memoria.core.Class crt;

        //we consider that the height of the current class is 0
        if (descs.size() == 0) return 0;

        for (i = 0; i < descs.size(); i++) {
            try {
                crt = (lrg.memoria.core.Class) descs.get(i);
            } catch (ClassCastException e) {
                continue;
            }
            aux = level(crt);
            if (aux > max) max = aux;
        }

        return max + 1;
    }

    public Result measure(lrg.memoria.core.Class act_class) {
        int count = level(act_class);        

        return new NumericalResult(act_class, count);
    }
}
