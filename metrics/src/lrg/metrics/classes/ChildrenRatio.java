package lrg.metrics.classes;

import lrg.metrics.Result;
import lrg.metrics.NumericalResult;

/**
 * <b>Name:</b> Children Ratio.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> CR
 * <br>
 * <b>Description:</b> Is the ration between the number of children (NOC) of the measured class
 * and the number of descendants (of the same class).
 * <br>
 * <b>Source:</b>
 */

public class ChildrenRatio extends ClassMeasure {

    public ChildrenRatio() {
        m_name = "ChildrenRatio";
        m_fullName = "Children Ratio";
    }

    /**
     * Is the ration between the number of children (NOC) of the measured class
     * and the number of descendants (of the same class).
     */

    public Result measure(lrg.memoria.core.Class c) {
        NumberOfDescendants nodMetric = new NumberOfDescendants();
        NumberOfChildren nocMetric = new NumberOfChildren();
        double nod, noc, temp;

        nod = ((NumericalResult) nodMetric.measure(c)).getValue();
        noc = ((NumericalResult) nocMetric.measure(c)).getValue();

        if (nod != 0)
            temp = noc / nod;
        else if (noc == 0)
            temp = 0;
        else
            temp = Integer.MAX_VALUE;   //if we have no methods, we return an Integer.MAX_VALUE

        return new NumericalResult(c, temp);
    }
}
