package lrg.metrics.classes;

import lrg.metrics.Result;
import lrg.metrics.NumericalResult;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * <b>Name:</b> Class Locality.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> CL
 * <br>
 * <b>Description:</b> The number of dependencies that a class has on its own package
 * divided by the total number of dependencies.
 * <br>
 * <b>Source:</b>
 */

public class ClassLocality extends ClassMeasure {

    public ClassLocality() {
        m_name = "ClassLocality";
        m_fullName = "Class Locality";
    }

    /**
     * The number of dependencies that a class has on its own package
     * divided by the total number of dependencies.
     */
    public Result measure(lrg.memoria.core.Class cls) {
        double den = 0, nom = 0, temp;
        int i, j;
        ArrayList calls, accesses, cl = new ArrayList();
        ArrayList methods = cls.getMethodList();
        lrg.memoria.core.Class crtcls;
        lrg.memoria.core.Package crtpck, pck = cls.getPackage();
        lrg.memoria.core.Method crtmeth, cm;
        HashSet pl = new HashSet();

        for (i = 0; i < methods.size(); i++) {
            cm = (lrg.memoria.core.Method) methods.get(i);

            accesses = cm.getBody().getAccessList();
            for (j = 0; j < accesses.size(); j++) {
                try {
                    crtcls = (lrg.memoria.core.Class)
                             ((lrg.memoria.core.Access) accesses.get(j)).getVariable().getScope();
                } catch (ClassCastException e) {
                    continue;
                }
                cl.add(crtcls);
            }

            calls = cm.getBody().getCallList();
            for (j = 0; j < calls.size(); j++) {
                try {
                    crtmeth = (lrg.memoria.core.Method) ((lrg.memoria.core.Call) calls.get(j)).getFunction();
                } catch (ClassCastException e) {
                    continue;
                }

                if (crtmeth.isConstructor()) continue;

                try {
                    crtcls = (lrg.memoria.core.Class) crtmeth.getScope();
                } catch (ClassCastException e) {
                    continue;
                }
                cl.add(crtcls);
            }
        }

        for (i = 0; i < cl.size(); i++) {
            crtcls = (lrg.memoria.core.Class) cl.get(i);
            crtpck = crtcls.getPackage();
            if (!pl.contains(crtpck) && (!crtcls.isAbstract()) && (crtcls != cls)) {
                if (crtpck == pck)
                    den++;
                pl.add(crtpck);
                nom++;
            }
        }

        if (den == 0)
            temp = 0;
        else if (nom == 0)
                temp = Integer.MAX_VALUE;
             else
                temp = den / nom;

        return new NumericalResult(cls, temp);
    }
}
