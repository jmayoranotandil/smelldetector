package lrg.metrics.classes;


/**
 * <b>Name:</b> Number Of Final Data Members.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> cl_data_final
 * <br>
 * <b>Description:</b> Number of final attributes for a class. (i.e. data members
 * declared after the "final" keyword)
 * <br>
 * <b>Source:</b> t?
 */
public class FinalAttributes extends AttributeIterator {

    public FinalAttributes() {
        m_name = "FinalAttributes";
        m_fullName = "Number of Final Data Members";
    }

    /**
     * Check if the current attribute is final.
     */
    protected boolean check(lrg.memoria.core.Attribute act_attr) {
        return act_attr.isFinal();
    }
}
