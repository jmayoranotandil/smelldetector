package lrg.metrics.classes;

import lrg.metrics.NumericalResult;
import lrg.metrics.Result;
import lrg.metrics.methods.ClassTypeLocalVars;
import lrg.metrics.methods.ClassTypeParameters;
import lrg.metrics.methods.NumberOfStaticCalls;

import java.util.ArrayList;

/**
 * <b>Name:</b> Class Coupling.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> cl_cobc
 * <br>
 * <b>Description:</b> Measures the "class coupling metric" for a class.
 * <br>
 * Class coupling is the sum of:
 * <br>  -- the number of inherited classes
 * <br>  -- the number of class type attributes for the class
 * <br>  -- two times the number of calls to static member functions
 * from the methods of this class
 * <br>  -- two times the number of class-type parameters of the methods
 * of this class
 * <br>  -- three times the number of class-type local variables from the
 * methods of this class
 * <br>
 * <b>Source:</b> Telelogic
 */

public class ClassCoupling extends ClassMeasure {

    public ClassCoupling() {
        m_name = "ClassCoupling";
        m_fullName = "Class Coupling";
    }

    /**
     * Measures the "class coupling metric" for a class.
     * <br>
     * Class coupling is the sum of:
     * <br>  -- the number of inherited classes
     * <br>  -- the number of class type attributes for the class
     * <br>  -- two times the number of calls to static member functions
     * from the methods of this class
     * <br>  -- two times the number of class-type parameters of the methods
     * of this class
     * <br>  -- three times the number of class-type local variables from the
     * methods of this class
     */
    public Result measure(lrg.memoria.core.Class c) {
        int size, i;
        double count = 0;
        ExtendedClasses cm_ex = new ExtendedClasses();
        ClassTypeAttributes cm_cta = new ClassTypeAttributes();
        ClassTypeParameters mm_ctp = new ClassTypeParameters();
        NumberOfStaticCalls mm_nsc = new NumberOfStaticCalls();
        ClassTypeLocalVars mm_ctlv = new ClassTypeLocalVars();
        //add the number of inherited classes
        count += ((NumericalResult) cm_ex.measure(c)).getValue();
        //add the number of class type attributes
        count += ((NumericalResult) cm_cta.measure(c)).getValue();
        //walk through methods
        lrg.memoria.core.Method actMethod;
        ArrayList method_list = c.getMethodList();
        size = method_list.size();
        for (i = 0; i < size; i++) {
            actMethod = (lrg.memoria.core.Method) method_list.get(i);
            if (actMethod.getScope() == c) {
                //two times the number of class-type parameters
                count += 2 * ((NumericalResult) mm_ctp.measure(actMethod)).getValue();
                //two times the number of static calls
                count += 2 * ((NumericalResult) mm_nsc.measure(actMethod)).getValue();
                //three times number of local-variables
                count += 3 * ((NumericalResult) mm_ctlv.measure(actMethod)).getValue();
            }
        }
        return new NumericalResult(c, count);
    }
}


