package lrg.metrics.classes;

import lrg.metrics.NumericalResult;
import lrg.metrics.Result;
import lrg.metrics.methods.ExteriorCalls;

import java.util.ArrayList;

/**
 * <b>Name:</b> Number Of Message Sends.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> NOM
 * <br>
 * <b>Description:</b> The number of external methods invocation from the a class's
 * methods.(i.e. the number of calls to methods defined outside this class from the
 * methods of this class).
 * <br>
 * <b>Source:</b> M. Lorenz, J. Kidd -- "Object-Oriented Software Metrics",
 * Prentice Hall, 1994.
 */
public class MessageSends extends ClassMeasure {

    public MessageSends() {
        m_name = "MessageSends";
        m_fullName = "Number Of Message Sends";
    }

    /**
     * The number of external methods invocation from the a class's
     * methods.(i.e. the number of calls to methods defined outside this
     * class from the methods of this class).
     */
    public Result measure(lrg.memoria.core.Class cls) {
        int i, size;
        double count = 0;
        ArrayList ml = cls.getMethodList();
        lrg.memoria.core.Method act_method;
        ExteriorCalls ec = new ExteriorCalls();
        size = ml.size();
        for (i = 0; i < size; i++) {
            act_method = (lrg.memoria.core.Method) ml.get(i);
            if (act_method.getScope() == cls)
                count += ((NumericalResult) ec.measure(act_method)).getValue();
        }
        return new NumericalResult(cls, count);
    }
}

