package lrg.metrics.classes;

import lrg.metrics.NumericalResult;
import lrg.metrics.Result;

import java.util.ArrayList;

/**
 * <b>Name:</b> Number Of Included Classes.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> cl_subclass
 * <br>
 * <b>Description:</b> Number of classes of interfaces included by a class.
 * <br>
 * <b>Source:</b> t?
 */
public class IncludedClasses extends ClassMeasure {

    public IncludedClasses() {
        m_name = "IncludedClasses";
        m_fullName = "Number Of Included Classes";
    }

    /**
     * Measures the number of classes or interfaces which are included
     * by a class.
     */
    public Result measure(lrg.memoria.core.Class c) {
        int count = 0, i, size;
        lrg.memoria.core.Package actPackage = c.getPackage();
        ArrayList cl = actPackage.getAllTypesList();
        lrg.memoria.core.Class actClass;
        size = cl.size();
        for (i = 0; i < size; i++) {
            actClass = (lrg.memoria.core.Class) cl.get(i);
            if (actClass.getScope() == c)
                count++;
        }
        return new NumericalResult(c, count);
    }
}


