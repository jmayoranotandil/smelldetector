package lrg.metrics.classes;

import lrg.metrics.Result;
import lrg.metrics.NumericalResult;

import java.util.ArrayList;

/**
 * <b>Name:</b> Average Usage Ratio Implementation.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> AURI
 * <br>
 * <b>Description:</b> This metric computes the average reuse ratio from all the super-classes
 * of a given class. We consider that a derived class reuses something
 * from its ancestor if:
 * - it accesses a protected attribute defined in the superclass
 * - it calls a protected method defined in the superclass
 * <br>
 * Accesses of constructors and destructors are not counted.
 * <br>
 * <b>Source:</b>
 */

public class AverageUsageRatioImplementation extends ClassMeasure {

    public AverageUsageRatioImplementation() {
        m_name = "AverageUsageRatioImplementation";
        m_fullName = "Average Usage Ratio Implementation";
    }

    /**
     * This metric computes the average reuse ratio from all the super-classes
     * of a given class. We consider that a derived class reuses something
     * from its ancestor if:
     * - it accesses a protected attribute defined in the superclass
     * - it calls a protected method defined in the superclass
     * - it redefines a (virtual) method of the superclass
     * <br>
     * Accesses of constructors and destructors are not counted.
     */

    public Result measure(lrg.memoria.core.Class c) {
        double temp, turi = 0;
        UsageRatioImplementation uriMetric = new UsageRatioImplementation();
        int i;
        ArrayList cl = c.getAncestorsList();
        double noa = cl.size();
        lrg.memoria.core.Class crtClass;

        for (i = 0; i < cl.size(); i++) {
            try {
                crtClass = (lrg.memoria.core.Class) cl.get(i);
            } catch (ClassCastException e) {
                continue;
            }
            if (!crtClass.isPrivate())
                turi += ((NumericalResult) uriMetric.measure(c, crtClass)).getValue();
        }

        if (noa != 0)
            temp = turi / noa;
        else if (turi == 0)
            temp = 0;
        else
            temp = Integer.MAX_VALUE;   //if we have no ancestors

        return new NumericalResult(c, temp);
    }
}
