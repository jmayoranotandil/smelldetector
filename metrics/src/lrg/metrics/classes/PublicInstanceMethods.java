package lrg.metrics.classes;


/**
 * <b>Name:</b> Number Of Public Instance Methods In A Class.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> PIM
 * <br>
 * <b>Description:</b> Measures the number of methods in a class defined using
 * the "public" keyword and without using the "static" keyword.
 * <br>
 * <b>Source:</b> M. Lorentz & J. Kidd -- "Object Oriented Software Metrics",
 * Prentica-Hall, 1994
 */
public class PublicInstanceMethods extends MethodIterator {

    public PublicInstanceMethods() {
        m_name = "PublicInstanceMethods";
        m_fullName = "Number of Public Instance Methods in a Class";
    }

    //returns "true" if the actual method is public and isn't static
    protected boolean check(lrg.memoria.core.Method m) {
        return (m.isPublic() && !m.isStatic());
    }

}
