package lrg.metrics.classes;

import lrg.metrics.Result;
import lrg.metrics.NumericalResult;

import java.util.ArrayList;

/**
 * <b>Name:</b> Average Use of Interface.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> AUF
 * <br>
 * <b>Description:</b> The AUF metric is defined as the average number of interface members
 * of the measured class that are used by other class.
 * <br>
 * A method uses a class if it calls a public method of that class or
 * if it accesses a public attribute of that class.
 * Accesses of constructors and destructors are not considered.
 * <br>
 * <b>Source:</b>
 */

public class AverageUseOfInterface extends ClassMeasure {

    public AverageUseOfInterface() {
        m_name = "AverageUseOfInterface";
        m_fullName = "Average Use of Interface";
    }

    /**
     * The AUF metric is defined as the average number of interface members
     * of the measured class that are used by other class.
     * <br>
     * A method uses a class if it calls a public method of that class or
     * if it accesses a public attribute of that class.
     * Accesses of constructors and destructors are not considered.
     */

    public Result measure(lrg.memoria.core.Class c) {
        ArrayList ml = c.getMethodList(), al = c.getAttributeList(), ul = new ArrayList();
        int i, j;
        lrg.memoria.core.Method crtm;
        lrg.memoria.core.Attribute crta;
        double den = 0, nom = 0, temp;

        for (i = 0; i < ml.size(); i++) {
            crtm = (lrg.memoria.core.Method) ml.get(i);
            if (!crtm.isPublic()) continue;
            ul = crtm.getCallList();
            for (j = 0; j < ul.size(); j++)
                if (((lrg.memoria.core.Call) ul.get(j)).getScope().getScope() != c) {
                    den++;
                    break;
                }
            nom++;
        }

        for (i = 0; i < al.size(); i++) {
            crta = (lrg.memoria.core.Attribute) al.get(i);
            if (!crta.isPublic()) continue;
            ul = crta.getAccessList();
            for (j = 0; j < ul.size(); j++)
                if (((lrg.memoria.core.Access) ul.get(j)).getScope().getScope() != c) {
                    den++;
                    break;
                }
            nom++;
        }

        if (den == 0)
            temp = 0;
        else if (nom == 0)
                temp = Integer.MAX_VALUE;
             else
                temp = den / nom;

        return new NumericalResult(c, temp);
    }
}
