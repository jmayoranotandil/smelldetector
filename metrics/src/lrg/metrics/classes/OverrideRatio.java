package lrg.metrics.classes;

import lrg.metrics.Result;
import lrg.metrics.NumericalResult;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * <b>Name:</b> Override Ratio.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> OVR
 * <br>
 * <b>Description:</b> It is computed between a base class and a child class, as the relative number
 * of methods from the interface of the base class that are overriden in the derived class.
 * <br>
 * OVR is the number of methods within a superclass that are overrided in its children /
 * number of methods that can be overrided.
 * It is implemented only for directly extension relation.
 * <br>
 * This implementation detects the relation between a base class and a
 * subclass, where the subclass doesn't overrides anything.
 * <br>
 * <b>Source:</b>
 */

public class OverrideRatio extends TwoClassesMeasure {

    public OverrideRatio() {
        m_name = "OverrideRatio";
        m_fullName = "Override Ratio";
    }

    /**
     * It is computed between a base class and a child class, as the relative number
     * of methods from the interface of the base class that are overriden in the derived class.
     * <br>
     * OVR is the number of methods within a superclass that are overrided in its children /
     * number of methods that can be overrided.
     * It is implemented only for directly extension relation.
     * <br>
     * This implementation detects the relation between a base
     */

    protected boolean filter(lrg.memoria.core.Class cls1, lrg.memoria.core.Class cls2) {
        //by default we count only "normal" classes
        //but for this metric we need to check if they are in an inheritance relation
        return ((cls1.getStatute() == lrg.memoria.core.Statute.NORMAL) &&
                (cls2.getStatute() == lrg.memoria.core.Statute.NORMAL) &&
                (cls1.getAncestorsList().contains(cls2)));
    }

    public Result measure(lrg.memoria.core.Class child, lrg.memoria.core.Class ancestor) {
        double temp, na, nd = 0;
        int i, j, l;
        boolean ok;
        ArrayList ml = child.getMethodList(), crtpars, aml = ancestor.getMethodList(), rewpars;
        HashSet um = new HashSet();
        lrg.memoria.core.Method crtm, crtcm, rewmeth;

        for (i = 0; i < ml.size(); i++) {
            crtm = (lrg.memoria.core.Method) ml.get(i);
            if (crtm.isConstructor()) continue;

            for (j = 0; j < aml.size(); j++) {
                rewmeth = (lrg.memoria.core.Method) aml.get(j);
                if ((rewmeth.getName() == crtm.getName()) && (!rewmeth.isPrivate())){
                    crtpars = crtm.getParameterList();
                    rewpars = rewmeth.getParameterList();
                    if (crtpars.size() == rewpars.size()) ok = true;
                    else ok = false;
                    for (l = 0; l < crtpars.size(); l++)
                        if (((lrg.memoria.core.Parameter) crtpars.get(l)).getType() !=
                            ((lrg.memoria.core.Parameter) rewpars.get(l)).getType())
                            ok = false;
                    if (ok) um.add(crtm);
                }
            }
        }

        na = um.size();
        for (i = 0; i < ancestor.getMethodList().size(); i++) {
            crtcm = (lrg.memoria.core.Method) ancestor.getMethodList().get(i);
            if (!crtcm.isPrivate()) nd++;
        }

        if (na == 0) temp = 0;
        else if (nd == 0) temp = Integer.MAX_VALUE;
             else temp = na / nd;

        return new NumericalResult(child, temp);
    }

}
