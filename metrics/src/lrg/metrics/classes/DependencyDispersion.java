package lrg.metrics.classes;

import lrg.metrics.Result;
import lrg.metrics.NumericalResult;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * <b>Name:</b> Dependency Dispersion.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> DD
 * <br>
 * <b>Description:</b> The number of other packages on which a class from a given package
 * depends on.
 * <br>
 * <b>Source:</b>
 */

public class DependencyDispersion extends ClassMeasure {

    public DependencyDispersion() {
        m_name = "DependencyDispersion";
        m_fullName = "Dependency Dispersion";
    }

    /**
     * The number of other packages on which a class from a given package
     * depends on.
     */

    public Result measure(lrg.memoria.core.Class cls) {
        int count = 0, i, j;
        ArrayList calls, accesses, cl = new ArrayList();
        ArrayList methods = cls.getMethodList();
        lrg.memoria.core.Class crtcls;
        lrg.memoria.core.Package crtpck, pck = cls.getPackage();
        lrg.memoria.core.Method crtmeth, cm;
        HashSet pl = new HashSet();

        for (i = 0; i < methods.size(); i++) {
            cm = (lrg.memoria.core.Method) methods.get(i);

            accesses = cm.getBody().getAccessList();
            for (j = 0; j < accesses.size(); j++) {
                try {
                    crtcls = (lrg.memoria.core.Class)
                             ((lrg.memoria.core.Access) accesses.get(j)).getVariable().getScope();
                } catch (ClassCastException e) {
                    continue;
                }
                cl.add(crtcls);
            }

            calls = cm.getBody().getCallList();
            for (j = 0; j < calls.size(); j++) {
                try {
                    crtmeth = (lrg.memoria.core.Method) ((lrg.memoria.core.Call) calls.get(j)).getFunction();
                } catch (ClassCastException e) {
                    continue;
                }
                if (crtmeth.isConstructor()) continue;
                try {
                    crtcls = (lrg.memoria.core.Class) crtmeth.getScope();
                } catch (ClassCastException e) {
                    continue;
                }
                cl.add(crtcls);
            }
        }

        for (i = 0; i < cl.size(); i++) {
            crtcls = (lrg.memoria.core.Class) cl.get(i);
            crtpck = crtcls.getPackage();
            if (!pl.contains(crtpck) && (!crtcls.isAbstract()) && (crtpck != pck)) {
                pl.add(crtpck);
                count++;
            }
        }

        return new NumericalResult(cls, count);
    }
}
