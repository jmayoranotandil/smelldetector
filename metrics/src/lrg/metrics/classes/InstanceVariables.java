package lrg.metrics.classes;


/**
 * <b>Name:</b> Number Of Instance Variables.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> cl_data_nostat, NIV
 * <br>
 * <b>Description:</b> Number attributes which are instance variable. (i.e. data members
 * declared without using "static" keyword)
 * <br>
 * <b>Source:</b> M. Lorentz, J. Kidd -- "Object Oriented Software Metrics",
 * Prentice Hall, 1994
 */
public class InstanceVariables extends AttributeIterator {

    public InstanceVariables() {
        m_name = "InstanceVariables";
        m_fullName = "Number Of Instance Variables";
    }

    /**
     * Check if the current attribute is an instance variable.
     */
    protected boolean check(lrg.memoria.core.Attribute act_attr) {
        return !act_attr.isStatic();
    }
}
