package lrg.metrics.methods;

import lrg.metrics.NumericalResult;
import lrg.metrics.Result;

import java.util.ArrayList;

/**
 * <b>Name:</b> Number of Local Variables.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> dc_lvars
 * <br>
 * <b>Description:</b> Measure the number of local variables declared in a method.
 * (i.e. variables declared in its body)
 * <br>
 * <b>Source:</b> t?
 */
public class LocalVariables extends MethodMeasure {

    public LocalVariables() {
        m_name = "LocalVariables";
        m_fullName = "Number Of Local Variables";
    }

    /**
     * Measure the number of local variables declared in a method.
     * (i.e. variables declared in its body)
     */
    public Result measure(lrg.memoria.core.Method m) {
        int count, size, i;
        ArrayList local_vars;
        lrg.memoria.core.LocalVariable act_var;
        count = 0;
        //add local variables
        lrg.memoria.core.FunctionBody mb = m.getBody();
        if (mb != null) {
            local_vars = mb.getLocalVarList();
            size = local_vars.size();
            for (i = 0; i < size; i++) {
                act_var = (lrg.memoria.core.LocalVariable) local_vars.get(i);
                if (!act_var.isFinal())
                    count++;
            }
        }
        return new NumericalResult(m, count);
    }
}


