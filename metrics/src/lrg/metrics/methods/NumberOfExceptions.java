package lrg.metrics.methods;

import lrg.metrics.NumericalResult;
import lrg.metrics.Result;

/**
 * <b>Name:</b> Number Of Exceptions.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b>
 * <br>
 * <b>Description:</b> Returns the number of catch blocks from the method's body.
 * <br>
 * <b>Source:</b>
 */
public class NumberOfExceptions extends MethodMeasure {

    public NumberOfExceptions() {
        m_name = "NumberOfExceptions";
        m_fullName = "Number Of Exceptions";
    }

    /**
     * Returns the number of "catch" blocks in a body.
     */
    public Result measure(lrg.memoria.core.Method m) {
        int count = 0;
        lrg.memoria.core.FunctionBody mb = m.getBody();
        if (mb != null)
            count = mb.getNumberOfExceptions();
        return new NumericalResult(m, count);
    }
}
