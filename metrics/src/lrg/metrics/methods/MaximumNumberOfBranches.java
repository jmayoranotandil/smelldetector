package lrg.metrics.methods;


import lrg.metrics.NumericalResult;
import lrg.metrics.Result;
import lrg.memoria.core.Statute;

public class MaximumNumberOfBranches extends MethodMeasure {

    public MaximumNumberOfBranches() {
        m_name = "MaximumNumberOfBranches";
        m_fullName = "Maximum Number of Branches";
    }

    /**
     * <b>Name:</b> Maximum Number of Branches.
     * <br>
     * <b>Alternative:</b> Cyclomatic Number 
     * <br>
     * <b>Acronym:</b> MNOB
     * <br>
     * <b>Description:</b> Returns the McCabe cyclomatic complexity
     * number of the function.
     * <br>
     * <b>Source:</b> McCabe
     */
    public Result measure(lrg.memoria.core.Method m) {
        int count = 0;
        lrg.memoria.core.FunctionBody mb = m.getBody();
        if ((mb != null) && (mb.getNumberOfStatements() > 0))
        	count = mb.getCyclomaticNumber();
                     
        return new NumericalResult(m, count);
    }
}




