package lrg.metrics.methods;

import lrg.metrics.Result;
import lrg.metrics.NumericalResult;

import java.util.ArrayList;

/**
 * <b>Name:</b> Lines of code.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> LOC
 * <br>
 * <b>Description:</b> It is the number of lines of code in the measured method,
 * does not include white-lines and comments.
 * <br>
 * Constructors and destructors have 0 lines of code.
 * <br>
 * <b>Source:</b>
 */

public class LinesOfCode extends MethodMeasure {

    public LinesOfCode() {
        m_name = "LinesOfCode";
        m_fullName = "Number of Lines of Code";
    }

    /**
     * It is the number of lines of code in the measured method,
     * does not include white-lines and comments.
     */
    public Result measure(lrg.memoria.core.Method m) {
        int count = 0;

        if (!m.isConstructor())
            count = m.getBody().getNumberOfLines();
        return new NumericalResult(m, count);
    }
}
