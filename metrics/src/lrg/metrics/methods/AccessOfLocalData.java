package lrg.metrics.methods;

import lrg.metrics.Result;
import lrg.metrics.NumericalResult;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * <b>Name:</b> Access Of Local Data.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> ALD
 * <br>
 * <b>Description:</b> Counts the number of local-attributes (defined in that class),
 * that are accessed directly or via accessor methods, within a method.
 * <br>
 * <b>Source:</b> t?
 */

public class AccessOfLocalData extends MethodMeasure {

    public AccessOfLocalData() {
        m_name = "AccessOfLocalData";
        m_fullName = "Access Of Local Data";
    }

    /**
     * Counts the number of local-attributes (defined in that class),
     * that are accessed directly or via accessor methods, within a method.
     */

    public Result measure(lrg.memoria.core.Method m) {
        int count = 0, i, j;
        lrg.memoria.core.Class crt_class = (lrg.memoria.core.Class) m.getScope();
        ArrayList dirl = m.getBody().getAccessList();
        lrg.memoria.core.Attribute crt_attribute;
        HashSet acc = new HashSet();
        lrg.memoria.core.Access crt_access;
        ArrayList calll = m.getBody().getCallList();
        ArrayList cdirl;
        lrg.memoria.core.Method crt_method;

        for (i = 0; i < calll.size(); i++) {
            try {
                crt_method = (lrg.memoria.core.Method) ((lrg.memoria.core.Call) calll.get(i)).getFunction();
            } catch (ClassCastException e) {
                continue;
            }
            if (crt_method.isAccessor() && (crt_method.getScope() == crt_class)) {
                cdirl = crt_method.getBody().getAccessList();
                for (j = 0; j < cdirl.size(); j++)
                    dirl.add(cdirl.get(j));
            }
        }

        for (i = 0; i < dirl.size(); i++) {
            crt_access = (lrg.memoria.core.Access) dirl.get(i);
            try {
                crt_attribute = (lrg.memoria.core.Attribute) crt_access.getVariable();
            } catch (ClassCastException e) {
                continue;
            }
            if ((!acc.contains(crt_attribute)) && (crt_attribute.getScope() == crt_class)) {
                acc.add(crt_attribute);
                count++;
            }
        }
        return new NumericalResult(m, count);
    }
}
