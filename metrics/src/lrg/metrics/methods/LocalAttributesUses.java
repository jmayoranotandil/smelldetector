package lrg.metrics.methods;

import lrg.metrics.NumericalResult;
import lrg.metrics.Result;

import java.util.ArrayList;

/**
 * <b>Name:</b> Number Of Times Local Attributes Are Used.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> ic_vari
 * <br>
 * <b>Description:</b> Number of uses of attributes defined in the class.
 * <br>
 * Obs.: All attribute occurences are counted.
 * <br>
 * <b>Source:</b> t?
 */
public class LocalAttributesUses extends MethodMeasure {

    public LocalAttributesUses() {
        m_name = "LocalAttributesUses";
        m_fullName = "Number Of Times Local Attributes Are Used";
    }

    /**
     * Measures the total number of uses of attributes defined
     * inside the current class.
     * <br>
     * Obs.: All attribute occurences are counted.
     */
    public Result measure(lrg.memoria.core.Method m) {
        int count, i;
        lrg.memoria.core.FunctionBody mb = m.getBody();
        ArrayList accessList;
        lrg.memoria.core.Access access;
        lrg.memoria.core.Variable var;
        count = 0;
        if (mb != null) {
            accessList = mb.getAccessList();
            for (i = 0; i < accessList.size(); i++) {
                access = (lrg.memoria.core.Access) accessList.get(i);
                var = access.getVariable();
                if (var instanceof lrg.memoria.core.Attribute && ((lrg.memoria.core.Attribute) var).getScope() == m.getScope())
                    count += access.getCount();
            }
        }
        return new NumericalResult(m, count);
    }
}
