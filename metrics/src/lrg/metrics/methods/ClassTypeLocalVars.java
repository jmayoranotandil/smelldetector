package lrg.metrics.methods;

import lrg.metrics.NumericalResult;
import lrg.metrics.Result;

import java.util.ArrayList;

/**
 * <b>Name:</b> Number Of Class Type Local Variables.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> dc_clas_var
 * <br>
 * <b>Description:</b> The number of class type variables that are declared in a
 * function (Local Variables).
 * <br>
 * <b>Source:</b> t?
 */
public class ClassTypeLocalVars extends MethodMeasure {

    public ClassTypeLocalVars() {
        m_name = "ClassTypeLocalVars";
        m_fullName = "Number of Class Type Local Variables";
    }

    /**
     * Measure the number of class type variables that are declared in a
     * function (Local Variables).
     */
    public Result measure(lrg.memoria.core.Method m) {
        int count, i;
        ArrayList local_vars;
        lrg.memoria.core.FunctionBody mb = m.getBody();
        lrg.memoria.core.Variable v;
        count = 0;
        if (mb != null) {
            local_vars = mb.getLocalVarList();
            if (local_vars != null) {
                int size = local_vars.size();
                for (i = 0; i < size; i++) {
                    v = (lrg.memoria.core.Variable) local_vars.get(i);
                    if (v.getType() instanceof lrg.memoria.core.Class)
                        count++;
                }
            }
        }
        return new NumericalResult(m, count);
    }
}
