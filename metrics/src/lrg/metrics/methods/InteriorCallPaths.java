package lrg.metrics.methods;

import lrg.metrics.NumericalResult;
import lrg.metrics.Result;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * <b>Name:</b> Number Of Distinct Calls To Functions Defined In The Class.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> dc_callpi
 * <br>
 * <b>Description:</b> Number of calls to functions defined in the class of
 * the function being analyzed.
 * <br>
 * Obs.: Different calls to a method count for one call.
 * <br>
 * <b>Source:</b> t?
 */
public class InteriorCallPaths extends MethodMeasure {

    public InteriorCallPaths() {
        m_name = "InteriorCallPaths";
        m_fullName = "Number of Distinct Calls to Functions Defined in the Class";
    }

    /**
     * Number of calls to functions defined in the class of
     * the function being analyzed.
     * This metric returns the number of "inside calls".
     * <br>
     * Obs.: Different calls to a method count for one call.
     */

    public Result measure(lrg.memoria.core.Method actMethod) {
        int size, count;
        String calledClassName, actClassName;
        lrg.memoria.core.Method calledMethod;
        lrg.memoria.core.FunctionBody mb = actMethod.getBody();
        ArrayList cl;
        HashSet hs = new HashSet();
        count = 0;
        if (mb != null) {
            cl = mb.getCallList();
            size = cl.size();
            for (int i = 0; i < size; i++) {
                calledMethod = ((lrg.memoria.core.Call) cl.get(i)).getMethod();
                calledClassName = calledMethod.getScope().getFullName();
                actClassName = actMethod.getScope().getFullName();
                if (calledClassName.equals(actClassName)) {
                    if (!hs.contains(calledMethod)) {
                        hs.add(calledMethod);
                        count++;
                    }
                }
            }
        }
        return new NumericalResult(actMethod, count);
    }
}
