package lrg.metrics.methods;

import lrg.metrics.Result;
import lrg.metrics.NumericalResult;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * <b>Name:</b> Access Of Import Data.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> AID
 * <br>
 * <b>Description:</b> Counts the number of non-inherited attributes, from other classes
 * that are accessed directly or via accessor methods, within a method.
 * <br>
 * Only the user-defined classes are considered.
 * <br>
 * <b>Source:</b> t?
 */

public class AccessOfImportData  extends MethodMeasure {

    public AccessOfImportData() {
        m_name = "AccessOfImportData";
        m_fullName = "Access Of Import Data";
    }

    /**
     * Counts the number of non-inherited attributes, from other classes
     * that are accessed directly or via accessor methods, within a method.
     * <br>
     * Only the user-defined classes are considered.
     */

    public Result measure(lrg.memoria.core.Method m) {
        int count = 0, i, j;
        lrg.memoria.core.Class crt_class = (lrg.memoria.core.Class) m.getScope();
        ArrayList dirl = m.getBody().getAccessList();
        lrg.memoria.core.Attribute crt_attribute;
        HashSet acc = new HashSet();
        lrg.memoria.core.Access crt_access;
        ArrayList calll = m.getBody().getCallList();
        ArrayList cdirl;
        lrg.memoria.core.Method crt_method;

        for (i = 0; i < calll.size(); i++) {
            try {
                crt_method = (lrg.memoria.core.Method) ((lrg.memoria.core.Call) calll.get(i)).getFunction();
            } catch (ClassCastException e) {
                continue;
            }

            if (crt_method.isAccessor()) {
                cdirl = crt_method.getBody().getAccessList();
                for (j = 0; j < cdirl.size(); j++)
                    dirl.add(cdirl.get(j));
            }
        }

        for (i = 0; i < dirl.size(); i++) {
            crt_access = (lrg.memoria.core.Access) dirl.get(i);
            try {
                crt_attribute = (lrg.memoria.core.Attribute) crt_access.getVariable();
            } catch (ClassCastException e) {
                continue;
            }
            if (!acc.contains(crt_attribute) &&
               (!crt_class.getAncestorsList().contains((lrg.memoria.core.Class) crt_attribute.getScope())) &&
               (!(crt_attribute.getScope() == crt_class)))
                if (!(crt_attribute.getType() instanceof lrg.memoria.core.Class) ||
                   (!(((lrg.memoria.core.Class)crt_attribute.getType()).getStatute() == lrg.memoria.core.Statute.LIBRARY)))
                {
                    acc.add(crt_attribute);
                    count++;
                }
        }
        return new NumericalResult(m, count);
    }
}
