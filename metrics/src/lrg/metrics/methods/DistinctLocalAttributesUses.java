package lrg.metrics.methods;

import lrg.metrics.NumericalResult;
import lrg.metrics.Result;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * <b>Name:</b> Number Of Distinct Uses Of Local Attributes.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> ic_varpi
 * <br>
 * <b>Description:</b> The total number of distinct uses of attributes defined
 * inside the current class.
 * <br>
 * Obs.: Different uses of the same attribute count for one.
 * <br>
 * <b>Source:</b> t?
 */
public class DistinctLocalAttributesUses extends MethodMeasure {

    public DistinctLocalAttributesUses() {
        m_name = "DistinctLocalAttributesUses";
        m_fullName = "Number Of Distinct Uses Of Local Attributes";
    }

    /**
     * Measures the total number of uses of attributes defined
     * inside the current class.
     * <br>
     * Obs.: Different uses of the same attribute count for one.
     */
    public Result measure(lrg.memoria.core.Method m) {
        int count, i, size;
        lrg.memoria.core.FunctionBody mb = m.getBody();
        ArrayList accessList;
        HashSet usedAttributes = new HashSet();
        ;
        lrg.memoria.core.Access access;
        lrg.memoria.core.Variable var;
        count = 0;
        if (mb != null) {
            accessList = mb.getAccessList();
            size = accessList.size();
            for (i = 0; i < size; i++) {
                access = (lrg.memoria.core.Access) accessList.get(i);
                var = access.getVariable();
                if (var instanceof lrg.memoria.core.Attribute && ((lrg.memoria.core.Attribute) var).getScope() == m.getScope())
                    if (!usedAttributes.contains(var)) {
                        usedAttributes.add(var);
                        count++;
                    }
            }
        }
        return new NumericalResult(m, count);
    }
}

