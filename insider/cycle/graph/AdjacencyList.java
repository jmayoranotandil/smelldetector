package graph;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.util.Vector;

public class AdjacencyList implements Copyable{

	private HashMap<AbstractNode, ArrayList<Edge>> adjacencies = new HashMap<AbstractNode, ArrayList<Edge>>();

	public AdjacencyList(HashMap<AbstractNode, ArrayList<Edge>> adjacencies){
		this.adjacencies = adjacencies;
	}

	public AdjacencyList(){
	}

	public void addEdge(AbstractNode source, AbstractNode target){
		ArrayList<Edge> list;
		if(!adjacencies.containsKey(source)){
			source.setId(adjacencies.size());
			list = new ArrayList<Edge>();
			adjacencies.put(source, list);
		}else{
			list = adjacencies.get(source);
		}
		list.add(new Edge(source, target));
	}

	public ArrayList<Edge> getAdjacent(AbstractNode source){
		return adjacencies.get(source);
	}

	public void reverseEdge(Edge e){
		adjacencies.get(e.getFrom()).remove(e);
		addEdge(e.getTo(), e.getFrom());
	}

	public void reverseGraph(){
		adjacencies = getReversedList().adjacencies;
	}

	public AdjacencyList getReversedList(){
		AdjacencyList newlist = new AdjacencyList();
		for(ArrayList<Edge> edges : adjacencies.values()){
			for(Edge e : edges){
				newlist.addEdge(e.getTo(), e.getFrom());
			}
		}
		return newlist;
	}

	public Set<AbstractNode> getSourceNodeSet(){
		return adjacencies.keySet();
	}

	public Collection<Edge> getAllEdges(){
		ArrayList<Edge> edges = new ArrayList<Edge>();
		for(ArrayList<Edge> e : adjacencies.values()){
			edges.addAll(e);
		}
		return edges;
	}

	public String toString(){
		String result = "";
		Iterator<AbstractNode> iterator = adjacencies.keySet().iterator();
		while (iterator.hasNext()){
			AbstractNode node = iterator.next();
			result += node.toString() + " : ";
			ArrayList<Edge> edges = adjacencies.get(node);
			Iterator<Edge> edgeIterator = edges.iterator();
			while (edgeIterator.hasNext()){
				Edge edge = edgeIterator.next();
				result += edge.getTo().toString()+ " ";
			}
			result += System.getProperty("line.separator");
		}
		return result;
	}

	public void addNode(AbstractNode source) {
		ArrayList<Edge> list;
		if(!adjacencies.containsKey(source)){
			list = new ArrayList<Edge>();
			adjacencies.put(source, list);
		}
	}

	public AdjacencyList copy(){
		return new AdjacencyList(adjacencies);
	}
	
	public int[][] getAdjencyList(){
		int i = 0;
		Collection<AbstractNode> allNodes = getSourceNodeSet();
		for (AbstractNode node : allNodes) {
			node.setId(i++);
		}
		
		for (Edge edge : getAllEdges()) {
			for (AbstractNode abstractNode : allNodes) {
				if (abstractNode.equals(edge.getFrom()))
					edge.getFrom().setId(abstractNode.getId());
				if (abstractNode.equals(edge.getTo()))
					edge.getTo().setId(abstractNode.getId());
			}
		}
		return getAdjacencyList(getAdjacencyMatrix());
	}
	
	private boolean[][] getAdjacencyMatrix(){
		boolean[][] adjacencyMatrix = new boolean[adjacencies.size()][adjacencies.size()];
		for (Edge edge : getAllEdges()) {
			adjacencyMatrix[edge.getFrom().getId()][edge.getTo().getId()] = true;
		}
		return adjacencyMatrix;
	}
	
	private int[][] getAdjacencyList(boolean[][] adjacencyMatrix) {
		int[][] list = new int[adjacencyMatrix.length][];

		for (int i = 0; i < adjacencyMatrix.length; i++) {
			Vector v = new Vector();
			for (int j = 0; j < adjacencyMatrix[i].length; j++) {
				if (adjacencyMatrix[i][j]) {
					v.add(new Integer(j));
				}
			}

			list[i] = new int[v.size()];
			for (int j = 0; j < v.size(); j++) {
				Integer in = (Integer) v.get(j);
				list[i][j] = in.intValue();
			}
		}
		
		return list;
	}

	public int[] getNodes() {
		int[] nodes = new int[adjacencies.size()];
		int i = 0;
		for (AbstractNode node : getSourceNodeSet()) {
			nodes[i++] = node.getId();
		}
		return nodes;
	}

	public int getId(AbstractNode node) {
		for (AbstractNode aNode : getSourceNodeSet()) {
			if (aNode.equals(node))
				return aNode.getId();
		}
		return -1;
	}

	public AbstractNode getNode(Integer id) {
		for (AbstractNode aNode : getSourceNodeSet()) {
			if (aNode.getId() == id)
				return aNode;
		}
		return null;
	}


}