package lrg.common.abstractions.plugins.conformities.creation.wrappers.operators;


public class Bracket extends ComparableOperator {
	{
		precedence = 0;
	}
	private boolean open;
	
	public Bracket(boolean open) {
		this.open = open;
	}
	
	public boolean isOpen() {
		return open;
	}
	
	public String toString() {
		if (open)
			return "(";
		return ")";
	}	
}