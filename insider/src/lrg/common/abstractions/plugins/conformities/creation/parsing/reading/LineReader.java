package lrg.common.abstractions.plugins.conformities.creation.parsing.reading;

public interface LineReader {
	public String readLine();

	public boolean isEmpty();
}