package lrg.common.abstractions.plugins.conformities.composed;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.Descriptor;
import lrg.common.abstractions.plugins.conformities.ConformityRule;
import lrg.common.abstractions.plugins.conformities.StackItem;
import lrg.common.abstractions.plugins.conformities.creation.wrappers.Filter;
import lrg.common.abstractions.plugins.filters.FilteringRule;

/**
 * Created by Horia Radu. User: horia Date: 21.09.2010 Time: 18:07:08 To change
 * this template use File | Settings | File Templates.
 */
public class ConformityRuleWrapper extends ConformityRule {
	private static final long serialVersionUID = 6297491355372229448L;
	private ConformityRuleExpression innerRule;
	private HashMap<FilteringRule, Boolean> filters;
	public ConformityRuleWrapper(String conformityRuleName, String targetEntityType, List<StackItem> result) {
		super(new Descriptor(conformityRuleName, targetEntityType));
		LinkedList<StackItem> expression = new LinkedList<StackItem>();
		expression.addAll(result);
		innerRule = new ConformityRuleExpression(expression);
	}

	public ConformityRuleWrapper(String ruleName, String targetEntityType, LinkedList<StackItem> expression, LinkedList<Filter> filters) {
		this(ruleName, targetEntityType, expression);
		this.filters = new HashMap<FilteringRule, Boolean>();
		for (Filter f : filters)
			this.filters.put(f.getFilteringRule(), f.getExpectedValue());
	}

	public Double applyConformity(AbstractEntityInterface anEntity) {
		if (filters == null)
			return (innerRule.applyConformity(anEntity));
		else {
			boolean filtered = true;
			for (FilteringRule filter : filters.keySet()) {
				if (filtered == false)
					break;
				filtered = filters.get(filter).equals(filter.applyFilter(anEntity));
			}
			if (filtered)
				return (innerRule.applyConformity(anEntity));
			return 0.0;
		}
	}
}