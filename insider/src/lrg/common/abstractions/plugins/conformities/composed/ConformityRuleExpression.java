package lrg.common.abstractions.plugins.conformities.composed;

import java.util.LinkedList;
import java.util.Stack;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.Descriptor;
import lrg.common.abstractions.plugins.conformities.ConformityRule;
import lrg.common.abstractions.plugins.conformities.StackItem;
import lrg.common.abstractions.plugins.conformities.operators.Operator;

public class ConformityRuleExpression extends ConformityRule {
	private static final long serialVersionUID = 6404613342345492079L;
	private ConformityRule innerRule;
	
	public ConformityRuleExpression (String conformityRuleName, String targetEntityType, LinkedList<StackItem> items) {
		super(new Descriptor(conformityRuleName, targetEntityType));
	}
	
	public ConformityRuleExpression(LinkedList<StackItem> items) {
		super(new Descriptor("bubu", "mumu"));
		
		Stack<ConformityRule> stack = new Stack<ConformityRule>();
		while (items.isEmpty() == false) {
			StackItem e = items.pop();
			if (e instanceof ConformityRule) {
				stack.push((ConformityRule) e);
			} else if (e instanceof Operator) {
				stack.push(((Operator) e).apply(stack.pop(), stack.pop()));
			}
		}
		innerRule = stack.pop();
	}
	
	public ConformityRuleExpression(ConformityRule innerRule) {
		super(new Descriptor("bubu", "mumu"));
		this.innerRule = innerRule;
	}

	public Double applyConformity(AbstractEntityInterface anEntity) {
		return innerRule.applyConformity(anEntity).doubleValue() / innerRule.getWeight();
	}
}