package lrg.common.abstractions.plugins.conformities.operators;

import lrg.common.abstractions.plugins.conformities.ConformityRule;
import lrg.common.abstractions.plugins.conformities.composed.MaxComposedConformityRule;

public class MaxOperator extends Operator {
	public String toString() {
		return "|";
	}

	@Override
	public ConformityRule apply(ConformityRule rule1, ConformityRule rule2) {
		return new MaxComposedConformityRule(rule1, rule2);
	}
}