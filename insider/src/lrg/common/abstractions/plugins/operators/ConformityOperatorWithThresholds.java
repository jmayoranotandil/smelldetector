package lrg.common.abstractions.plugins.operators;

import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.AbstractPlugin;
import lrg.common.abstractions.plugins.Descriptor;

public abstract class ConformityOperatorWithThresholds extends AbstractPlugin {
    public ConformityOperatorWithThresholds(String name, String entityTypeName) {
        super(new Descriptor(name, entityTypeName));
    }

    public abstract Double apply(ResultEntity theResult, Object threshold);

    public Double apply(ResultEntity theResult)
    {
        return new Double(0);
    }
}
