package lrg.common.metamodel;

import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;

import lrg.common.abstractions.plugins.AbstractPlugin;

public class Loader {
    public Loader(String outsiderPath) {
        this.outsiderPath = outsiderPath;

        outsiderNames = new ArrayList();

        //collectOutsiderNamesFromPath(outsiderPath);
        collectOutsiderNamesFromPath(outsiderPath, "");
    }

    public ArrayList getNames() {
        return outsiderNames;
    }

    public AbstractPlugin buildFrom(String propertyClassName, String directory)
    {
        if(propertyClassName.contains(directory))
        {
            return buildFrom(propertyClassName);
        }
        return null;
    }
    
    public AbstractPlugin buildFrom(String propertyClassName) {
        if (propertyClassName.contains("plugins")) {
            File file = new File(outsiderPath);
            try {
                URL url = file.toURL();
                URL urls[] = new URL[]{url};
                //ClassLoader classLoader = new URLClassLoader(urls);
                //Class c = classLoader.loadClass(propertyClassName);
                AbstractPlugin aPlugin = null;
                	//Object aPlugin =  c.newInstance();
                	aPlugin = LoadClasses(propertyClassName);
                /*AbstractPlugin aPlugin = (AbstractPlugin) c.newInstance();
                System.out.println(" else if (propertyClassName.equals(\""+propertyClassName+"\"))");
                System.out.println(" 		aPlugin = new "+propertyClassName+"();");*/
                return aPlugin;
            } catch (Exception e) {
                //System.out.println("WARNING: " + propertyClassName + " is not derived from AbstractPlugin or does not have a no-arg constructor. (SKIPPING)");
                //System.err.println("ERROR: " + propertyClassName +" plugin was not loaded successfully !");
                // System.out.println(e);
                //e.printStackTrace();
                return null;
            } catch (Error e) {
            	//System.err.println("ERROR: " + propertyClassName +" plugin was not loaded successfully !");
            	//e.printStackTrace();
                return null;
            }
        } else
            return null;
    }

    private AbstractPlugin LoadClasses(String propertyClassName){
    	AbstractPlugin aPlugin = null;
    	try {
	    	 if (propertyClassName.equals("lrg.insider.plugins.conformities.memoria.classes.NotDwarfConformityRule"))
	    	 		aPlugin = new lrg.insider.plugins.conformities.memoria.classes.NotDwarfConformityRule();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.conformities.memoria.classes.RefusedParentBequest"))
	    	 		aPlugin = new lrg.insider.plugins.conformities.memoria.classes.RefusedParentBequest();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.conformities.memoria.classes.RefusedParentBequestInterface"))
	    	 		aPlugin = new lrg.insider.plugins.conformities.memoria.classes.RefusedParentBequestInterface();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.conformities.memoria.classes.TraditionBreaker"))
	    	 		aPlugin = new lrg.insider.plugins.conformities.memoria.classes.TraditionBreaker();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.conformities.memoria.methods.HighCyclo"))
	    	 		aPlugin = new lrg.insider.plugins.conformities.memoria.methods.HighCyclo();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.conformities.memoria.methods.HighFanin"))
	    	 		aPlugin = new lrg.insider.plugins.conformities.memoria.methods.HighFanin();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.conformities.memoria.methods.LongMethod"))
	    	 		aPlugin = new lrg.insider.plugins.conformities.memoria.methods.LongMethod();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.conformities.memoria.methods.MyMaxConformity"))
	    	 		aPlugin = new lrg.insider.plugins.conformities.memoria.methods.MyMaxConformity();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.conformities.memoria.methods.MyWeightedConformityRule"))
	    	 		aPlugin = new lrg.insider.plugins.conformities.memoria.methods.MyWeightedConformityRule();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.conformities.memoria.tabel.classes.BrainClass"))
	    	 		aPlugin = new lrg.insider.plugins.conformities.memoria.tabel.classes.BrainClass();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.conformities.memoria.tabel.classes.DataClass"))
	    	 		aPlugin = new lrg.insider.plugins.conformities.memoria.tabel.classes.DataClass();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.conformities.memoria.tabel.methods.BrainMethod"))
	    	 		aPlugin = new lrg.insider.plugins.conformities.memoria.tabel.methods.BrainMethod();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.filters.memoria.InnerTypesFilter"))
	    	 		aPlugin = new lrg.insider.plugins.core.filters.memoria.InnerTypesFilter();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.filters.memoria.ModelClassFilter"))
	    	 		aPlugin = new lrg.insider.plugins.core.filters.memoria.ModelClassFilter();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.filters.memoria.ModelFunctionFilter"))
	    	 		aPlugin = new lrg.insider.plugins.core.filters.memoria.ModelFunctionFilter();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.filters.memoria.ModelPackageFilter"))
	    	 		aPlugin = new lrg.insider.plugins.core.filters.memoria.ModelPackageFilter();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.filters.memoria.ModelTypeFilter"))
	    	 		aPlugin = new lrg.insider.plugins.core.filters.memoria.ModelTypeFilter();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.filters.memoria.ModelVariableFilter"))
	    	 		aPlugin = new lrg.insider.plugins.core.filters.memoria.ModelVariableFilter();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.filters.memoria.UnnamedNamespacesFilter"))
	    	 		aPlugin = new lrg.insider.plugins.core.filters.memoria.UnnamedNamespacesFilter();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.groups.memoria.containment.ClassHasAttributes"))
	    	 		aPlugin = new lrg.insider.plugins.core.groups.memoria.containment.ClassHasAttributes();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.groups.memoria.containment.ClassHasMethods"))
	    	 		aPlugin = new lrg.insider.plugins.core.groups.memoria.containment.ClassHasMethods();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.groups.memoria.containment.ComponentContainsClasses"))
	    	 		aPlugin = new lrg.insider.plugins.core.groups.memoria.containment.ComponentContainsClasses();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.groups.memoria.containment.NamespaceHasTypes"))
	    	 		aPlugin = new lrg.insider.plugins.core.groups.memoria.containment.NamespaceHasTypes();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.groups.memoria.containment.OperationHasLocalVariables"))
	    	 		aPlugin = new lrg.insider.plugins.core.groups.memoria.containment.OperationHasLocalVariables();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.groups.memoria.containment.OperationHasParameters"))
	    	 		aPlugin = new lrg.insider.plugins.core.groups.memoria.containment.OperationHasParameters();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.groups.memoria.containment.PackageHasAnnotations"))
	    	 		aPlugin = new lrg.insider.plugins.core.groups.memoria.containment.PackageHasAnnotations();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.groups.memoria.containment.PackageHasClasses"))
	    	 		aPlugin = new lrg.insider.plugins.core.groups.memoria.containment.PackageHasClasses();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.groups.memoria.containment.PackageHasGlobalFunctions"))
	    	 		aPlugin = new lrg.insider.plugins.core.groups.memoria.containment.PackageHasGlobalFunctions();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.groups.memoria.containment.PackageHasGlobalVariables"))
	    	 		aPlugin = new lrg.insider.plugins.core.groups.memoria.containment.PackageHasGlobalVariables();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.groups.memoria.containment.ProgramElementHasBugs"))
	    	 		aPlugin = new lrg.insider.plugins.core.groups.memoria.containment.ProgramElementHasBugs();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.groups.memoria.containment.SubsytemHasPackages"))
	    	 		aPlugin = new lrg.insider.plugins.core.groups.memoria.containment.SubsytemHasPackages();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.groups.memoria.containment.SystemHasNamespaces"))
	    	 		aPlugin = new lrg.insider.plugins.core.groups.memoria.containment.SystemHasNamespaces();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.groups.memoria.containment.SystemHasSubsystems"))
	    	 		aPlugin = new lrg.insider.plugins.core.groups.memoria.containment.SystemHasSubsystems();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.groups.memoria.uses.AnnotationsForEntity"))
	    	 		aPlugin = new lrg.insider.plugins.core.groups.memoria.uses.AnnotationsForEntity();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.groups.memoria.uses.ClassDependsOnAncestors"))
	    	 		aPlugin = new lrg.insider.plugins.core.groups.memoria.uses.ClassDependsOnAncestors();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.groups.memoria.uses.ClassDependsOnBase"))
	    	 		aPlugin = new lrg.insider.plugins.core.groups.memoria.uses.ClassDependsOnBase();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.groups.memoria.uses.ClassUsedByDerived"))
	    	 		aPlugin = new lrg.insider.plugins.core.groups.memoria.uses.ClassUsedByDerived();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.groups.memoria.uses.ClassUsedByDescendants"))
	    	 		aPlugin = new lrg.insider.plugins.core.groups.memoria.uses.ClassUsedByDescendants();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.groups.memoria.uses.MethodOverrides"))
	    	 		aPlugin = new lrg.insider.plugins.core.groups.memoria.uses.MethodOverrides();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.groups.memoria.uses.OperationAccesses"))
	    	 		aPlugin = new lrg.insider.plugins.core.groups.memoria.uses.OperationAccesses();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.groups.memoria.uses.OperationCalls"))
	    	 		aPlugin = new lrg.insider.plugins.core.groups.memoria.uses.OperationCalls();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.groups.memoria.uses.OperationIsCalled"))
	    	 		aPlugin = new lrg.insider.plugins.core.groups.memoria.uses.OperationIsCalled();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.groups.memoria.uses.TypeUsedInDeclaration"))
	    	 		aPlugin = new lrg.insider.plugins.core.groups.memoria.uses.TypeUsedInDeclaration();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.groups.memoria.uses.VariableHasType"))
	    	 		aPlugin = new lrg.insider.plugins.core.groups.memoria.uses.VariableHasType();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.groups.memoria.uses.VariableIsAccessed"))
	    	 		aPlugin = new lrg.insider.plugins.core.groups.memoria.uses.VariableIsAccessed();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.operators.aggregation.AverageOperator"))
	    	 		aPlugin = new lrg.insider.plugins.core.operators.aggregation.AverageOperator();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.operators.aggregation.CountTrueOperator"))
	    	 		aPlugin = new lrg.insider.plugins.core.operators.aggregation.CountTrueOperator();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.operators.aggregation.LongestNameOperator"))
	    	 		aPlugin = new lrg.insider.plugins.core.operators.aggregation.LongestNameOperator();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.operators.aggregation.MaximumValueOperator"))
	    	 		aPlugin = new lrg.insider.plugins.core.operators.aggregation.MaximumValueOperator();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.operators.aggregation.MinimumValueOperator"))
	    	 		aPlugin = new lrg.insider.plugins.core.operators.aggregation.MinimumValueOperator();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.operators.aggregation.PercTrueOperator"))
	    	 		aPlugin = new lrg.insider.plugins.core.operators.aggregation.PercTrueOperator();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.operators.aggregation.SumOperator"))
	    	 		aPlugin = new lrg.insider.plugins.core.operators.aggregation.SumOperator();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.operators.bool.IsTrue"))
	    	 		aPlugin = new lrg.insider.plugins.core.operators.bool.IsTrue();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.operators.conformityOperators.DefuzzifyOperator"))
	    	 		aPlugin = new lrg.insider.plugins.core.operators.conformityOperators.DefuzzifyOperator();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.operators.conformityOperators.Equal"))
	    	 		aPlugin = new lrg.insider.plugins.core.operators.conformityOperators.Equal();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.operators.conformityOperators.HigherThanOperator"))
	    	 		aPlugin = new lrg.insider.plugins.core.operators.conformityOperators.HigherThanOperator();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.operators.conformityOperators.LowerThanOperator"))
	    	 		aPlugin = new lrg.insider.plugins.core.operators.conformityOperators.LowerThanOperator();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.operators.numerical.Equal"))
	    	 		aPlugin = new lrg.insider.plugins.core.operators.numerical.Equal();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.operators.numerical.HigherEqual"))
	    	 		aPlugin = new lrg.insider.plugins.core.operators.numerical.HigherEqual();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.operators.numerical.HigherThanOperator"))
	    	 		aPlugin = new lrg.insider.plugins.core.operators.numerical.HigherThanOperator();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.operators.numerical.LowerEqual"))
	    	 		aPlugin = new lrg.insider.plugins.core.operators.numerical.LowerEqual();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.operators.numerical.LowerThanOperator"))
	    	 		aPlugin = new lrg.insider.plugins.core.operators.numerical.LowerThanOperator();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.operators.string.Contains"))
	    	 		aPlugin = new lrg.insider.plugins.core.operators.string.Contains();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.operators.string.Equals"))
	    	 		aPlugin = new lrg.insider.plugins.core.operators.string.Equals();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.operators.string.StartsWith"))
	    	 		aPlugin = new lrg.insider.plugins.core.operators.string.StartsWith();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.operators.string.StringLengthHigher"))
	    	 		aPlugin = new lrg.insider.plugins.core.operators.string.StringLengthHigher();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.properties.groups.Address"))
	    	 		aPlugin = new lrg.insider.plugins.core.properties.groups.Address();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.properties.memoria.annotation.Address"))
	    	 		aPlugin = new lrg.insider.plugins.core.properties.memoria.annotation.Address();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.properties.memoria.annotation.Detail"))
	    	 		aPlugin = new lrg.insider.plugins.core.properties.memoria.annotation.Detail();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.properties.memoria.annotation.Name"))
	    	 		aPlugin = new lrg.insider.plugins.core.properties.memoria.annotation.Name();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.properties.memoria.annotation.Scope"))
	    	 		aPlugin = new lrg.insider.plugins.core.properties.memoria.annotation.Scope();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.properties.memoria.annotationinstance.Address"))
	    	 		aPlugin = new lrg.insider.plugins.core.properties.memoria.annotationinstance.Address();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.properties.memoria.annotationinstance.Detail"))
	    	 		aPlugin = new lrg.insider.plugins.core.properties.memoria.annotationinstance.Detail();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.properties.memoria.annotationinstance.Name"))
	    	 		aPlugin = new lrg.insider.plugins.core.properties.memoria.annotationinstance.Name();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.properties.memoria.annotationinstance.Scope"))
	    	 		aPlugin = new lrg.insider.plugins.core.properties.memoria.annotationinstance.Scope();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.properties.memoria.bug.Address"))
	    	 		aPlugin = new lrg.insider.plugins.core.properties.memoria.bug.Address();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.properties.memoria.bug.Detail"))
	    	 		aPlugin = new lrg.insider.plugins.core.properties.memoria.bug.Detail();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.properties.memoria.bug.Name"))
	    	 		aPlugin = new lrg.insider.plugins.core.properties.memoria.bug.Name();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.properties.memoria.classes.Address"))
	    	 		aPlugin = new lrg.insider.plugins.core.properties.memoria.classes.Address();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.properties.memoria.classes.Detail"))
	    	 		aPlugin = new lrg.insider.plugins.core.properties.memoria.classes.Detail();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.properties.memoria.classes.Name"))
	    	 		aPlugin = new lrg.insider.plugins.core.properties.memoria.classes.Name();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.properties.memoria.classes.Scope"))
	    	 		aPlugin = new lrg.insider.plugins.core.properties.memoria.classes.Scope();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.properties.memoria.component.Address"))
	    	 		aPlugin = new lrg.insider.plugins.core.properties.memoria.component.Address();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.properties.memoria.component.Detail"))
	    	 		aPlugin = new lrg.insider.plugins.core.properties.memoria.component.Detail();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.properties.memoria.component.Name"))
	    	 		aPlugin = new lrg.insider.plugins.core.properties.memoria.component.Name();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.properties.memoria.component.Scope"))
	    	 		aPlugin = new lrg.insider.plugins.core.properties.memoria.component.Scope();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.properties.memoria.functions.Address"))
	    	 		aPlugin = new lrg.insider.plugins.core.properties.memoria.functions.Address();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.properties.memoria.functions.Detail"))
	    	 		aPlugin = new lrg.insider.plugins.core.properties.memoria.functions.Detail();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.properties.memoria.functions.Name"))
	    	 		aPlugin = new lrg.insider.plugins.core.properties.memoria.functions.Name();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.properties.memoria.functions.Scope"))
	    	 		aPlugin = new lrg.insider.plugins.core.properties.memoria.functions.Scope();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.properties.memoria.inheritance.Address"))
	    	 		aPlugin = new lrg.insider.plugins.core.properties.memoria.inheritance.Address();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.properties.memoria.inheritance.Detail"))
	    	 		aPlugin = new lrg.insider.plugins.core.properties.memoria.inheritance.Detail();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.properties.memoria.inheritance.Name"))
	    	 		aPlugin = new lrg.insider.plugins.core.properties.memoria.inheritance.Name();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.properties.memoria.inheritance.Scope"))
	    	 		aPlugin = new lrg.insider.plugins.core.properties.memoria.inheritance.Scope();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.properties.memoria.namespaces.Address"))
	    	 		aPlugin = new lrg.insider.plugins.core.properties.memoria.namespaces.Address();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.properties.memoria.namespaces.Detail"))
	    	 		aPlugin = new lrg.insider.plugins.core.properties.memoria.namespaces.Detail();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.properties.memoria.namespaces.Name"))
	    	 		aPlugin = new lrg.insider.plugins.core.properties.memoria.namespaces.Name();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.properties.memoria.packages.Address"))
	    	 		aPlugin = new lrg.insider.plugins.core.properties.memoria.packages.Address();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.properties.memoria.packages.Detail"))
	    	 		aPlugin = new lrg.insider.plugins.core.properties.memoria.packages.Detail();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.properties.memoria.packages.Name"))
	    	 		aPlugin = new lrg.insider.plugins.core.properties.memoria.packages.Name();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.properties.memoria.packages.Scope"))
	    	 		aPlugin = new lrg.insider.plugins.core.properties.memoria.packages.Scope();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.properties.memoria.packages.Subsystem"))
	    	 		aPlugin = new lrg.insider.plugins.core.properties.memoria.packages.Subsystem();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.properties.memoria.subsystems.Address"))
	    	 		aPlugin = new lrg.insider.plugins.core.properties.memoria.subsystems.Address();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.properties.memoria.subsystems.Detail"))
	    	 		aPlugin = new lrg.insider.plugins.core.properties.memoria.subsystems.Detail();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.properties.memoria.subsystems.Name"))
	    	 		aPlugin = new lrg.insider.plugins.core.properties.memoria.subsystems.Name();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.properties.memoria.subsystems.Scope"))
	    	 		aPlugin = new lrg.insider.plugins.core.properties.memoria.subsystems.Scope();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.properties.memoria.system.Address"))
	    	 		aPlugin = new lrg.insider.plugins.core.properties.memoria.system.Address();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.properties.memoria.system.Detail"))
	    	 		aPlugin = new lrg.insider.plugins.core.properties.memoria.system.Detail();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.properties.memoria.system.Name"))
	    	 		aPlugin = new lrg.insider.plugins.core.properties.memoria.system.Name();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.properties.memoria.types.Address"))
	    	 		aPlugin = new lrg.insider.plugins.core.properties.memoria.types.Address();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.properties.memoria.types.Detail"))
	    	 		aPlugin = new lrg.insider.plugins.core.properties.memoria.types.Detail();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.properties.memoria.types.Name"))
	    	 		aPlugin = new lrg.insider.plugins.core.properties.memoria.types.Name();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.properties.memoria.types.Scope"))
	    	 		aPlugin = new lrg.insider.plugins.core.properties.memoria.types.Scope();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.properties.memoria.variables.Address"))
	    	 		aPlugin = new lrg.insider.plugins.core.properties.memoria.variables.Address();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.properties.memoria.variables.Detail"))
	    	 		aPlugin = new lrg.insider.plugins.core.properties.memoria.variables.Detail();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.properties.memoria.variables.Name"))
	    	 		aPlugin = new lrg.insider.plugins.core.properties.memoria.variables.Name();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.core.properties.memoria.variables.Scope"))
	    	 		aPlugin = new lrg.insider.plugins.core.properties.memoria.variables.Scope();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.details.DesignFlawsClassDetail"))
	    	 		aPlugin = new lrg.insider.plugins.details.DesignFlawsClassDetail();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.details.DesignFlawsMethodDetail"))
	    	 		aPlugin = new lrg.insider.plugins.details.DesignFlawsMethodDetail();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.details.PackageDesignFlaws"))
	    	 		aPlugin = new lrg.insider.plugins.details.PackageDesignFlaws();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.filters.memoria.classes.BrainClass"))
	    	 		aPlugin = new lrg.insider.plugins.filters.memoria.classes.BrainClass();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.filters.memoria.classes.DataClass"))
	    	 		aPlugin = new lrg.insider.plugins.filters.memoria.classes.DataClass();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.filters.memoria.classes.FutileAbstractPipeline"))
	    	 		aPlugin = new lrg.insider.plugins.filters.memoria.classes.FutileAbstractPipeline();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.filters.memoria.classes.FutileHierarchy"))
	    	 		aPlugin = new lrg.insider.plugins.filters.memoria.classes.FutileHierarchy();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.filters.memoria.classes.GodClass"))
	    	 		aPlugin = new lrg.insider.plugins.filters.memoria.classes.GodClass();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.filters.memoria.classes.HierarchyDuplication"))
	    	 		aPlugin = new lrg.insider.plugins.filters.memoria.classes.HierarchyDuplication();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.filters.memoria.classes.IsAbstract"))
	    	 		aPlugin = new lrg.insider.plugins.filters.memoria.classes.IsAbstract();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.filters.memoria.classes.IsInner"))
	    	 		aPlugin = new lrg.insider.plugins.filters.memoria.classes.IsInner();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.filters.memoria.classes.IsInterface"))
	    	 		aPlugin = new lrg.insider.plugins.filters.memoria.classes.IsInterface();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.filters.memoria.classes.IsLeafClass"))
	    	 		aPlugin = new lrg.insider.plugins.filters.memoria.classes.IsLeafClass();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.filters.memoria.classes.IsRootClass"))
	    	 		aPlugin = new lrg.insider.plugins.filters.memoria.classes.IsRootClass();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.filters.memoria.classes.RefusedParentBequest"))
	    	 		aPlugin = new lrg.insider.plugins.filters.memoria.classes.RefusedParentBequest();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.filters.memoria.classes.RefusedParentBequest2"))
	    	 		aPlugin = new lrg.insider.plugins.filters.memoria.classes.RefusedParentBequest2();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.filters.memoria.classes.RefusedParentBequestInterface"))
	    	 		aPlugin = new lrg.insider.plugins.filters.memoria.classes.RefusedParentBequestInterface();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.filters.memoria.classes.SchizofrenicClass"))
	    	 		aPlugin = new lrg.insider.plugins.filters.memoria.classes.SchizofrenicClass();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.filters.memoria.classes.TemporaryFieldHost"))
	    	 		aPlugin = new lrg.insider.plugins.filters.memoria.classes.TemporaryFieldHost();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.filters.memoria.classes.TraditionBreaker"))
	    	 		aPlugin = new lrg.insider.plugins.filters.memoria.classes.TraditionBreaker();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.filters.memoria.methods.ABUSEINH"))
	    	 		aPlugin = new lrg.insider.plugins.filters.memoria.methods.ABUSEINH();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.filters.memoria.methods.BrainMethod"))
	    	 		aPlugin = new lrg.insider.plugins.filters.memoria.methods.BrainMethod();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.filters.memoria.methods.ExtensiveCoupling"))
	    	 		aPlugin = new lrg.insider.plugins.filters.memoria.methods.ExtensiveCoupling();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.filters.memoria.methods.FeatureEnvy"))
	    	 		aPlugin = new lrg.insider.plugins.filters.memoria.methods.FeatureEnvy();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.filters.memoria.methods.IntensiveCoupling"))
	    	 		aPlugin = new lrg.insider.plugins.filters.memoria.methods.IntensiveCoupling();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.filters.memoria.methods.IsAbstract"))
	    	 		aPlugin = new lrg.insider.plugins.filters.memoria.methods.IsAbstract();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.filters.memoria.methods.IsAccessor"))
	    	 		aPlugin = new lrg.insider.plugins.filters.memoria.methods.IsAccessor();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.filters.memoria.methods.IsConstructor"))
	    	 		aPlugin = new lrg.insider.plugins.filters.memoria.methods.IsConstructor();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.filters.memoria.methods.IsEmpty"))
	    	 		aPlugin = new lrg.insider.plugins.filters.memoria.methods.IsEmpty();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.filters.memoria.methods.IsGlobalFunction"))
	    	 		aPlugin = new lrg.insider.plugins.filters.memoria.methods.IsGlobalFunction();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.filters.memoria.methods.IsOverriden"))
	    	 		aPlugin = new lrg.insider.plugins.filters.memoria.methods.IsOverriden();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.filters.memoria.methods.IsPrivate"))
	    	 		aPlugin = new lrg.insider.plugins.filters.memoria.methods.IsPrivate();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.filters.memoria.methods.IsProtected"))
	    	 		aPlugin = new lrg.insider.plugins.filters.memoria.methods.IsProtected();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.filters.memoria.methods.IsPublicMethod"))
	    	 		aPlugin = new lrg.insider.plugins.filters.memoria.methods.IsPublicMethod();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.filters.memoria.methods.IsSpecialization"))
	    	 		aPlugin = new lrg.insider.plugins.filters.memoria.methods.IsSpecialization();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.filters.memoria.methods.IsStatic"))
	    	 		aPlugin = new lrg.insider.plugins.filters.memoria.methods.IsStatic();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.filters.memoria.methods.LeafinCallgraph"))
	    	 		aPlugin = new lrg.insider.plugins.filters.memoria.methods.LeafinCallgraph();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.filters.memoria.methods.LiskovViolator"))
	    	 		aPlugin = new lrg.insider.plugins.filters.memoria.methods.LiskovViolator();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.filters.memoria.methods.ShortcircuitingMethod"))
	    	 		aPlugin = new lrg.insider.plugins.filters.memoria.methods.ShortcircuitingMethod();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.filters.memoria.methods.ShotgunSurgery"))
	    	 		aPlugin = new lrg.insider.plugins.filters.memoria.methods.ShotgunSurgery();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.filters.memoria.methods.ShouldBePrivate"))
	    	 		aPlugin = new lrg.insider.plugins.filters.memoria.methods.ShouldBePrivate();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.filters.memoria.methods.ShouldBeProtected"))
	    	 		aPlugin = new lrg.insider.plugins.filters.memoria.methods.ShouldBeProtected();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.filters.memoria.methods._BrainMethod"))
	    	 		aPlugin = new lrg.insider.plugins.filters.memoria.methods._BrainMethod();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.filters.memoria.packages.CyclicSubsystemsDependency"))
	    	 		aPlugin = new lrg.insider.plugins.filters.memoria.packages.CyclicSubsystemsDependency();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.filters.memoria.packages.SDPViolation"))
	    	 		aPlugin = new lrg.insider.plugins.filters.memoria.packages.SDPViolation();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.filters.memoria.packages.SDPViolationSubsystem"))
	    	 		aPlugin = new lrg.insider.plugins.filters.memoria.packages.SDPViolationSubsystem();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.filters.memoria.variables.IsAttribute"))
	    	 		aPlugin = new lrg.insider.plugins.filters.memoria.variables.IsAttribute();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.filters.memoria.variables.IsConstant"))
	    	 		aPlugin = new lrg.insider.plugins.filters.memoria.variables.IsConstant();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.filters.memoria.variables.IsParameter"))
	    	 		aPlugin = new lrg.insider.plugins.filters.memoria.variables.IsParameter();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.filters.memoria.variables.IsProtected"))
	    	 		aPlugin = new lrg.insider.plugins.filters.memoria.variables.IsProtected();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.filters.memoria.variables.IsPublic"))
	    	 		aPlugin = new lrg.insider.plugins.filters.memoria.variables.IsPublic();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.filters.memoria.variables.IsStatic"))
	    	 		aPlugin = new lrg.insider.plugins.filters.memoria.variables.IsStatic();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.filters.memoria.variables.IsTemporaryField"))
	    	 		aPlugin = new lrg.insider.plugins.filters.memoria.variables.IsTemporaryField();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.filters.memoria.variables.IsUserDefined"))
	    	 		aPlugin = new lrg.insider.plugins.filters.memoria.variables.IsUserDefined();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.groups.memoria.AllInheritanceRelations"))
	    	 		aPlugin = new lrg.insider.plugins.groups.memoria.AllInheritanceRelations();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.groups.memoria.ClassesWithHierarchyDuplication"))
	    	 		aPlugin = new lrg.insider.plugins.groups.memoria.ClassesWithHierarchyDuplication();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.groups.memoria.ClassExternalServiceProviders"))
	    	 		aPlugin = new lrg.insider.plugins.groups.memoria.ClassExternalServiceProviders();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.groups.memoria.ClassificationDisharmony"))
	    	 		aPlugin = new lrg.insider.plugins.groups.memoria.ClassificationDisharmony();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.groups.memoria.ClassWithOverwritingMethods"))
	    	 		aPlugin = new lrg.insider.plugins.groups.memoria.ClassWithOverwritingMethods();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.groups.memoria.CollaborationDisharmony"))
	    	 		aPlugin = new lrg.insider.plugins.groups.memoria.CollaborationDisharmony();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.groups.memoria.CriticalIdentityHarmonyMethods"))
	    	 		aPlugin = new lrg.insider.plugins.groups.memoria.CriticalIdentityHarmonyMethods();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.groups.memoria.DataProvidersForClass"))
	    	 		aPlugin = new lrg.insider.plugins.groups.memoria.DataProvidersForClass();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.groups.memoria.DataProvidersForLargeGodClasses"))
	    	 		aPlugin = new lrg.insider.plugins.groups.memoria.DataProvidersForLargeGodClasses();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.groups.memoria.EfferentCoupling"))
	    	 		aPlugin = new lrg.insider.plugins.groups.memoria.EfferentCoupling();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.groups.memoria.ExternalData"))
	    	 		aPlugin = new lrg.insider.plugins.groups.memoria.ExternalData();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.groups.memoria.ExternalServiceProviders"))
	    	 		aPlugin = new lrg.insider.plugins.groups.memoria.ExternalServiceProviders();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.groups.memoria.FaninClassGroup"))
	    	 		aPlugin = new lrg.insider.plugins.groups.memoria.FaninClassGroup();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.groups.memoria.FanoutClassGroup"))
	    	 		aPlugin = new lrg.insider.plugins.groups.memoria.FanoutClassGroup();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.groups.memoria.HeavyCalledMethods"))
	    	 		aPlugin = new lrg.insider.plugins.groups.memoria.HeavyCalledMethods();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.groups.memoria.HeavyServiceProviders"))
	    	 		aPlugin = new lrg.insider.plugins.groups.memoria.HeavyServiceProviders();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.groups.memoria.IdentityDisharmony"))
	    	 		aPlugin = new lrg.insider.plugins.groups.memoria.IdentityDisharmony();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.groups.memoria.MaxCallPath"))
	    	 		aPlugin = new lrg.insider.plugins.groups.memoria.MaxCallPath();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.groups.memoria.MethodsDependingOnBaseclass"))
	    	 		aPlugin = new lrg.insider.plugins.groups.memoria.MethodsDependingOnBaseclass();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.groups.memoria.MethodsWithExternalDuplication"))
	    	 		aPlugin = new lrg.insider.plugins.groups.memoria.MethodsWithExternalDuplication();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.groups.memoria.MethodsWithHierarchyDuplication"))
	    	 		aPlugin = new lrg.insider.plugins.groups.memoria.MethodsWithHierarchyDuplication();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.groups.memoria.NewlyAddedServices"))
	    	 		aPlugin = new lrg.insider.plugins.groups.memoria.NewlyAddedServices();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.groups.memoria.RefusedAncestors"))
	    	 		aPlugin = new lrg.insider.plugins.groups.memoria.RefusedAncestors();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.groups.memoria.SubclassesDependedOn"))
	    	 		aPlugin = new lrg.insider.plugins.groups.memoria.SubclassesDependedOn();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.groups.memoria.SystemHasComponents"))
	    	 		aPlugin = new lrg.insider.plugins.groups.memoria.SystemHasComponents();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.groups.memoria.TemporaryFieldsGroup"))
	    	 		aPlugin = new lrg.insider.plugins.groups.memoria.TemporaryFieldsGroup();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.classes.NOM"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.classes.NOM();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.classes.AMW"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.classes.AMW();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.classes.AncestorName"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.classes.AncestorName();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.classes.ATFD"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.classes.ATFD();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.classes.AttributeUsageClusters"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.classes.AttributeUsageClusters();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.classes.BOvM"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.classes.BOvM();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.classes.BUGS"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.classes.BUGS();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.classes.BUR"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.classes.BUR();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.classes.CBO"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.classes.CBO();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.classes.CC"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.classes.CC();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.classes.CM"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.classes.CM();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.classes.CodeReuseIntention"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.classes.CodeReuseIntention();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.classes.CRIX"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.classes.CRIX();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.classes.CW"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.classes.CW();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.classes.DAC"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.classes.DAC();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.classes.DIT"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.classes.DIT();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.classes.EDUPCLS"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.classes.EDUPCLS();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.classes.FANOUT"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.classes.FANOUT();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.classes.FDP"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.classes.FDP();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.classes.GREEDY"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.classes.GREEDY();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.classes.HDUPCLS"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.classes.HDUPCLS();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.classes.HIT"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.classes.HIT();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.classes.IDUPLINES"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.classes.IDUPLINES();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.classes.InterfaceReuseIntention"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.classes.InterfaceReuseIntention();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.classes.LOC"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.classes.LOC();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.classes.MethodsUsageClusters"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.classes.MethodsUsageClusters();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.classes.ModelClass"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.classes.ModelClass();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.classes.NAbsM"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.classes.NAbsM();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.classes.NAS"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.classes.NAS();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.classes.NDU"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.classes.NDU();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.classes.NOA"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.classes.NOA();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.classes.NOAM"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.classes.NOAM();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.classes.NOD"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.classes.NOD();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.classes.NODD"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.classes.NODD();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.classes.NOPA"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.classes.NOPA();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.classes.NProtM"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.classes.NProtM();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.classes.NrBM"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.classes.NrBM();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.classes.NrEC"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.classes.NrEC();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.classes.NrFE"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.classes.NrFE();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.classes.NrIC"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.classes.NrIC();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.classes.NrSS"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.classes.NrSS();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.classes.NSPECM"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.classes.NSPECM();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.classes.NTempF"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.classes.NTempF();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.classes.PAGERANK"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.classes.PAGERANK();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.classes.PNAS"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.classes.PNAS();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.classes.SCHIZO"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.classes.SCHIZO();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.classes.TCC"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.classes.TCC();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.classes.UsageClusters"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.classes.UsageClusters();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.classes.WMC"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.classes.WMC();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.classes.WOC"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.classes.WOC();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.methods.EDUPLINES"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.methods.EDUPLINES();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.methods.AccessedModelClasses"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.methods.AccessedModelClasses();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.methods.AccessedModelData"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.methods.AccessedModelData();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.methods.ALD"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.methods.ALD();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.methods.ATFD"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.methods.ATFD();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.methods.CalinsProperty"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.methods.CalinsProperty();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.methods.CallChainLength"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.methods.CallChainLength();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.methods.CC"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.methods.CC();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.methods.CDISP"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.methods.CDISP();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.methods.CEXT"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.methods.CEXT();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.methods.ChangingClasses"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.methods.ChangingClasses();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.methods.ChangingMethods"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.methods.ChangingMethods();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.methods.CINT"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.methods.CINT();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.methods.CM"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.methods.CM();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.methods.CurrentClassAndAncestors"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.methods.CurrentClassAndAncestors();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.methods.Cyclo"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.methods.Cyclo();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.methods.DUPSTRING"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.methods.DUPSTRING();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.methods.FANIN"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.methods.FANIN();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.methods.FANOUT"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.methods.FANOUT();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.methods.FANOUTCLASS"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.methods.FANOUTCLASS();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.methods.FDP"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.methods.FDP();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.methods.HDUPLINES"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.methods.HDUPLINES();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.methods.IDUPLINES"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.methods.IDUPLINES();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.methods.LAA"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.methods.LAA();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.methods.LOC"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.methods.LOC();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.methods.MAXNESTING"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.methods.MAXNESTING();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.methods.NOAV"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.methods.NOAV();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.methods.NOEU"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.methods.NOEU();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.methods.NOLV"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.methods.NOLV();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.methods.NOP"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.methods.NOP();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.methods.NOVRM"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.methods.NOVRM();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.methods.PureCodeReuse"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.methods.PureCodeReuse();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.methods.ScopeName"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.methods.ScopeName();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.methods.SpecializedCodeReuse"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.methods.SpecializedCodeReuse();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.packages.AbstractRatio"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.packages.AbstractRatio();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.packages.InstabilityFactor"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.packages.InstabilityFactor();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.packages.ModelPackage"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.packages.ModelPackage();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.packages.NOEC"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.packages.NOEC();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.packages.NumberOfClasses"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.packages.NumberOfClasses();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.packages.SAPDistance"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.packages.SAPDistance();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.packages.TOTALDEP"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.packages.TOTALDEP();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.subsystems.AbstractRatio"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.subsystems.AbstractRatio();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.subsystems.InstabilityFactor"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.subsystems.InstabilityFactor();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.subsystems.SAPDistance"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.subsystems.SAPDistance();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.subsystems.TOTALDEP"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.subsystems.TOTALDEP();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.system.AVG_DEPEXT"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.system.AVG_DEPEXT();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.system.AVG_FANOUT"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.system.AVG_FANOUT();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.system.AVG_HIT"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.system.AVG_HIT();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.system.AVG_NDD"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.system.AVG_NDD();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.system.NOP"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.system.NOP();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.variable.NMAV"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.variable.NMAV();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.properties.memoria.variable.Type"))
	    	 		aPlugin = new lrg.insider.plugins.properties.memoria.variable.Type();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.tools.bugreports.BugReportsImporter"))
	    	 		aPlugin = new lrg.insider.plugins.tools.bugreports.BugReportsImporter();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.tools.ConnectMethods"))
	    	 		aPlugin = new lrg.insider.plugins.tools.ConnectMethods();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.tools.dat.DatesTool"))
	    	 		aPlugin = new lrg.insider.plugins.tools.dat.DatesTool();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.tools.DesignFlawsReport"))
	    	 		aPlugin = new lrg.insider.plugins.tools.DesignFlawsReport();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.tools.dude.DudeMethodTool"))
	    	 		aPlugin = new lrg.insider.plugins.tools.dude.DudeMethodTool();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.tools.dude.DudeTool"))
	    	 		aPlugin = new lrg.insider.plugins.tools.dude.DudeTool();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.tools.EntityIdentifierNames"))
	    	 		aPlugin = new lrg.insider.plugins.tools.EntityIdentifierNames();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.tools.HippoReportGenerator"))
	    	 		aPlugin = new lrg.insider.plugins.tools.HippoReportGenerator();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.tools.MapGraphGenerator"))
	    	 		aPlugin = new lrg.insider.plugins.tools.MapGraphGenerator();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.tools.memoria.CDIFExporter"))
	    	 		aPlugin = new lrg.insider.plugins.tools.memoria.CDIFExporter();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.tools.memoria.graphgen.ClassGraphGenerator"))
	    	 		aPlugin = new lrg.insider.plugins.tools.memoria.graphgen.ClassGraphGenerator();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.tools.memoria.graphgen.ClassGroupGraphGenerator"))
	    	 		aPlugin = new lrg.insider.plugins.tools.memoria.graphgen.ClassGroupGraphGenerator();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.tools.memoria.graphgen.RMIOneFileSystemGraphGenerator"))
	    	 		aPlugin = new lrg.insider.plugins.tools.memoria.graphgen.RMIOneFileSystemGraphGenerator();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.tools.memoria.graphgen.RMISystemGraphGenerator"))
	    	 		aPlugin = new lrg.insider.plugins.tools.memoria.graphgen.RMISystemGraphGenerator();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.tools.memoria.graphgen.SystemGraphGenerator"))
	    	 		aPlugin = new lrg.insider.plugins.tools.memoria.graphgen.SystemGraphGenerator();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.tools.memoria.graphgen.SystemOneFileGraphGenerator"))
	    	 		aPlugin = new lrg.insider.plugins.tools.memoria.graphgen.SystemOneFileGraphGenerator();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.tools.memoria.MSEExporter"))
	    	 		aPlugin = new lrg.insider.plugins.tools.memoria.MSEExporter();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.tools.OverviewPyramid"))
	    	 		aPlugin = new lrg.insider.plugins.tools.OverviewPyramid();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.tools.PageRankComputer"))
	    	 		aPlugin = new lrg.insider.plugins.tools.PageRankComputer();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.tools.PluginsGenerator"))
	    	 		aPlugin = new lrg.insider.plugins.tools.PluginsGenerator();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.tools.SaveCache"))
	    	 		aPlugin = new lrg.insider.plugins.tools.SaveCache();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.visualizations.AbstractnessInstabilityPlot"))
	    	 		aPlugin = new lrg.insider.plugins.visualizations.AbstractnessInstabilityPlot();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.visualizations.AISubsystemPlot"))
	    	 		aPlugin = new lrg.insider.plugins.visualizations.AISubsystemPlot();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.visualizations.AllDescendantsWithName"))
	    	 		aPlugin = new lrg.insider.plugins.visualizations.AllDescendantsWithName();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.visualizations.AttributeUsage"))
	    	 		aPlugin = new lrg.insider.plugins.visualizations.AttributeUsage();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.visualizations.ClassBlueprint"))
	    	 		aPlugin = new lrg.insider.plugins.visualizations.ClassBlueprint();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.visualizations.MethodInteraction"))
	    	 		aPlugin = new lrg.insider.plugins.visualizations.MethodInteraction();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.visualizations.RootClassDetection"))
	    	 		aPlugin = new lrg.insider.plugins.visualizations.RootClassDetection();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.visualizations.SystemComplexity"))
	    	 		aPlugin = new lrg.insider.plugins.visualizations.SystemComplexity();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.visualizations.SystemInteraction"))
	    	 		aPlugin = new lrg.insider.plugins.visualizations.SystemInteraction();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.visualizations.ClassInteraction"))
	    	 		aPlugin = new lrg.insider.plugins.visualizations.ClassInteraction();
	    	 else if (propertyClassName.equals("lrg.insider.plugins.visualizations.DesignFlawsOverview"))
	    	 		aPlugin = new lrg.insider.plugins.visualizations.DesignFlawsOverview();
	    }
		catch (Exception e) {
			System.err.println("Failed a new instance of class " + propertyClassName);
			
		}
		return aPlugin;
	}
    private void collectOutsiderNamesFromPath(String path, String pathToAdd) {
        File thisPath = new File(path);
        File[] files = thisPath.listFiles();

        if (files == null) return;
        for (int i = 0; i < files.length; i++) {
            if (files[i].getName().endsWith(".class") && files[i].isFile())
                outsiderNames.add(pathToAdd + files[i].getName().substring(0, files[i].getName().lastIndexOf(".")));

            if (files[i].isDirectory())
                collectOutsiderNamesFromPath(path + File.separator + files[i].getName(), pathToAdd + files[i].getName() + ".");
        }
    }

    private ArrayList outsiderNames;
    private String outsiderPath;
}
