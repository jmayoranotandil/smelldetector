package lrg.insider.gui;

import java.util.ArrayList;

import javax.swing.JOptionPane;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.plugins.tools.AbstractEntityTool;
import lrg.common.metamodel.MetaModel;
import lrg.insider.gui.ui.loader.ModelLoaderUI;
import lrg.insider.gui.ui.stories.StoryTreeUI;
import lrg.insider.gui.ui.utils.ProgressBar;
import lrg.insider.metamodel.Address;
import lrg.insider.metamodel.MemoriaJavaModelBuilder;

public class InsiderTextMain {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		if (args.length < 2) {
			System.out.println("Param Usage: [project source path] [report class name] (result source path)");
			return;
		}

		String strSourcePath = args[0];
		String strReportName = "lrg.insider.plugins.tools." + args[1];				
		String strReportPath = (args.length > 2 ? args[2] : "");

		//create model
		ProgressBar progress = new ProgressBar("Loading the model ...");
		try {
			MetaModel.createFrom(new MemoriaJavaModelBuilder(strSourcePath,
					ModelLoaderUI.getCachePath(), InsiderGUIMain
							.getAdditioanClassPath(), progress), strSourcePath);
		} catch (Exception e2) {
			e2.printStackTrace();
			JOptionPane.showMessageDialog(StoryTreeUI.instance()
					.getTopComponent(), "The model could not be loaded !",
					"ERROR", JOptionPane.WARNING_MESSAGE);
			return;
		} finally {
			progress.close();
		}		

		ArrayList dummyList = new ArrayList();
		dummyList.add(MetaModel.instance().findEntityByAddress(
				Address.buildForRoot()));
		AbstractEntityInterface selectedEntity = (new GroupEntity(Address
				.buildForRoot(), dummyList)).getElementAt(0);
		
		// create report
		ArrayList paramList = new ArrayList();
		paramList.add(strReportPath);	
		
		AbstractEntityTool aEntityTool = null;
		try {
			Class classReport = Class.forName(strReportName);
			aEntityTool = (AbstractEntityTool) classReport.newInstance();			
		} catch (ClassNotFoundException e) {			 
             JOptionPane.showMessageDialog(StoryTreeUI.instance()
 					.getTopComponent(), "Class "+strReportName+" not found!", "EXCEPTION", JOptionPane.WARNING_MESSAGE);
             e.printStackTrace();
		} catch (InstantiationException instExc) {		
			System.out.println("Class "+ strReportName+" is a non-instantiable class !");					
			instExc.printStackTrace();
		} catch (IllegalAccessException illegalExc) {				
			System.out.println("Class "+ strReportName+" or its nullary constructor is not accessible !");	
			illegalExc.printStackTrace();
		}

		//run report
		try {
			aEntityTool.run(selectedEntity, paramList);			
		}catch (RuntimeException exc) {
			System.err.println(aEntityTool.getToolName()
					+ " could not be run !");
			exc.printStackTrace();
		}
	}
}