package lrg.insider.gui;

import java.awt.Toolkit;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.UIManager;

import lrg.insider.gui.ui.MainForm;

/* my first CVS recorded comment */
public class InsiderGUIMain
{
    private static boolean useSAIL = false;
    private static String additionalClassPath = "";

    public static String getAdditioanClassPath() {return additionalClassPath; }
    
    public static boolean withSAIL() { return useSAIL; }
    public static void main(String[] args)
    {
        if((args.length > 0) && (args[0].compareTo("useSAIL") == 0)) useSAIL = true;
        else if(args.length > 0) additionalClassPath = args[0];

        JDialog.setDefaultLookAndFeelDecorated(true);
        JFrame.setDefaultLookAndFeelDecorated(true);
        try{
            UIManager.setLookAndFeel("com.birosoft.liquid.LiquidLookAndFeel");
        }catch(Exception e){
            e.printStackTrace();
        }

        javax.swing.SwingUtilities.invokeLater(new Runnable()
        {
            public void run()
            {
                MainForm mainForm = MainForm.instance();
                //ToolToipManager.sharedInstance().setDismissDelay(Integer.MAX_VALUE);
                //ToolTipManager.sharedInstance().setInitialDelay(1750);
                //ToolTipManager.sharedInstance().setReshowDelay(1500);
                frame = new JFrame("iPlasma 6.0");
                frame.setContentPane(mainForm.getTopComponent());
                frame.setJMenuBar(mainForm.getMenuBar());
                frame.pack();
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                frame.setLocation((int)Toolkit.getDefaultToolkit().getScreenSize().getWidth() / 2 - 450, (int)Toolkit.getDefaultToolkit().getScreenSize().getHeight() / 2 - 300);
                frame.setSize(900, 600);
                frame.show();
            }
        });
    }

    public static JFrame getFrame()
    {
        return frame;
    }

    private static JFrame frame;
}
