package lrg.insider.gui.ui.stories;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JDialog;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTree;
import javax.swing.ToolTipManager;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;

import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.metamodel.MetaModel;
import lrg.insider.gui.InsiderGUIMain;
import lrg.insider.gui.ui.EntityMouseAction;
import lrg.insider.gui.ui.filter.CustomFilterUI;
import lrg.insider.gui.ui.views.ViewUI;
import lrg.insider.metamodel.Address;

public class StoryTreeUI extends MouseAdapter implements TreeSelectionListener,
        TreeModelListener, ActionListener, KeyListener {
    public final static int BROTHER = 1;
    public final static int CHILD = 2;
    public final static int FROM_ROOT = 3;

    public static StoryTreeUI instance() {
        if (theStoryTreeUI == null)
            theStoryTreeUI = new StoryTreeUI();

        return theStoryTreeUI;
    }

    private static StoryTreeUI theStoryTreeUI;

    private StoryTreeUI() {
        storyTree = new JTree();

        ToolTipManager.sharedInstance().registerComponent(storyTree);
        storyTree.setCellRenderer(new StoryTreeRenderer());

        storyTree.setEditable(false);
        storyTree.addTreeSelectionListener(this);

        storyTreeRoot = new DefaultMutableTreeNode("Use File->Open to load your sources");
        storyTreeModel = new DefaultTreeModel(storyTreeRoot);
        storyTreeModel.addTreeModelListener(this);
        storyTree.addMouseListener(this);
        storyTree.addKeyListener(this);
        storyTree.addTreeSelectionListener(this);
        storyTree.setModel(storyTreeModel);
        storyTree.setEnabled(false);
        treeScrollPane = new JScrollPane(storyTree);

        splitPane = new JSplitPane();
        splitPane.setDividerLocation(520);
        splitPane.setOrientation(JSplitPane.HORIZONTAL_SPLIT);
        splitPane.setLeftComponent(treeScrollPane);
        splitPane.setRightComponent(null);

        popup = new JPopupMenu();

        deleteStoryMenuItem = new JMenuItem("Delete This Story");
        deleteStoryMenuItem.addActionListener(this);
        
        conformityRulesMenu = new JMenu("Apply Conformity Rule");
        filteringRulesMenu = new JMenu("Apply Filtering Rule");
        applyCustomFilter = new JMenuItem("Custom");
        applyCustomFilter.addActionListener(this);
        selectColumnsMenuItem = new JMenuItem("Select Columns");
        selectColumnsMenuItem.addActionListener(this);

//        filteringRulesMenu.getPopupMenu().setLayout(new GridLayout(0, 2));

        popup.add(selectColumnsMenuItem);
        popup.add(filteringRulesMenu);
        popup.add(conformityRulesMenu);
        popup.addSeparator();

        popup.add(deleteStoryMenuItem);
    }

    public Component getTopComponent() {
        return splitPane;
    }

    public void setMetaModel() {
        ArrayList dummyList = new ArrayList();
        dummyList.add(MetaModel.instance().findEntityByAddress(Address.buildForRoot()));

        StoryUI dummyStoryUI = new StoryUI(new ViewUI(new GroupEntity(Address.buildForRoot(), dummyList)));
        dummyStoryUI.setName(Address.buildForRoot());
        storyTreeRoot = new DefaultMutableTreeNode(dummyStoryUI);
        storyTreeModel.setRoot(storyTreeRoot);
        setSelectedNode(storyTreeRoot);
        storyTree.setEnabled(true);
    }

    public void unloadMetaModel() {
        storyTree = new JTree();
//        storyTree.setEditable(true);
        storyTree.addTreeSelectionListener(this);
        storyTreeRoot = new DefaultMutableTreeNode("Use File->Open to load your sources");
        storyTreeModel = new DefaultTreeModel(storyTreeRoot);
        storyTreeModel.addTreeModelListener(this);
        storyTree.addMouseListener(this);
        storyTree.setModel(storyTreeModel);
        storyTree.setEnabled(false);

        treeScrollPane = new JScrollPane(storyTree);
        splitPane.setLeftComponent(treeScrollPane);
        splitPane.setRightComponent(null);
        selectedStoryUI = null;
    }

    public void addStoryUI(StoryUI aStoryUI, int parent) {
        DefaultMutableTreeNode parentNode = null;
        TreePath selectedPath = storyTree.getSelectionPath();

        if (selectedPath == null) {
            parentNode = storyTreeRoot;
        } else {
            switch (parent) {
                case CHILD:
                    parentNode = (DefaultMutableTreeNode) selectedPath.getLastPathComponent();
                    break;
                case BROTHER:
                    parentNode = (DefaultMutableTreeNode)
                            ((DefaultMutableTreeNode) selectedPath.getLastPathComponent()).getParent();
                    break;
                default:
                    parentNode = storyTreeRoot;
            }
        }

        if (parentNode == null) {
            parentNode = storyTreeRoot;
        }

        DefaultMutableTreeNode anotherNode = new DefaultMutableTreeNode(aStoryUI);

        //anotherNode.getUserObject();

        storyTreeModel.insertNodeInto(anotherNode, parentNode, parentNode.getChildCount());
        setSelectedNode(anotherNode);


    }

    public void valueChanged(TreeSelectionEvent e) {
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) storyTree.getLastSelectedPathComponent();

        if (node == null)
            return;

        setSelectedNode(node);
        selectedStoryUI.getSelectedViewUI().updateGroupMenu();
        EntityMouseAction.instance().addFiltersAndPropertiesToMenu();
    }

    private void deleteSelctedTreeNode() {
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) storyTree.getLastSelectedPathComponent();
        if (node == null || node == storyTreeRoot)
            return;

        setSelectedNode((DefaultMutableTreeNode) node.getParent());
        storyTreeModel.removeNodeFromParent(node);
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == deleteStoryMenuItem) {
            deleteSelctedTreeNode();
            return;
        }

        if (e.getSource() == applyCustomFilter) {
            ViewUI selectedViewUI = selectedStoryUI.getSelectedViewUI();
            CustomFilterUI cfUI = new CustomFilterUI(selectedViewUI);
            JDialog customFilterDialog = new JDialog(InsiderGUIMain.getFrame(), "Custom filter editor", false);

            customFilterDialog.setContentPane(cfUI.getTopComponent());
            customFilterDialog.setSize(600, 400);
            customFilterDialog.setLocationRelativeTo(null);
            customFilterDialog.show();
            return;
        }

        if (e.getSource() == selectColumnsMenuItem) {
            ViewUI selectedViewUI = selectedStoryUI.getSelectedViewUI();
            SelectColumnsUI x = new SelectColumnsUI(selectedViewUI);
            x.pack();
            x.show();
            x = null;
            return;
        }
    }


    public void mousePressed(MouseEvent e) {
        maybeShowPopup(e);
    }

    public void mouseReleased(MouseEvent e) {
        maybeShowPopup(e);
    }

    private void maybeShowPopup(MouseEvent e) {
        if (e.isPopupTrigger() && e.getComponent().isEnabled()) {
            DefaultMutableTreeNode node = (DefaultMutableTreeNode) storyTree.getPathForLocation(e.getX(), e.getY()).getLastPathComponent();
            setSelectedNode(node);

            deleteStoryMenuItem.setEnabled(node != storyTreeRoot);

            popup.show(e.getComponent(), e.getX(), e.getY());
        }
    }

    private void setSelectedNode(DefaultMutableTreeNode theNode) {
        selectedStoryUI = (StoryUI) theNode.getUserObject();
        splitPane.setRightComponent(selectedStoryUI.getTopComponent());
        splitPane.setDividerLocation(0.25);
        storyTree.setSelectionPath(new TreePath(theNode.getPath()));
    }

    private JSplitPane splitPane;

    private JScrollPane treeScrollPane;
    private JTree storyTree;
    private DefaultTreeModel storyTreeModel;
    private DefaultMutableTreeNode storyTreeRoot;

    public void treeNodesChanged(TreeModelEvent e) {

        if (e.getSource() == storyTreeModel) {
            DefaultMutableTreeNode node;
            node = (DefaultMutableTreeNode)
                    (e.getTreePath().getLastPathComponent());

            /*
             * If the event lists children, then the changed
             * node is the child of the node we've already
             * gotten.  Otherwise, the changed node and the
             * specified node are the same.
             */
            try {
                int index = e.getChildIndices()[0];
                node = (DefaultMutableTreeNode)
                        (node.getChildAt(index));
            } catch (NullPointerException exc) {
            }

            //System.out.println("The user has finished editing the node.");
            //System.out.println("New value: " + node.getUserObject());
            selectedStoryUI.setName(node.getUserObject().toString());
            node.setUserObject(selectedStoryUI);
        }

    }

    public void treeNodesInserted(TreeModelEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void treeNodesRemoved(TreeModelEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void treeStructureChanged(TreeModelEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public StoryUI getSelectedView() {
        return selectedStoryUI;
    }

    public void keyReleased(KeyEvent e) {}

    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_DELETE) {
            deleteSelctedTreeNode();
        }
    }

    public void keyTyped(KeyEvent e) {}

    private StoryUI selectedStoryUI;
    private JPopupMenu popup;
    private JMenuItem deleteStoryMenuItem;
    private JMenuItem selectColumnsMenuItem;
    public JMenu filteringRulesMenu;
    public JMenu conformityRulesMenu;
    public JMenuItem applyCustomFilter;
}
