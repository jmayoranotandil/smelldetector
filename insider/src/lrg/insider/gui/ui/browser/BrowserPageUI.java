package lrg.insider.gui.ui.browser;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JEditorPane;
import javax.swing.JScrollPane;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.metamodel.MetaModel;
import lrg.insider.gui.ui.EntityMouseAction;
import lrg.insider.gui.ui.MainForm;
import lrg.insider.gui.ui.stories.StoryTreeUI;
import lrg.insider.gui.ui.stories.StoryUI;
import lrg.insider.gui.ui.views.ViewUI;

public class BrowserPageUI extends MouseAdapter implements HyperlinkListener {
	private String detailName = "";

	public BrowserPageUI(BrowserUI myBrowserUI) {
        this.myBrowserUI = myBrowserUI;
        history = new History(500);

        myBrowserUI.addBrowser(this);
        htmlOutput = new JEditorPane();
        htmlOutput.setEditable(false);
        htmlOutput.setContentType("text/html");
        htmlOutput.addHyperlinkListener(this);
        htmlOutput.setBackground(new Color(16777113).brighter());
        htmlOutput.addMouseListener(this);
        scrollPane.setViewportView(htmlOutput);        
    }

	public String getDetailName() { return detailName; }
	
    public BrowserPageUI(BrowserUI myBrowserUI, String address, String detailName) {
        this(myBrowserUI);
        this.goTo(address, detailName);
        this.detailName = detailName;
    }

    public void goTo(String address, String detailName) {
    	this.detailName = detailName;
        entity = MetaModel.instance().findEntityByAddress(address);
        if ((entity instanceof GroupEntity) && (entity.getName().compareTo("~root") != 0)) {
            GroupEntity gp = (GroupEntity) entity;
            if (gp.size()>0) {
                StoryTreeUI.instance().addStoryUI(new StoryUI(new ViewUI(gp)),StoryTreeUI.FROM_ROOT);
                //storyTreeUI.instance().getSelectedView().addViewUI(new ViewUI((GroupEntity) entity));
            }
        } else {
            history.addNewDetail(address, detailName);
            htmlOutput.setText(buildHTMLTextFor(entity, detailName));
            htmlOutput.setCaretPosition(0);
            myBrowserUI.update(this);
        }
    }

    public void goBack() {
        HistoryDetail back = history.getBackDetail();
    	this.detailName = back.getDetailName();
        entity = MetaModel.instance().findEntityByAddress(back.getAddress());
        htmlOutput.setText(buildHTMLTextFor(entity, back.getDetailName()));
        htmlOutput.setCaretPosition(0);
        myBrowserUI.update(this);
    }

    public void goForward() {
        HistoryDetail forward = history.getForwardDetail();
        this.detailName = forward.getDetailName();
        entity = MetaModel.instance().findEntityByAddress(forward.getAddress());
        htmlOutput.setText(buildHTMLTextFor(entity, forward.getDetailName()));
        htmlOutput.setCaretPosition(0);
        myBrowserUI.update(this);
    }

    public AbstractEntityInterface getCurrentEntity() {
        return entity;
    }

    public History history() {
        return history;
    }

    public Component getTopComponent() {
        return scrollPane;
    }

    private String buildHTMLTextFor(AbstractEntityInterface anEntity, String detailName) {
        String text;
        /////////////////////////////////////////////////////////
        // TODO is this a hack or what?
        if (anEntity.getName().compareTo("~root") == 0) {
            anEntity = ((GroupEntity) entity).getElementAt(0);
        }
        ////////////////////////////////////////////////////////
        if (anEntity.getEntityType().findDetails(detailName) != null) {
            text = anEntity.getEntityType().findDetails(detailName).compute(anEntity).toString();
            text = text.replaceAll("\n", "<br>");
            text = text.replaceAll(" ", "&nbsp;");
            text = text.replaceAll("\t", "&nbsp;&nbsp;&nbsp;&nbsp;");
        } else {
            text = "No \"" + detailName + "\" defined for \"" + anEntity.getEntityType().getName() + "\".";
        }
        
        return text;
    }

    public void hyperlinkUpdate(HyperlinkEvent e) {
        if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
            if (lastMouseEvent.getButton() != MouseEvent.BUTTON1)
                EntityMouseAction.instance().buildFor(MetaModel.instance().findEntityByAddress(e.getDescription()), lastMouseEvent);
            else {
                this.goTo(e.getDescription(), "Detail");
            }
        }
        if (e.getEventType() == HyperlinkEvent.EventType.ENTERED) {
            currentEntityDescription = e.getDescription();
            MainForm.instance().setStatusBarText(currentEntityDescription);
        }
        if (e.getEventType() == HyperlinkEvent.EventType.EXITED) {
            MainForm.instance().setStatusBarText(" ");
        }
    }

    public void mousePressed(MouseEvent e) {
        lastMouseEvent = e;
        EntityMouseAction.instance().buildFor(MetaModel.instance().findEntityByAddress(currentEntityDescription), lastMouseEvent);
    }

    public String getCurrentDetailText() {
    	return htmlOutput.getText();
    }
    private AbstractEntityInterface entity;
    private History history;
    private JScrollPane scrollPane;
    private JEditorPane htmlOutput;
    private MouseEvent lastMouseEvent;
    private BrowserUI myBrowserUI;
    String currentEntityDescription;

    {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
        $$$setupUI$$$();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     */
    private void $$$setupUI$$$() {
        final JScrollPane _1;
        _1 = new JScrollPane();
        scrollPane = _1;
    }

}

