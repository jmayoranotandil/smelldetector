package lrg.insider.gui.ui.views;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Vector;

import javax.swing.table.DefaultTableModel;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.insider.gui.ui.views.sort.CoolSortStrategy;
import lrg.insider.gui.ui.views.sort.SortStrategy;

/**
 * Created by IntelliJ IDEA.
 * User: cristic
 * Date: May 2, 2004
 * Time: 3:59:19 PM
 * To change this template use File | Settings | File Templates.
 */

public class InsiderViewTableModel extends DefaultTableModel {
    public InsiderViewTableModel() {
        // sortStrategy = new BubbleSortStrategy();
        sortStrategy = new CoolSortStrategy();
    }

    public boolean isCellEditable(int row, int col) {
        return false;
    }

    private void clear() {
        while (getRowCount() > 0)
            removeRow(0);
    }

    public void feedWithData(ArrayList columnNames, Iterator groupElementIterator) {
        this.clear();

        setColumnIdentifiers(columnNames.toArray());
        while (groupElementIterator.hasNext()) {
            AbstractEntityInterface currentEntity = (AbstractEntityInterface) groupElementIterator.next();
            if (currentEntity == null) continue;

            ArrayList rowData = new ArrayList(columnNames.size());
            Iterator it = columnNames.iterator();
            String columnName;
            int index;
            while (it.hasNext()) {
                columnName = it.next().toString();
                index = columnName.indexOf(".");
                if (index != -1) {//is an aggregated property
                    String groupName = columnName.substring(0, index);
                    String propertyName = columnName.substring(index + 1);
                    int indexOfProp = propertyName.indexOf("_");
                    GroupEntity crtGroup = currentEntity.getGroup(groupName + " group");
                    rowData.add(crtGroup.getProperty(propertyName.substring(indexOfProp + 1)).aggregate(propertyName.substring(0, indexOfProp)));
                } else
                    rowData.add(currentEntity.getProperty(columnName));
            }
            addRow(rowData.toArray());
        }

        setSorterRowCount(getRowCount());

        if (dataVector.size() < 2) return;

        try {
            Vector rowVector = (Vector) dataVector.elementAt(0);
            ResultEntity aResult = (ResultEntity) rowVector.elementAt(rowVector.size() - 1);
            if (aResult.getEntityType().getName().compareTo("string") == 0)
                this.sortByColumn(this.getColumnCount() - 1, InsiderViewTableModel.ASCENDING);
            else
                this.sortByColumn(this.getColumnCount() - 1, InsiderViewTableModel.DESCENDING);
        } catch (ClassCastException e) {
            this.sortByColumn(this.getColumnCount() - 1, InsiderViewTableModel.DESCENDING);
        }
    }


    public void sortByColumn(int column, boolean ascending) {
        ArrayList unsortedColumnValues = new ArrayList();
        for (int i = 0; i < this.getRowCount(); i++)
            unsortedColumnValues.add(super.getValueAt(i, column));

        sortStrategy.sort(unsortedColumnValues, indexMap, ascending);
    }

    public Object getValueAt(int row, int column) {
        return super.getValueAt(indexMap[row], column);
    }

    public int mapToGroupElementIndex(int selectedRow) {
        return indexMap[selectedRow];
    }

    private void setSorterRowCount(int rowCount) {
        indexMap = new int[rowCount];
        for (int i = 0; i < rowCount; i++)
            indexMap[i] = i;
    }

    public static final boolean ASCENDING = false;
    public static final boolean DESCENDING = true;

    private int[] indexMap;
    private SortStrategy sortStrategy;
}