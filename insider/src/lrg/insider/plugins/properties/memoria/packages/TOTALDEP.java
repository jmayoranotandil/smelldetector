package lrg.insider.plugins.properties.memoria.packages;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.properties.PropertyComputer;

public class TOTALDEP extends PropertyComputer {
    public TOTALDEP() {
        super("TOTALDEP", "Fanin plus Fanout", "package", "numerical");
    }
    
    public ResultEntity compute(AbstractEntityInterface aPackage) {
        double fanout = aPackage.getGroup("fanout class group").size();
        double fanin = aPackage.getGroup("fanin class group").size();
   	
    	return new ResultEntity(fanin+fanout);
    }
}
