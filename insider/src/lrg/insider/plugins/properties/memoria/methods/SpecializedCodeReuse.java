package lrg.insider.plugins.properties.memoria.methods;

import java.util.ArrayList;
import java.util.Iterator;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.filters.FilteringRule;
import lrg.common.abstractions.plugins.filters.composed.NotComposedFilteringRule;
import lrg.common.abstractions.plugins.properties.PropertyComputer;
import lrg.memoria.core.DataAbstraction;
import lrg.memoria.core.Method;
import lrg.memoria.core.ModelElementList;
import lrg.memoria.core.Parameter;

public class SpecializedCodeReuse extends PropertyComputer {

    public SpecializedCodeReuse() {
            super("SCR", "Specialized Code Reuse", "method", "numerical");
    }

    public ResultEntity compute(AbstractEntityInterface m) {

        if(!(m instanceof Method)) {
            return new ResultEntity(-1);
        }

        Method theMethod = (Method)m;
        GroupEntity allConcreteDescendants = theMethod.belongsTo("class").getGroup("all descendants").applyFilter(
                new NotComposedFilteringRule(
                            new FilteringRule("is abstract","is true", "class", null)));

        double reuse = 0;
        double total = allConcreteDescendants.size();

        if(total == 0) {
            return new ResultEntity(-1);
        }

        if(theMethod.isConstructor() || theMethod.isPrivate() || theMethod.isStatic() || theMethod.isProtected() || theMethod.isPackage()) {
            return new ResultEntity(-1);
        }

        if(theMethod.isAbstract()) {
            return new ResultEntity(0);
        }

        Iterator it = allConcreteDescendants.iterator();
        while(it.hasNext()) {
            DataAbstraction aClass = (DataAbstraction)it.next();
            Method my = findMyMethod(aClass,theMethod);
            if(my != theMethod && isSpecialization(my,theMethod)) {
                reuse++;
            }
        }
        return new ResultEntity(reuse / total);
        
    }

    private boolean isSpecialization(Method my, Method initial) {
            if(initial.getGroup("operations calling me").isInGroup(my)) {
                return true;
            }
            GroupEntity called = my.getGroup("operations called");
            Iterator it = called.iterator();
            while(it.hasNext()) {
                Method other = (Method)it.next();
                if(isOverriding(other,initial) && isSpecialization(other,initial)) {
                    return true;
                }
            }
            return false;
    }

    private Method findMyMethod(DataAbstraction aClass, Method initial) {
            GroupEntity methods = aClass.getGroup("method group");
            Iterator it = methods.iterator();
            while(it.hasNext()) {
                Method tmp = (Method)it.next();
                if(isOverriding(tmp,initial)) {
                    return tmp;
                }
            }
            GroupEntity superClass = aClass.getGroup("base classes").applyFilter(
                    new NotComposedFilteringRule(
                                new FilteringRule("is interface","is true", "class", null)));
            return findMyMethod((DataAbstraction)superClass.getElementAt(0),initial);
    }

    private boolean isOverriding(Method aMethod, Method ancestorMethod) {
            if (aMethod.getName().compareTo(ancestorMethod.getName()) != 0) return false;
            ModelElementList<Parameter> methodParameters = aMethod.getParameterList();
            ModelElementList<Parameter> ancestorsParameters = ancestorMethod.getParameterList();
            if (methodParameters.size() != ancestorsParameters.size()) return false;
            ArrayList paramTypes = new ArrayList();
            ArrayList paramTypesOfAncestor = new ArrayList();
            for (Iterator it = methodParameters.iterator(); it.hasNext(); paramTypes.add(((Parameter) it.next()).getType())) ;
            for (Iterator it = ancestorsParameters.iterator(); it.hasNext(); paramTypesOfAncestor.add(((Parameter) it.next()).getType())) ;
            return paramTypes.containsAll(paramTypesOfAncestor);
    }

}
