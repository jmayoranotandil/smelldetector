package lrg.insider.plugins.properties.memoria.methods;

import java.util.ArrayList;

import lrg.common.abstractions.entities.AbstractEntity;
import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.properties.PropertyComputer;

/**
 * Created by IntelliJ IDEA.
 * User: marinescu
 * Date: 06.06.2005
 * Time: 18:22:18
 * To change this template use File | Settings | File Templates.
 */

class CoolResultType {
   private AbstractEntity scopeClass;
   private AbstractEntity theMethod;
   private ArrayList instantiatedClasses;


   public CoolResultType(AbstractEntity cls, AbstractEntity mth, ArrayList instCls) {
       scopeClass = cls; theMethod = mth; instantiatedClasses = instCls;
   }

   public String toString() {
       String theString = "[" + scopeClass.getName() + ";" + theMethod.getName() + "; (";

       for(int i = 0;  i < instantiatedClasses.size(); i++)
           theString += ((AbstractEntity)instantiatedClasses.get(i)).getName() +",";
       theString += ")]";
       return theString;
   }
}
public class CalinsProperty extends PropertyComputer {
    public CalinsProperty() {
        super("CALIN", "Calins Propert", "method", "numerical");
    }

    public ResultEntity compute(AbstractEntityInterface theMethod) {
        GroupEntity instantiatedClasses = (GroupEntity)theMethod.uses("operations called").applyFilter("is constructor").belongsTo("class");

        return new ResultEntity(instantiatedClasses.size());
        /*CoolResultType theResult = new CoolResultType(theMethod.belongsTo("class"),
        (AbstractEntity)theMethod, instantiatedClasses.distinct().getElements());

        return new ResultEntity(theResult);
        */
    }
}
