package lrg.insider.plugins.properties.memoria.methods;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.properties.PropertyComputer;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 22.01.2005
 * Time: 14:55:57
 * To change this template use File | Settings | File Templates.
 */
public class CDISP extends PropertyComputer {
    public CDISP() {
        super("CDISP", "", new String[]{"method", "global function"}, "numerical");
    }

    public lrg.common.abstractions.entities.ResultEntity compute(AbstractEntityInterface anEntity) {
        double depExt = ((Double) anEntity.getProperty("CEXT").getValue()).doubleValue();
        double depIntens = ((Double) anEntity.getProperty("CINT").getValue()).doubleValue();

        if (depIntens == 0) return new ResultEntity(0);
        return new ResultEntity(depExt / depIntens);
    }
}
