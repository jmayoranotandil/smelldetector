package lrg.insider.plugins.properties.memoria.methods;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.properties.PropertyComputer;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 16.02.2005
 * Time: 17:51:29
 * To change this template use File | Settings | File Templates.
 */
class BCD extends PropertyComputer {
    public BCD() {
        super("BCD", "BaseClass Dependencies", "method", "numerical");
    }

    public ResultEntity compute(AbstractEntityInterface anEntity) {
        GroupEntity baseClasses = anEntity.belongsTo("class").uses("base classes").applyFilter("model class");
        GroupEntity protecteMethodsCalled = anEntity.uses("operations called").applyFilter("is protected").distinct();
        GroupEntity protecteAttributesUsed = anEntity.uses("variables accessed").applyFilter("is attribute").applyFilter("is protected").distinct();

        GroupEntity membersUsed = anEntity.uses("methods overriden").union(protecteMethodsCalled);
        membersUsed = membersUsed.union(protecteAttributesUsed);

        return new ResultEntity(((GroupEntity) membersUsed.belongsTo("class")).intersect(baseClasses).size());
    }

}
