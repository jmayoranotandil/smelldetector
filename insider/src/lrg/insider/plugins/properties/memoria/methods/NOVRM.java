package lrg.insider.plugins.properties.memoria.methods;

import lrg.common.abstractions.plugins.properties.PropertyComputer;
import lrg.insider.plugins.core.groups.memoria.uses.MethodOverrides;


public class NOVRM extends PropertyComputer {
    public NOVRM() {
        super("NOVRM", "Number of Overriding Methods", new String[]{"method", "global function"}, "numerical");
        basedOnGroup(new MethodOverrides());
    }
}
