package lrg.insider.plugins.properties.memoria.methods;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.properties.PropertyComputer;
import lrg.memoria.core.Body;

public class LOC extends PropertyComputer
{
    public LOC()
    {
        super("LOC", "Lines of Code", new String[]{"method", "global function"}, "numerical");
    }

    public lrg.common.abstractions.entities.ResultEntity compute(AbstractEntityInterface anEntity)
    {
        if (anEntity instanceof lrg.memoria.core.Function == false)
            return null;

        Body aBody = ((lrg.memoria.core.Function) anEntity).getBody();
        if (aBody == null) return new ResultEntity(0);

        return new ResultEntity(aBody.getNumberOfLines());
    }
}
