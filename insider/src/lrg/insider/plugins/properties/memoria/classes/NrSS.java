package lrg.insider.plugins.properties.memoria.classes;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.properties.PropertyComputer;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 25.02.2005
 * Time: 18:45:58
 * To change this template use File | Settings | File Templates.
 */
public class NrSS extends PropertyComputer {
    public NrSS() {
        super("NrSS", "Number of methods with ShotgunSurgery", "class", "numerical");
    }

    public ResultEntity compute(AbstractEntityInterface measuredClass) {
        GroupEntity aGroup = measuredClass.getGroup("method group");

        return new ResultEntity(aGroup.applyFilter("Shotgun Surgery").size());
    }
}
