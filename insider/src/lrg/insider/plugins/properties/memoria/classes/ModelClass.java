package lrg.insider.plugins.properties.memoria.classes;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.properties.PropertyComputer;
import lrg.memoria.core.DataAbstraction;
import lrg.memoria.core.Statute;

public class ModelClass extends PropertyComputer
{
    public ModelClass()
    {
        super("Model Class", "Is class part of the model", "class", "boolean");
    }

    public ResultEntity compute(AbstractEntityInterface anEntity)
    {
        if (anEntity instanceof lrg.memoria.core.Class)
        {
            DataAbstraction aClass = (DataAbstraction) anEntity;
            return new ResultEntity(aClass.getStatute()==Statute.NORMAL);
        }
        else
            return null;
    }
}
