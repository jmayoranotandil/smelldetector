package lrg.insider.plugins.properties.memoria.classes;

import lrg.common.abstractions.plugins.properties.PropertyComputer;
import lrg.insider.plugins.groups.memoria.DataProvidersForClass;

public class FDP extends PropertyComputer {
    public FDP() {
        super("FDP", "Access of Import Data", "class", "numerical");
        basedOnDistinctGroup(new DataProvidersForClass());
    }
}
