package lrg.insider.plugins.properties.memoria.classes;

import lrg.common.abstractions.plugins.properties.PropertyComputer;
import lrg.insider.plugins.groups.memoria.ExternalData;

public class ATFD extends PropertyComputer {
    public ATFD() {
        super("ATFD", "Access to Foreign Data", "class", "numerical");
        basedOnGroup(new ExternalData());
    }
}
