package lrg.insider.plugins.properties.memoria.classes;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.filters.FilteringRule;
import lrg.common.abstractions.plugins.filters.composed.NotComposedFilteringRule;
import lrg.common.abstractions.plugins.properties.PropertyComputer;
import lrg.insider.plugins.filters.memoria.methods.IsConstructor;
import lrg.insider.plugins.filters.memoria.methods.IsOverriden;

public class PNAS extends PropertyComputer {
    public PNAS() {
        super("PNAS", "Percentage Number of Added Services", "class", "numerical");
    }

    public ResultEntity compute(AbstractEntityInterface measuredClass) {
        GroupEntity allAncestors = measuredClass.uses("all ancestors").applyFilter("model class");

        if (allAncestors.size() == 0) return new ResultEntity(-1);

        FilteringRule notOverriden = new NotComposedFilteringRule(new IsOverriden());
        FilteringRule notConstructor = new NotComposedFilteringRule(new IsConstructor());

        GroupEntity myPublicMethods = measuredClass.contains("method group").applyFilter("is public").applyFilter(notConstructor);

        double countAllPublic = myPublicMethods.size();
        if (countAllPublic == 0) return new ResultEntity(0);

        double countNotOverriden = myPublicMethods.applyFilter(notOverriden).size();

        return new ResultEntity(countNotOverriden / countAllPublic);
    }
}
