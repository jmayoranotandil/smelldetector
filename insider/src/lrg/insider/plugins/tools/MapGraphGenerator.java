package lrg.insider.plugins.tools;

import java.io.FileOutputStream;
import java.io.PrintStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;

import lrg.common.abstractions.entities.AbstractEntity;
import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.plugins.filters.FilteringRule;
import lrg.common.abstractions.plugins.filters.composed.NotComposedFilteringRule;
import lrg.common.abstractions.plugins.tools.AbstractEntityTool;
import lrg.insider.plugins.filters.memoria.classes.IsInner;

/**
 * Created by IntelliJ IDEA.
 * User: marinescu
 * Date: 31.10.2006
 * Time: 22:16:54
 * To change this template use File | Settings | File Templates.
 */
public class MapGraphGenerator extends AbstractEntityTool {
    public MapGraphGenerator() {
        super("MapGraphGenerator","MapGraphGenerator", "system");
    }

    public void run(AbstractEntityInterface anEntity, Object theToolParameters) {
        ArrayList<String> params = (ArrayList<String>) theToolParameters;
        if (params.get(0).equals(""))
            System.out.println(buildMapGraph(anEntity));
        else {
            try {
                PrintStream out_stream = new PrintStream(new FileOutputStream(params.get(0)));
                out_stream.print(buildMapGraph(anEntity));
                out_stream.close();
            } catch (Exception ex) {
            }
        }
    }

    private GroupEntity typesOfAttributesAndParameters(AbstractEntityInterface crtClass) {
        GroupEntity typesOfAttributes = crtClass.getGroup("attribute group").getGroup("type of variable");
        GroupEntity typesOfParameters = crtClass.getGroup("parameter group").getGroup("type of variable");
        GroupEntity typesOfLocalVariables = crtClass.getGroup("local variable group").getGroup("type of variable");

        GroupEntity modelAncestors = crtClass.getGroup("all ancestors");
        return typesOfAttributes.union(typesOfParameters).union(typesOfLocalVariables).union(modelAncestors).distinct().
                applyFilter("model class").exclude((AbstractEntity)crtClass);
    }


    private GroupEntity usedClasses(AbstractEntityInterface crtClass) {
        GroupEntity calledClasses = (GroupEntity) crtClass.getGroup("operations called").distinct().belongsTo("class");
        GroupEntity accessedClasses = (GroupEntity) crtClass.getGroup("variables accessed").distinct().belongsTo("class");
        GroupEntity methodsOverriden = (GroupEntity) crtClass.getGroup("methods overriden").distinct().belongsTo("class");

        // return calledClasses.union(accessedClasses).union(methodsOverriden).applyFilter("model class");
        // return methodsOverriden.applyFilter("model class");
        return accessedClasses.applyFilter("model class");
    }

    private int howManyDependencies(AbstractEntity declaredClass, GroupEntity usedClasses) {
        int cnt = 0;
        for(Object crt : usedClasses.getElements()) {
            if(declaredClass == (AbstractEntity)crt) cnt++;
        }
        return cnt;
    }

    private String buildMapGraph(AbstractEntityInterface theSystem) {
        FilteringRule notInnerClass = new NotComposedFilteringRule(new IsInner());
        GroupEntity allModelClasses = theSystem.getGroup("class group").applyFilter("model class").applyFilter(notInnerClass);
        HashMap resultsTable = new HashMap();
        int cnt = 0;

        for (Object crt : allModelClasses.getElements()) {
            AbstractEntityInterface crtClass = (AbstractEntityInterface) crt;

            GroupEntity modelTypesOfAttributesAndParameters = typesOfAttributesAndParameters(crtClass);
            modelTypesOfAttributesAndParameters = modelTypesOfAttributesAndParameters.applyFilter("model class");
            for (Object crt2 : modelTypesOfAttributesAndParameters.getElements()) {
                AbstractEntity secondClass = (AbstractEntity) crt2;
                int resultValue = howManyDependencies(secondClass, usedClasses(crtClass));
                if(resultValue == 0) continue;
                Object partialResult = resultsTable.get(buildKeyName(secondClass, (AbstractEntity) crtClass));
                if(partialResult == null) {
                    System.out.println("@@@@ "+ resultValue + " " + buildKeyName(crtClass, secondClass));
                    resultsTable.put(buildKeyName(crtClass, secondClass), new Integer(resultValue));
                }
                else {
                    resultValue += (Integer) partialResult;
                    System.out.println("#### " + resultValue + " " + buildKeyName(secondClass, (AbstractEntity) crtClass));
                    resultsTable.put(buildKeyName(secondClass, (AbstractEntity) crtClass), new Integer(resultValue));
                }
            }
        }

       return printGraph(allModelClasses, resultsTable);

    }

    private String buildKeyName(AbstractEntityInterface crtClass, AbstractEntity secondClass) {
        return crtClass.getName()+"\t"+ crtClass.belongsTo("package").getName() +"\t" +
               secondClass.getName() + "\t" + secondClass.belongsTo("package").getName();
    }

    private String printGraph(GroupEntity allModelClasses, HashMap resultsTable) {
        String resultString = "NODES\t"+ allModelClasses.size()+"\n";
        DecimalFormat twoDecimals = new DecimalFormat("#0.00");

        for(Object crt : allModelClasses.getElements()) {
            AbstractEntityInterface crtClass = (AbstractEntityInterface) crt;
            double pr = ((Double)crtClass.getProperty("WMC").getValue()).doubleValue();
            resultString += crtClass.getName() + "\t" + crtClass.belongsTo("package").getName() + "\t" + twoDecimals.format(pr) + "\n";
        }
        resultString += "EDGES\t"+ resultsTable.keySet().size()+"\n";
        for(Object key : resultsTable.keySet())
            resultString += key.toString() + "\t" + resultsTable.get(key).toString() + "\n";

        return resultString;
    }

    public String getToolName() {
          return "MapGraphGenerator";
    }

    public ArrayList<String> getParameterList() {
        ArrayList<String> parList = new ArrayList<String>();
        parList.add("Filename");
        return parList;
    }

    public ArrayList<String> getParameterExplanations() {
        ArrayList<String> parList = new ArrayList<String>();
        parList.add("Filename");
        return parList;
    }
}
