package lrg.insider.plugins.tools.memoria.graphgen;



import org.jgrapht.EdgeFactory;
import org.jgrapht.graph.DirectedWeightedMultigraph;

import cdc.clusters.ExtendedWeightedEdge;
import cdc.clusters.Node;

public class SystemGraph<V, E> extends DirectedWeightedMultigraph<Node, ExtendedWeightedEdge> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8496579278148059150L;

	public SystemGraph(Class<? extends ExtendedWeightedEdge> arg0)
	{
		super(arg0);
	}

	public SystemGraph(EdgeFactory<Node, ExtendedWeightedEdge> arg0)
	{
		super(arg0);
	}
	
	public void exportAsGraphviz(String graphName, String dirName, String fileName)
	{
		GraphvizExporter.exportGraph(this, graphName, dirName, fileName);
	}
}
