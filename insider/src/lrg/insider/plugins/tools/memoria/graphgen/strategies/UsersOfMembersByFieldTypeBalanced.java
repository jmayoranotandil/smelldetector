package lrg.insider.plugins.tools.memoria.graphgen.strategies;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;

public class UsersOfMembersByFieldTypeBalanced extends UsersOfMembersByFieldType {
    public UsersOfMembersByFieldTypeBalanced(AbstractEntityInterface aei, String fieldType) {
        super(aei, fieldType);
    }

    public GroupEntity methodRelated() {
        /* BALANCED VERSION --- > codul de mai sus DISPARE SE INLOC. cu cel de mai jos */
       GroupEntity myInterestingMethods = getAttributesWithSpecificTypeName().isUsed("methods accessing variable").
                intersect(theCurrentEntity.contains("method group"));

        // ownCallers se califica "la baraj" ca le apeleaza pe alea calificate direct
       GroupEntity ownCallers = myInterestingMethods.isUsed("operations calling me").intersect(theCurrentEntity.contains("method group"));
       return myInterestingMethods.union(ownCallers).isUsed("operations calling me");
    }
}

