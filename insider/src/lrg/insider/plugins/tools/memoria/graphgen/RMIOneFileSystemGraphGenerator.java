package lrg.insider.plugins.tools.memoria.graphgen;

public class RMIOneFileSystemGraphGenerator extends RMISystemGraphGenerator {

	public RMIOneFileSystemGraphGenerator()
	{
		super("One-File RMIGraphGenerator", "Creates class dependency graphs as Graphviz dot format files", "system");
		// TODO Auto-generated constructor stub
	}

	@Override
	protected boolean useCommonFiles()
	{
		return true;
	}

}
