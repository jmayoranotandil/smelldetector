package lrg.insider.plugins.tools.memoria.graphgen.strategies;

import java.util.ArrayList;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.memoria.core.DataAbstraction;

public class AllUsedMembers extends InterestingMembersProvider {

	public AllUsedMembers(AbstractEntityInterface aei)
	{
		super(aei);
	}

	@Override
	public GroupEntity attributeRelated()
	{
		return new GroupEntity("empty group", new ArrayList());
		/*AbstractEntityInterface entity = theCurrentEntity;
		
		if(theCurrentEntity instanceof lrg.memoria.core.Class)
		{
			lrg.memoria.core.Class theClass = (lrg.memoria.core.Class)theCurrentEntity;
			
			if(theClass.isInterface())
				entity = theClass.getGroup("all descendants");
		}
		
		return entity.uses("variables accessed").exclude(theCurrentEntity.getGroup("attribute group"));
		*/
	}

	@Override
	public GroupEntity methodRelated()
	{
		AbstractEntityInterface entity = theCurrentEntity;
		
		if(theCurrentEntity instanceof lrg.memoria.core.Class)
		{
			DataAbstraction theClass = (DataAbstraction)theCurrentEntity;
			
			if(theClass.isInterface())
			{
				/*GroupEntity group = theClass.getGroup("all descendants");
			
				FilteringRule derivedOf = new FilteringRule("ancestor name", "==", "class", theClass);
				entity = group.applyFilter("model class").applyFilter(derivedOf);*/
				
				entity = theClass.getGroup("derived classes");
			}
		}
		
		return entity.uses("operations called").distinct().exclude(theCurrentEntity.contains("method group"));
	}

}
