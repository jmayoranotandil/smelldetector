package lrg.insider.plugins.tools.memoria.graphgen.rules;

import lrg.insider.plugins.tools.memoria.graphgen.strategies.AllUsedMembers;
import lrg.insider.plugins.tools.memoria.graphgen.strategies.UsedMembersLevelTwo;

public class RMICalledHierarcyRule extends AbstractGraphBuildRule {

	public RMICalledHierarcyRule()
	{
		super();
		
		strategies.add(new StrategyDefinition("orange", true, AllUsedMembers.class, null, 
				  UsedMembersLevelTwo.class, null));
	}
	
	@Override
	protected String fileNameSuffix()
	{
		return "-rmi-called-all";
	}
	
}
