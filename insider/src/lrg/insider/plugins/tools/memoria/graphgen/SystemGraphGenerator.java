package lrg.insider.plugins.tools.memoria.graphgen;

import java.util.ArrayList;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.plugins.filters.FilteringRule;
import lrg.insider.plugins.tools.memoria.graphgen.rules.AllBuildRule;
import lrg.insider.plugins.tools.memoria.graphgen.rules.BalancedFieldTypeBuildRule;
import lrg.insider.plugins.tools.memoria.graphgen.rules.FieldTypeBuildRule;

public class SystemGraphGenerator extends AbstractGraphGenerator {

	public SystemGraphGenerator()
	{
		super("GraphGenerator", "Creates class dependency graphs as GraphViz dot format files", "system");
	}

	public SystemGraphGenerator(String name, String description, String entity)
	{
		super(name, description, entity);
	}

	@Override
	protected GroupEntity getGroupOfRootClasses(AbstractEntityInterface entity, String attributeType)
	{
		// we are in the generator for System, therefore the entity must be the system
		lrg.memoria.core.System system = (lrg.memoria.core.System) entity;
		
		FilteringRule hasTypeName = new FilteringRule("Type", "contain", "attribute", attributeType);
        return system.contains("class group").applyFilter("model class").applyFilter(hasTypeName);
	}

	@Override
	protected boolean useCommonFiles()
	{
		return false;
	}

	@Override
	protected void defineBuildStrategies(Object toolParameters)
	{
		ArrayList<String> params = (ArrayList<String>)toolParameters;
		
		addRule(new AllBuildRule());
		addRule(new FieldTypeBuildRule(params.get(0)));// the type name is the first param to the tool
		addRule(new BalancedFieldTypeBuildRule(params.get(0)));// the type name is the first param to the tool
		
	}
}
