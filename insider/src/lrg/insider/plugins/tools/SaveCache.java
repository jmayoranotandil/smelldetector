package lrg.insider.plugins.tools;

import java.io.File;
import java.util.ArrayList;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.tools.AbstractEntityTool;

public class SaveCache extends AbstractEntityTool {

    public SaveCache() {
        super("SaveCache", "Saves to Cache", "system");
    }

    public void run(AbstractEntityInterface anEntity, Object theToolParameters) {
        ArrayList<String> params = (ArrayList<String>) theToolParameters;
        File aFile = new File(params.get(0));

        lrg.memoria.core.System.serializeToFile(aFile, (lrg.memoria.core.System)anEntity);
    }

    public String getToolName() {
        return "SaveCache";
    }

    public ArrayList<String> getParameterList() {
        ArrayList<String> parList = new ArrayList<String>();
        parList.add("Cache File Name (to Save)");
        return parList;
    }

    public ArrayList<String> getParameterExplanations() {
        ArrayList<String> parList = new ArrayList<String>();
        parList.add("Name of the cache file where the model will be saved");
        return parList;
    }

}

