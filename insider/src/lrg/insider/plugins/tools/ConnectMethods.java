package lrg.insider.plugins.tools;

import java.util.ArrayList;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.plugins.filters.FilteringRule;
import lrg.common.abstractions.plugins.tools.AbstractEntityTool;
import lrg.memoria.core.Body;
import lrg.memoria.core.Call;
import lrg.memoria.core.Method;

public class ConnectMethods extends AbstractEntityTool {

    public ConnectMethods() {
        super("ConnectMethods", "Connects two methods", "method");
    }

    public void run(AbstractEntityInterface anEntity, Object theToolParameters) {
        ArrayList<String> params = (ArrayList<String>) theToolParameters;
        Method firstMethod = (Method) anEntity;
        Body myBody = firstMethod.getBody();

        if(myBody == null) {
            System.out.println("Method has no body");
            return;
        }


        String methodName = params.get(0);
        String className = params.get(1);

        GroupEntity allClasses = firstMethod.belongsTo("system").getGroup("class group");

        FilteringRule aClassFilter = new FilteringRule("Name", "==", "class", className);
        FilteringRule aMethodFilter = new FilteringRule("Name", "==", "method", methodName);

        GroupEntity ourClasses = allClasses.applyFilter(aClassFilter);
        GroupEntity ourMethods = ourClasses.getGroup("method group").applyFilter(aMethodFilter);

        System.out.println("Cate metode: " + ourMethods.size());
        Method secondMethod = (Method) ourMethods.getElementAt(0);

        myBody.getCodeStripe().addCall(new Call(secondMethod, myBody.getCodeStripe()));
        // myBody.addCall(new Call(secondMethod, myBody.getCodeStripe()));        

        System.out.println("here");
        // TestEvaluationManager testUpdate
        // PresenceNote  PresenceNotesManager :: readPresenceNote ( Employee emp, int month, int year )
    }

    public String getToolName() {
        return "ConnectMethods";
    }

    public ArrayList<String> getParameterList() {
        ArrayList<String> parList = new ArrayList<String>();
        parList.add("Method to Connect to (only methodname)");
        parList.add("Class of Method (only classname)");
        return parList;
    }

    public ArrayList<String> getParameterExplanations() {
        ArrayList<String> parList = new ArrayList<String>();
        parList.add("Method to Connect to (only methodname)");
        parList.add("Class of Method (only classname)");
        return parList;
    }

}
