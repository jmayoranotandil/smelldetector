package lrg.insider.plugins.groups.memoria;

import java.util.ArrayList;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.plugins.groups.GroupBuilder;

public class FanoutClassGroup extends GroupBuilder {
    public FanoutClassGroup() {
        super("fanout class group", "", new String[]{"package", "subsystem"});
    }

    public ArrayList buildGroup(AbstractEntityInterface aPackageOrSubsystem) {
        GroupEntity ancestorClasses = aPackageOrSubsystem.uses("all ancestors");
        GroupEntity classesOfCalledOperations  = (GroupEntity)aPackageOrSubsystem.uses("operations called").belongsTo("class");
        GroupEntity classesOfAccessedVariables  = (GroupEntity)aPackageOrSubsystem.uses("variables accessed").belongsTo("class");


        GroupEntity usedClasses = ancestorClasses.union(classesOfCalledOperations).union(classesOfAccessedVariables).distinct().applyFilter("model class");
        
        return usedClasses.exclude(aPackageOrSubsystem.getGroup("model classes in package")).getElements();
    
    }

}
