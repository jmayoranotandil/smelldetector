package lrg.insider.plugins.groups.memoria;

import java.util.ArrayList;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.plugins.groups.GroupBuilder;

public class ClassesWithHierarchyDuplication extends GroupBuilder {
    public ClassesWithHierarchyDuplication() {
        super("same hierarchy class duplication", "", "class");
    }

    public ArrayList buildGroup(AbstractEntityInterface anEntity) {
        GroupEntity siblingClasses = (GroupEntity) anEntity.contains("method group").getGroup("same hierarchy duplicated methods").belongsTo("class");

        return siblingClasses.distinct().getElements();
    }
}
