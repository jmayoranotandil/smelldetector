package lrg.insider.plugins.groups.memoria;

import java.util.ArrayList;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.plugins.groups.GroupBuilder;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 25.01.2005
 * Time: 19:46:35
 * To change this template use File | Settings | File Templates.
 */
public class HeavyServiceProviders extends GroupBuilder {
    public HeavyServiceProviders() {
        super("heavy service providers", "", "system");
    }

    public ArrayList buildGroup(AbstractEntityInterface aSystem) {
        GroupEntity heavyCoupledMethods = aSystem.getGroup("method group").applyFilter("model function").applyFilter("Heavy Coupling");

        return heavyCoupledMethods.getGroup("external service providers").distinct().getElements();
    }

}

