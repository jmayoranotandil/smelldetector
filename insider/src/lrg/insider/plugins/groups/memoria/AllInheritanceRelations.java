package lrg.insider.plugins.groups.memoria;

import java.util.ArrayList;
import java.util.Iterator;

import lrg.common.abstractions.entities.AbstractEntity;
import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.plugins.groups.GroupBuilder;

public class AllInheritanceRelations extends GroupBuilder {

    public AllInheritanceRelations() {
        super("all inheritance relations","All inheritance relation in the system",new String[]{"system","package root","package"});
    }

    public ArrayList buildGroup(AbstractEntityInterface aSystem) {
        ArrayList resultGroup = new ArrayList();
        GroupEntity allClasses = aSystem.getGroup("class group").applyFilter("model class");
        
        for (Iterator iterator = allClasses.iterator(); iterator.hasNext();) {
			AbstractEntity crtClass = (AbstractEntity) iterator.next();
			GroupEntity derivedClasses = crtClass.getGroup("derived classes");
			for (Iterator iterator2 = derivedClasses.iterator(); iterator2.hasNext();)
				resultGroup.add(new InheritanceRelation(crtClass, (AbstractEntity) iterator2.next()));
		}
        return resultGroup;
    }

}
