package lrg.insider.plugins.groups.memoria;

import java.util.ArrayList;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.groups.GroupBuilder;
import lrg.memoria.core.Component;
import lrg.memoria.core.ModelElementList;

/**
 * Created by IntelliJ IDEA.
 * User: marinescu
 * Date: 22.11.2006
 * Time: 13:17:27
 * To change this template use File | Settings | File Templates.
 */
public class SystemHasComponents extends GroupBuilder {

    public SystemHasComponents()
    {
        super("component group", "", "system");
    }

    public ArrayList buildGroup(AbstractEntityInterface anEntity)
    {
        if (anEntity instanceof lrg.memoria.core.System == false)
            return new ArrayList<Component>();

        return (ModelElementList<Component>)anEntity.getAnnotation("allComponents");
    }

}