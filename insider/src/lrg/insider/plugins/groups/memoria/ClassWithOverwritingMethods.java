package lrg.insider.plugins.groups.memoria;

import java.util.ArrayList;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.plugins.filters.FilteringRule;
import lrg.common.abstractions.plugins.filters.composed.AndComposedFilteringRule;
import lrg.common.abstractions.plugins.filters.composed.NotComposedFilteringRule;
import lrg.common.abstractions.plugins.groups.GroupBuilder;
import lrg.insider.plugins.filters.memoria.methods.IsOverriden;
import lrg.insider.plugins.filters.memoria.methods.IsSpecialization;

public class ClassWithOverwritingMethods extends GroupBuilder {
    public ClassWithOverwritingMethods() {
        super("overwriting methods", "", "class");
    }

    public ArrayList buildGroup(AbstractEntityInterface derivedClass) {
        FilteringRule notSpecialization = new NotComposedFilteringRule(new IsSpecialization());
        notSpecialization = new AndComposedFilteringRule(notSpecialization, new IsOverriden());

        FilteringRule notAbstract = new NotComposedFilteringRule(new lrg.insider.plugins.filters.memoria.methods.IsAbstract());

        GroupEntity baseclassMethods = derivedClass.uses("base classes").applyFilter("model class").
                contains("method group").applyFilter(notAbstract);

        if (baseclassMethods.size() < 1) return new ArrayList();

        GroupEntity notSpecializingMethods = derivedClass.getGroup("method group").applyFilter(notSpecialization);

        return notSpecializingMethods.uses("methods overriden").intersect(baseclassMethods).distinct().getElements();
    }
}
