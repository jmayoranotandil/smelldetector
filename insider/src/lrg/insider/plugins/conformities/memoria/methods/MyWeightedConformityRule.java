package lrg.insider.plugins.conformities.memoria.methods;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.Descriptor;
import lrg.common.abstractions.plugins.conformities.ConformityRule;
import lrg.common.abstractions.plugins.conformities.composed.ConformityRuleExpression;
import lrg.common.abstractions.plugins.conformities.operators.AddOperator;
import lrg.common.abstractions.plugins.conformities.operators.MaxOperator;
import lrg.common.abstractions.plugins.conformities.weights.Weight;

/**
 * Created by Horia Radu. User: horia Date: 21.09.2010 Time: 18:07:08 To change
 * this template use File | Settings | File Templates.
 */
public class MyWeightedConformityRule extends ConformityRule {
	public MyWeightedConformityRule() {
		super(new Descriptor("MyWeightedConformity", "method"));
	}

	public Double applyConformity(AbstractEntityInterface anEntity) {
		ConformityRule highLOC = new ConformityRule("LOC", ">", "method", 80);
		ConformityRule highCYCLO = new ConformityRule("CYCLO", ">", "method", 20);
		ConformityRule highFANIN = new ConformityRule("FANIN", ">", "method", 5);

		/*
		 * (high highLOC & medium highCYCLO) | very_high highFANIN
		 */
		
		highLOC.setWeight(Weight.HIGH.value());
		highCYCLO.setWeight(Weight.MEDIUM.value());
		highFANIN.setWeight(Weight.VERY_HIGH.value());
		return new ConformityRuleExpression(new MaxOperator().apply(new AddOperator().apply(highLOC, highCYCLO), highFANIN)).applyConformity(anEntity);
	}
}