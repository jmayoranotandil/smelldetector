package lrg.insider.plugins.filters.memoria.classes;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.Descriptor;
import lrg.common.abstractions.plugins.filters.FilteringRule;
import lrg.memoria.core.DataAbstraction;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 05.11.2004
 * Time: 18:15:08
 * To change this template use File | Settings | File Templates.
 */
public class IsInterface extends FilteringRule {
    public IsInterface() {
        super(new Descriptor("is interface", "class"));
    }

    public boolean applyFilter(AbstractEntityInterface anEntity) {
        if (anEntity instanceof lrg.memoria.core.Class == false) return false;

        return ((DataAbstraction) anEntity).isInterface();
    }
}
