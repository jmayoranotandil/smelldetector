package lrg.insider.plugins.filters.memoria.classes;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.plugins.Descriptor;
import lrg.common.abstractions.plugins.filters.FilteringRule;
import lrg.common.abstractions.plugins.filters.composed.NotComposedFilteringRule;
import lrg.memoria.core.DataAbstraction;
import lrg.memoria.core.Statute;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 14.01.2005
 * Time: 09:53:57
 * To change this template use File | Settings | File Templates.
 */
public class IsRootClass extends FilteringRule {
    public IsRootClass() {
        super(new Descriptor("is root-class", "class"));
    }

    public boolean applyFilter(AbstractEntityInterface anEntity) {
        if (anEntity instanceof lrg.memoria.core.Class == false) return false;

        String address = (String) anEntity.getProperty("Address").getValue();
        if (address.indexOf("$") >= 0) return false;

        FilteringRule notInterface = new NotComposedFilteringRule(new IsInterface());

        if (((DataAbstraction) anEntity).isInterface()) return false;
        if (((DataAbstraction) anEntity).getStatute() != Statute.NORMAL) return false;

        GroupEntity classAncestors = anEntity.uses("base classes");
        GroupEntity modelClassAncestors = classAncestors.applyFilter("model class").applyFilter(notInterface);
        if (modelClassAncestors.size() != 0) return false;
        return true;
    }
}
