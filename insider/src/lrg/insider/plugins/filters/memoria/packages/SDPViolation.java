package lrg.insider.plugins.filters.memoria.packages;

import java.util.ArrayList;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.plugins.Descriptor;
import lrg.common.abstractions.plugins.filters.FilteringRule;

public class SDPViolation extends FilteringRule {
    public SDPViolation() {
        super(new Descriptor("SDP Violation", "package"));
    }
    
    public static ArrayList<AbstractEntityInterface> SDPBreakers(AbstractEntityInterface aPackage) {
    	
       	ArrayList<AbstractEntityInterface> usedPacakges = ((GroupEntity)aPackage.getGroup("fanout class group").belongsTo("package")).distinct().getElements();
    	double referenceIFValue = (Double)aPackage.getProperty("IF").getValue();
    	ArrayList<AbstractEntityInterface> sdpBreakers = new ArrayList<AbstractEntityInterface>();
    	
    	for(AbstractEntityInterface crtPackage : usedPacakges) {
        	double crtIFValue = (Double)crtPackage.getProperty("IF").getValue();    		
    		if(referenceIFValue < crtIFValue*0.9) sdpBreakers.add(crtPackage); 
    	}
    	return sdpBreakers;
    }
    
    public boolean applyFilter(AbstractEntityInterface aPackage) {    	    
    	return SDPBreakers(aPackage).size() > 0;
    }
}
