package lrg.insider.plugins.filters.memoria.methods;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.Descriptor;
import lrg.common.abstractions.plugins.filters.FilteringRule;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 23.02.2005
 * Time: 20:11:56
 * To change this template use File | Settings | File Templates.
 */
public class IsStatic extends FilteringRule {
    public IsStatic() {
        super(new Descriptor("is static", "method"));
    }

    public boolean applyFilter(AbstractEntityInterface anEntity) {
        if (anEntity instanceof lrg.memoria.core.Method == false) return false;

        return ((lrg.memoria.core.Method) anEntity).isStatic();
    }
}