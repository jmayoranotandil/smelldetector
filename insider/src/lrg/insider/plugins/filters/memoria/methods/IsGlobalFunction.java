package lrg.insider.plugins.filters.memoria.methods;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.Descriptor;
import lrg.common.abstractions.plugins.filters.FilteringRule;

public class IsGlobalFunction extends FilteringRule {

    public IsGlobalFunction() {
        super(new Descriptor("is global", new String[]{"method", "global function"}));
    }

    public boolean applyFilter(AbstractEntityInterface anEntity) {

        if (anEntity instanceof lrg.memoria.core.GlobalFunction) return true;
        return false;

    }
}
