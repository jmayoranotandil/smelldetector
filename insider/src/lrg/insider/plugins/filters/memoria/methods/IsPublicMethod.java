package lrg.insider.plugins.filters.memoria.methods;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.Descriptor;
import lrg.common.abstractions.plugins.filters.FilteringRule;

public class IsPublicMethod extends FilteringRule
{
    public IsPublicMethod()
    {
        super(new Descriptor("is public", "method"));
    }

    public boolean applyFilter(AbstractEntityInterface anEntity)
    {
        if (anEntity instanceof lrg.memoria.core.Method == false) return false;

        return ((lrg.memoria.core.Method) anEntity).isPublic();
    }
}
