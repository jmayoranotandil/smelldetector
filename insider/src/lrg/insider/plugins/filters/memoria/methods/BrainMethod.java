package lrg.insider.plugins.filters.memoria.methods;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.Descriptor;
import lrg.common.abstractions.plugins.filters.FilteringRule;
import lrg.common.abstractions.plugins.filters.composed.AndComposedFilteringRule;
import lrg.insider.plugins.filters.Threshold;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 31.01.2005
 * Time: 18:07:08
 * To change this template use File | Settings | File Templates.
 */
public class BrainMethod extends FilteringRule {
    public BrainMethod() {
        super(new Descriptor("Brain Method", "method"));
    }

    public boolean applyFilter(AbstractEntityInterface anEntity) {
        FilteringRule highLOC = new FilteringRule("LOC", ">", "method", Threshold.LOC_VERYHIGH);
        FilteringRule highCYCLO = new FilteringRule("CYCLO", ">=", "method", Threshold.MANY);
        FilteringRule highNESTING = new FilteringRule("MAXNESTING", ">=", "method", Threshold.DEEP);
        FilteringRule manyVariablesAccessed = new FilteringRule("NOAV", ">=", "method", Threshold.SMemCap);

        FilteringRule longAndComplex = new AndComposedFilteringRule(highNESTING,
                new AndComposedFilteringRule(highCYCLO, highLOC));

        return (longAndComplex.applyFilter(anEntity) &&
                manyVariablesAccessed.applyFilter(anEntity));
    }
}