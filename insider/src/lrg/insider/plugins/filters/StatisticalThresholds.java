package lrg.insider.plugins.filters;

/**
 * Created by IntelliJ IDEA.
 * User: marinescu
 * Date: 20.04.2005
 * Time: 13:36:51
 * To change this template use File | Settings | File Templates.
 */
public class StatisticalThresholds {
    public static final double CYCLO_LOC_LOW = 0.17;
    public static final double CYCLO_LOC_AVG = 0.21;
    public static final double CYCLO_LOC_HIGH = 0.25;

    public static final double LOC_NOM_LOW = 6;
    public static final double LOC_NOM_AVG = 9;
    public static final double LOC_NOM_HIGH = 13;

    public static final double NOM_NOC_LOW = 4;
    public static final double NOM_NOC_AVG = 7;
    public static final double NOM_NOC_HIGH = 9;

    public static final double NOC_NOP_LOW = 6;
    public static final double NOC_NOP_AVG = 15;
    public static final double NOC_NOP_HIGH = 24;

    public static final double CALL_NOM_LOW = 2;
    public static final double CALL_NOM_AVG = 2.6;
    public static final double CALL_NOM_HIGH = 3.2;

    public static final double FOUT_CALL_LOW = 0.57;
    public static final double FOUT_CALL_AVG = 0.63;
    public static final double FOUT_CALL_HIGH = 0.69;

    public static final double NDD_LOW = 0.20;
    public static final double NDD_AVG = 0.41;
    public static final double NDD_HIGH = 0.6;

    public static final double HIT_LOW = 0.1;
    public static final double HIT_AVG = 0.21;
    public static final double HIT_HIGH = 0.3;
}
