package lrg.insider.plugins.visualizations;

import java.awt.Color;
import java.util.ArrayList;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.EntityType;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.insider.plugins.filters.memoria.methods.IsAccessor;
import lrg.insider.plugins.filters.memoria.methods.IsConstructor;
import lrg.insider.util.Visualization;
import lrg.jMondrian.commands.AbstractFigureDescriptionCommand;
import lrg.jMondrian.commands.AbstractNumericalCommand;
import lrg.jMondrian.figures.Figure;
import lrg.jMondrian.layouts.FlowLayout;
import lrg.jMondrian.painters.LineEdgePainter;
import lrg.jMondrian.painters.RectangleNodePainter;
import lrg.jMondrian.util.CommandColor;


public class ClassBlueprintOperations {
	private GroupEntity allLayers = new GroupEntity("class layer", new EntityType(""));
    private GroupEntity initLayer = new GroupEntity("Initialization",new EntityType(""));
    private GroupEntity interfaceLayer = new GroupEntity("Interface",new EntityType(""));
    private GroupEntity implementationLayer = new GroupEntity("Implementation",new EntityType(""));
    private GroupEntity accessorLayer = new GroupEntity("Accessors",new EntityType(""));
    private GroupEntity attributeLayer = new GroupEntity("Attributes",new EntityType(""));
	private Figure entireBlueprintFigure;

	
    Figure addMethodCallsEdges(Figure figure, GroupEntity allMethods){
        ArrayList<Call> edges = new ArrayList<Call>();
        ArrayList<AbstractEntityInterface> methodsList = allMethods.getElements();
        
        for (AbstractEntityInterface crtMethod : methodsList) {
        	ArrayList<AbstractEntityInterface> methodAccessing = crtMethod.getGroup("operations called").intersect(allMethods).distinct().getElements();
        	for (AbstractEntityInterface calledMethod : methodAccessing) edges.add(new Call(crtMethod, calledMethod));
		}
        
        figure.edgesUsing(edges, new LineEdgePainter(
				Visualization.entityCommand("getCallee"),   
				Visualization.entityCommand("getCaller")
            ).color( new AbstractNumericalCommand(){ public double execute(){return Color.blue.getRGB();} }
        ));
        return figure;     
    }

	Figure addAttributeAccessEdges(Figure figure,  final GroupEntity allMethods,  final GroupEntity allAttributes) {
       ArrayList<Access> edges = new ArrayList<Access>();
       ArrayList<AbstractEntityInterface> methodsList = allMethods.getElements();
       
       for (AbstractEntityInterface aMethod : methodsList) {
       	ArrayList<AbstractEntityInterface> fieldsAccessed = aMethod.getGroup("fields accessed").intersect(allAttributes).distinct().getElements();
       	for (AbstractEntityInterface crtAttribute : fieldsAccessed) edges.add(new Access(aMethod, crtAttribute));
		}
       

       figure.edgesUsing(edges, new LineEdgePainter(
				Visualization.entityCommand("getMethod"),   
				Visualization.entityCommand("getAttribute")
           ).color( new AbstractNumericalCommand(){ public double execute(){return Color.cyan.getRGB();} }
       ));
       return figure;

	}
	
	private boolean isInterfaceMethod(AbstractEntityInterface crtMethod, GroupEntity methods) {
		return crtMethod.getGroup("operations calling me").intersect(methods).size() == 0;
	}
	
	private ArrayList<AbstractEntityInterface> createClassBluePrintNodes(final GroupEntity allMethods, final GroupEntity allAttributes) {
		ArrayList<AbstractEntityInterface> methodEntities = allMethods.getElements();
	    ArrayList<AbstractEntityInterface> classBlueprint = new ArrayList<AbstractEntityInterface>();
        
	    for(AbstractEntityInterface crtMethod : methodEntities) {
	    	if(new IsConstructor().applyFilter(crtMethod) || crtMethod.getName().contains("init")) initLayer.add(crtMethod);
	    	else if(new IsAccessor().applyFilter(crtMethod)) accessorLayer.add(crtMethod);
	    	else if(isInterfaceMethod(crtMethod, allMethods)) interfaceLayer.add(crtMethod);
	    	else implementationLayer.add(crtMethod);
	    }
        
        attributeLayer.addAll(allAttributes);

        allLayers.add(initLayer);
        allLayers.add(interfaceLayer);
        allLayers.add(implementationLayer);
        allLayers.add(accessorLayer);
        allLayers.add(attributeLayer);
        classBlueprint.add(allLayers);
        return classBlueprint;
	}

	public void buildFigure(final AbstractEntityInterface entity) {
		final GroupEntity allMethods = entity.getGroup("method group");
		final GroupEntity allAttributes = entity.getGroup("attribute group");
		entireBlueprintFigure = new Figure();
		ArrayList<AbstractEntityInterface> classBluePrintNodes = createClassBluePrintNodes(allMethods, allAttributes);
		entireBlueprintFigure.nodesUsingForEach(
				classBluePrintNodes, 
				new RectangleNodePainter(true)
				.frameColor(CommandColor.LIGHT_GRAY),
				new AbstractFigureDescriptionCommand(){
					public Figure describe(){
						Figure figure = new Figure();
						figure.nodesUsingForEach(
								((GroupEntity) receiver).getElements(), 
								new RectangleNodePainter(true).frameColor(CommandColor.LIGHT_GRAY),
								new BlueprintFigureDescriptionCommand(entity));
						figure = addAttributeAccessEdges(figure, allMethods, allAttributes);
						figure = addMethodCallsEdges(figure, allMethods);
						figure.layout(new FlowLayout());
						return figure;
					}
				}
		);
		entireBlueprintFigure.layout(new FlowLayout());
	}

	public Figure getFigure(){
		return entireBlueprintFigure;
	}

}

