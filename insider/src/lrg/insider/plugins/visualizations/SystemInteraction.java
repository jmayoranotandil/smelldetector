package lrg.insider.plugins.visualizations;

import java.util.ArrayList;
import java.util.Iterator;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.plugins.filters.FilteringRule;
import lrg.common.abstractions.plugins.filters.composed.NotComposedFilteringRule;
import lrg.common.abstractions.plugins.visualization.AbstractVisualization;
import lrg.insider.util.Visualization;
import lrg.jMondrian.commands.AbstractFigureDescriptionCommand;
import lrg.jMondrian.figures.Figure;
import lrg.jMondrian.layouts.FlowLayout;
import lrg.jMondrian.layouts.TreeLayout;
import lrg.jMondrian.painters.LineEdgePainter;
import lrg.jMondrian.painters.RectangleNodePainter;
import lrg.jMondrian.util.CommandColor;
import lrg.jMondrian.view.ViewRenderer;
import lrg.memoria.core.Call;
import lrg.memoria.core.InheritanceRelation;
import lrg.memoria.core.Method;

public class SystemInteraction extends AbstractVisualization {

    public SystemInteraction() {
        super("System Interaction", "System Interaction", "system");
    }

    public void view(AbstractEntityInterface entity) {

        GroupEntity allClasses = entity.getGroup("class group").applyFilter("model class").
                applyFilter(new NotComposedFilteringRule(new FilteringRule("is interface","is true","class")));
        GroupEntity allEdges = entity.getGroup("all inheritance relations");
        ArrayList edges = new ArrayList();
        Iterator<InheritanceRelation> it = allEdges.iterator();
        while(it.hasNext()) {
            InheritanceRelation rel = it.next();
            if(allClasses.isInGroup(rel.getSubClass()) && allClasses.isInGroup(rel.getSuperClass())) {
                edges.add(rel);
            }
        }
        GroupEntity allMethods = allClasses.getGroup("method group");
        ArrayList calls = new ArrayList();
        Iterator it1 = allMethods.iterator();
        while(it1.hasNext()) {
            ArrayList<Call> tmp = ((Method)it1.next()).getCallList();
            for(int i = 0; i < tmp.size(); i++) {
                if(allMethods.getElements().contains(tmp.get(i).getBody().getScope()) && allMethods.getElements().contains(tmp.get(i).getFunction())) {
                    calls.add(tmp.get(i));
                }
            }
        }

        Figure f = new Figure();
        f.nodesUsingForEach(allClasses.getElements(), new RectangleNodePainter(true), new AbstractFigureDescriptionCommand() {

            public Figure describe() {
                Figure fig = new Figure();
                fig.nodesUsing(((AbstractEntityInterface)receiver).getGroup("method group").getElements(), new RectangleNodePainter(10,10,true));
                fig.layout(new FlowLayout(50));
                return fig;
            }

        });
        f.edgesUsing(edges, new LineEdgePainter(Visualization.entityCommand("getSubClass"),Visualization.entityCommand("getSuperClass")));
        f.edgesUsing(calls, new LineEdgePainter(Visualization.indirectionCommand("getScope",Visualization.entityCommand("getBody")),Visualization.entityCommand("getFunction"),true).color(CommandColor.RED));
        f.layout(new TreeLayout(20,50));

        ViewRenderer r = new ViewRenderer("SystemInteraction");
        f.renderOn(r);
        r.open();

    }

}
