package lrg.insider.plugins.core.filters.memoria;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.Descriptor;
import lrg.common.abstractions.plugins.filters.FilteringRule;
import lrg.memoria.core.DataAbstraction;
import lrg.memoria.core.Statute;

public class ModelClassFilter extends FilteringRule
{
    public ModelClassFilter()
    {
        super(new Descriptor("model class", "class"));
    }

    public boolean applyFilter(AbstractEntityInterface anEntity)
    {
        if (anEntity instanceof lrg.memoria.core.Class == false) return false;

        return ((DataAbstraction) anEntity).getStatute() == Statute.NORMAL;
    }
}

