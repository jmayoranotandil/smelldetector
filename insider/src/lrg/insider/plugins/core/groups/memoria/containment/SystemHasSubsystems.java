package lrg.insider.plugins.core.groups.memoria.containment;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.groups.GroupBuilder;
import lrg.memoria.core.ModelElementList;
import lrg.memoria.core.Subsystem;

public class SystemHasSubsystems extends GroupBuilder {
    public SystemHasSubsystems() {
        super("subsystem group", "", "system");
    }

    public ModelElementList<Subsystem> buildGroup(AbstractEntityInterface theSystem) {
    	return ((lrg.memoria.core.System)theSystem).getSubsystems();
    }
}