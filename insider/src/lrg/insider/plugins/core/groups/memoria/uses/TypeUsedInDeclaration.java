package lrg.insider.plugins.core.groups.memoria.uses;

import java.util.ArrayList;
import java.util.Iterator;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.plugins.groups.GroupBuilder;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 14.10.2004
 * Time: 18:52:37
 * To change this template use File | Settings | File Templates.
 */
public class TypeUsedInDeclaration extends GroupBuilder {

    public TypeUsedInDeclaration() {
        super("group of variables of this type", "", "class");
    }

    public ArrayList buildGroup(AbstractEntityInterface anEntity) {
        ArrayList resultList = new ArrayList();

        if (anEntity instanceof lrg.memoria.core.Type == false)
            return resultList;

        lrg.memoria.core.Type aType = (lrg.memoria.core.Type) anEntity;
        if (aType.getScope() instanceof lrg.memoria.core.Package == false)
            return resultList;

        lrg.memoria.core.System theSystem = ((lrg.memoria.core.Package) aType.getScope()).getSystem();

        GroupEntity aGroup = theSystem.getGroup("attribute group").union(
                             theSystem.getGroup("global variable group")).union(
                             theSystem.getGroup("parameter group")).union(
                             theSystem.getGroup("local variable group"));

        Iterator it = aGroup.iterator();

        while (it.hasNext()) {
            lrg.memoria.core.Variable aVariable = (lrg.memoria.core.Variable) it.next();
            if (aVariable.getType() == aType) resultList.add(aVariable);
        }

        return resultList;
    }
}
