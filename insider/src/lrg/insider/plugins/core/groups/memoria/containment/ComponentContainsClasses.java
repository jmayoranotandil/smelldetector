package lrg.insider.plugins.core.groups.memoria.containment;

import java.util.ArrayList;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.groups.GroupBuilder;

/**
 * Created by IntelliJ IDEA.
 * User: marinescu
 * Date: 22.11.2006
 * Time: 13:04:09
 * To change this template use File | Settings | File Templates.
 */
public class ComponentContainsClasses  extends GroupBuilder
{
    public ComponentContainsClasses()
    {
        super("class group", "", "component");
    }

    public ArrayList buildGroup(AbstractEntityInterface anEntity)
    {
        if (anEntity instanceof lrg.memoria.core.Component == false)
            return new ArrayList();

        return ((lrg.memoria.core.Component) anEntity).getScopedElements();
    }
}
