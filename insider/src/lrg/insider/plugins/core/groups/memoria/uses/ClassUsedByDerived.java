package lrg.insider.plugins.core.groups.memoria.uses;

import java.util.ArrayList;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.groups.GroupBuilder;
import lrg.memoria.core.DataAbstraction;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 15.10.2004
 * Time: 19:38:25
 * To change this template use File | Settings | File Templates.
 */
public class ClassUsedByDerived extends GroupBuilder {
    public ClassUsedByDerived() {
        super("derived classes", "", "class");
    }

    public ArrayList buildGroup(AbstractEntityInterface anEntity) {
        ArrayList resultList = new ArrayList();
        if (anEntity instanceof lrg.memoria.core.Class == false)
            return resultList;

        return ((DataAbstraction) anEntity).getDescendants();
    }
}
