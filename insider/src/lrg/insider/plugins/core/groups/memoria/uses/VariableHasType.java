package lrg.insider.plugins.core.groups.memoria.uses;

import java.util.ArrayList;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.groups.GroupBuilder;
import lrg.memoria.core.Variable;

public class VariableHasType extends GroupBuilder {
    public VariableHasType() {
        super("type of variable", "", new String[]{"global variable", "attribute", "local variable", "parameter"});
    }

    public ArrayList buildGroup(AbstractEntityInterface aVariable) {
        ArrayList resultList = new ArrayList();
        if (aVariable instanceof Variable == false) return resultList;
        resultList.add(((Variable) aVariable).getType());

        return resultList;
    }
}
