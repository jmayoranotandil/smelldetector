package lrg.insider.plugins.core.properties.memoria.annotation;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.insider.plugins.core.properties.AbstractDetail;

/**
 * Created by IntelliJ IDEA.
 * User: marinescu
 * Date: 22.11.2006
 * Time: 12:57:34
 * To change this template use File | Settings | File Templates.
 */
public class Detail extends AbstractDetail {
    public Detail() {
        super("Detail", "Constructs a detailed HTML String to be shown in the browser.", "annotation", "string");
    }

    public ResultEntity compute(AbstractEntityInterface anAnnotation) {
		return new ResultEntity("Annotation " + anAnnotation.getName());
    }

}
