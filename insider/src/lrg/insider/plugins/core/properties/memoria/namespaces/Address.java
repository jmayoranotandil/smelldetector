package lrg.insider.plugins.core.properties.memoria.namespaces;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.properties.PropertyComputer;

public class Address extends PropertyComputer {
    public Address() {
        super("Address", "The address of the namespace", "namespace", "string");
    }

    public ResultEntity compute(AbstractEntityInterface anEntity) {
        if (anEntity instanceof lrg.memoria.core.Namespace == false)
            return null;

        return new ResultEntity(lrg.insider.metamodel.Address.buildFor((lrg.memoria.core.Namespace) anEntity));
    }
}
