package lrg.insider.plugins.core.properties.memoria.component;

import java.util.ArrayList;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.insider.plugins.core.properties.AbstractDetail;
import lrg.insider.plugins.tools.memoria.graphgen.AnnotationConstants;
import lrg.insider.plugins.tools.memoria.graphgen.SystemGraph;
import lrg.memoria.core.Component;
import lrg.memoria.core.DataAbstraction;
import lrg.memoria.core.System;
import cdc.clusters.ExtendedWeightedEdge;
import cdc.clusters.Node;

/**
 * Created by IntelliJ IDEA.
 * User: marinescu
 * Date: 22.11.2006
 * Time: 12:57:34
 * To change this template use File | Settings | File Templates.
 */
public class Detail extends AbstractDetail {
    public Detail() {
        super("Detail", "Constructs a detailed HTML String to be shown in the browser.", "component", "string");
    }

    public ResultEntity compute(AbstractEntityInterface aComponent) {
        String text = "";
        text += "<h1>" + "Component " + linkTo(aComponent) + "</h1><hr><br>";

       GroupEntity classes = aComponent.getGroup("class group");

       text += "<b>Component Summary</b><br>";
       text += "Classes (" + linkToNumber(classes) + ") <br><br>";
       text += bulletedLinkList(classes.getElements());

       ArrayList<ExtendedWeightedEdge> remoteEdges =(ArrayList<ExtendedWeightedEdge>) aComponent.getAnnotation(AnnotationConstants.COMPONENT_REMOTEEDGES);
       System system = (System)aComponent.getAnnotation(AnnotationConstants.COMPONENT_SYSTEM);
	   SystemGraph<Node, ExtendedWeightedEdge> graph = (SystemGraph<Node, ExtendedWeightedEdge>)system.getAnnotation(AnnotationConstants.SYSTEM_JGRAPHTGRAPH);
       
	   text+="<p>";
	   text += "<b>Remote edges:</b><br><br>";
	   for(ExtendedWeightedEdge edge : remoteEdges)
		{
			Node source = graph.getEdgeSource(edge);
			Node target = graph.getEdgeTarget(edge);
			DataAbstraction targetClass = (DataAbstraction)target.getObject();
			text += linkTo((DataAbstraction)source.getObject()) + " -> " + linkTo(targetClass) + "  in component " + linkTo((Component)targetClass.getAnnotation(AnnotationConstants.CLASS_COMPONENT)) + "<br>";
			
		}
	   
	   text+="<br>";
       return new ResultEntity(text);
    }

}
