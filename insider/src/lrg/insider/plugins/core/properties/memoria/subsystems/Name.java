package lrg.insider.plugins.core.properties.memoria.subsystems;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.properties.PropertyComputer;

public class Name extends PropertyComputer
{
    public Name()
    {
        super("Name", "The name of the subsystem", "subsystem", "string");
    }

    public ResultEntity compute(AbstractEntityInterface anEntity)
    {
        return new ResultEntity(anEntity.getName());
    }
}