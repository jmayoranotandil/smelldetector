package lrg.insider.plugins.core.properties.memoria.bug;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.insider.plugins.core.properties.AbstractDetail;
import lrg.memoria.core.Bug;

public class Detail extends AbstractDetail {
    public Detail() {
        super("Detail", "Constructs a detailed HTML String to be shown in the browser.", "bug", "string");
    }

    public ResultEntity compute(AbstractEntityInterface aBug) {
    	String text = "";
    	
    	Bug theBug = (Bug) aBug; 
    	text += theBug.getDescription();
    	
    	text += bulletedLinkList(theBug.getReferredModelElements());
    	return new ResultEntity(text);	
    }

}
