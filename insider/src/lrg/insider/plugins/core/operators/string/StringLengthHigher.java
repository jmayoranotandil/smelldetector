package lrg.insider.plugins.core.operators.string;

import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.operators.FilteringOperatorWithThresholds;

/**
 * Created by IntelliJ IDEA.
 * User: marinescu
 * Date: 06.12.2006
 * Time: 11:17:51
 * To change this template use File | Settings | File Templates.
 */
public class StringLengthHigher extends FilteringOperatorWithThresholds {
    public StringLengthHigher()
    {
        super("length higher", "string");
    }

    public boolean apply(ResultEntity theResult, Object threshold)
    {
        if (theResult.getValue() instanceof String == false) return false;

        String temp = (String) theResult.getValue();
        Double thresholdValue = (Double) threshold;

        return temp.length() > thresholdValue.intValue();
    }
}

