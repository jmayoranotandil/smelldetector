package lrg.insider.metamodel;

import lrg.common.abstractions.entities.GroupEntity;
import lrg.memoria.core.Annotation;
import lrg.memoria.core.AnnotationInstance;
import lrg.memoria.core.Attribute;
import lrg.memoria.core.Bug;
import lrg.memoria.core.Component;
import lrg.memoria.core.Function;
import lrg.memoria.core.GlobalVariable;
import lrg.memoria.core.Method;
import lrg.memoria.core.Package;
import lrg.memoria.core.Parameter;
import lrg.memoria.core.Subsystem;
import lrg.memoria.core.Type;
import lrg.memoria.core.Variable;
import lrg.memoria.hismo.core.ClassHistory;
import lrg.memoria.hismo.core.FunctionHistory;
import lrg.memoria.hismo.core.GlobalFunctionHistory;
import lrg.memoria.hismo.core.NamespaceHistory;
import lrg.memoria.hismo.core.VariableHistory;

public class Address {
    public static String buildForRoot() {
        return "~root";
    }

    public static String buildFor(Annotation anAnnotation) {
        return anAnnotation.getFullName();
    }

    public static String buildFor(AnnotationInstance anAnnotation) {
    	return anAnnotation.getFullName()+"{"+anAnnotation.getAnnotatedElement().getFullName()+"}";    
    }

    
    public static String buildFor(Component aComponent) {
        return aComponent.getName();
    }
    public static String buildFor(Package aPackage) {
        return aPackage.getName();
    }

    public static String buildFor(Type aType) {
        return aType.getFullName();
    }

    public static String buildFor(Subsystem aSubsystem) {
        return aSubsystem.getName();
    }

    public static String buildFor(Bug aBug) {
        return aBug.getName();
    }

    
    public static String buildFor(Variable aVariable) {
        String returnValue = "";
        String scope = "";
        if (aVariable instanceof GlobalVariable)
            scope = ((GlobalVariable) aVariable).getScope().getName();
        else if (aVariable instanceof Attribute)
            scope = ((Attribute) aVariable).getScope().getScope().getName() + "." +
                    ((Attribute) aVariable).getScope().getName();
        else if (aVariable instanceof Parameter)
            scope = ((Parameter) aVariable).getScope().getFullName();


        return scope + "." + aVariable.getName();
    }

    public static String buildFor(Function aFunction) {
        String returnValue = "";
        if (aFunction instanceof Method)
            returnValue += aFunction.getScope().getScope().getName() + ".";

        returnValue += aFunction.getScope().getName() + "." + aFunction.getName() + "(";
        for (int i = 0; i < aFunction.getParameterList().size() - 1; i++) {
            Parameter currentParameter = (Parameter) aFunction.getParameterList().get(i);
            returnValue += currentParameter.getType().getFullName() + ",";
        }
        if (aFunction.getParameterList().size() > 0) {
            Parameter lastParameter = (Parameter) aFunction.getParameterList().get(aFunction.getParameterList().size() - 1);
            returnValue += lastParameter.getType().getFullName();
        }

        returnValue += ")";
        return returnValue;

    }

    public static String buildFor(GlobalFunctionHistory currentGlobalFunctionHistory) {
        return currentGlobalFunctionHistory.getFullName();
    }

    public static String buildFor(VariableHistory variableHistory) {
        return variableHistory.getFullName();
    }

    public static String buildFor(FunctionHistory functionHistory) {
        return functionHistory.getFullName();
    }

    public static String buildFor(ClassHistory currentClassHistory) {
        return currentClassHistory.getFullName();
    }

    public static String buildFor(NamespaceHistory currentNamespaceHistory) {
        return currentNamespaceHistory.getFullName();
    }


    public static String buildFor(GroupEntity aGroup) {
        return new String((String) aGroup.getProperty("Address").getValue());
    }


}
