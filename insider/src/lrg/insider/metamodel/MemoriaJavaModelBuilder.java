package lrg.insider.metamodel;

import lrg.common.utils.ProgressObserver;
import lrg.memoria.importer.recoder.JavaModelLoader;

public class MemoriaJavaModelBuilder extends MemoriaModelBuilder {
    private String additionalClassPath;
    public MemoriaJavaModelBuilder(String sourcePath, String cachePath, String additionalCP, ProgressObserver observer) {
        super(sourcePath, cachePath, observer, "Java");
        additionalClassPath = additionalCP;
    }

    protected void loadModel() throws Exception {
        currentSystem = new JavaModelLoader(sourcePath, cachePath, additionalClassPath, progressObserver).getSystem();
    }
}
