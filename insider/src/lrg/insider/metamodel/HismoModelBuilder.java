package lrg.insider.metamodel;

import java.io.File;
import java.util.Iterator;

import lrg.common.abstractions.managers.EntityTypeManager;
import lrg.common.metamodel.ModelBuilder;
import lrg.common.utils.ProgressObserver;
import lrg.memoria.hismo.core.AbstractHistory;
import lrg.memoria.hismo.core.AttributeHistory;
import lrg.memoria.hismo.core.AttributeHistoryGroup;
import lrg.memoria.hismo.core.ClassHistory;
import lrg.memoria.hismo.core.ClassHistoryGroup;
import lrg.memoria.hismo.core.FunctionHistory;
import lrg.memoria.hismo.core.GlobalFunctionHistory;
import lrg.memoria.hismo.core.GlobalFunctionHistoryGroup;
import lrg.memoria.hismo.core.GlobalVariableHistory;
import lrg.memoria.hismo.core.GlobalVariableHistoryGroup;
import lrg.memoria.hismo.core.HismoLoader;
import lrg.memoria.hismo.core.LocalVariableHistory;
import lrg.memoria.hismo.core.LocalVariableHistoryGroup;
import lrg.memoria.hismo.core.MethodHistory;
import lrg.memoria.hismo.core.MethodHistoryGroup;
import lrg.memoria.hismo.core.NamespaceHistory;
import lrg.memoria.hismo.core.SystemHistory;

public class HismoModelBuilder extends ModelBuilder {

    private SystemHistory currentSystemHistory;
    private File cachesDir;
    private String name;
    private ProgressObserver observer;

    public HismoModelBuilder(String cachesDir, String name, ProgressObserver observer) {
        this.cachesDir = new File(cachesDir);
        this.name = name;
        this.observer = observer;
    }

    public void buildModel() throws Exception {
        loadModel();
        createEntityTypes();
        attachSpecificEntityTypes();
    }

    public void cleanModel() {
        for (Iterator it = currentSystemHistory.getVersionIterator(); it.hasNext();)
            lrg.memoria.core.System.unloadSystemFromMemory((lrg.memoria.core.System) it.next());
        currentSystemHistory = null;
    }

    protected void loadModel() throws Exception {
        currentSystemHistory = new HismoLoader(cachesDir, name, observer).getSystemHistory();
    }

    private void attachSpecificEntityTypes() {

        for (AbstractHistory temp : currentSystemHistory.getNamespaceHistories().getHistoriesCollection()) {

            NamespaceHistory currentNamespaceHistory = (NamespaceHistory) temp;
            GlobalFunctionHistoryGroup gfhg = currentNamespaceHistory.getGlobalFunctionHistories();
            for (AbstractHistory tmp : gfhg.getHistoriesCollection()) {
                GlobalFunctionHistory currentGlobalFunctionHistory = (GlobalFunctionHistory) tmp;
                addLocalVariablesHistories(currentGlobalFunctionHistory);

                currentGlobalFunctionHistory.setEntityType(EntityTypeManager.getEntityTypeForName("global function history"));
                addressMap.put(Address.buildFor(currentGlobalFunctionHistory), currentGlobalFunctionHistory);
            }

            GlobalVariableHistoryGroup gvhg = currentNamespaceHistory.getGlobalVariableHistories();
            for (AbstractHistory tmp : gvhg.getHistoriesCollection()) {
                GlobalVariableHistory currentGlobalVariableHistory = (GlobalVariableHistory) tmp;
                currentGlobalVariableHistory.setEntityType(EntityTypeManager.getEntityTypeForName("global variable history"));
                addressMap.put(Address.buildFor(currentGlobalVariableHistory), currentGlobalVariableHistory);
            }

            ClassHistoryGroup chg = currentNamespaceHistory.getClassHistories();
            for (AbstractHistory tmp : chg.getHistoriesCollection()) {
                ClassHistory currentClassHistory = (ClassHistory) tmp;

                AttributeHistoryGroup ahg = currentClassHistory.getAttributeHistories();
                for (AbstractHistory atmp : ahg.getHistoriesCollection()) {
                    AttributeHistory currentAttributeHistory = (AttributeHistory) atmp;
                    currentAttributeHistory.setEntityType(EntityTypeManager.getEntityTypeForName("attribute history"));
                    addressMap.put(Address.buildFor(currentAttributeHistory), currentAttributeHistory);
                }

                MethodHistoryGroup mhg = currentClassHistory.getMethodHistories();
                for (AbstractHistory mtmp : mhg.getHistoriesCollection()) {
                    MethodHistory currentMethodHistory = (MethodHistory) mtmp;
                    addLocalVariablesHistories(currentMethodHistory);

                    currentMethodHistory.setEntityType(EntityTypeManager.getEntityTypeForName("method history"));
                    addressMap.put(Address.buildFor(currentMethodHistory), currentMethodHistory);
                }

                currentClassHistory.setEntityType(EntityTypeManager.getEntityTypeForName("class history"));
                addressMap.put(Address.buildFor(currentClassHistory), currentClassHistory);
            }

            currentNamespaceHistory.setEntityType(EntityTypeManager.getEntityTypeForName("namespace history"));
            addressMap.put(Address.buildFor(currentNamespaceHistory), currentNamespaceHistory);
        }

        /*for (Package currentPackage : currentSystem.getPackages()) {
            for (Type currentType : currentPackage.getAllTypesList())
            {
                if (currentType instanceof DataAbstraction) {
                    DataAbstraction currentDataAbstraction = (DataAbstraction)currentType;
                    for (int k = 0; k < currentDataAbstraction.getMethodList().size(); k++)
                    {
                        Method currentMethod = (Method) currentDataAbstraction.getMethodList().get(k);
                        currentMethod.setEntityType(EntityTypeManager.getEntityTypeForName("method"));
                        addressMap.put(Address.buildFor(currentMethod), currentMethod);
                    }
                    for (int k = 0; k < currentDataAbstraction.getAttributeList().size(); k++)
                    {
                        Attribute currentAttribute = (Attribute) currentDataAbstraction.getAttributeList().get(k);
                        currentAttribute.setEntityType(EntityTypeManager.getEntityTypeForName("attribute"));
                        addressMap.put(Address.buildFor(currentAttribute), currentAttribute);
                    }
                    currentDataAbstraction.setEntityType(EntityTypeManager.getEntityTypeForName("class"));
                    addressMap.put(Address.buildFor(currentDataAbstraction), currentDataAbstraction);
                }

                if (currentType instanceof TypedefDecorator) {
                    currentType.setEntityType(EntityTypeManager.getEntityTypeForName("type"));
                    addressMap.put(Address.buildFor(currentType), currentType);
                }
            }
            currentPackage.setEntityType(EntityTypeManager.getEntityTypeForName("package"));
            addressMap.put(Address.buildFor(currentPackage), currentPackage);
        }
        */
        currentSystemHistory.setEntityType(EntityTypeManager.getEntityTypeForName("system history"));
        addressMap.put(Address.buildForRoot(), currentSystemHistory);

    }

    private void addLocalVariablesHistories(FunctionHistory currentFunctionHistory) {
        LocalVariableHistoryGroup lvhg = currentFunctionHistory.getLocalVariableHistories();
        for (AbstractHistory tmp : lvhg.getHistoriesCollection()) {
            LocalVariableHistory currentLocalVariableHistory = (LocalVariableHistory) tmp;
            currentLocalVariableHistory.setEntityType(EntityTypeManager.getEntityTypeForName("local variable history"));
            addressMap.put(Address.buildFor(currentLocalVariableHistory), currentLocalVariableHistory);
        }
    }

    protected void registerEntityTypes() {
        EntityTypeManager.registerEntityType("result", "");
        EntityTypeManager.registerEntityType("numerical", "");
        EntityTypeManager.registerEntityType("boolean", "");
        EntityTypeManager.registerEntityType("string", "");

        EntityTypeManager.registerEntityType("system history", "");
        EntityTypeManager.registerEntityType("package history", "system history");
        EntityTypeManager.registerEntityType("namespace history", "system history");
        EntityTypeManager.registerEntityType("class history", "package history");
        EntityTypeManager.registerEntityType("type history", "namespace history");
        EntityTypeManager.registerEntityType("method history", "class history");
        EntityTypeManager.registerEntityType("attribute history", "class history");

        EntityTypeManager.registerEntityType("global function history", "package history");
        EntityTypeManager.registerEntityType("global variable history", "package history");
    }
}
