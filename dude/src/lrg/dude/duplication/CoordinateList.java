package lrg.dude.duplication;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Richard
 * Date: 28.03.2004
 * Time: 18:52:51
 * To change this template use Options | File Templates.
 */
public class CoordinateList {
    private List list = new ArrayList();

    public void add(Coordinate c) {
        list.add(c);
    }

    public Coordinate get(int index) {
        return (Coordinate) list.get(index);
    }

    public int size() {
        return list.size();
    }

    public void remove(int index) {
        list.remove(index);
    }
}
