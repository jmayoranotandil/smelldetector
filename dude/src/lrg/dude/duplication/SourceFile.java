  // 
  // dsfsdfdsfds;
  
  package lrg.dude.duplication;

import java.io.*;

/**
 * Created by IntelliJ IDEA.
 * User: Richard
 * Date: 25.02.2004
 * Time: 18:36:30
 * To change this template use duplication.File | Settings | duplication.File Templates.
 */


public class SourceFile implements Entity {
    private String fileName;
    private StringList codelines;

    private int noOfRelevantLines = 0;

    /**
     * Constructor
     *
     * @param file File
     */
    public SourceFile(File file, String shortName) {
        fileName = shortName;
        codelines = new StringList();
        
        //System.out.println(file);
        
        int i = 0;
        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
            String linie = null;
            while ((linie = in.readLine()) != null) {
                codelines.add(linie);
            }
            in.close();
        } catch (FileNotFoundException fe) {
            System.out.println("Nu exista fisierul " + file + ": " + fe);
        } catch (IOException ioe) {
            System.out.println("Eroare citire fisier : " + ioe);
        }
    }

    public String getName() {
        return fileName;
    }

    public StringList getCode() {
        return codelines;
    }

    public int getNoOfRelevantLines() {
        return noOfRelevantLines;
    }

    public void setNoOfRelevantLines(int norl) {
        noOfRelevantLines = norl;
    }
}
