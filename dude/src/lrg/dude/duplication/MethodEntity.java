package lrg.dude.duplication;

import lrg.common.abstractions.entities.AbstractEntity;


/**
 * Created by IntelliJ IDEA.
 * User: Richard
 * Date: 25.02.2004
 * Time: 18:35:36
 * To change this template use duplication.File | Settings | duplication.File Templates.
 * wdsadsadsadfd
 */

public class MethodEntity implements IMethodEntity {
    private int noOfRelevantLines = 0;
    lrg.memoria.core.Method aMethod;

    public MethodEntity(lrg.memoria.core.Method anEntity) {
        aMethod = anEntity;
}

    public AbstractEntity getMethod() {
        return aMethod;
    }


    public String getName() {
        if(aMethod != null)
            return aMethod.getName();
        return null;
    }

    public StringList getCode() {
        if((aMethod != null) && (aMethod.getBody() != null)){
            String theSourceCode = aMethod.getBody().getSourceCode();
            if(theSourceCode != null) {
                StringList strings = new StringList(aMethod.getBody().getSourceCode().split("\n"));
                return strings;
            }
        }
        return new StringList();
    }

    public int getNoOfRelevantLines() //for clustering reasons
    {
        return noOfRelevantLines;
    }

    public void setNoOfRelevantLines(int norl) {
        noOfRelevantLines = norl;
    }


}
