package lrg.dude.duplication;

import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: Richard
 * Date: 18.05.2004
 * Time: 22:56:22
 * To change this template use File | Settings | File Templates.
 */
public class StringList {
    private ArrayList list = null;

    public StringList(String[] tab) {
        this();
        for (int i = 0; i < tab.length; i++)
            list.add(tab[i]);
    }

    public StringList() {
        list = new ArrayList();
    }

    public void add(String s) {
        list.add(s);
    }

    public void addAll(StringList aList) {
        int size = aList.size();
        for (int i = 0; i < size; i++)
            list.add(aList.get(i));
    }

    public String get(int index) {
        return (String) list.get(index);
    }

    public void set(int i, String value) {
        list.set(i, value);
    }

    public int size() {
        return list.size();
    }

}
