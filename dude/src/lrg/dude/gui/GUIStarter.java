package lrg.dude.gui;

import javax.swing.*;

/**
 * Created by IntelliJ IDEA.
 * User: Richard
 * Date: 07.05.2004
 * Time: 21:01:17
 * To change this template use File | Settings | File Templates.
 */
public class GUIStarter {
        public static void main(String[] args) {
        //Schedule a job for the event-dispatching thread:
        //creating and showing this application's GUI.
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
    }

    private static void createAndShowGUI() {
        //Make sure we have nice window decorations.
        JFrame.setDefaultLookAndFeelDecorated(true);
        JDialog.setDefaultLookAndFeelDecorated(true);


        String lnfName;
        //lnfName = "com.sun.java.swing.plaf.windows.WindowsLookAndFeel";
        lnfName = "com.birosoft.liquid.LiquidLookAndFeel";
        try {
            UIManager.setLookAndFeel(lnfName);
            //SwingUtilities.updateComponentTreeUI(this);
        }
        catch (UnsupportedLookAndFeelException ex1) {
            System.err.println("Unsupported LookAndFeel: " + lnfName);
        }
        catch (ClassNotFoundException ex2) {
            System.err.println("LookAndFeel class not found: " + lnfName);
        }
        catch (InstantiationException ex3) {
            System.err.println("Could not load LookAndFeel: " + lnfName);
        }
        catch (IllegalAccessException ex4) {
            System.err.println("Cannot use LookAndFeel: " + lnfName);
        }


        //Create and set up the window.
        JFrame frame = new JFrame("DuDe");
        //frame.setIconImage();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //Create and set up the content pane.
        GUI gui = new GUI();
        gui.setOpaque(true);
        frame.setContentPane(gui);

        //Display the window.
        frame.pack();
        frame.setVisible(true);
    }
}
