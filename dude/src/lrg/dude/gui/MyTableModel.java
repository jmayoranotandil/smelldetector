package lrg.dude.gui;

import javax.swing.table.AbstractTableModel;

/**
 * Created by IntelliJ IDEA.
 * User: Richard
 * Date: 12.05.2004
 * Time: 01:05:58
 * To change this template use File | Settings | File Templates.
 */
public class MyTableModel extends AbstractTableModel {
    private String[] columnNames = {"Reference File",
                                    "Start",
                                    "End",
                                    "Duplication File",
                                    "Start",
                                    "End",
                                    "Copied length",
                                    "Length in file",
                                    "Type",
                                    "Signature"};
    private Object[][] data;

    public MyTableModel() {
        data = new Object[0][0];
    }

    public MyTableModel(Object[][] initData) {
        data = initData;
    }

    public int getColumnCount() {
        return columnNames.length;
    }

    public int getRowCount() {
        return data.length;
    }

    public String getColumnName(int col) {
        return columnNames[col];
    }

    public Object getValueAt(int row, int col) {
        return data[row][col];
    }

    /*
    * JTable uses this method to determine the default renderer/
    * editor for each cell.  If we didn't implement this method,
    * then the last column would contain text ("true"/"false"),
    * rather than a check box.
    */
    public Class getColumnClass(int c) {
        return getValueAt(0, c).getClass();
    }

    /*
    * Don't need to implement this method unless your table's
    * data can change.
    */
    public void setValueAt(Object value, int row, int col) {
        data[row][col] = value;
        fireTableCellUpdated(row, col);
    }
}

