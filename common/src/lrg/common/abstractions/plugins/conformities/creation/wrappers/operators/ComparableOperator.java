package lrg.common.abstractions.plugins.conformities.creation.wrappers.operators;



public abstract class ComparableOperator implements Comparable<ComparableOperator> {
	protected int precedence;
	public int compareTo(ComparableOperator o) {
		return precedence - o.precedence;
	}
}