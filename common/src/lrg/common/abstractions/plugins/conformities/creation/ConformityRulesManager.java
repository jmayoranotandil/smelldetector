package lrg.common.abstractions.plugins.conformities.creation;

import java.io.FileNotFoundException;

import lrg.common.abstractions.managers.EntityTypeManager;
import lrg.common.abstractions.plugins.conformities.ConformityRule;
import lrg.common.abstractions.plugins.conformities.creation.parsing.reading.FileReader;
import lrg.common.abstractions.plugins.conformities.creation.parsing.reading.LineReader;

public class ConformityRulesManager {
	public void createRulesFromFile(String filename) {
		try {
			LineReader r = new FileReader(filename);
			while (r.isEmpty() == false) {
				String rawRule = r.readLine();
				System.out.println(rawRule);
				ConformityRule rule = new ConformityRuleCreator().createConformityRule(rawRule);
				if (rule != null)
					EntityTypeManager.attach(rule);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
}