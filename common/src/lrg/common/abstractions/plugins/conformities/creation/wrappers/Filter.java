package lrg.common.abstractions.plugins.conformities.creation.wrappers;

import lrg.common.abstractions.plugins.conformities.StackItem;
import lrg.common.abstractions.plugins.conformities.creation.parsing.ParsingItemWrapper;
import lrg.common.abstractions.plugins.filters.FilteringRule;

public class Filter implements ParsingItemWrapper, StackItem {
	private String filterName;
	private boolean expectedValue;
	private String targetEntityType;
	
	public Filter(String filterName, boolean expectedValue, String targetEntityType) {
		this.filterName = filterName;
		this.expectedValue = expectedValue;
		this.targetEntityType = targetEntityType;
	}
	
	public String toString() {
		return "[" + filterName + "]";
	}

	@Override
	public StackItem getItemInside() {
		return this;
	}
	
	public FilteringRule getFilteringRule() {
		Class filterClass = getFilterClass();
		try {
			return (FilteringRule) filterClass.newInstance();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	private Class getFilterClass() {
		String className = "lrg.insider.plugins.filters.memoria.";
		if ("method".equals(targetEntityType))
			className += "methods.";
		else if ("class".equals(targetEntityType))
			className += "classes.";
		className += filterName;
		try {
			Class result = Class.forName(className);
			return result;
		} catch (ClassNotFoundException e) {
			className = "lrg.insider.plugins.core.filters.memoria.";
			className += filterName;
			try {
				Class result = Class.forName(className);
				return result;
			} catch (ClassNotFoundException e1) {
				return null;
			}
		}
	}

	public Boolean getExpectedValue() {
		return expectedValue;
	}
}