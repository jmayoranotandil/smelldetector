package lrg.common.abstractions.plugins.conformities.composed;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.Descriptor;
import lrg.common.abstractions.plugins.conformities.ConformityRule;

public class MaxComposedConformityRule extends ConformityRule {
	private static final long serialVersionUID = 4592429925130373897L;
	private ConformityRule firstConformity, secondConformity;
	private double weight;
	
	public MaxComposedConformityRule(ConformityRule firstRule, ConformityRule secondRule) {
		super(new Descriptor("(" + firstRule.getDescriptorObject().getName() + " or " + secondRule.getDescriptorObject().getName() + ")",
				firstRule.getIntersectionofEntityTypeNames(secondRule)));
		firstConformity = firstRule;
		secondConformity = secondRule;
		if (firstConformity.getWeight() > secondConformity.getWeight())
			weight = firstConformity.getWeight();
		else
			weight = secondConformity.getWeight();
	}
	
	public Double applyConformity(AbstractEntityInterface anEntity) {
		if ((firstConformity == null) || (secondConformity == null)) return new Double(0);
		double first = firstConformity.applyConformity(anEntity);
		double second = secondConformity.applyConformity(anEntity);
		if (first > second)
			return first;
		return second;
	}
	
	public double getWeight() {
		return weight;
	}
}