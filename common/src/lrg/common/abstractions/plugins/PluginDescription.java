package lrg.common.abstractions.plugins;

/**
 * User: mihai
 * Date: Nov 30, 2006
 * Time: 2:09:27 PM
 */
@java.lang.annotation.Retention(value = java.lang.annotation.RetentionPolicy.RUNTIME)
@java.lang.annotation.Target(value = {java.lang.annotation.ElementType.METHOD})
public @interface PluginDescription {
    String name() default "";
    String description() default "";
    String entityType() default "";
    String resultEntityType() default "";
}
