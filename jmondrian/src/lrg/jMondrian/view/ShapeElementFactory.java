/*
 *  JMondrian
 *  Copyright (c) 2007-2008 Loose Research Group
 *  Petru Florin Mihancea - petru.mihancea@cs.upt.ro
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package lrg.jMondrian.view;

import java.awt.*;
import java.util.List;

public interface ShapeElementFactory {

    public void addRectangle(Object ent, String descr, int x1, int y1, int width, int heigth, int color, boolean border);

    public void addRectangle(Object ent, String descr, int x1, int y1, int width, int heigth, int color, int frameColor);

    public void addEllipse(Object ent, String descr, int x1, int y1, int width, int heigth, int color, boolean border);

    public void addEllipse(Object ent, String descr, int x1, int y1, int width, int heigth, int color, int frameColor);

    public void addLine(Object ent, String descr, int x1, int y1, int x2, int y2, int color, boolean oriented);

    public void addText(Object ent, String descr, String text, int x1, int y1, int color);

    public void addPolyLine(Object ent, String descr, List<Integer> x, List<Integer> y);

    public void update(Graphics g);

    public String findStatusInformation(int x, int y);

    public Object findEntity(int x, int y);

}
