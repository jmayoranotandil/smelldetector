/*
 *  JMondrian
 *  Copyright (c) 2007-2008 Loose Research Group
 *  Petru Florin Mihancea - petru.mihancea@cs.upt.ro
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package lrg.jMondrian.painters;

import lrg.jMondrian.view.ViewRendererInterface;
import lrg.jMondrian.commands.AbstractNumericalCommand;
import lrg.jMondrian.commands.AbstractStringCommand;

public abstract class AbstractNodePainter {

    protected AbstractNumericalCommand widthCommand;
    protected AbstractNumericalCommand heightCommand;
    protected AbstractNumericalCommand colorCommand;
    protected AbstractNumericalCommand xCommand;
    protected AbstractNumericalCommand yCommand;
    protected AbstractStringCommand textCommand;
    protected AbstractStringCommand nameCommand;
    protected AbstractNumericalCommand frameColorCommand;

    public AbstractNodePainter width(AbstractNumericalCommand widthCommand){
        this.widthCommand = widthCommand;
        return this;
    }

    public double getWidth(Object entity){
        this.widthCommand.setReceiver(entity);
        return widthCommand.execute();
    }

    public AbstractNodePainter height(AbstractNumericalCommand heightCommand){
        this.heightCommand = heightCommand;
        return this;
    }

    public double getHeight(Object entity){
        heightCommand.setReceiver(entity);
        return heightCommand.execute();
    }

    public AbstractNodePainter color(AbstractNumericalCommand colorCommand){
        this.colorCommand = colorCommand;
        return this;
    }

    public AbstractNodePainter frameColor(AbstractNumericalCommand colorCommand){
        this.frameColorCommand = colorCommand;
        return this;
    }

    public AbstractNodePainter x(AbstractNumericalCommand xCommand){
        this.xCommand = xCommand;
        return this;
    }

    public AbstractNodePainter y(AbstractNumericalCommand yCommand){
         this.yCommand = yCommand;
         return this;
     }

    public double getX(Object entity){
       xCommand.setReceiver(entity);
       return xCommand.execute();
    }

    public double getY(Object entity){
       yCommand.setReceiver(entity);
       return yCommand.execute();
    }

    public AbstractNodePainter label(AbstractStringCommand textCommand) {
        this.textCommand = textCommand;
        return this;
    }

    public AbstractNodePainter name(AbstractStringCommand nameCommand) {
        this.nameCommand = nameCommand;
        return this;
    }

    public String toString() {
        String desc = "";
        if(!nameCommand.toString().equals("")) {
            desc+="["+nameCommand.toString()+"] ";
        }
        if(!xCommand.toString().equals("")) {
            desc+="x["+xCommand.toString()+"] ";
        }
        if(!yCommand.toString().equals("")) {
            desc+="y["+yCommand.toString()+"] ";
        }
        if(!widthCommand.toString().equals("")) {
            desc+="width["+widthCommand.toString()+"] ";
        }
        if(!heightCommand.toString().equals("")) {
            desc+="height["+heightCommand.toString()+"] ";
        }
        if(!colorCommand.toString().equals("")) {
            desc+="color["+colorCommand.toString()+"] ";
        }
        return desc;
    }

    public abstract void paint(ViewRendererInterface window, Object entity, double x1Bias, double y1Bias, boolean last);

}
