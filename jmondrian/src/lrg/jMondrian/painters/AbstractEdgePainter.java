/*
 *  JMondrian
 *  Copyright (c) 2007-2008 Loose Research Group
 *  Petru Florin Mihancea - petru.mihancea@cs.upt.ro
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package lrg.jMondrian.painters;

import lrg.jMondrian.view.ViewRendererInterface;
import lrg.jMondrian.commands.AbstractEntityCommand;
import lrg.jMondrian.commands.AbstractNumericalCommand;
import lrg.jMondrian.commands.AbstractStringCommand;
import lrg.jMondrian.util.CommandColor;

public abstract class AbstractEdgePainter {

    protected AbstractEntityCommand fromCommand;
    protected AbstractEntityCommand toCommand;
    protected AbstractNumericalCommand colorCommand;
    protected AbstractStringCommand nameCommand;

    public AbstractEdgePainter(AbstractEntityCommand fromCommand, AbstractEntityCommand toCommand) {
        this.fromCommand = fromCommand;
        this.toCommand = toCommand;
        colorCommand = CommandColor.BLACK;
        nameCommand = new AbstractStringCommand() {
            public String execute() {
                return "";
            }
        };
    }

    public AbstractEdgePainter color(AbstractNumericalCommand colorCommand){
        this.colorCommand = colorCommand;
        return this;
    }

    public AbstractEdgePainter name(AbstractStringCommand nameCommand){
        this.nameCommand = nameCommand;
        return this;
    }

    public abstract void paint(ViewRendererInterface window, Object entity, double x1Bias, double y1Bias, double x2Bias, double y2Bias);

    public final Object getFrom(Object entity) {
        fromCommand.setReceiver(entity);
        return fromCommand.execute();
    }

    public final Object getTo(Object entity) {
        toCommand.setReceiver(entity);
        return toCommand.execute();
    }

    public String toString() {
        String desc = "";
        if(!nameCommand.toString().equals("")) {
            desc+="["+nameCommand.toString()+"] ";
        }
        if(!fromCommand.toString().equals("")) {
            desc+="from["+fromCommand.toString()+"] ";
        }
        if(!toCommand.toString().equals("")) {
            desc+="to["+toCommand.toString()+"] ";
        }
        if(!colorCommand.toString().equals("")) {
            desc+="color["+colorCommand.toString()+"] ";
        }
        return desc;
    }

}
