/*
 *  JMondrian
 *  Copyright (c) 2007-2008 Loose Research Group
 *  Petru Florin Mihancea - petru.mihancea@cs.upt.ro
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package lrg.jMondrian.layouts;

import lrg.jMondrian.commands.AbstractNumericalCommand;
import lrg.jMondrian.figures.EdgeFigure;
import lrg.jMondrian.figures.Node;

import java.util.*;

public class Checker extends AbstractLayout {

    private int prefferedWidth = 800;
    private final AbstractNumericalCommand measure;
    private int xDist = 5, yDist = 5;

    public Checker(AbstractNumericalCommand measure) {
        this.measure = measure;
    }

    public Checker(AbstractNumericalCommand measure, int maxWidth) {
        this(measure);
        this.prefferedWidth = maxWidth;
    }

    public double[] distributeNodes(List<Node> nodeList, List<EdgeFigure> edgeList) {

        //Sort the list
        List<Node> tmpList = new ArrayList<Node>();
        tmpList.addAll(nodeList);
        Collections.sort(tmpList, new Comparator<Node>() {
            public int compare(Node a, Node b) {
                measure.setReceiver(a.getEntity());
                double x = measure.execute();
                measure.setReceiver(b.getEntity());
                double y = measure.execute();
                return (int)x - (int)y;
            }
        });

        //Flow
        double nextX = xDist;
        double nextY = yDist;
        double lastHeight;
        double maxHeight = 0;
        double maxWidth = 0;

        ControlXY xCmd = new ControlXY();
        ControlXY yCmd = new ControlXY();

        Iterator<Node> it = tmpList.iterator();
        while(it.hasNext()) {

            Node figure = it.next();
            xCmd.link(figure,nextX);
            yCmd.link(figure,nextY);

            nextX += xDist + figure.getWidth();
            if(nextX >= maxWidth) {
                maxWidth = nextX;
            }
            lastHeight = figure.getHeight();
            if(maxHeight < lastHeight) {
                maxHeight = lastHeight;
            }
            if(nextX > prefferedWidth) {
                nextY += maxHeight + yDist;
                nextX = xDist;
                maxHeight = 0;
            }
        }

        for(int i = 0; i < nodeList.size(); i++) {
            nodeList.get(i).translateTo(xCmd,yCmd);
        }

        double rez[] = new double[2];
        rez[0] = maxWidth;
        rez[1] = maxHeight + nextY + yDist;

        return rez;

    }
}
