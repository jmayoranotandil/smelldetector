/*
 *  JMondrian
 *  Copyright (c) 2007-2008 Loose Research Group
 *  Petru Florin Mihancea - petru.mihancea@cs.upt.ro
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package lrg.jMondrian.layouts;

import lrg.jMondrian.figures.Node;
import lrg.jMondrian.figures.EdgeFigure;

import java.util.List;
import java.util.Iterator;

public class ScatterPlotLayout extends AbstractLayout {

    public double[] distributeNodes(List<Node> nodeList, List<EdgeFigure> edgeList) {

        double maxX = 0;
        double maxY = 0;

        Iterator<Node> it = nodeList.iterator();
        while(it.hasNext()){

            Node figure = it.next();
            double auxX = figure.getWidth() + figure.getRelativeX();
            double auxY = figure.getHeight() + figure.getRelativeY();
            if(auxX >= maxX) maxX = auxX;
            if(auxY >= maxY) maxY = auxY;

        }

        double[] rez = new double[2];
        rez[0] = maxX;
        rez[1] = maxY;

        return rez;
    }

}