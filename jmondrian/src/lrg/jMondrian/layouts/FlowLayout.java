/*
 *  JMondrian
 *  Copyright (c) 2007-2008 Loose Research Group
 *  Petru Florin Mihancea - petru.mihancea@cs.upt.ro
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package lrg.jMondrian.layouts;

import lrg.jMondrian.figures.*;

import java.util.List;
import java.util.Iterator;

public class FlowLayout extends AbstractLayout {

    private double prefferedWidth;
    private double xDist, yDist;

    public FlowLayout() {
        this(5,5,0);
    }

    public FlowLayout(double maxWidth) {
        this.prefferedWidth = maxWidth;
        xDist = 5;
        yDist = 5;
    }

    public FlowLayout(double xDist, double yDist, double maxWidth) {
        this.prefferedWidth = maxWidth;
        this.xDist = xDist;
        this.yDist = yDist;
    }

    public double[] distributeNodes(List<Node> nodeList, List<EdgeFigure> edgeList) {
        double nextX = xDist;
        double nextY = yDist;
        double lastHeight;
        double maxHeight = 0;
        double maxWidth = 0;

        ControlXY xCmd = new ControlXY();
        ControlXY yCmd = new ControlXY();

        Iterator<Node> it = nodeList.iterator();
        while(it.hasNext()) {

            Node figure = it.next();
            xCmd.link(figure,nextX);
            yCmd.link(figure,nextY);

            nextX += xDist + figure.getWidth();
            if(nextX >= maxWidth) {
                maxWidth = nextX;
            }
            lastHeight = figure.getHeight();
            if(maxHeight < lastHeight) {
                maxHeight = lastHeight;
            }
            if(prefferedWidth != 0 && nextX > prefferedWidth) {
                nextY += maxHeight + yDist;
                nextX = xDist;
                maxHeight = 0;
            }
        }

        for(int i = 0; i < nodeList.size(); i++) {
            nodeList.get(i).translateTo(xCmd,yCmd);
        }

        double rez[] = new double[2];
        rez[0] = maxWidth;
        rez[1] = maxHeight + nextY + yDist;

        return rez;
    }

}