/*
 *  JMondrian
 *  Copyright (c) 2007-2008 Loose Research Group
 *  Petru Florin Mihancea - petru.mihancea@cs.upt.ro
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package lrg.jMondrian.figures;

import lrg.jMondrian.commands.AbstractFigureDescriptionCommand;
import lrg.jMondrian.commands.AbstractNumericalCommand;
import lrg.jMondrian.layouts.AbstractLayout;
import lrg.jMondrian.layouts.FlowLayout;
import lrg.jMondrian.painters.AbstractEdgePainter;
import lrg.jMondrian.painters.AbstractNodePainter;
import lrg.jMondrian.view.ViewRendererInterface;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class Figure extends Node {

    //Node interface
    private double[] dimension = null;

    public double getWidth() {
        if(dimension == null) {
            dimension = layout.applyLayout(nodes,edges);
        }
        return dimension[0] < minimumW ? minimumW : dimension[0];
    }

    public double getHeight() {
        if(dimension == null) {
            dimension = layout.applyLayout(nodes,edges);
        }
        return dimension[1] < minimumH ? minimumH : dimension[1];
    }

    public void show(ViewRendererInterface window, double x1Bias, double y1Bias, boolean last) {
        if(dimension == null) {
            dimension = layout.applyLayout(nodes, edges);
        }
        if(painter != null) {
            painter.width(new AbstractNumericalCommand() {
                public double execute() {
                    return Figure.this.getWidth();
                }
            });
            painter.height(new AbstractNumericalCommand() {
                public double execute() {
                    return Figure.this.getHeight();
                }
            });
            super.show(window, x1Bias, y1Bias, last);
        }
        for(int i = 0; i < nodes.size(); i++) {
            nodes.get(i).show(window,this.getAbsoluteX(), this.getAbsoluteY(), i == nodes.size() - 1);
        }
        for(int j = 0; j < edges.size(); j++) {
            edges.get(j).show(window);
        }
    }

    //Figure interface
    public Figure() {
        this(10,10);
    }

    private int minimumW, minimumH;

    public Figure(int minimumW, int minimumH) {
        super(null,null);
        layout = new FlowLayout();
        this.minimumW = minimumW;
        this.minimumH = minimumH;
        nodeLookUp = new HashMap<Object, Node>();
    }

    private HashMap<Object,Node> nodeLookUp;
    private List<Node> nodes = new ArrayList<Node>();
    private List<EdgeFigure> edges = new ArrayList<EdgeFigure>();
    private AbstractLayout layout;

    public void nodesUsing(List nodes, AbstractNodePainter painter) {
        Iterator it = nodes.iterator();
        while(it.hasNext()) {
            Object anEntity = it.next();
            Node aNode = new Node(anEntity,painter);
            nodeLookUp.put(anEntity,aNode);
            this.nodes.add(aNode);
        }
    }

    public void nodesUsingForEach(List nodes, AbstractNodePainter painter, AbstractFigureDescriptionCommand cmd) {
        Iterator it = nodes.iterator();
        while(it.hasNext()) {
            Object anEntity = it.next();
            Figure aNode = cmd.setReceiver(anEntity).describe();
            aNode.entity = anEntity;
            aNode.painter = painter;
            nodeLookUp.putAll(aNode.nodeLookUp);
            nodeLookUp.put(anEntity,aNode);
            this.nodes.add(aNode);
        }
    }

    public void edgesUsing(List edges, AbstractEdgePainter painter) {
        Iterator it = edges.iterator();
        while(it.hasNext()) {
            Object anEntity = it.next();
            Object f = painter.getFrom(anEntity);
            Object t = painter.getTo(anEntity);
            Node ff = nodeLookUp.get(f);
            Node tt = nodeLookUp.get(t);
            EdgeFigure edge = new EdgeFigure(anEntity,painter,ff,tt);
            this.edges.add(edge);
        }
    }

    public void layout(AbstractLayout layout) {
        this.layout = layout;
    }

    public void renderOn(ViewRendererInterface renderer) {
        show(renderer,0,0,false);
        renderer.setPreferredSize((int)this.getWidth(),(int)this.getHeight());
    }

}