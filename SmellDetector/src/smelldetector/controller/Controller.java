package smelldetector.controller;

import java.util.HashMap;

import smelldetector.controller.IndexController.ClickEvent;
import smelldetector.mvc.Event;
import smelldetector.mvc.IObserver;
import smelldetector.mvc.Observer;
import smelldetector.view.*;
import smelldetector.model.*;
public abstract class Controller implements IObserver {
	
	private Observer observer =  new Observer();
	
	protected Model model;
	
	public Controller(){
		model = new Model();
		defineEvents();
	}
	/************Inicio Metodos de interfaz IObserver*************/
	public void addEvent(String name, Event event){
		observer.addEvent(name, event);
	}
	
	public boolean removeEvent(String name){
		return observer.removeEvent(name);
	}
	
	public HashMap<String, Event> getEvents(){
		return observer.getEvents();
	}
	/************Fin Metodos de interfaz IObserver*************/
	public Controller (Model model){
		this.model = model;
		defineEvents();
	}
	
	protected abstract void defineEvents();
	
}
