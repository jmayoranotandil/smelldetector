package smelldetector.controller;

import java.util.HashMap;

import org.eclipse.core.resources.IProject;

import lrg.common.abstractions.entities.GroupEntity;
import smelldetector.model.detector.IPlasma;
import smelldetector.mvc.Event;



public class IndexController extends Controller{

	
	
	public IndexController(){
		super();
	}
	
	class ClickEvent implements Event {

		@Override
		public void execute(HashMap<String, Object> parameters) {
			System.out.println("Ejecutamos el evento");					
			IPlasma project = new IPlasma();					
			//project.setProject(new Project());					
		}		
	}
	
	
	class DblClickEvent implements Event{

		@Override
		public void execute(HashMap<String, Object> parameters) {
			System.out.println("Ejecutamos DBLCLICK el evento");	
			llamarModelo();
		}
		
	}
	
	class focusEvent implements Event{

		@Override
		public void execute(HashMap<String, Object> parameters) {
			// TODO Auto-generated method stub
			
		}
		
	}

	public void llamarModelo(){
		
	}
	@Override
	protected void defineEvents() {

		/*addEvent("click", new ClickEvent());
		
		addEvent("dbl-click", new DblClickEvent());
		
		addEvent("focus", new Event() {

			@Override
			public void execute(HashMap<String, Object> parameters) {
				int i = 0;
				
			}
		});*/
		
		
		this.addEvent("load-succeful", new Event() {
			private GroupEntity group;
			@Override
			public void execute(HashMap<String, Object> parameters) {
			}
				
			
		});
		
		this.addEvent("load_model", new Event() {
			
			@Override
			public void execute(HashMap<String, Object> parameters) {
				IProject project = (IProject) parameters.get("project");
				model.load(project);
				
			}
		});
	}
	
	
	
		
}
