package smelldetector.view;

import java.util.HashMap;

import smelldetector.model.Model;
import smelldetector.mvc.Event;
import smelldetector.mvc.IObserver;
import smelldetector.mvc.Observer;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.part.ViewPart;


public abstract class View extends ViewPart implements IObserver{
	private Observer observer =  new Observer();
	
	public View(){		
		defineEvents();
	}
	
	
	/************Inicio Metodos de interfaz IObserver*************/
	public void addEvent(String name, Event event){
		observer.addEvent(name, event);
	}
	
	public boolean removeEvent(String name){
		return observer.removeEvent(name);
	}
	
	public HashMap<String, Event> getEvents(){
		return observer.getEvents();
	}
	/************Fin Metodos de interfaz IObserver*************/

	protected abstract void defineEvents();
	
}
