package smelldetector.mvc;

import java.util.ArrayList;
import java.util.HashMap;

public interface Event {

	public abstract void execute(HashMap<String, Object> parameters);
}
