package smelldetector.mvc;

import java.util.HashMap;

public class Observer {
	private HashMap<String, Event> eventMapper = new HashMap<String, Event>();
	
	
	public void addEvent(String name, Event event){
		this.eventMapper.put(name, event);
	}
	
	public boolean removeEvent(String name){
		return eventMapper.remove(name) != null;
	}
		
	public HashMap<String, Event> getEvents(){
		return this.eventMapper;
	}
}
