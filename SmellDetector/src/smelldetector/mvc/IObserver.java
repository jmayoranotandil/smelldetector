package smelldetector.mvc;

import java.util.HashMap;

public interface IObserver {

	public abstract void addEvent(String name, Event event);

	public abstract boolean removeEvent(String name);
	
	public abstract HashMap<String,Event> getEvents();
}
