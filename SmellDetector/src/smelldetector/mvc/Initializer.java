package smelldetector.mvc;

import java.util.ArrayList;
import java.util.HashMap;

import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

import smelldetector.controller.BaseController;
import smelldetector.controller.Controller;
import smelldetector.controller.IndexController;
import smelldetector.view.MainView;
import smelldetector.view.View;

public class Initializer {
	 
	protected static HashMap<String, ArrayList<Event>> viewEvents = new HashMap<String, ArrayList<Event> >();
	protected static HashMap<String, ArrayList<Event>> controllerEvents = new HashMap<String, ArrayList<Event> >();
	
	protected static Controller[] controllers = {
			new IndexController(),
			new BaseController()
	};
	protected static View[] views = new View[1];
	/*= {
		new MainView()
	};*/
	
	public static void start(){
		startControllers();
		startViews();
	}
	
	public static void startControllers(){
		startObservers(controllers,controllerEvents);
	}
	public static void startViews(){
		String a = "";
		try {
			MainView mainView =(MainView) PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().showView("ew",null,IWorkbenchPage.VIEW_CREATE);
			
			views[0] = mainView;
		} catch (PartInitException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		startObservers(views,viewEvents);
	}
	
	private static void startObservers(IObserver[] observers, HashMap<String, ArrayList<Event>> observerEvents ){
		for (IObserver observer : observers){
			HashMap<String, Event> mapper = observer.getEvents();
			//Obtenemos los eventos de un controller y los mapeamos dentro de events
			for (String key : mapper.keySet()){
				Event event = mapper.get(key);
							
				//aca asignamos el evento del controller, al final del arreglo correspondiente a cada evento
				ArrayList<Event> eventArray = observerEvents.get(key);
				if (eventArray == null){
					eventArray = new ArrayList<Event>();
				}
				eventArray.add(event);
				observerEvents.put(key, eventArray);
			}
			
		}
	}
	
	
	public static void dispatchEventView(String key,HashMap<String, Object> parameters){
		ArrayList<Event> evs = viewEvents.get(key);
		
		for (Event ev : evs){			
			ev.execute(parameters);
		}
	}
		
	public static void dispatchEventController(String key ,HashMap<String, Object> parameters){
		ArrayList<Event> evs = controllerEvents.get(key);
		
		for (Event ev : evs){			
			ev.execute(parameters);
		}
	}
}
