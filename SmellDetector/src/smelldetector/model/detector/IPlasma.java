package smelldetector.model.detector;

import java.util.ArrayList;
import java.util.HashMap;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.metamodel.MetaModel;
import lrg.insider.gui.InsiderGUIMain;
import lrg.insider.gui.ui.utils.ProgressBar;
import lrg.insider.metamodel.Address;
import lrg.insider.metamodel.MemoriaJavaModelBuilder;
import lrg.memoria.core.NamedModelElement;

import org.eclipse.core.resources.IProject;
import org.eclipse.ui.PlatformUI;

import smelldetector.model.detector.filter.Filter;
import smelldetector.mvc.Event;
import smelldetector.mvc.Initializer;

public class IPlasma implements SmellDetector{

	private GroupEntity group;
	
	@Override
	public ArrayList<NamedModelElement> getClasses() {
		AbstractEntityInterface entity = MetaModel.instance().findEntityByAddress(Address.buildForRoot());
		
		
		return entity.getGroup("method group").getElements();
	}

	@Override
	public void setProject(IProject project) {		
			final String location = project.getLocation().toOSString();
            new Thread() {
                public void run() {
                    ProgressBar progress = new ProgressBar("Loading the model ...");
                    try {
                        Thread.currentThread().setPriority(Thread.MAX_PRIORITY);
                        String projectPath = location;
                		String cachePath = "";
                	
                		try {
                			MetaModel.createFrom(
                					new MemoriaJavaModelBuilder(projectPath,
                							cachePath, InsiderGUIMain.getAdditioanClassPath(), progress),
                							projectPath
                			);
                			
                			AbstractEntityInterface entity = MetaModel.instance().findEntityByAddress(Address.buildForRoot());                			
            				            			                			
                			group = entity.getGroup("base classes");
                			group = group.distinct();
                			                	
							HashMap<String, Object> parameters = new HashMap<String, Object>();
							parameters.put("classes", group);					
							Initializer.dispatchEventView("load-succeful",parameters);                			
                		} catch (Exception e) {                			
                			e.printStackTrace();
                		}finally {
                            progress.close();
                        }
                   
                    } catch (Exception e2) {
                        e2.printStackTrace();                       
                        return;
                    } finally {
                        progress.close();
                    }                   
                }
            }.start();     					
	}

	public ArrayList<NamedModelElement> applyFilter(Filter aFilteringRule)
    {
		ArrayList<NamedModelElement> elements = this.group.getElements();
		if (aFilteringRule != null){
			GroupEntity filteredGroupEntity = this.group.applyFilter(aFilteringRule);
			if (filteredGroupEntity.size() < 1) {
				System.out.println("There is no entity matching the filter criteria\n Filter was not applied");
			}
			else {
				elements = filteredGroupEntity.getElements();
				
				/*String g = elements.get(0).getClass().toString(); 
				for (NamedModelElement element : elements){
					System.out.println("Nombre: " + element.getName());
					System.out.println("Nombre: " + element.getFullName());
					String b = element.getClass().toString();
					String a = element.toString();
					System.out.println(element.toString());
				}
				System.out.println("LA AGREGAMOS");*/
			}
		}
		return elements;
    }		

}
