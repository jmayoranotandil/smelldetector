package smelldetector.model.detector;

import java.util.ArrayList;

import lrg.memoria.core.NamedModelElement;

import org.eclipse.core.resources.IProject;

import smelldetector.model.detector.filter.Filter;

public interface SmellDetector {

	public abstract ArrayList<NamedModelElement> getClasses();
	
	public abstract void setProject(IProject project);
	
	public abstract ArrayList<NamedModelElement> applyFilter(Filter filter);
		
}
