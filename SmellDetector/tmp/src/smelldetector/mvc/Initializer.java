package smelldetector.mvc;

import java.util.ArrayList;
import java.util.HashMap;

import smelldetector.controller.BaseController;
import smelldetector.controller.Controller;
import smelldetector.controller.IndexController;

public class Initializer {
	 
	protected static HashMap<String, ArrayList<Event>> events = new HashMap<String, ArrayList<Event> >();
	
	protected static Controller[] controllers = {
			new IndexController(),
			new BaseController()
	};
	
	public static void start(){
		for (Controller controller : controllers){
			HashMap<String, Event> mapper = controller.getMapper();
			//Obtenemos los eventos de un controller y los mapeamos dentro de events
			for (String key : mapper.keySet()){
				Event event = mapper.get(key);
							
				//aca asignamos el evento del controller, al final del arreglo correspondiente a cada evento
				ArrayList<Event> eventArray = events.get(key);
				if (eventArray == null){
					eventArray = new ArrayList<Event>();
				}
				//eventArray[eventArray.length] = event;
				eventArray.add(event);
				events.put(key, eventArray);
			}
			
		}
	}
	
	public static void dispatchEvent(String key){
		ArrayList<Event> evs = events.get(key);
		
		for (Event ev : evs){
			ev.execute();
		}
		
	}
}
