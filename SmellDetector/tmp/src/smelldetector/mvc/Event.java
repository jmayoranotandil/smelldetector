package smelldetector.mvc;

public interface Event {

	public abstract void execute();
}
