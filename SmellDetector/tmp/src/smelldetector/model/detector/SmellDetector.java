package smelldetector.model.detector;

import java.util.ArrayList;

import smelldetector.model.Project;
import smelldetector.model.detector.filter.Filter;

public interface SmellDetector {

	public abstract ArrayList<Object> getClasses();
	
	public abstract void setProject(Project project);
	
	public abstract ArrayList<Object> applyFilter(Filter filter);
		
}
