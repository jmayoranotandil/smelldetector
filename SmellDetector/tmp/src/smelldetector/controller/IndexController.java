package smelldetector.controller;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.metamodel.MetaModel;
import lrg.insider.gui.InsiderGUIMain;
import smelldetector.model.Project;
import smelldetector.model.detector.IPlasma;
import smelldetector.mvc.Event;


public class IndexController extends Controller{

	public IndexController(){
		super();
	}
	
	class ClickEvent implements Event {

		@Override
		public void execute() {
			System.out.println("Ejecutamos el evento");		
			//IPlasma project = new IPlasma();
			
			String[] args;
			InsiderGUIMain.main(args);
			//project.setProject(new Project());
			//AbstractEntityInterface entity= MetaModel.instance().findEntityByAddress("~root");
		}		
	}
	
	class DblClickEvent implements Event{

		@Override
		public void execute() {
			System.out.println("Ejecutamos DBLCLICK el evento");	
			llamarModelo();
		}
		
	}
	
	class focusEvent implements Event{

		@Override
		public void execute() {
			// TODO Auto-generated method stub
			
		}
		
	}

	public void llamarModelo(){
		
	}
	@Override
	protected void defineEvents() {
		this.eventMapper.put("click", new ClickEvent());
		
		this.eventMapper.put("dbl-click", new DblClickEvent());
		
		this.eventMapper.put("focus", new Event() {
			
			@Override
			public void execute() {
				int i = 0;
				
			}
		});
	}
		
}
