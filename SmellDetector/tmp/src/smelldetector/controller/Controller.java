package smelldetector.controller;

import java.util.HashMap;

import smelldetector.mvc.Event;

public abstract class Controller {

	protected HashMap<String, Event> eventMapper = new HashMap<String, Event>();
	
	public Controller(){
		defineEvents();
	}
	
	protected abstract void defineEvents();
	
	public HashMap<String, Event> getMapper(){
		return this.eventMapper;
	}
}
