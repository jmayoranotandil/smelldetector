//MyRule1: method: HIGH [LOC > 80] & MEDIUM [CYCLO > 20] | VERY_HIGH [FANIN > 5]
//__God Class: class: [ATFD > FEW] & [WMC > WMC_VERYHIGH] & [TCC < ONE_THIRD]
//__Feature Envy: method: HIGH [ATFD > FEW] & HIGH [FDP < FEW] & VERY_HIGH [LAA < ONE_THIRD]
//__Feature ATFD: method: [ATFD > FEW]
//__Feature FDP: method: [FDP < FEW]
//__Feature LAA: method: [LAA < ONE_THIRD]
//__GOD ATFD: class: [ATFD > FEW]
//__GOD WMC: class: [WMC > WMC_VERYHIGH]
//__GOD TCC: class: [TCC < ONE_THIRD]
//__Data Class: class: HIGH [WOC < ONE_THIRD] & ( ( VERY_HIGH [{NOPA+NOAM} > FEW] & LOW [WMC < WMC_HIGH] ) | ( VERY_HIGH [{NOPA+NOAM} > MANY] & LOW [WMC < WMC_VERYHIGH] ) )
//__DATA WOC: class: [WOC < ONE_THIRD]
//__DATA NOPA+NOAM: class: [{NOPA+NOAM} > FEW]
//__DATA WMC: class: [WMC < WMC_HIGH]

__Brain Method: method: HIGH [LOC > LOC_VERYHIGH] & HIGH [CYCLO > MANY] & MEDIUM [MAXNESTING > DEEP] & MEDIUM [NOAV > SMemCap]
//__Brain LOC: method: [LOC > LOC_VERYHIGH]
//__Brain CYCLO: method: [CYCLO > MANY]
//__Brain MAXNESTING: method: [MAXNESTING > DEEP]
//__Brain NOAV: method: [NOAV > SMemCap]
//__BAD_Brain Method: method: VERY_LOW [LOC > LOC_VERYHIGH] | VERY_LOW [CYCLO > MANY] & VERY_LOW [MAXNESTING > DEEP] & VERY_LOW [NOAV > SMemCap]
__Brain Class: class: ( ( VERY_HIGH [NrBM > 1] & HIGH [LOCC > LOC_CLASS_VERYHIGH] ) | ( VERY_HIGH [NrBM = 1] & HIGH [LOCC > 2*LOC_CLASS_VERYHIGH] & VERY_HIGH [WMC > 2*WMC_VERYHIGH] ) ) & ( HIGH [WMC > WMC_VERYHIGH] & HIGH [TCC < HALF] )
//__Brain TCC: class: [TCC < HALF]
//__Brain WMC: class: [WMC > WMC_VERYHIGH]
//__Brain LOC: class: [LOCC > LOC_CLASS_VERYHIGH]
__Intensive Coupling: method: ( ( MEDIUM [CINT > SMemCap] & MEDIUM [CDISP < HALF] ) | ( MEDIUM [CINT > FEW] & MEDIUM [CDISP < ONE_QUARTER] ) ) & HIGH [MAXNESTING > SHALLOW]
//__IntensCoupl CINT (call many methods a couple classes): method: [CINT > SMemCap] & [CDISP < HALF]
//__IntensCoupl CDISP (call a couple methods very few classes): method: [CINT > FEW] & [CDISP < ONE_QUARTER]
//__IntensCoupl MAXNESTIN: method: [MAXNESTING > SHALLOW]
__Dispersed Coupling: method: ( MEDIUM [CINT > SMemCap] & MEDIUM [CDISP > HALF] ) & HIGH [MAXNESTING > SHALLOW]
//__Dispersed many meth few classes: method: [CINT > SMemCap] & [CDISP > HALF]
//__Dispersed MAXNESTING: method: [MAXNESTING > SHALLOW]
//!!!!__Shotgun Surgery: method: VERY_LOW [CM > SMemCap] & VERY_LOW [CC > SMemCap/2]
//__Refused Parent Bequest1: class:
//__Refused Parent Bequest2: class:
//__Refused Parent Bequest Interface: class:
//__Tradition Breaker: class: 

//__TEST WITH FILTER: method: !IsConstructor [LOC > LOC_VERYHIGH]
__SHOTGUN SURGERY: method: ModelFunctionFilter !IsConstructor !IsStatic VERY_HIGH [CM > SMemCap] & HIGH [CC > 0.5*SMemCap] & MEDIUM [CYCLO > MANY]
__SHOTGUN called by many method: method: [CM > SMemCap]
__SHOTGUN called from many class: method: [CC > 0.5*SMemCap]
__SHOTGUN complex: method: [CYCLO > MANY]
