package graph;

import java.util.List;
import java.util.Vector;



/**
 * Searchs all elementary cycles in a given directed graph. The implementation
 * is independent from the concrete objects that represent the graphnodes, it
 * just needs an array of the objects representing the nodes the graph
 * and an adjacency-matrix of type boolean, representing the edges of the
 * graph. It then calculates based on the adjacency-matrix the elementary
 * cycles and returns a list, which contains lists itself with the objects of the 
 * concrete graphnodes-implementation. Each of these lists represents an
 * elementary cycle.<br><br>
 *
 * The implementation uses the algorithm of Donald B. Johnson for the search of
 * the elementary cycles. For a description of the algorithm see:<br>
 * Donald B. Johnson: Finding All the Elementary Circuits of a Directed Graph.
 * SIAM J. Comput. 4(1): 77-84 (1975).<br><br>
 *
 * The algorithm of Johnson is based on the search for strong connected
 * components in a graph. For a description of this part see:<br>
 * Robert Tarjan: Depth-first search and linear graph algorithms. In: SIAM
 * Journal on Computing. Bd. 1 (1972), Nr. 2, S. 146-160.<br>
 * 
 * @author Frank Meyer
 * @version 1.0, 08.08.2006
 *
 */
public class ElementaryCyclesSearch {

	public static final int MAX_NR_CYCLES = 500;
	/** List of cycles */
	private List cycles = null;

	/** Adjacency-list of graph */
	private int[][] adjList = null;

	/** Graphnodes */
	private int[] graphNodes = null;

	/** Blocked nodes, used by the algorithm of Johnson */
	private boolean[] blocked = null;

	/** B-Lists, used by the algorithm of Johnson */
	private Vector[] B = null;

	/** Stack for nodes, used by the algorithm of Johnson */
	private Vector stack = null;

	/**
	 * Constructor.
	 *
	 * @param matrix adjacency-matrix of the graph
	 * @param graphNodes array of the graphnodes of the graph; this is used to
	 * build sets of the elementary cycles containing the objects of the original
	 * graph-representation
	 */
	public ElementaryCyclesSearch(int[][] adjList, int[] graphNodes) {
		this.graphNodes = graphNodes;
		this.adjList = adjList;
	}

	/**
	 * Returns List::List::Object with the Lists of nodes of all elementary
	 * cycles in the graph.
	 *
	 * @return List::List::Object with the Lists of the elementary cycles.
	 */

	public List getCycles(int s){
		cycles = new Vector();
		blocked = new boolean[this.adjList.length];
		B = new Vector[this.adjList.length];
		stack = new Vector();

		for (int j = 0; j < adjList.length; j++) {
			if ((adjList[j] != null) && (adjList[j].length > 0)) {
				blocked[j] = false;
				B[j] = new Vector();
			}
		}
		try {
			findCycles(s, s, adjList);
		} catch (MaxCyclesNumberException e) {
			System.out.println(e.getMessage());
		}
		return this.cycles;
	}

	/**
	 * Calculates the cycles containing a given node in a strongly connected
	 * component. The method calls itself recursivly.
	 *
	 * @param node current node to check
	 * @param s given node, for which all cycles containing it are searched for
	 * @param adjList adjacency-list with the subgraph of the strongly
	 * connected component s is part of.
	 * @return true, if cycle found; false otherwise
	 */
	private boolean findCycles(int node, int s, int[][] adjList) throws MaxCyclesNumberException{
		boolean f = false;
		stack.add(new Integer(node));
		blocked[node] = true;

		for (int i = 0; i < adjList[node].length; i++) {
			int w = ((Integer) adjList[node][i]).intValue();
			// found cycle
			if (w == s) {
				Vector cycle = new Vector();
				for (int j = 0; j < stack.size(); j++) {
					int index = ((Integer) stack.get(j)).intValue();
					cycle.add(graphNodes[index]);
				}
				if (!cycles.contains(cycle)){
					cycles.add(cycle);
					if (cycles.size() == MAX_NR_CYCLES)
						throw new MaxCyclesNumberException();
				}
				f = true;
			} else if (!blocked[w]) 
				if (findCycles(w, s, adjList)) 
					f = true;
		}
		if (f) {
			unblock(node);
		} else {
			for (int i = 0; i < adjList[node].length; i++) {
				int w = ((Integer) adjList[node][i]).intValue();
				Vector v = this.B[w];
				if (!v.contains(new Integer(node))) 
					v.add(new Integer(node));
			}
		}
		stack.remove(new Integer(node));
		return f;
	}

	/**
	 * Unblocks recursivly all blocked nodes, starting with a given node.
	 *
	 * @param node node to unblock
	 */
	private void unblock(int node) {
		blocked[node] = false;
		Vector Bnode = this.B[node];
		while (Bnode.size() > 0) {
			Integer w = (Integer) Bnode.get(0);
			Bnode.remove(0);
			if (blocked[w.intValue()]) {
				unblock(w.intValue());
			}
		}
	}

}

