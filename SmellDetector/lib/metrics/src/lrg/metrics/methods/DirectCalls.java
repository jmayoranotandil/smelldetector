package lrg.metrics.methods;

import lrg.metrics.NumericalResult;
import lrg.metrics.Result;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * <b>Name:</b> Number Of Direct Calls.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> dc_calls
 * <br>
 * <b>Description:</b> The number of direct calls in a function.
 * <br>
 * Obs.: Different calls to the same method count for one call.
 * <br>
 * <b>Source:</b> t?
 */
public class DirectCalls extends MethodMeasure {

    public DirectCalls() {
        m_name = "DirectCalls";
        m_fullName = "Number Of Direct Calls";
    }

    /**
     * Measures the number of direct calls in a function.
     * Different calls to the same method count for one call.
     */
    public Result measure(lrg.memoria.core.Method actMethod) {
        int count;
        lrg.memoria.core.FunctionBody mb = actMethod.getBody();
        ArrayList cl;
        lrg.memoria.core.Method calledMethod;
        HashSet hs = new HashSet();
        count = 0;
        if (mb != null) {
            cl = mb.getCallList();
            for (int i = 0; i < cl.size(); i++) {
                calledMethod = ((lrg.memoria.core.Call) cl.get(i)).getMethod();
                //we have only one object for each method in lrg.memoria
                if (!hs.contains(calledMethod)) {
                    hs.add(calledMethod);
                    count++;
                }
            }
        }
        return new NumericalResult(actMethod, count);
    }
}



