package lrg.metrics.methods;

import lrg.metrics.NumericalResult;
import lrg.metrics.Result;
import lrg.memoria.core.DataAbstraction;

import java.util.ArrayList;

/**
 * <b>Name:</b> Number Of Other Class Type Parameters.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> ic_par_othercl
 * <br>
 * <b>Description:</b> Measures the number of a method's "class parameters" where
 * the class is different from the curent class.
 * <br>
 * <b>Source:</b> t?
 */
public class OtherClassTypeParameters extends MethodMeasure {

    public OtherClassTypeParameters() {
        m_name = "OtherClassTypeParameters";
        m_fullName = "Number Of Other Class Type Parameters";
    }

    /**
     * Measures the number of a method's "class parameters" where
     * the class is different from the curent class.
     */
    public Result measure(lrg.memoria.core.Method m) {
        int count = 0, i;
        ArrayList pl = m.getParameterList();
        lrg.memoria.core.Parameter param;
        lrg.memoria.core.Type para_type;
        DataAbstraction actScope = (DataAbstraction)m.getScope();
        if (pl != null)
            for (i = 0; i < pl.size(); i++) {
                param = (lrg.memoria.core.Parameter) pl.get(i);
                para_type = param.getType();
                if ((para_type instanceof lrg.memoria.core.Class) && (para_type != actScope))
                    count++;
            }
        return new NumericalResult(m, count);
    }
}
