package lrg.metrics.methods;

import lrg.metrics.NumericalResult;
import lrg.metrics.Result;

import java.util.ArrayList;

/**
 * <b>Name:</b> Number Of Parameters Used.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> ic_usedp
 * <br>
 * <b>Description:</b> Number of method's parameters that are used in the method's body.
 * <br>
 * <b>Source:</b> t?
 */
public class NumberOfUsedParameters extends MethodMeasure {

    public NumberOfUsedParameters() {
        m_name = "NumberOfUsedParameters";
        m_fullName = "Number Of Used Parameters";
    }

    /**
     * Measures the number of parameters that are used in
     * the method's body.
     */
    public Result measure(lrg.memoria.core.Method m) {
        int count, i, size;
        ArrayList pl, accessList;
        lrg.memoria.core.Parameter actParam;
        pl = m.getParameterList();
        count = 0;
        size = pl.size();
        for (i = 0; i < size; i++) {
            actParam = (lrg.memoria.core.Parameter) pl.get(i);
            accessList = actParam.getAccessList();
            if (accessList.size() != 0)
                count++;
        }
        return new NumericalResult(m, count);
    }

}
