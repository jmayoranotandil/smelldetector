package lrg.metrics.methods;

import lrg.metrics.NumericalResult;
import lrg.metrics.Result;

import java.util.ArrayList;

/**
 * <b>Name:</b> Number Of Raised Exceptions.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> ic_except
 * <br>
 * <b>Description:</b> The number of exceptions declared by the keyword "throws"
 * in a method.
 * <br>
 * <b>Source:</b> t?
 */
public class RaisedExceptions extends MethodMeasure {

    public RaisedExceptions() {
        m_name = "RaisedExceptions";
        m_fullName = "Number Of Raised Exceptions";
    }

    /**
     * Measures the number of exceptions declared by the keyword
     * "throws" in a method.
     */
    public Result measure(lrg.memoria.core.Method m) {
        ArrayList el = m.getExceptionList();
        return new NumericalResult(m, el.size());
    }
}
