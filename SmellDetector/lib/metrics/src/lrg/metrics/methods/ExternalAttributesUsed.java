package lrg.metrics.methods;

import lrg.metrics.NumericalResult;
import lrg.metrics.Result;

import java.util.ArrayList;

/**
 * <b>Name:</b> Number Of Times External Attributes Are Used.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> ic_vare
 * <br>
 * <b>Description:</b> The total number of uses of attributes defined outside the
 * current class.
 * <br>
 * Obs.: All attribute occurences are counted.
 * <br>
 * <b>Source:</b> t?
 */
public class ExternalAttributesUsed extends MethodMeasure {

    public ExternalAttributesUsed() {
        m_name = "ExternalAttributesUsed";
        m_fullName = "External Attributes Used";
    }

    /**
     * Measures the total number of uses of attributes defined
     * outside the current class.
     * <br>
     * Obs.: All attribute occurences are counted.
     */
    public Result measure(lrg.memoria.core.Method m) {
        int count, i;
        lrg.memoria.core.FunctionBody mb = m.getBody();
        ArrayList accessList;
        lrg.memoria.core.Access access;
        lrg.memoria.core.Variable var;
        count = 0;
        if (mb != null) {
            accessList = mb.getAccessList();
            int size = accessList.size();
            for (i = 0; i < size; i++) {
                access = (lrg.memoria.core.Access) accessList.get(i);
                var = access.getVariable();
                if (var instanceof lrg.memoria.core.Attribute && ((lrg.memoria.core.Attribute) var).getScope() != m.getScope())
                    count += access.getCount();
            }
        }
        return new NumericalResult(m, count);
    }
}
