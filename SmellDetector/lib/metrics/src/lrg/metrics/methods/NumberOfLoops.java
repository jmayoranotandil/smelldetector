package lrg.metrics.methods;

import lrg.metrics.NumericalResult;
import lrg.metrics.Result;

/**
 * <b>Name:</b> Number Of Loops.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> ct_loop
 * <br>
 * <b>Description:</b> Measures the number of loop statements (pre- and
 * post- tested loops).
 * <br>
 * <b>Source:</b> t?
 */
public class NumberOfLoops extends MethodMeasure {

    public NumberOfLoops() {
        m_name = "NumberOfLoops";
        m_fullName = "Number Of Loops";
    }

    /**
     * Measures the number of loop statements (pre- and post- tested loops).
     */
    public Result measure(lrg.memoria.core.Method m) {
        //Todo: some problems with loops contained in other loops.
        int count = 0;
        lrg.memoria.core.FunctionBody mb = m.getBody();
        if (mb != null)
            count = mb.getNumberOfLoops();
        return new NumericalResult(m, count);
    }

}
