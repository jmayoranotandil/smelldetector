package lrg.metrics.classes;

import lrg.metrics.NumericalResult;
import lrg.metrics.Result;

import java.util.ArrayList;

/**
 * <b>Name:</b> Number Of Interfaces.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> cl_interf
 * <br>
 * <b>Description:</b> Number of declared interfaces implemented by a class or extended
 * by an interface.
 * <br>
 * <b>Source:</b> t?
 */
public class NumberOfInterfaces extends ClassMeasure {

    public NumberOfInterfaces() {
        m_name = "NumberOfInterfaces";
        m_fullName = "Number Of Interfaces";
    }

    /**
     * Measures the number of declared interfaces implemented by a class or extended by an interface.
     */
    public Result measure(lrg.memoria.core.Class c) {
        ArrayList il;
        il = c.getInterfaces();
        return new NumericalResult(c, il.size());
    }
}
