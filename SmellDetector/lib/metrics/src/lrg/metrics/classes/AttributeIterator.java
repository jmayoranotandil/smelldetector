package lrg.metrics.classes;

import lrg.metrics.NumericalResult;
import lrg.metrics.Result;

import java.util.ArrayList;

/**
 * An abstract class used to iterate through the attributes of a class.
 */
public abstract class AttributeIterator extends ClassMeasure {

    /**
     * An abstract method for performing the each metric specific check.
     */
    protected abstract boolean check(lrg.memoria.core.Attribute act_attribute);

    /**
     * This method iterates through attributes and counts the number of times
     * that the attributes pass the specific test.
     */
    public Result measure(lrg.memoria.core.Class act_class) {
        int i, size, count = 0;
        ArrayList attrib_list = act_class.getAttributeList();
        lrg.memoria.core.Attribute actAttribute;
        size = attrib_list.size();
        for (i = 0; i < size; i++) {
            actAttribute = (lrg.memoria.core.Attribute) attrib_list.get(i);
            if (check(actAttribute))
                count += 1;
        }
        return new NumericalResult(act_class, count);
    }
}