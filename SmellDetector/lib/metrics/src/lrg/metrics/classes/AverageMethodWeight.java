package lrg.metrics.classes;

import lrg.metrics.Result;
import lrg.metrics.NumericalResult;
import lrg.metrics.methods.MaximumNumberOfBranches;

import java.util.ArrayList;

/**
 * <b>Name:</b> Average Method Weight.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> AMW
 * <br>
 * <b>Description:</b> AMW is computed as the average cyclomatic complexity for the class.
 * This is measured by dividing WMC by the number of methods in the class.
 * <br>
 * Constructors and destructors are not counted.
 * <br>
 * <b>Source:</b>
 */

public class AverageMethodWeight extends ClassMeasure {

    public AverageMethodWeight() {
        m_name = "AverageMethodWeight";
        m_fullName = "Average Method Weight";
    }

    /**
     * AMW is computed as the average cyclomatic complexity for the class.
     * This is measured by dividing WMC by the number of methods in the class.
     * <br>
     * Constructors and destructors are not counted.
     */

    public Result measure(lrg.memoria.core.Class c) {
        double temp;
        MaximumNumberOfBranches cycloMetric = new MaximumNumberOfBranches();
        int count = c.getMethodList().size();
        int no_of_methods = count;
        double wmc = 0;
        ArrayList methodList = c.getMethodList();
        lrg.memoria.core.Method crtMethod;

        for (int i = 0; i < count; i++) {
            crtMethod = (lrg.memoria.core.Method) methodList.get(i);
            if (crtMethod.isConstructor())
                --no_of_methods;
            else wmc += ((NumericalResult) cycloMetric.measure(crtMethod)).getValue();
        }
        if (no_of_methods != 0)
            temp = wmc / no_of_methods;
        else if (wmc == 0)
            temp = 0;
        else
            temp = Integer.MAX_VALUE;   //if we have no methods, we return an Integer.MAX_VALUE

        return new NumericalResult(c, temp);
    }
}
