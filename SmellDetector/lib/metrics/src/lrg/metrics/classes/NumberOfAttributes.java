package lrg.metrics.classes;

/**
 * <b>Name:</b> Number Of Attributes.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> NOA
 * <br>
 * <b>Description:</b> It is the number of attributes defined in the measured class.
 * Inherited-attributes are not counted.
 * <br>
 * <b>Source:</b>
 */

public class NumberOfAttributes extends AttributeIterator {

    public NumberOfAttributes() {
        m_name = "NumberOfAttributes";
        m_fullName = "Number Of Attributes";
    }

    /**
     * It is the number of attributes defined in the measured class.
     * Inherited-attributes are not counted.
     */

    protected boolean check(lrg.memoria.core.Attribute act_attr) {
        return true;
    }
}
