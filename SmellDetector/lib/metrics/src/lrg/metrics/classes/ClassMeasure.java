package lrg.metrics.classes;

import lrg.metrics.CollectionResult;
import lrg.metrics.Result;
import lrg.metrics.packages.PackageMeasure;

import java.util.ArrayList;

public abstract class ClassMeasure extends PackageMeasure {

    public ClassMeasure() {
        m_kind = "classes";
    }

    public abstract Result measure(lrg.memoria.core.Class cls);

    /**
     * Used as a filter of classes. (i.e. this offers the possibility select only
     * classes we are interested in)
     */
    protected boolean filter(lrg.memoria.core.Class cls) {
        //by default we count only "normal" classes
        return (cls.getStatute() == lrg.memoria.core.Statute.NORMAL);
    }

    public Result measure(lrg.memoria.core.Package pack) {
        int i, size;
        ArrayList cl = pack.getAllTypesList();
        CollectionResult cr = new CollectionResult();
        lrg.memoria.core.Class act_class;
        size = cl.size();
        for (i = 0; i < size; i++) {
            act_class = (lrg.memoria.core.Class) cl.get(i);
            if (filter(act_class))
                cr.add(measure(act_class));
        }
        return cr;
    }
}
