package lrg.metrics.classes;


/**
 * <b>Name:</b> Number Of Static Data Members.
 * <br>
 * <b>Alternative:</b> Number Of Class Variables.
 * <br>
 * <b>Acronym:</b> cl_data_stat, NCV
 * <br>
 * <b>Description:</b> Number of static attributes for a class. (i.e. data members
 * declared after the "static" keyword)
 * <br>
 * <b>Source:</b> M. Lorenz, J. Kidd -- "Object-Oriented Software Metrics",
 * Prentice Hall, 1994.
 */
public class StaticAttributes extends AttributeIterator {

    public StaticAttributes() {
        m_name = "StaticAttributes";
        m_fullName = "Number of Static Data Members";
    }

    /**
     * Check if the current attribute is static.
     */
    protected boolean check(lrg.memoria.core.Attribute act_attr) {
        return act_attr.isStatic();
    }
}
