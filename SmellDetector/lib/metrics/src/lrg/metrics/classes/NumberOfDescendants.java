package lrg.metrics.classes;

import lrg.metrics.Result;
import lrg.metrics.NumericalResult;

import java.util.ArrayList;

/**
 * <b>Name:</b> Number Of Descendants.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> NOD
 * <br>
 * <b>Description:</b> This metric is the number of classes
 * directly or indirectly derived from the measured class.
 * <br>
 * <b>Source:</b>
 */

public class NumberOfDescendants extends ClassMeasure {

    public NumberOfDescendants() {
        m_name = "NumberOfDescendants";
        m_fullName = "Number of Descendants";
    }

    /**
     * This metric is the number of classes
     * directly or indirectly derived from the measured class.
     */
    private int getNoOfDescs(lrg.memoria.core.Class actcls) {
        int i, nd = 0, aux;
        ArrayList descs = actcls.getDescendants();
        lrg.memoria.core.Class crt;

        if (descs.size() == 0) return 1;

        for (i = 0; i < descs.size(); i++) {
            try {
                crt = (lrg.memoria.core.Class) descs.get(i);
            } catch(ClassCastException e) {
                continue;
            }
            aux = getNoOfDescs(crt);
            nd += aux;
        }

        return nd + 1;
    }

    public Result measure(lrg.memoria.core.Class act_class) {
        int count = getNoOfDescs(act_class) - 1;

        return new NumericalResult(act_class, count);
    }
}
