package lrg.metrics.classes;

import lrg.metrics.Result;
import lrg.metrics.NumericalResult;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * <b>Name:</b> Access To Foreign Data.
 * <br>
 * <b>Alternative:</b> Access Of Foreign Data.
 * <br>
 * <b>Acronym:</b> ATFD
 * <br>
 * <b>Description:</b> ATFD (known as AOFD too) represents the number of external classes
 * from which a given class access attributes, directly or via accesor methods.
 * <br>
 * Accesses of constructors and destructors are not counted.
 * If the measured class accesses attributes from another class that is a superclass for it,
 * the superclass is not counted.
 * <br>
 * <b>Source:</b>
 */

public class AccessToForeignData extends ClassMeasure {

    public AccessToForeignData() {
        m_name = "AccessToForeignData";
        m_fullName = "Access To Foreign Data";
    }

    /**
     * ATFD (known as AOFD too) represents the number of external classes
     * from which a given class access attributes, directly or via accesor methods.
     * <br>
     * Accesses of constructors and destructors are not counted.
     * If the measured class accesses attributes from another class that is a superclass for it,
     * the superclass is not counted.
     */

    public Result measure(lrg.memoria.core.Class cls) {
        int count = 0, i, j, k;
        ArrayList methl = cls.getMethodList();
        lrg.memoria.core.Method crtMethod, accMethod;
        ArrayList diraccl, indiraccl, indirdirl, accesses = new ArrayList();
        lrg.memoria.core.Access crtAccess;
        lrg.memoria.core.Attribute crtAttribute;
        lrg.memoria.core.Class crtAttClass;
        HashSet classes = new HashSet();

        //Accesses from own methods

        for (i = 0; i < methl.size(); i++) {
            crtMethod = (lrg.memoria.core.Method) methl.get(i);
            if (!crtMethod.isConstructor()) {
                diraccl = crtMethod.getBody().getAccessList();
                indiraccl = crtMethod.getBody().getCallList();

                for (k = 0; k < indiraccl.size(); k++) {
                    accMethod = (lrg.memoria.core.Method) ((lrg.memoria.core.Call) indiraccl.get(k)).getFunction();
                    if (accMethod.isAccessor()) {
                        indirdirl = accMethod.getBody().getAccessList();
                        diraccl.addAll(indirdirl);
                    }
                }
                accesses.addAll(diraccl);
            }
        }

        //Accesses from initialization body

        ArrayList inits = cls.getInitializerList();

        for (i = 0; i < inits.size(); i++) {
            diraccl = ((lrg.memoria.core.InitializerBody) inits.get(i)).getAccessList();
            indiraccl = ((lrg.memoria.core.InitializerBody) inits.get(i)).getCallList();

            for (k = 0; k < indiraccl.size(); k++) {
                accMethod = (lrg.memoria.core.Method) ((lrg.memoria.core.Call) indiraccl.get(k)).getFunction();
                if (accMethod.isAccessor()) {
                    indirdirl = accMethod.getBody().getAccessList();
                    diraccl.addAll(indirdirl);
                }
            }
            accesses.addAll(diraccl);
        }

        // Select accessed classes

        for (j = 0; j < accesses.size(); j++) {
            crtAccess = (lrg.memoria.core.Access) accesses.get(j);
            try {
                crtAttribute = (lrg.memoria.core.Attribute) crtAccess.getVariable();
            } catch (ClassCastException e) {
                continue;
            }
            crtAttClass = (lrg.memoria.core.Class) crtAttribute.getScope();

            if (!classes.contains(crtAttClass) &&
               (!cls.getAncestorsList().contains((lrg.memoria.core.Class) crtAttClass)) &&
               (crtAttClass != cls)) {
                classes.add(crtAttClass);
                count++;
            }
        }

        return new NumericalResult(cls, count);
    }
}
