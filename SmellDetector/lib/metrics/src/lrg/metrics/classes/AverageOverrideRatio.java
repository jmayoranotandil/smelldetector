package lrg.metrics.classes;

import lrg.metrics.Result;
import lrg.metrics.NumericalResult;

import java.util.ArrayList;

/**
 * <b>Name:</b> Average Override Ratio.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> AOVR
 * <br>
 * <b>Description:</b> The AOVR metric is defined as the average OVR value of the base class.
 * <br>
 * It is implemented only for directly extension relation.
 * <br>
 * This implementation detects the relation between a base class and a
 * subclass, where the subclass doesn't overrides anything.
 * <br>
 * <b>Source:</b>
 */

public class AverageOverrideRatio extends ClassMeasure {

    public AverageOverrideRatio() {
        m_name = "AverageOverrideRatio";
        m_fullName = "Average Override Ratio";
    }

    /**
     * The AOVR metric is defined as the average OVR value of the base class.
     * <br>
     * It is implemented only for directly extension relation.
     * <br>
     * This implementation detects the relation between a base class and a
     * subclass, where the subclass doesn't overrides anything.
     */

    public Result measure(lrg.memoria.core.Class c) {
        double temp, tovr = 0;
        OverrideRatio ovrMetric = new OverrideRatio();
        int i;
        ArrayList cl = c.getDescendants();
        double noc = cl.size();
        lrg.memoria.core.Class crtClass;

        for (i = 0; i < cl.size(); i++) {
            try {
                crtClass = (lrg.memoria.core.Class) cl.get(i);
            } catch (ClassCastException e) {
                continue;
            }
            if (!crtClass.isPrivate())
                tovr += ((NumericalResult) ovrMetric.measure(crtClass, c)).getValue();
        }

        if (noc != 0)
            temp = tovr / noc;
        else if (tovr == 0)
            temp = 0;
        else
            temp = Integer.MAX_VALUE;   //if we have no methods, we return an Integer.MAX_VALUE

        return new NumericalResult(c, temp);
    }
}
