package lrg.metrics.classes;


/**
 * <b>Name:</b> Number Of Constant Data Members.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> cl_data_constant
 * <br>
 * <b>Description:</b> Number of constant attributes for a class. (i.e. data members
 * declared after  "final" and "static"  keywords)
 * <br>
 * <b>Source:</b> t?
 */
public class ConstantAttributes extends AttributeIterator {

    public ConstantAttributes() {
        m_name = "ConstantAttributes";
        m_fullName = "Number Of Constant Data Members";
    }

    /**
     * Check if the current attribute is constant.
     */
    protected boolean check(lrg.memoria.core.Attribute act_attr) {
        return (act_attr.isFinal() && act_attr.isStatic());
    }
}
