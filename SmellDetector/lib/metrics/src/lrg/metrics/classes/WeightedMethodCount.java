package lrg.metrics.classes;

import lrg.metrics.NumericalResult;
import lrg.metrics.Result;
import lrg.metrics.methods.MaximumNumberOfBranches;

import java.util.ArrayList;

/**
 * <b>Name:</b> Weighted Method Count.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> WMC
 * <br>
 * <b>Description:</b> Consider a class C1, with methods M1, M2, .... , Mn
 * that are defined in the class. Let c1, ...., cn be the cyclomatic
 * complexity of the methods of that class. Then this metric rerturns the
 * sum of the cyclomatic complexities of all methods of the current class.
 * <br>
 * <b>Source:</b> S.R. Chidamber, C.F. Kemerer. A metrics Suite for Object Oriented Design.
 * IEEE Transactions on Softw. Engineering, Vol.20, No.6, June 1994.
 */
public class WeightedMethodCount extends ClassMeasure {

    public WeightedMethodCount() {
        m_name = "WeightedMethodCount";
        m_fullName = "Weighted Method Count";
    }

    /**
     * Consider a class C1, with methods M1, M2, .... , Mn
     * that are defined in the class. Let c1, ...., cn be the cyclomatic
     * complexity of the methods of that class. Then this metric returns the
     * sum of the cyclomatic complexities of all methods of the current class.
     */
    public Result measure(lrg.memoria.core.Class cls) {
        double count = 0;
        int i, size;
        MaximumNumberOfBranches cyclo = new MaximumNumberOfBranches();
        ArrayList method_list = cls.getMethodList();
        lrg.memoria.core.Method act_method;
        NumericalResult act_result;
        size = method_list.size();
        for (i = 0; i < size; i++) {
            act_method = (lrg.memoria.core.Method) method_list.get(i);
            act_result = (NumericalResult) cyclo.measure(act_method);
            count += act_result.getValue();
        }
        return new NumericalResult(cls, count);
    }
}
