package lrg.metrics.classes;

import lrg.metrics.Result;
import lrg.metrics.NumericalResult;

/**
 * <b>Name:</b> Leaves Ratio.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> LR
 * <br>
 * <b>Description:</b> Is the ratio between the number of leaves in the hierarchy tree
 * rooted by the measured class, and the total number of descendants.
 * <br>
 * <b>Source:</b>
 */

public class LeavesRatio extends ClassMeasure {

    public LeavesRatio() {
        m_name = "LeavesRatio";
        m_fullName = "Leaves Ratio";
    }

    /**
     * Is the ratio between the number of leaves in the hierarchy tree
     * rooted by the measured class, and the total number of descendants.
     */

    public Result measure(lrg.memoria.core.Class c) {
        NumberOfDescendants nodMetric = new NumberOfDescendants();
        NumberOfLeaves nolMetric = new NumberOfLeaves();
        double nod, nol, temp;

        nod = ((NumericalResult) nodMetric.measure(c)).getValue();
        nol = ((NumericalResult) nolMetric.measure(c)).getValue();

        if (nod != 0)
            temp = nol / nod;
        else if (nol == 0)
            temp = 0;
        else
            temp = Integer.MAX_VALUE;   //if we have no methods, we return an Integer.MAX_VALUE

        return new NumericalResult(c, temp);
    }
}
