package lrg.metrics.classes;

import lrg.metrics.Result;
import lrg.metrics.NumericalResult;

import java.util.ArrayList;

/**
 * <b>Name:</b> Number of Leaves.
 * <br>
 * <b>Alternative:</b>
 * <br>
 * <b>Acronym:</b> NOL
 * <br>
 * <b>Description:</b> This metric is the number of leaves in the hierarchy tree
 * rooted by the measured class.
 * <br>
 * <b>Source:</b>
 */

public class NumberOfLeaves extends ClassMeasure {

    public NumberOfLeaves() {
        m_name = "NumberOfLeaves";
        m_fullName = "Number of Leaves";
    }

    /**
     * This metric is the number of leaves in the hierarchy tree
     * rooted by the measured class.
     */
    private int getNoOfLeaves(lrg.memoria.core.Class actcls) {
        int i, nd = 0, aux;
        ArrayList descs = actcls.getDescendants();
        lrg.memoria.core.Class crt;

        if (descs.size() == 0) return 1;

        for (i = 0; i < descs.size(); i++) {
            try {
                crt = (lrg.memoria.core.Class) descs.get(i);
            } catch(ClassCastException e) {
                continue;
            }
            aux = getNoOfLeaves(crt);
            nd += aux;
        }

        return nd;
    }

    public Result measure(lrg.memoria.core.Class act_class) {
        //if there's no descendant for the root, there will be considered 0 leaves
        int count;

        if (act_class.getDescendants().size() == 0)
            count = 0;
        else
            count = getNoOfLeaves(act_class);

        return new NumericalResult(act_class, count);
    }
}
