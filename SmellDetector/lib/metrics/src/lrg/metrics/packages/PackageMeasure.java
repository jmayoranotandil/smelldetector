package lrg.metrics.packages;

import lrg.metrics.CollectionResult;
import lrg.metrics.Result;
import lrg.metrics.system.SystemMeasure;

import java.util.ArrayList;
import java.io.File;

public abstract class PackageMeasure extends SystemMeasure {

    public PackageMeasure() {
	m_kind = "packages";
    }

    public abstract Result measure(lrg.memoria.core.Package pack);

    public Result measure(lrg.memoria.core.System sys) {
	int i, size;
	ArrayList pl = sys.getNormalPackages();    
	CollectionResult cr = new CollectionResult(m_path + File.separator + m_kind + File.separator + m_name + ".out", m_fullName);
	lrg.memoria.core.Package act_pack;
	size = pl.size();
	for (i = 0; i < size; i++) {
	    act_pack = (lrg.memoria.core.Package)pl.get(i);
	    cr.add(measure(act_pack));
	}
	return cr;
    }
}
