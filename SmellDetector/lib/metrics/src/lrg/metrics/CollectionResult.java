package lrg.metrics;

import java.io.FileWriter;
import java.io.IOException;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.SortedSet;
import java.util.TreeSet;

public class CollectionResult extends Result {
    //the results of this collection
    private ArrayList m_results;
    //the name of this collection 
    protected final String m_name;
    //the full name of the file where the result will be saved (i.e. the file name and the path)
    protected final String m_fileName;
    //the path of the analized source
    public static String sourcePath = "unknown";
    
    /**
	We call a no-arg constructor only for inner collections.(i.e. collection
	contained inside another collection)
     */
    public CollectionResult() {
	m_results = new ArrayList();
	m_fileName = "";
	m_name = "inner_collection";
    }

    /**
       Constructs a new collection of results having given the name of this
       collection and the name of the file where the result will be saved.
     */
    public CollectionResult(String fileName, String name) {
	m_name = name;
	m_fileName = fileName;
	m_results = new ArrayList();
    }

    /**
       Add a new result to the collection.
     */
    public void add(Result result) {
	m_results.add(result);
    }

    //make up the set of results
    protected SortedSet getResultsSet() {
	int i, size;
	TreeSet temp_set = new TreeSet();
	Result act_result;
	size = m_results.size();
	for (i = 0; i < size; i++) {
	    act_result = (Result)m_results.get(i);
	    temp_set.addAll(act_result.getResultsSet());
	}
	return temp_set;
    }
    
    /**
       Save the collection.
     */
    public void saveResults() {
	FileWriter fw;
	try {
        File file = new File(m_fileName);
        File path = new File(file.getParent());
        path.mkdirs();
        //file.createNewFile();
	    fw = new FileWriter(m_fileName);
	    fw.write("Metric's full name: \"" + m_name + "\"\n");
	    fw.write("Source path: \"" + sourcePath + "\"\n");
	    Iterator set_iterator = getResultsSet().iterator(); 
	    String str;
	    //save data
	    while (set_iterator.hasNext()) {
		//get the "atomic" result
		str = (String)set_iterator.next();
		fw.write(str);
	    }
	    fw.close();
	} catch (IOException e) {
	    java.lang.System.err.println("IOException occured while saving the results in " + m_fileName);
        java.lang.System.err.println(e);
	}
    }
}
