package lrg.metrics;

import lrg.memoria.core.DataAbstraction;

import java.util.ArrayList;
import java.util.SortedSet;
import java.util.TreeSet;

public class NumericalResult extends Result{
    //the value of the result
    private double m_value = 0.0;
    //the string that the value represents 
    private String m_what;

    /**
       Constructs a new numerical result for a method.
       <br>
       The parameters are: the method that produced this result and the result value.
     */
    public NumericalResult(lrg.memoria.core.Method meth, double v) {
	int i, size;
	lrg.memoria.core.DataAbstraction cls = (DataAbstraction)meth.getScope();
	lrg.memoria.core.Package pack = cls.getPackage();
	StringBuffer temp_name = new StringBuffer();
	//build the name of the NumericalResult for a method
	//attach the name of the class
	temp_name.append(cls.getName());	
	temp_name.append("\t");
	//attach the signature of the current method	
	temp_name.append(meth.getName());
	temp_name.append("(");
      	ArrayList param_list = meth.getParameterList();
	lrg.memoria.core.Parameter param;
	size = param_list.size();
	for (i = 0; i < size; i++) {
	    param = (lrg.memoria.core.Parameter)param_list.get(i);
	    temp_name.append(param.getType().getName());
	    temp_name.append(", ");
	}
	if (size > 0) {
	    int len = temp_name.length();
	    temp_name.delete(len - 2, len);
	}
	temp_name.append(")");
	temp_name.append("\t");
	//attach the name of the package
	String pack_name = pack.getName();
	if (pack_name.equals(""))
	    temp_name.append("unnamed_package");
	else
	    temp_name.append(pack_name);
	temp_name.append("\t");
	m_what = temp_name.toString();
	//save the numerical value
	m_value = v;
    }

    /**
       Constructs a new numerical result for a class.
       <br>
       The parameters are: the class that produced this result and the result value.
     */
    public NumericalResult(lrg.memoria.core.Class cls, double v) {
	lrg.memoria.core.Package pack = cls.getPackage();
	StringBuffer temp_name = new StringBuffer();
	//build the name of the Numerical Result for a class
	//attach the name of the class
	temp_name.append(cls.getName());
	temp_name.append("\t");
	//attach the name of the package
	String pack_name = pack.getName();
	if (pack_name.equals(""))
	    temp_name.append("unnamed_package");
	else
	    temp_name.append(pack_name);
	temp_name.append("\t");
	m_what = temp_name.toString();
	//save the numerical value
	m_value = v;
    }    

    /**
       Constructs a new numerical result for a package.
       <br>
       The parameters are: the package that produced this result and the result value.
     */
    public NumericalResult(lrg.memoria.core.Package pack, double v) {
	//build the name of the Numerical Result for a package
	m_what = pack.getName();
	if (m_what.equals(""))
	    m_what = "unnamed_package";
	m_what = m_what + "\t";
	//save the numerical value
	m_value = v;
    }

    /**
       Returns the value contained by this metric.
     */
    public double getValue() {
	return m_value;
    }

    /**
       Returns the result.
     */
    protected SortedSet getResultsSet() {
	TreeSet ts = new TreeSet();
	ts.add(m_what + m_value + "\n");
	return ts;
    }
}
