package lrg.metrics.system;

import lrg.metrics.Result;

/**
    This class is the root of the class hierarcy which implement metrics. 
 */
public abstract class SystemMeasure {
    //the metric's name (the name of the class which implements this metric)
    protected String m_name;
    //the metric's full name
    protected String m_fullName;
    //the path to the file where the result is saved
    public static String m_path = ".";
    //what kind of metric
    protected String m_kind;

    public abstract Result measure(lrg.memoria.core.System sys);
}


