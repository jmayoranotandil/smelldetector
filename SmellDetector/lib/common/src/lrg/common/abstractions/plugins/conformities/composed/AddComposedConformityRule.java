package lrg.common.abstractions.plugins.conformities.composed;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.Descriptor;
import lrg.common.abstractions.plugins.conformities.ConformityRule;

public class AddComposedConformityRule extends ConformityRule {
	private static final long serialVersionUID = -3780888984291854056L;
	private ConformityRule firstConformity, secondConformity;
	private double weight;
	
	public AddComposedConformityRule(ConformityRule firstRule, ConformityRule secondRule) {
		super(new Descriptor("(" + firstRule.getDescriptorObject().getName() + " and " + secondRule.getDescriptorObject().getName() + ")",
				firstRule.getIntersectionofEntityTypeNames(secondRule)));
		firstConformity = firstRule;
		secondConformity = secondRule;
		weight = firstConformity.getWeight() + secondConformity.getWeight();
	}
	
	public Double applyConformity(AbstractEntityInterface anEntity) {
		if ((firstConformity == null) || (secondConformity == null)) return new Double(0);
		double first = firstConformity.applyConformity(anEntity);
		double second = secondConformity.applyConformity(anEntity);
		return (first + second);
	}
	
	public double getWeight() {
		return weight;
	}
}