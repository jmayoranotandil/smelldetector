package lrg.common.abstractions.plugins.conformities.creation.wrappers;

import lrg.common.abstractions.plugins.conformities.ConformityRule;
import lrg.common.abstractions.plugins.conformities.StackItem;
import lrg.common.abstractions.plugins.conformities.composed.ComposedMetricConformityRule;
import lrg.common.abstractions.plugins.conformities.creation.parsing.ParsingItemWrapper;

public class Rule implements ParsingItemWrapper {
	public String metric;
	public String operator;
	private String targetEntityType;
	public double threshold;
	
	public Rule(String metric, String operator, double threshold, String targetEntityType) {
		this.metric = metric;
		this.operator = operator;
		this.threshold = threshold;
		this.targetEntityType = targetEntityType;
	}
	
	public String toString() {
		return "[" + metric + " " + operator + " " + threshold + "]";
	}

	@Override
	public StackItem getItemInside() {
		if (metric.startsWith("{")) {
			metric = metric.substring(1, metric.length() - 1);
			String[] metrics = metric.split("\\+");
			return new ComposedMetricConformityRule(metrics, operator, targetEntityType, threshold);
		}
		return new ConformityRule(metric, operator, targetEntityType, threshold);
	}
}