/*
 *  JMondrian
 *  Copyright (c) 2007-2008 Loose Research Group
 *  Petru Florin Mihancea - petru.mihancea@cs.upt.ro
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package lrg.jMondrian.util;

import lrg.jMondrian.commands.AbstractNumericalCommand;

public class CommandColor {

    private static class MondrianColor extends AbstractNumericalCommand {
        private java.awt.Color c;
        public MondrianColor(java.awt.Color c) {
            this.c = c;
        }
        public double execute() {
            return c.getRGB();
        }
    }

    private static class Invisible extends AbstractNumericalCommand {
        public double execute() {
            throw new InvisibleException();
        }
    }

    public static class InvisibleException extends RuntimeException {
        public InvisibleException() {
            super("Invisible color!");
        }
    }

    public final static AbstractNumericalCommand BLACK = new MondrianColor(java.awt.Color.BLACK);
    public final static AbstractNumericalCommand BLUE = new MondrianColor(java.awt.Color.BLUE);
    public final static AbstractNumericalCommand CYAN = new MondrianColor(java.awt.Color.CYAN);
    public final static AbstractNumericalCommand DARK_GRAY = new MondrianColor(java.awt.Color.DARK_GRAY);
    public final static AbstractNumericalCommand GRAY = new MondrianColor(java.awt.Color.GRAY);
    public final static AbstractNumericalCommand GREEN = new MondrianColor(java.awt.Color.GREEN);
    public final static AbstractNumericalCommand LIGHT_GRAY = new MondrianColor(java.awt.Color.LIGHT_GRAY);
    public final static AbstractNumericalCommand MAGENTA = new MondrianColor(java.awt.Color.MAGENTA);
    public final static AbstractNumericalCommand ORANGE = new MondrianColor(java.awt.Color.ORANGE);
    public final static AbstractNumericalCommand PINK = new MondrianColor(java.awt.Color.PINK);
    public final static AbstractNumericalCommand RED = new MondrianColor(java.awt.Color.RED);
    public final static AbstractNumericalCommand WHITE = new MondrianColor(java.awt.Color.WHITE);
    public final static AbstractNumericalCommand YELLOW = new MondrianColor(java.awt.Color.YELLOW);
    public final static AbstractNumericalCommand INVISIBLE = new Invisible();

}
