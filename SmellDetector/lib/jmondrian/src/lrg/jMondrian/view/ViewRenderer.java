/* Copyright (c) 2008, LOOSE Research Group
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted
 * provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions
 * and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions
 * and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package lrg.jMondrian.view;

import lrg.jMondrian.util.MenuReaction;

import java.util.*;
import java.util.List;

import java.awt.image.BufferedImage;
import java.awt.event.*;
import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.AffineTransform;
import java.awt.event.ActionListener;
import java.io.FileOutputStream;

import javax.swing.*;
import javax.imageio.ImageIO;

public class ViewRenderer extends JPanel implements ActionListener, ViewRendererInterface {

    private ShapeElementFactory factory = new ShapeJavaFactory();
    private double zoom = 1;
    private String title;
    private static MenuReaction common;

    public static void setMenuReaction(MenuReaction listener) {
        common = listener;
    }

    private final class ShapeJavaFactory implements ShapeElementFactory {

        private List<Shape> shapes = new ArrayList<Shape>();
        private List<Color> colors = new ArrayList<Color>();
        private List<String> labels = new ArrayList<String>();
        private List<Integer> xLabel = new ArrayList<Integer>();
        private List<Integer> yLabel = new ArrayList<Integer>();
        private List<Object> m = new ArrayList<Object>();
        private List<String> d = new ArrayList<String>();

        private ShapeJavaFactory() {}

        public void addRectangle(Object ent, String descr, int x1, int y1, int width, int heigth, int color, boolean border) {
            shapes.add(new Rectangle2D.Double(x1,y1,width,heigth));
            colors.add(new Color(color));
            m.add(ent);
            d.add(descr);
            if(border) {
                shapes.add(new Line2D.Double(x1,y1,x1+width,y1));
                colors.add(Color.BLACK);
                m.add(null);
                d.add(null);
                shapes.add(new Line2D.Double(x1,y1,x1,y1+heigth));
                colors.add(Color.BLACK);
                m.add(null);
                d.add(null);
                shapes.add(new Line2D.Double(x1+width,y1,x1+width,y1+heigth));
                colors.add(Color.BLACK);
                m.add(null);
                d.add(null);
                shapes.add(new Line2D.Double(x1,y1+heigth,x1+width,y1+heigth));
                colors.add(Color.BLACK);
                m.add(null);
                d.add(null);
            }
        }

        public void addRectangle(Object ent, String descr, int x1, int y1, int width, int heigth, int color, int frameColor) {
            shapes.add(new Rectangle2D.Double(x1,y1,width,heigth));
            colors.add(new Color(color));
            m.add(ent);
            d.add(descr);
            shapes.add(new Line2D.Double(x1,y1,x1+width,y1));
            colors.add(new Color(frameColor));
            m.add(null);
            d.add(null);
            shapes.add(new Line2D.Double(x1,y1,x1,y1+heigth));
            colors.add(new Color(frameColor));
            m.add(null);
            d.add(null);
            shapes.add(new Line2D.Double(x1+width,y1,x1+width,y1+heigth));
            colors.add(new Color(frameColor));
            m.add(null);
            d.add(null);
            shapes.add(new Line2D.Double(x1,y1+heigth,x1+width,y1+heigth));
            colors.add(new Color(frameColor));
            m.add(null);
            d.add(null);
        }

        public void addEllipse(Object ent, String descr, int x1, int y1, int width, int heigth, int color, boolean border) {
            if(border) {
                shapes.add(new Ellipse2D.Double(x1,y1,width,heigth));
                colors.add(Color.BLACK);
                m.add(null);
                d.add(null);
            }
            shapes.add(new Ellipse2D.Double(x1+1,y1+1,width-2,heigth-2));
            colors.add(new Color(color));
            m.add(ent);
            d.add(descr);
        }

        public void addEllipse(Object ent, String descr, int x1, int y1, int width, int heigth, int color, int frameColor) {
            shapes.add(new Ellipse2D.Double(x1,y1,width,heigth));
            colors.add(new Color(frameColor));
            m.add(null);
            d.add(null);
            shapes.add(new Ellipse2D.Double(x1+1,y1+1,width-2,heigth-2));
            colors.add(new Color(color));
            m.add(ent);
            d.add(descr);
        }

        public void addLine(Object ent, String descr, int x1, int y1, int x2, int y2, int color, boolean oriented) {
            if(oriented) {
                double epsilon = Math.PI / 6;
                double dist = 10;
                double angleWithOY;
                if(x2 - x1 != 0 || y2 - y1 != 0) {
                    if(x2 - x1 == 0) {
                        if(y2 - y1 > 0) angleWithOY = 0;
                        else angleWithOY = Math.PI;
                    } else if(y2 - y1 == 0) {
                        if(x2 - x1 > 0) angleWithOY = Math.PI / 2;
                        else angleWithOY = Math.PI / 2 + Math.PI;
                    } else {
                        angleWithOY = Math.atan(((double)(x2 - x1)) / (y2 - y1));
                        if(y2 - y1 < 0) {
                            angleWithOY = angleWithOY + Math.PI;
                        }
                    }
                    double xa = x2 - dist / Math.cos(epsilon) * Math.cos(Math.PI / 2 - angleWithOY - epsilon);
                    double ya = y2 - dist / Math.cos(epsilon) * Math.sin(Math.PI / 2 - angleWithOY - epsilon);
                    double xap= x2 - dist / Math.cos(epsilon) * Math.cos(Math.PI / 2 - angleWithOY + epsilon);
                    double yap= y2 - dist / Math.cos(epsilon) * Math.sin(Math.PI / 2 - angleWithOY + epsilon);
                    shapes.add(new Line2D.Double(xa,ya,x2,y2));
                    shapes.add(new Line2D.Double(xap,yap,x2,y2));
                    colors.add(new Color(color));
                    colors.add(new Color(color));
                    m.add(ent);
                    m.add(ent);
                    d.add(descr);
                    d.add(descr);
                }
            }
            shapes.add(new Line2D.Double(x1,y1,x2,y2));
            colors.add(new Color(color));
            m.add(ent);
            d.add(descr);
        }

        public void addPolyLine(Object ent, String descr, List<Integer> x, List<Integer> y) {
            for(int i = 1; i < x.size(); i++) {
                shapes.add(new Line2D.Double(x.get(i-1),y.get(i-1),x.get(i),y.get(i)));
                colors.add(Color.LIGHT_GRAY);
                m.add(ent);
                d.add(descr);
            }
            shapes.add(new Line2D.Double(x.get(0),y.get(0),x.get(x.size()-1),y.get(y.size()-1)));
            colors.add(Color.LIGHT_GRAY);
            m.add(ent);
            d.add(descr);            
        }

        public void addText(Object ent, String descr, String text, int x1, int y1, int color) {
            labels.add(text);
            xLabel.add(x1);
            yLabel.add(y1);
        }

        public void update(Graphics g) {
            Graphics2D g2 = (Graphics2D)g;
            Iterator<Shape> it = shapes.iterator();
            Iterator<Color> it1 = colors.iterator();
            Shape s;
            AffineTransform transf = new AffineTransform(zoom,0,0,zoom,0,0);

            g2.setColor(Color.WHITE);
            g2.fillRect(0,0,getWidth(), getHeight());
            while(it.hasNext()){
                s = transf.createTransformedShape(it.next());
                Color c = it1.next();
                g2.setPaint(c);
                g2.draw(s);
                g2.setPaint(c);
                g2.fill(s);
            }

            g2.setPaint(Color.BLACK);
            Iterator<String> it2 = labels.iterator();
            double point[] = new double[2];
            double trans[] = new double[2];
            int i = 0;
            while(it2.hasNext()) {
                point[0] = xLabel.get(i);
                point[1] = yLabel.get(i);
                transf.transform(point,0,trans,0,1);
                g2.drawString(it2.next(),(int)trans[0],(int)trans[1]);
                i++;
            }
        }

        public String findStatusInformation(int x, int y) {
            AffineTransform transf = new AffineTransform(zoom,0,0,zoom,0,0);
            for(int i = shapes.size() - 1; i >=0; i--) {
                Shape newShape = transf.createTransformedShape(shapes.get(i));
                if(newShape.contains(x,y,1,1) && m.get(i)!=null) {
                    return d.get(i);
                }
            }
            return "";
        }

        public Object findEntity(int x, int y) {
            AffineTransform transf = new AffineTransform(zoom,0,0,zoom,0,0);
            for(int i = shapes.size() - 1; i >=0; i--) {
                Shape newShape = transf.createTransformedShape(shapes.get(i));
                if(newShape.contains(x,y,1,1) && m.get(i)!=null) {
                    return m.get(i);
                }
            }
            return null;
        }

    }

    public ShapeElementFactory getShapeFactory() {
        return factory;
    }

    public void setPreferredSize(int width, int height) {
        this.setPreferredSize(new Dimension(width,height));        
    }

    public ViewRenderer(String title) {
        this.title = title;
    }

    public ViewRenderer() {
        this("lrg.jMondrian");
    }

    public void update(Graphics g) {
        factory.update(g);
    }

    public void paint(Graphics g){
        update(g);
        this.setPreferredSize(new Dimension(this.getWidth(), this.getHeight()));
        this.revalidate();
    }

    public void open() {

        JDialog.setDefaultLookAndFeelDecorated(true);
        JFrame.setDefaultLookAndFeelDecorated(true);

        final JFrame f = new JFrame(title);
        JMenuBar menuBar = new JMenuBar();
        JMenu menu = new JMenu("File");
        JMenuItem item = new JMenuItem("Save");
        item.addActionListener(this);
        item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S,java.awt.event.InputEvent.CTRL_MASK));
        menu.add(item);
        item = new JMenuItem("Close");
        item.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                f.dispose();    
            }
        });
        item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C,java.awt.event.InputEvent.CTRL_MASK));
        menu.add(item);
        menuBar.add(menu);
        menu = new JMenu("Zoom");
        item = new JMenuItem("Zoom In");
        item.setAccelerator(KeyStroke.getKeyStroke('+'));
        item.addActionListener(this);
        menu.add(item);
        item = new JMenuItem("Zoom Out");
        item.setAccelerator(KeyStroke.getKeyStroke('-'));
        item.addActionListener(this);
        menu.add(item);
        menuBar.add(menu);

        f.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {f.dispose();}
        });

        JScrollPane scroll = new JScrollPane(this);
        JPanel statusBar = new JPanel(new FlowLayout());
        final JLabel statusText = new JLabel(title);
        statusBar.add(statusText);
        JPanel content = new JPanel(new BorderLayout());

        if(common != null) {
            this.addMouseListener(new MouseListener() {
                public void mouseClicked(MouseEvent e) {
                    Object ent = factory.findEntity(e.getX(),e.getY());
                    if(ent != null) {
                        common.buildFor(ent,e);
                    }
                }
                public void mousePressed(MouseEvent e) {}
                public void mouseReleased(MouseEvent e) {}
                public void mouseEntered(MouseEvent e) {}
                public void mouseExited(MouseEvent e) {}
            });
        }

        this.addMouseMotionListener(new MouseMotionListener(){
            public void mouseDragged(MouseEvent e) {
            }

            public void mouseMoved(MouseEvent e) {
                String info = factory.findStatusInformation((int)e.getPoint().getX(),(int)e.getPoint().getY());
                if(!info.equals("")) {
                    statusText.setText(info);
                }
            }
        });

        content.setOpaque(true);
        content.add(scroll, BorderLayout.CENTER);
        content.add(statusBar, BorderLayout.SOUTH);
        f.setPreferredSize(new Dimension(800,500));
        f.setJMenuBar(menuBar);
        f.setContentPane(content);
        f.pack();
        f.setLocation(25,25);
        f.setVisible(true);
    }

    public void actionPerformed(ActionEvent e) {
        JMenuItem item  = (JMenuItem) e.getSource();
        if(item.getText().equals("Zoom In")){
            zoom += 0.2;
            this.setPreferredSize(new Dimension((int) (this.getWidth() * 1.2), (int)(this.getHeight() * 1.2)));
            this.revalidate();
            repaint();
        }
        if(item.getText().equals("Zoom Out")){
            if(zoom >= 0.4){
                zoom -= 0.2;
                this.setPreferredSize(new Dimension((int) (this.getWidth() / 1.2), (int)(this.getHeight() / 1.2)));
                this.revalidate();
                repaint();
            }
        }
        if(item.getText().equals("Save")) {
            try {
            	title = title.replaceAll("/", "_");
                FileOutputStream out = new FileOutputStream("../../results/"+ title + ".png");
                BufferedImage img = new BufferedImage(this.getWidth(),this.getHeight(),BufferedImage.TYPE_INT_RGB);
                Graphics2D g = img.createGraphics();
                g.setClip(0,0,this.getWidth(),this.getHeight());
                g.setColor(Color.WHITE);
                g.fillRect(0,0,this.getWidth(),this.getHeight());
                this.paint(g);
                g.dispose();
                ImageIO.write(img,"png",out);
                out.close();
            } catch(Exception ex) {
                System.out.println("jMondrian : Error saving the image - " + ex);
            }
        }
    }
}
