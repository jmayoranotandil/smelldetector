/*
 *  JMondrian
 *  Copyright (c) 2007-2008 Loose Research Group
 *  Petru Florin Mihancea - petru.mihancea@cs.upt.ro
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package lrg.jMondrian.figures;

import lrg.jMondrian.painters.*;
import lrg.jMondrian.view.ViewRendererInterface;

public class EdgeFigure extends AbstractFigure {

    public static abstract class Connection {
        public abstract double computeX(Node n);
        public abstract double computeY(Node n);
    }

    public static final Connection MIDDLE = new Connection() {
        public double computeX(Node n) {
            return n.getAbsoluteX() + n.getWidth() / 2;
        }
        public double computeY(Node n) {
            return n.getAbsoluteY() + n.getHeight() / 2;
        }
    };

    public static final Connection UP_MIDDLE = new Connection() {
        public double computeX(Node n) {
            return n.getAbsoluteX() + n.getWidth() / 2;
        }
        public double computeY(Node n) {
            return n.getAbsoluteY();
        }
    };

    public static final Connection DOWN_MIDDLE = new Connection() {
        public double computeX(Node n) {
            return n.getAbsoluteX() + n.getWidth() / 2;
        }
        public double computeY(Node n) {
            return n.getAbsoluteY() + n.getHeight();
        }
    };


    private Node from;
    private Node to;
    private AbstractEdgePainter painter;
    private Connection fromType, toType;

    public EdgeFigure(Object entity, AbstractEdgePainter painter, Node from, Node to){
        super(entity);
        this.painter = painter;
        this.from = from;
        this.to = to;
        toType = fromType = MIDDLE;
    }

    public Node getFrom() {
        return from;
    }

    public Node getTo() {
        return to;
    }

    public void setConnectionStyle(Connection type, boolean select) {
        if(select) {
            this.toType = type;
        } else {
            this.fromType = type;
        }
    }

    public void show(ViewRendererInterface renderer) {
        painter.paint(renderer, entity, fromType.computeX(from), fromType.computeY(from), toType.computeX(to), toType.computeY(to));
    }
}
