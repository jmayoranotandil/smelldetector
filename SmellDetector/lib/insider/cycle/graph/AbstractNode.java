package graph;

public abstract class AbstractNode {

	protected int lowlink = -1;
	protected int index = -1;
	private int id;

	public AbstractNode() {
		super();
	}


	public void setId(int id){
		this.id = id;
	}
	
	public int getId() {
		return id;
	}

}