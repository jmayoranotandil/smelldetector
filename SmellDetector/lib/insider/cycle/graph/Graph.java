package graph;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class Graph {
	
	public Graph(AdjacencyList adjacencyList){
		this.adjacencyList = adjacencyList;
	}
	
	private final AdjacencyList adjacencyList;
	
	public ArrayList<ArrayList<AbstractNode>> findAllCycles(AbstractNode node){
		ArrayList<ArrayList<AbstractNode>> cycles = new ArrayList<ArrayList<AbstractNode>>();
		AdjacencyList sccAdjacencyList = strongConnectedComponentsAdjacencyList(node);
		int[][] b = sccAdjacencyList.getAdjencyList();
		int[] nodes = sccAdjacencyList.getNodes();
		ElementaryCyclesSearch elementaryCyclesSearch = new ElementaryCyclesSearch(b, nodes);
		List<List<Integer>> allCycles = elementaryCyclesSearch.getCycles(sccAdjacencyList.getId(node));
		Iterator<List<Integer>> iterator = allCycles.iterator();
		for (List<Integer> list : allCycles) {
			ArrayList<AbstractNode> aCyle = new ArrayList<AbstractNode>();
			for (Integer id : list) {
				aCyle.add(sccAdjacencyList.getNode(id));
			}
			cycles.add(aCyle);
		}
		return cycles;
	}

	public ArrayList<AbstractNode> findStrongConnectedComponents(AbstractNode node){
		Tarjan tarjan = new Tarjan();
		ArrayList<ArrayList<AbstractNode>> strongConnectedComponent = tarjan.tarjan(node, adjacencyList.copy());
		Iterator<ArrayList<AbstractNode>> iterator = strongConnectedComponent.iterator();
		while (iterator.hasNext()){
			ArrayList<AbstractNode> nodes = iterator.next();
			if (nodes.contains(node) && (nodes.size() > 1))
				return nodes;
		}
		return new ArrayList<AbstractNode>();
	}


	private AdjacencyList strongConnectedComponentsAdjacencyList(AbstractNode node){
		AdjacencyList list = new AdjacencyList();
		ArrayList<AbstractNode> strongConnectedComponents = findStrongConnectedComponents(node);
		Collection<Edge> allEdges = adjacencyList.getAllEdges();
		Iterator<Edge> iterator = allEdges.iterator();
		while (iterator.hasNext()){
			Edge edge = iterator.next();
			if (strongConnectedComponents.contains(edge.getTo()) && strongConnectedComponents.contains(edge.getFrom()))
				list.addEdge(edge.getFrom(), edge.getTo());
		}
		return list;
	}
	
	public ArrayList<AbstractNode> getConnectedTo(AbstractNode vertex, ArrayList<AbstractNode> nodes) {
		ArrayList<AbstractNode> allConnectedVertices = new ArrayList<AbstractNode>();
		if (adjacencyList == null)
			return allConnectedVertices;
		ArrayList<Edge> adjacentNodes = adjacencyList.getAdjacent(vertex);
		Iterator<Edge> iterator = adjacentNodes.iterator();
		while (iterator.hasNext()){
			AbstractNode to = iterator.next().getTo();
			if (nodes.contains(to))
				allConnectedVertices.add(to);
		}
		return allConnectedVertices;
	}
	public String toString(){
		return adjacencyList.toString();
	}
	
	/**
	 * detects if a node belongs to a cycle
	 * @param node - the start node
	 * @return true if the node belongs to a cycle, false otherwise
	 */
	public boolean belongstoCycle(AbstractNode node){
		return findStrongConnectedComponents(node).size() != 0;
	}
	
}
