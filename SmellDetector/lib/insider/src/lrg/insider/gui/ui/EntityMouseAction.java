package lrg.insider.gui.ui;

import java.awt.Component;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.plugins.filters.FilteringRule;
import lrg.common.abstractions.plugins.tools.AbstractEntityTool;
import lrg.insider.gui.ui.browser.BrowserUI;
import lrg.insider.gui.ui.codeviewer.CodeViewerUI;
import lrg.insider.gui.ui.stories.StoryTreeUI;
import lrg.insider.gui.ui.stories.StoryUI;
import lrg.insider.gui.ui.utils.ToolsStarter;
import lrg.insider.gui.ui.views.ViewUI;
import lrg.jMondrian.util.MenuReaction;
import lrg.jMondrian.view.ViewRenderer;
import lrg.memoria.core.Method;

public class EntityMouseAction implements MenuReaction, ActionListener {

    public static EntityMouseAction instance() {
        if (theInstance == null) {
            theInstance = new EntityMouseAction();
            ViewRenderer.setMenuReaction(theInstance);
        }
        return theInstance;
    }

    private static EntityMouseAction theInstance;

    private EntityMouseAction() {
        popup = new JPopupMenu();

        groupBuildersMenu = new JMenu("Open group...");
        groupBuildersMenu.getPopupMenu().setLayout(new GridLayout(0, 2));
        toolsMenu = new JMenu("Run tool...");
        vizualizationsMenu = new JMenu("Visualizing...");
        detailsMenu = new JMenu("Open detail in current browser...");
        detailsMenuInNewBrowser = new JMenu("Open detail in new browser...");

        popup.add(groupBuildersMenu);
        popup.addSeparator();
        popup.add(toolsMenu);
        popup.addSeparator();
        popup.add(vizualizationsMenu);
        popup.addSeparator();
        popup.add(detailsMenu);
        popup.add(detailsMenuInNewBrowser);
    }

    @SuppressWarnings("unchecked")
	public void addFiltersAndPropertiesToMenu() {
        MainForm.instance().propertiesMenu.removeAll();
        MainForm.instance().filtersMenu.removeAll();
        MainForm.instance().conformityMenu.removeAll();
        StoryTreeUI.instance().filteringRulesMenu.removeAll();
        StoryTreeUI.instance().filteringRulesMenu.add(StoryTreeUI.instance().applyCustomFilter);
        StoryTreeUI.instance().filteringRulesMenu.addSeparator();
        StoryTreeUI.instance().conformityRulesMenu.removeAll();
        StoryTreeUI.instance().conformityRulesMenu.add(StoryTreeUI.instance().applyCustomFilter);
        StoryTreeUI.instance().conformityRulesMenu.addSeparator();

        GroupEntity crtGroup = StoryTreeUI.instance().getSelectedView().getSelectedViewUI().
                getGroupEntity();
        if (crtGroup.getElements().size() < 1) {
            return;
        }
        ArrayList propertyList = crtGroup.getEntityTypeOfElements().nameAllPropertyComputers();
        MainForm.instance().propertiesMenu.setEnabled(propertyList.size() > 0);
        Iterator<String> it = (Iterator<String>) propertyList.iterator();
        while (it.hasNext()) {
            String currentGroupBuilderName = it.next();
            JMenuItem menuItem = new JMenuItem(currentGroupBuilderName);
            MainForm.instance().propertiesMenu.add(menuItem);
            menuItem.addActionListener(this);
        }

        ArrayList filterList = crtGroup.getEntityTypeOfElements().nameAllFilteringRules();
        MainForm.instance().filtersMenu.setEnabled(filterList.size() > 0);
        StoryTreeUI.instance().filteringRulesMenu.setEnabled(filterList.size() > 0);
        it = (Iterator<String>) filterList.iterator();
        while (it.hasNext()) {
            String currentFilterName = it.next();
            JMenuItem menuItem = new JMenuItem(currentFilterName);
            JMenu filterMenu = MainForm.instance().filtersMenu;
            filterMenu.add(menuItem);
            menuItem.addActionListener(this);

            menuItem = new JMenuItem(currentFilterName);
            StoryTreeUI.instance().filteringRulesMenu.add(menuItem);
            menuItem.addActionListener(this);
        }
        
        /*
         * TODO: _task Horia
         * aici nush de ce adauga de 2 ori aceeasi conformity rule
         */
        ArrayList<String> conformityList = crtGroup.getEntityTypeOfElements().nameAllConformityRules();
        MainForm.instance().conformityMenu.setEnabled(conformityList.size() > 0);
        StoryTreeUI.instance().conformityRulesMenu.setEnabled(conformityList.size() > 0);
        for (String conformity : conformityList) {
        	JMenuItem menuItem = new JMenuItem(conformity);
        	JMenu conformityMenu = MainForm.instance().conformityMenu;
        	conformityMenu.add(menuItem);
        	menuItem.addActionListener(this);
        	
        	menuItem = new JMenuItem(conformity);
        	StoryTreeUI.instance().conformityRulesMenu.add(menuItem);
        	menuItem.addActionListener(this);
        }
        
    }

    public void buildFor(Object anEntity, Object mouseEvent) {
        buildFor((AbstractEntityInterface)anEntity,(MouseEvent)mouseEvent);
    }

    public void buildFor(AbstractEntityInterface anEntity, MouseEvent e) {
        selectedEntity = anEntity;
        Iterator<String> it;

        if (e.getClickCount() == 1) {
            if (e.getButton() == MouseEvent.BUTTON1) {
                MainForm.instance().groupBuildersMenu.removeAll();

                if (selectedEntity != null) {
                    MainForm.instance().groupBuildersMenu.setEnabled(selectedEntity.getEntityType().nameAllGroupBuilders().size() > 0);
                    it = (Iterator<String>) selectedEntity.getEntityType().nameAllGroupBuilders().iterator();
                    while (it.hasNext()) {
                        String currentGroupBuilderName = it.next();
                        JMenuItem menuItem = new JMenuItem(currentGroupBuilderName);
                        MainForm.instance().groupBuildersMenu.add(menuItem);
                        // menuItem.setVisible(selectedEntity.getGroup(currentGroupBuilderName).iterator().hasNext());
                        menuItem.addActionListener(this);
                    }
                }
            }

            if (e.getButton() == MouseEvent.BUTTON2) {
                if (selectedEntity instanceof Method) {
                    Method aMethod = (Method) selectedEntity;
                    CodeViewerUI codeViewer = new CodeViewerUI(aMethod);
                    codeViewer.pack();
                    codeViewer.setVisible(true);
                } else
                    JOptionPane.showMessageDialog(e.getComponent(), "CodeViewer supports only methods so far.", "oops!!!", JOptionPane.INFORMATION_MESSAGE);
                return;
            }

            // e.getButton()==3 is here because on Win32 e.isPopupTrigger doesn't seem to work in the browserPage 
            if (e.isPopupTrigger() || e.getButton() == 3) {
                toolsMenu.removeAll();
                if (selectedEntity != null) {
                    toolsMenu.setEnabled(selectedEntity.getEntityType().nameAllTools().size() > 0);
                    it = (Iterator<String>) selectedEntity.getEntityType().nameAllTools().iterator();
                    while (it.hasNext()) {
                        String toolName = it.next();
                        JMenuItem menuItem = new JMenuItem(toolName);
                        toolsMenu.add(menuItem);
                        menuItem.addActionListener(this);
                    }
                }

                groupBuildersMenu.removeAll();
                if (selectedEntity != null) {
                    groupBuildersMenu.setEnabled(selectedEntity.getEntityType().nameAllGroupBuilders().size() > 0);
                    it = (Iterator<String>) selectedEntity.getEntityType().nameAllGroupBuilders().iterator();
                    while (it.hasNext()) {
                        String currentGroupBuilderName = it.next();
                        JMenuItem menuItem = new JMenuItem(currentGroupBuilderName);
                        groupBuildersMenu.add(menuItem);
                        // menuItem.setVisible(selectedEntity.getGroup(currentGroupBuilderName).iterator().hasNext());
                        menuItem.addActionListener(this);
                    }
                }

                vizualizationsMenu.removeAll();
                if (selectedEntity != null) {
                    vizualizationsMenu.setEnabled(selectedEntity.getEntityType().nameAllVisualizations().size() > 0);
                    it = (Iterator<String>) selectedEntity.getEntityType().nameAllVisualizations().iterator();
                    while (it.hasNext()) {
                        String currentVisualizationsName = it.next();
                        JMenuItem menuItem = new JMenuItem(currentVisualizationsName);
                        vizualizationsMenu.add(menuItem);
                        menuItem.addActionListener(this);
                    }
                }

                detailsMenu.removeAll();
                detailsMenuInNewBrowser.removeAll();
                if (selectedEntity != null) {
                    detailsMenu.setEnabled(selectedEntity.getEntityType().nameAllDetails().size() > 0);
                    detailsMenuInNewBrowser.setEnabled(selectedEntity.getEntityType().nameAllDetails().size() > 0);
                    it = (Iterator<String>) selectedEntity.getEntityType().nameAllDetails().iterator();
                    while (it.hasNext()) {
                        String currentDetailName = it.next();
                        JMenuItem menuItem = new JMenuItem(currentDetailName);
                        JMenuItem menuItemInNewBrowser = new JMenuItem(currentDetailName);
                        detailsMenu.add(menuItem);
                        detailsMenuInNewBrowser.add(menuItemInNewBrowser);
                        menuItem.addActionListener(this);
                        menuItemInNewBrowser.addActionListener(this);
                    }
                }

                popup.show(e.getComponent(), e.getX(), e.getY());
            }
        }

        if (e.getClickCount() == 2)         // catches double clicks
        {
            BrowserUI.instance().pointTo(selectedEntity.getProperty("Address").toString(), "Detail");
        }
    }

    public void actionPerformed(ActionEvent e) {    	
    	Component[] menuItems = MainForm.instance().groupBuildersMenu.getPopupMenu().getComponents();
        
        for (int i = 0; i < menuItems.length; i++)
            if (e.getSource() == menuItems[i]) {
                JMenuItem menuItem = (JMenuItem) e.getSource();
                GroupEntity newGroup = selectedEntity.getGroup(menuItem.getText());
                if (newGroup != null) {
                    if (newGroup.getElements().size() > 0) {
                        StoryTreeUI.instance().addStoryUI(new StoryUI(new ViewUI(newGroup)),
                                StoryTreeUI.BROTHER);
                    } else {
                        JOptionPane.showMessageDialog(null, "There is no entity in this group.\n",
                                "Empty group", JOptionPane.YES_NO_OPTION);
                    }
                }
                return;
            }

        Component[] filtersMenuItems = MainForm.instance().filtersMenu.getPopupMenu().getComponents();
        for (int i = 0; i < filtersMenuItems.length; i++)
            if (e.getSource() == filtersMenuItems[i]) {
                JMenuItem menuItem = (JMenuItem) e.getSource();

                ViewUI selectedViewUI = StoryTreeUI.instance().getSelectedView().getSelectedViewUI();
                FilteringRule aRule = (FilteringRule) selectedViewUI.getGroupEntity().getEntityTypeOfElements().findFilteringRule(menuItem.getText());
                selectedViewUI.applyFilter(aRule);

                return;
            }

        Component[] conformityMenuItems = MainForm.instance().conformityMenu.getPopupMenu().getComponents();
        for (int i = 0; i < conformityMenuItems.length; i++)
        	if (e.getSource() == conformityMenuItems[i]) {
        		JMenuItem menuItem = (JMenuItem) e.getSource();
        		
        		ViewUI selectedViewUI = StoryTreeUI.instance().getSelectedView().getSelectedViewUI();
        		if (selectedViewUI.hasPropertyCalled(menuItem.getText()))
        			selectedViewUI.removeColumnForPropertyCalled(menuItem.getText());
        		else
        			selectedViewUI.addColumnForPropertyCalled(menuItem.getText());
        		return;
        	}
        /*
         * TODO: _task Horia
         * adauga aici action pt conformity
         */
        
        Component[] propertiesMenuItems = MainForm.instance().propertiesMenu.getPopupMenu().getComponents();
        for (int i = 0; i < propertiesMenuItems.length; i++)
            if (e.getSource() == propertiesMenuItems[i]) {
                JMenuItem menuItem = (JMenuItem) e.getSource();

                ViewUI selectedViewUI = StoryTreeUI.instance().getSelectedView().getSelectedViewUI();

                if (selectedViewUI.hasPropertyCalled(menuItem.getText())) {
                    selectedViewUI.removeColumnForPropertyCalled(menuItem.getText());
                } else {
                    selectedViewUI.addColumnForPropertyCalled(menuItem.getText());
                }
//                        (String) selectedPropertyNames[i]);

                return;
            }

        menuItems = groupBuildersMenu.getPopupMenu().getComponents();
        for (int i = 0; i < menuItems.length; i++)
            if (e.getSource() == menuItems[i]) {
                JMenuItem menuItem = (JMenuItem) e.getSource();
                GroupEntity newGroup = selectedEntity.getGroup(menuItem.getText());
                if (newGroup != null) {
                    if (newGroup.getElements().size() > 0) {
                        StoryTreeUI.instance().addStoryUI(new StoryUI(new ViewUI(newGroup)),
                                StoryTreeUI.BROTHER);
                    } else {
                        JOptionPane.showMessageDialog(null, "There is no entity in this group.\n",
                                "Empty group", JOptionPane.YES_NO_OPTION);
                    }
                }
                return;
            }

        menuItems = toolsMenu.getPopupMenu().getComponents();
        for (int i = 0; i < menuItems.length; i++)
            if (e.getSource() == menuItems[i]) {
                JMenuItem menuItem = (JMenuItem) e.getSource();
                AbstractEntityTool aEntityTool = selectedEntity.getTool(menuItem.getText());
                ToolsStarter starter = new ToolsStarter(aEntityTool.getToolName(), aEntityTool.getParameterList(), aEntityTool.getParameterExplanations(), aEntityTool.getParameterInitialValue());
                try {
                    if (starter.dislay())
                        aEntityTool.run(selectedEntity, starter.getParameterValues());
                } catch (RuntimeException exc) {
                    System.err.println(aEntityTool.getToolName() + " could not be run !");
                    exc.printStackTrace();
                }
                return;
            }

        menuItems = vizualizationsMenu.getPopupMenu().getComponents();
        for (int i = 0; i < menuItems.length; i++)
            if (e.getSource() == menuItems[i]) {
                JMenuItem menuItem = (JMenuItem) e.getSource();
                selectedEntity.getEntityType().findVisualizations(menuItem.getText()).view(selectedEntity);
                return;
            }

        menuItems = detailsMenu.getPopupMenu().getComponents();
        for (int i = 0; i < menuItems.length; i++)
            if (e.getSource() == menuItems[i]) {
                JMenuItem menuItem = (JMenuItem) e.getSource();
                BrowserUI.instance().pointTo(selectedEntity.getProperty("Address").toString(), menuItem.getText());
                return;
            }

        menuItems = detailsMenuInNewBrowser.getPopupMenu().getComponents();
        for (int i = 0; i < menuItems.length; i++)
            if (e.getSource() == menuItems[i]) {
                JMenuItem menuItem = (JMenuItem) e.getSource();
                BrowserUI.instance().pointToInNewTab(selectedEntity.getProperty("Address").toString(), menuItem.getText());
                return;
            }

        JMenuItem menuItem = (JMenuItem) e.getSource();
        ViewUI selectedViewUI = StoryTreeUI.instance().getSelectedView().getSelectedViewUI();
        FilteringRule aRule = (FilteringRule) selectedViewUI.getGroupEntity().getEntityTypeOfElements().findFilteringRule(menuItem.getText());
        selectedViewUI.applyFilter(aRule);
    }


    private AbstractEntityInterface selectedEntity;

    private JPopupMenu popup;
    private JMenu groupBuildersMenu;
    private JMenu toolsMenu;
    private JMenu vizualizationsMenu;
    private JMenu detailsMenu;
    private JMenu detailsMenuInNewBrowser;
}
