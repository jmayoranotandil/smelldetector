package lrg.insider.gui.ui.utils;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class ToolsStarter extends JDialog implements ActionListener {

    public static ArrayList kindOfButtons;
    JButton okButton, cancelButton;
    JButton fileChoosers[];
    ArrayList<String> parameterNames, parameterToolTips, parameterInitialValues;
    ArrayList<String> params;
    ArrayList<JTextField> textFields;
    String toolName;
    private boolean validated = false;

    public ToolsStarter(String toolName, ArrayList<String> parameterNames, ArrayList<String> parameterToolTips, ArrayList<String> parameterInitialValues) {
        this.toolName = toolName;
        this.parameterNames = parameterNames;
        this.parameterToolTips = parameterToolTips;
        this.parameterInitialValues = parameterInitialValues;
    }

    public boolean dislay() {
        setTitle(toolName);
        GridLayout gl;
        if (kindOfButtons == null)
            gl = new GridLayout(parameterNames.size() + 1, 2);
        else
            gl = new GridLayout(parameterNames.size() + 1, 3);
        gl.setHgap(5); gl.setVgap(5);
        JPanel utilPanel = new JPanel(gl);

        int max = -1;
        for(String param : parameterNames) {
            if (param.length() > max)
                max = param.length();
        }

        JLabel label;
        fileChoosers = new JButton[parameterNames.size()];
        textFields = new ArrayList<JTextField>();
        for(int i = 0; i < parameterNames.size(); i++) {
            String param = parameterNames.get(i);
            label = new JLabel(param);
            label.setSize(max * 10, 25);
            utilPanel.add(label);

            JTextField current;
            if(parameterInitialValues.size()>0)
                current = new JTextField(parameterInitialValues.get(i));
            else
                current = new JTextField();

            textFields.add(current);
            label.setSize(100, 20);
            utilPanel.add(current);
            current.setToolTipText(parameterToolTips.get(i));
            if (kindOfButtons != null && i < kindOfButtons.size()) {
                if (((Integer)kindOfButtons.get(i)).intValue() == 1) {
                    fileChoosers[i] = new JButton("...");
                    fileChoosers[i].addActionListener(this);
                    fileChoosers[i].setSize(10, 10);
                    fileChoosers[i].setSize(10, 10);
                    utilPanel.add(fileChoosers[i]);
                } else
                    utilPanel.add(new JLabel(""));
            }
        }

        kindOfButtons = null;

        okButton = new JButton("Ok");
        okButton.addActionListener(this);
        utilPanel.add(okButton);

        cancelButton = new JButton("Cancel");
        cancelButton.addActionListener(this);
        utilPanel.add(cancelButton);

        BorderLayout bl = new BorderLayout();
        JPanel main = new JPanel(bl);

        Dimension d = new Dimension(10, 10);
        Box.Filler dummyNorth = new Box.Filler(d, d, d);
        Box.Filler dummySouth = new Box.Filler(d, d, d);
        Box.Filler dummyEast = new Box.Filler(d, d, d);
        Box.Filler dummyWest = new Box.Filler(d, d, d);
        main.add(dummyEast, BorderLayout.EAST);
        main.add(dummyWest, BorderLayout.WEST);
        main.add(dummyNorth, BorderLayout.NORTH);
        main.add(dummySouth, BorderLayout.SOUTH);
        main.add(utilPanel, BorderLayout.CENTER);

        getContentPane().add(main);
        setResizable(false);
        setModal(true);
        pack();
        this.setLocation(
                (int)(Toolkit.getDefaultToolkit().getScreenSize().getWidth() / 2 - this.getPreferredSize().getWidth()/2),
                (int)(Toolkit.getDefaultToolkit().getScreenSize().getHeight() / 2 - this.getPreferredSize().getHeight()/2)-50);

        setVisible(true);

        return validated;
    }

    public void actionPerformed(ActionEvent e) {
        for (int i = 0; i < fileChoosers.length; i++) {
            if (e.getSource() == fileChoosers[i]) {
                JFileChooser jfc = new JFileChooser();
                jfc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
                if ((jfc.showOpenDialog(this) == JFileChooser.APPROVE_OPTION))
                    textFields.get(i).setText(jfc.getSelectedFile().getPath());
                }
        }
        if (e.getSource() == okButton) {
            validated = true;
            params = new ArrayList<String>();
            for (JTextField jtf : textFields)
                params.add(jtf.getText());
            dispose();
        }
        if (e.getSource() == cancelButton) {
            validated = false;
            dispose();
        }
    }

    public ArrayList<String> getParameterValues() {
        return params;
    }
}
