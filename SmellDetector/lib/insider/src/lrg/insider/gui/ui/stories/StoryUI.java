package lrg.insider.gui.ui.stories;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.JDialog;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import lrg.common.abstractions.plugins.filters.FilteringRule;
import lrg.insider.gui.InsiderGUIMain;
import lrg.insider.gui.ui.filter.CustomFilterUI;
import lrg.insider.gui.ui.utils.ToolsStarter;
import lrg.insider.gui.ui.views.ViewUI;
import lrg.insider.metamodel.Address;

/**
 * Created by IntelliJ IDEA.
 * User: cristic
 * Date: May 3, 2004
 * Time: 3:17:55 PM
 * To change this template use File | Settings | File Templates.
 */
public class StoryUI extends MouseAdapter implements ActionListener, ChangeListener {
    public StoryUI(ViewUI aViewUI) {
        viewTabs = new JTabbedPane();
        viewTabs.addMouseListener(this);
        viewTabs.addChangeListener(this);
        viewUIs = new ArrayList();

        name = aViewUI.getGroupEntity().getName();

//        name = "Click here to rename this story";
        addViewUI(aViewUI);

        popup = new JPopupMenu();

        filteringRulesMenu = new JMenu("Apply Filtering Rule");
        applyCustomFilter = new JMenuItem("Custom");
        applyCustomFilter.addActionListener(this);
        selectColumnsMenuItem = new JMenuItem("Select Columns");
        selectColumnsMenuItem.addActionListener(this);

        printColumns = new JMenuItem("Print Columns");
        printColumns.addActionListener(this);

        popup.add(selectColumnsMenuItem);
        popup.addSeparator();
        popup.add(printColumns);
        popup.addSeparator();
        popup.add(filteringRulesMenu);
    }

    public Component getTopComponent() {
        return viewTabs;
    }

    public void addViewUI(ViewUI aViewUI) {
        viewUIs.add(aViewUI);
        viewTabs.add(aViewUI.getTopComponent());
        viewTabs.setSelectedComponent(aViewUI.getTopComponent());
        aViewUI.setStoryUI(this);
    }

    public void setName(String newName) {
        name = newName;
    }

    private void deleteViewUI() {
        // okToDelete() allow deletion of tab only if there's more than one left and if the tab to be deleted is not the <root> tab
        if (okToDelete()) {
            viewUIs.remove(viewTabs.getSelectedIndex());
            viewTabs.remove(viewTabs.getSelectedIndex());
        }
    }

    private boolean okToDelete() {
        return (viewUIs.size() > 1) && ((getSelectedViewUI()).getGroupEntity().getName().compareTo(Address.buildForRoot()) != 0);
    }

    public String toString() {
        return name;
    }

    public void stateChanged(ChangeEvent e) { // some other tab has been selected
        ((ViewUI) viewUIs.get(0)).updateGroupMenu();
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == applyCustomFilter) {
            ViewUI selectedViewUI = getSelectedViewUI();
            CustomFilterUI cfUI = new CustomFilterUI(selectedViewUI);
            JDialog customFilterDialog = new JDialog(InsiderGUIMain.getFrame(), "Custom filter editor", false);

            customFilterDialog.setContentPane(cfUI.getTopComponent());
            customFilterDialog.setSize(600, 400);
            customFilterDialog.setLocationRelativeTo(null);
            customFilterDialog.show();
            return;
        }

        if(e.getSource() == printColumns) {
           ViewUI selectedViewUI = getSelectedViewUI();
           ArrayList<String> paramNames = new ArrayList<String>(); paramNames.add("Filename");
           ArrayList<String> paramDescription = new ArrayList<String>(); paramDescription.add("File where table will be stored");
           ArrayList<String> initialValues = new ArrayList<String>(); initialValues.add("");

           ToolsStarter filenameDialog = new ToolsStarter("Table Printer", paramNames, paramDescription, initialValues);
           filenameDialog.dislay();
           String filename = filenameDialog.getParameterValues().get(0);
            if (filename.equals("")) System.out.println(selectedViewUI);
            else try {
                    PrintStream out_stream = new PrintStream(new FileOutputStream(filename));
                    out_stream.print(selectedViewUI);
                    out_stream.close();
            } catch (Exception ex) {}

           return;
        }

        if (e.getSource() == selectColumnsMenuItem) {
            ViewUI selectedViewUI = getSelectedViewUI();
            SelectColumnsUI x = new SelectColumnsUI(selectedViewUI);
            x.pack();
            x.show();
            x = null;
            return;
        }

        JMenuItem menuItem = (JMenuItem) e.getSource();
        ViewUI selectedViewUI = getSelectedViewUI();
        FilteringRule aRule = (FilteringRule) selectedViewUI.getGroupEntity().getEntityTypeOfElements().findFilteringRule(menuItem.getText());
        selectedViewUI.applyFilter(aRule);
    }

    public ViewUI getSelectedViewUI() {
        return (ViewUI) viewUIs.get(0);
    }

    public void mousePressed(MouseEvent e) {
        maybeShowPopup(e);
    }

    public void mouseReleased(MouseEvent e) {
        maybeShowPopup(e);
    }

    private void maybeShowPopup(MouseEvent e) {
        if (e.isPopupTrigger()) {
            ViewUI selectedViewUI = getSelectedViewUI();
            filteringRulesMenu.removeAll();
            filteringRulesMenu.add(applyCustomFilter);
            applyCustomFilter.setEnabled(selectedViewUI.getGroupEntity().size() > 1);
            filteringRulesMenu.addSeparator();

            Iterator it = selectedViewUI.getAllFilteringRules().iterator();
            while (it.hasNext()) {
                // FilteringRule currentRule = (FilteringRule) it.next();
                JMenuItem menuItem = new JMenuItem(it.next().toString());
                menuItem.addActionListener(this);
                filteringRulesMenu.add(menuItem);
            }

            popup.show(e.getComponent(), e.getX(), e.getY());
        }
    }

    JTabbedPane viewTabs;
    String name;

    ArrayList viewUIs;

    JPopupMenu popup;
    JMenuItem selectColumnsMenuItem;
    JMenuItem printColumns;
    JMenu filteringRulesMenu;
    JMenuItem applyCustomFilter;
}
