package lrg.insider.plugins.tools;

import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.plugins.filters.FilteringRule;
import lrg.common.abstractions.plugins.filters.composed.NotComposedFilteringRule;
import lrg.common.abstractions.plugins.tools.AbstractEntityTool;
import lrg.insider.gui.ui.utils.ProgressBar;
import lrg.insider.plugins.core.properties.AbstractDetail;
import lrg.insider.plugins.filters.StatisticalThresholds;
import lrg.insider.plugins.filters.memoria.classes.IsInner;


public class OverviewPyramid extends AbstractEntityTool {
    private ProgressBar progress;
    private lrg.memoria.core.System currentSystem;

    private GroupEntity methodGroup;
    private GroupEntity globalFunctionGroup;

    public OverviewPyramid() {
        super("OverviewPyramid", "Building the Overview Pyramid", "system");
    }

    private void initMembers(AbstractEntityInterface theSystem) {
        currentSystem = (lrg.memoria.core.System) theSystem;

        methodGroup = currentSystem.getGroup("method group").applyFilter("model function");
        globalFunctionGroup = currentSystem.getGroup("global function group").applyFilter("model function");
    }


    private String getColor(double value, double min, double avg, double max) {
        String red = "#FF0000", blue = "#0000FF", green = "#009900";

        double dist_min = Math.abs(value - min), dist_avg = Math.abs(value - avg), dist_max = Math.abs(value - max);

        if (dist_min < dist_avg) return blue;
        if (dist_avg < dist_max) return green;

        return red;
    }

    public void run(AbstractEntityInterface abstractEntityInterface, Object o) {
        if (abstractEntityInterface instanceof lrg.memoria.core.System == false) return;
        initMembers(abstractEntityInterface);
        ArrayList<String> params = (ArrayList<String>) o;
        if (params.get(0).equals(""))
            System.out.println(buildPyramid());
        else {
            try {
                PrintStream out_stream = new PrintStream(new FileOutputStream(params.get(0)));
                out_stream.print(buildPyramid());
                out_stream.close();
            } catch (Exception ex) {
            }
        }
    }

    public String buildHTMLPyramid(AbstractEntityInterface theSystem) {
        initMembers(theSystem);
        String result = "";
        double cyc = getCYCLO(), loc = getLOC(), nom = getNOM().size();
        double noc = getNOC().size(), nop = getNOP().size();
        double fout = getFANOUT(), call = getCALLS();
        double andd = getAVG_NDD(), ahit = getAVG_HIT();

        String anddColor = getColor(andd, StatisticalThresholds.NDD_LOW, StatisticalThresholds.NDD_AVG, StatisticalThresholds.NDD_HIGH);
        String ahitColor = getColor(ahit, StatisticalThresholds.HIT_LOW, StatisticalThresholds.HIT_AVG, StatisticalThresholds.HIT_HIGH);

        String fout_callColor = getColor(fout / call, StatisticalThresholds.FOUT_CALL_LOW, StatisticalThresholds.FOUT_CALL_AVG, StatisticalThresholds.FOUT_CALL_HIGH);
        String call_nomColor = getColor(call / nom, StatisticalThresholds.CALL_NOM_LOW, StatisticalThresholds.CALL_NOM_AVG, StatisticalThresholds.CALL_NOM_HIGH);

        String cyc_locColor = getColor(cyc / loc, StatisticalThresholds.CYCLO_LOC_LOW, StatisticalThresholds.CYCLO_LOC_AVG, StatisticalThresholds.CYCLO_LOC_HIGH);
        String loc_nomColor = getColor(loc / nom, StatisticalThresholds.LOC_NOM_LOW, StatisticalThresholds.LOC_NOM_AVG, StatisticalThresholds.LOC_NOM_HIGH);
        String nom_nocColor = getColor(nom / noc, StatisticalThresholds.NOM_NOC_LOW, StatisticalThresholds.NOM_NOC_AVG, StatisticalThresholds.NOM_NOC_HIGH);
        String noc_nopColor = getColor(noc / nop, StatisticalThresholds.NOC_NOP_LOW, StatisticalThresholds.NOC_NOP_AVG, StatisticalThresholds.NOC_NOP_HIGH);


//        result+="<br>System Overview: &ltNOT YET&gt<br>";
//        result+="numbers are real (I hope), colors are not<br>";
        result += "<table border=0 cellpadding=0 cellspacing=0>";

        result += "<tr>";
        result += "<td>&nbsp;</td>";
        result += "<td>&nbsp;</td>";
        result += "<td>&nbsp;</td>";
        result += "<td>&nbsp;</td>";
        result += "<td bgcolor=#99FF99><b>NDD</b>&nbsp;&nbsp;</td>";
        // result += "<td bgcolor=#99FF99><div align=right><font color=#FFFFFF><font bgcolor=" + anddColor + "><b> " + round(andd) + "&nbsp;</b></font></font></div></td>";
        result += "<td bgcolor=" + anddColor+ "><div align=right><font color=#FFFFFF>  " + round(andd) + "&nbsp; </font></div></td>";
        result += "<td>&nbsp;</td>";
        result += "</tr>";

        result += "<tr>";
        result += "<td>&nbsp;</td>";
        result += "<td>&nbsp;</td>";
        result += "<td>&nbsp;</td>";
        result += "<td>&nbsp;</td>";
        result += "<td bgcolor=#99FF99><b>HIT</b>&nbsp;&nbsp;</td>";
        // result += "<td bgcolor=#99FF99><div align=right><font color=#FFFFFF><font bgcolor=" + ahitColor + "><b> " + round(ahit) + "&nbsp;</b></font></font></div></td>";
        result += "<td bgcolor=" + ahitColor+ "><div align=right><font color=#FFFFFF>  " + round(ahit) + "&nbsp; </font></div></td>";
        result += "<td>&nbsp;</td>";
        result += "</tr>";

        result += "<tr>";
        result += "<td>&nbsp;</td>";
        result += "<td>&nbsp;</td>";
        result += "<td>&nbsp;</td>";
        result += "<td bgcolor=#FFFF33><font color=#FFFFFF><font bgcolor=" + noc_nopColor + "><b> " + round(noc / nop) + "&nbsp;</b></font></font></td>";
        result += "<td bgcolor=#FFFF33><b>NOP</b>&nbsp;</td>";
        // result += "<td bgcolor=#FFFF33><div align=right><b>" + AbstractDetail.linkToNumber(getNOP()) + "</b>&nbsp;</div></td>";
        result += "<td bgcolor=#FFFF33><div align=right> " + AbstractDetail.linkToNumber(getNOP()) + " &nbsp;</div></td>";
        result += "<td>&nbsp;</td>";
        result += "</tr>";

        result += "<tr>";
        result += "<td>&nbsp;</td>";
        result += "<td>&nbsp;</td>";
//      result += "<td bgcolor=#FFFF33><font color=#FFFFFF><font bgcolor=" + nom_nocColor + "><b> " + round(nom / noc) + "&nbsp;</b></font></font></td>";
		result += "<td bgcolor=" + nom_nocColor+ "><font color=#FFFFFF>  " + round(nom / noc) + "&nbsp; </font></td>";
        result += "<td bgcolor=#FFFF33><b>NOC</b>&nbsp;&nbsp;</td>";
        result += "<td bgcolor=#FFFF33>&nbsp;</td>";
        // result += "<td bgcolor=#FFFF33><div align=right><b>" + AbstractDetail.linkToNumber(getNOC()) + "<b>&nbsp;</div></td>";
        result += "<td bgcolor=#FFFF33><div align=right> " + AbstractDetail.linkToNumber(getNOC()) + " &nbsp;</div></td>";
        result += "<td>&nbsp;</td>";
        result += "</tr>";

        result += "<tr>";
        result += "<td>&nbsp;</td>";
        // result += "<td bgcolor=#FFFF33><font color=#FFFFFF><font bgcolor=" + loc_nomColor + "><b> " + round(loc / nom) + "&nbsp;</b></font></font></td>";
        result += "<td bgcolor=" + loc_nomColor+ "><font color=#FFFFFF>  " + round(loc / nom) + "&nbsp; </font></td>";
        result += "<td bgcolor=#FFFF33><b>NOM</b>&nbsp;&nbsp;</td>";
        result += "<td bgcolor=#FFFF33>&nbsp;</td>";
        result += "<td bgcolor=#FFFF33>&nbsp;</td>";
//        result += "<td bgcolor=#FFFF33><div align=right><b>" + AbstractDetail.linkToNumber(getNOM()) + "</b>&nbsp;</div></td>";
		result += "<td bgcolor=#FFFF33><div align=right> " + AbstractDetail.linkToNumber(getNOM()) + " &nbsp;</div></td>";
        result += "<td bgcolor=#FF99FF><div align=right><b>NOM</b>&nbsp;</div></td>";
//        result += "<td bgcolor=#FF99FF><div align=right><font color=#FFFFFF><font bgcolor=" + call_nomColor + "><b> " + round(call / nom) + "&nbsp;</b></font></font></div></td>";
		result += "<td bgcolor=" + call_nomColor + "><div align=right><font color=#FFFFFF>  " + round(call / nom) + "&nbsp; </font></div></td>";
        result += "<td>&nbsp;</td>";
        result += "</tr>";

        result += "<tr>";
        // result += "<td bgcolor=#FFFF33><font color=#FFFFFF><font bgcolor="+cyc_locColor+"><b> " + round(cyc / loc) + "&nbsp;</b></font></font></td>";
		result += "<td bgcolor=" + cyc_locColor + "><font color=#FFFFFF>  " + round(cyc / loc) + "&nbsp; </font></td>";
        result += "<td bgcolor=#FFFF33><b>LOC</b>&nbsp;&nbsp;</td>";
        result += "<td bgcolor=#FFFF33>&nbsp;</td>";
        result += "<td bgcolor=#FFFF33>&nbsp;</td>";
        result += "<td bgcolor=#FFFF33>&nbsp;</td>";
//        result += "<td bgcolor=#FFFF33><div align=right><b>" + intValue(loc) + "</b>&nbsp;</div></td>";
        result += "<td bgcolor=#FF99FF>&nbsp;<b>" + intValue(call) + "</b>&nbsp;</td>";
        result += "<td bgcolor=#FF99FF><div align=right>&nbsp;<b>CALL</b>&nbsp;</div></td>";
//        result += "<td bgcolor=#FF99FF><div align=right><font color=#FFFFFF><font bgcolor=" + fout_callColor + "><b> " + round(fout / call) + "&nbsp;</b></font></font></div></td>";
		result += "<td bgcolor=" + fout_callColor + "><div align=right><font color=#FFFFFF>  " + round(fout / call) + "&nbsp; </font></div></td>";
        result += "</tr>";

        result += "<tr>";
        result += "<td bgcolor=#FFFF33><b>CYC</b>&nbsp;&nbsp;</td>";
        result += "<td bgcolor=#FFFF33>&nbsp" + ";</td>";
        result += "<td bgcolor=#FFFF33>&nbsp;</td>";
        result += "<td bgcolor=#FFFF33>&nbsp;</td>";
        result += "<td bgcolor=#FFFF33>&nbsp;</td>";
        result += "<td bgcolor=#FFFF33><div align=right><b>" + intValue(cyc) + "</b>&nbsp;</div></td>";
        result += "<td bgcolor=#FF99FF><b>&nbsp;" + intValue(fout) + "&nbsp;</b></td>";
        result += "<td bgcolor=#FF99FF>&nbsp;</td>";
        result += "<td bgcolor=#FF99FF><div align=right>&nbsp;<b>FOUT</b>&nbsp;</div></td>";
        result += "</tr>";

        result += "</table>";

        return result;
    }

    private String buildPyramid() {
        String pyramid = new String();

        double cyclo = getCYCLO();
        double loc = getLOC();
        double nom = getNOM().size();
        double noc = getNOC().size();
        double nop = getNOP().size();

        double avgHIT = getAVG_HIT();
        double avgNDD = getAVG_NDD();

        double calls = getCALLS();
        double fanout = getFANOUT();

        pyramid += "\t\t\t\t" + "ANDC" + "\t" + round(avgNDD) + "\n";
        pyramid += "\t\t\t\t" + "AHH" + "\t" + round(avgHIT) + "\n";
        pyramid += "\t\t\t" + round(noc / nop) + "\t" + "NOP" + "\t" + nop + "\n";
        pyramid += "\t\t" + round(nom / noc) + "\t" + "NOC" + "\t\t" + noc + "\n";
        pyramid += "\t" + round(loc / nom) + "\t" + "NOM" + "\t" + "\t\t" + nom + "\t" + "NOM" + "\t" + round(calls / nom) + "\n";
        pyramid += round(cyclo / loc) + "\t" + "LOC" + "\t\t\t\t" + loc + "\t" + calls + "\t" + "CALLS" + "\t" + round(fanout / calls) + "\n";
        pyramid += "CYCLO" + "\t\t\t\t\t" + cyclo + "\t" + fanout + "\t\t" + "FANOUT" + "\n";

        return pyramid;
    }

    private double getCYCLO() {
        double cyclo;
        cyclo = ((Double) methodGroup.getProperty("CYCLO").aggregate("sum").getValue()).doubleValue();
        cyclo += ((Double) globalFunctionGroup.getProperty("CYCLO").aggregate("sum").getValue()).doubleValue();
        return cyclo;
    }

    private double getLOC() {
        double loc;
        loc = ((Double) methodGroup.getProperty("LOC").aggregate("sum").getValue()).doubleValue();
        loc += ((Double) globalFunctionGroup.getProperty("LOC").aggregate("sum").getValue()).doubleValue();
        return loc;
    }

    private GroupEntity getNOM() {
        return methodGroup.union(globalFunctionGroup);
    }

    private GroupEntity getNOC() {
        FilteringRule notInnerClass = new NotComposedFilteringRule(new IsInner());
        return currentSystem.getGroup("class group").applyFilter("model class").applyFilter(notInnerClass);
    }

    private GroupEntity getNOP() {
        return currentSystem.getGroup("package group").applyFilter("model package");
    }

    private double getAVG_NDD() {
        return ((Double) currentSystem.getProperty("AVG_NDD").getValue()).doubleValue();
    }

    private double getAVG_HIT() {
        return ((Double) currentSystem.getProperty("AVG_HIT").getValue()).doubleValue();
    }

    private double getCALLS() {
        double loc;
        loc = ((Double) methodGroup.getProperty("FANOUT").aggregate("sum").getValue()).doubleValue();
        loc += ((Double) globalFunctionGroup.getProperty("FANOUT").aggregate("sum").getValue()).doubleValue();
        return loc;
    }

    private double getFANOUT() {
        double loc;
        loc = ((Double) methodGroup.getProperty("FANOUTCLASS").aggregate("sum").getValue()).doubleValue();
        loc += ((Double) globalFunctionGroup.getProperty("FANOUTCLASS").aggregate("sum").getValue()).doubleValue();
        return loc;
    }

    private String round(double x) {
        String s = new String(x + "");
        int index = s.indexOf(".");
        if ((s.length() - index) > 2)
            return s.substring(0, index + 3);
        else
            return s;

    }

    private String intValue(double x) {
        return (int) x + "";
    }

    public String getToolName() {
        return "OverviewPyramid";
    }

    public ArrayList<String> getParameterList() {
        ArrayList<String> parList = new ArrayList<String>();
        parList.add("File name ");
        return parList;
    }

    public ArrayList<String> getParameterExplanations() {
        ArrayList<String> parList = new ArrayList<String>();
        parList.add("Name of the file which will store the overview pyramide");
        return parList;
    }
}