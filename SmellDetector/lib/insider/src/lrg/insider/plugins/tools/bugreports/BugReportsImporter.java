package lrg.insider.plugins.tools.bugreports;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.filters.FilteringRule;
import lrg.common.abstractions.plugins.filters.composed.NotComposedFilteringRule;
import lrg.common.abstractions.plugins.tools.AbstractEntityTool;
import lrg.common.metamodel.MetaModel;
import lrg.insider.plugins.filters.memoria.classes.IsInner;
import lrg.memoria.core.Bug;
import lrg.memoria.core.Class;
import lrg.memoria.core.Function;
import lrg.memoria.core.GlobalFunction;
import lrg.memoria.core.Method;
import lrg.memoria.core.NamedModelElement;
import lrg.memoria.core.Package;

import org.dom4j.DocumentException;
import org.xml.sax.SAXException;


//TODO exceptii la constructorul de bugprocessor, daca rateaza parserul (cel mai bine verifica fisierul inainte sa il incarci)
public class BugReportsImporter extends AbstractEntityTool {
    private lrg.memoria.core.System currentSystem;

    //initializate la inceput de initMembers()
	private ArrayList<Method> methodGroup;
    private ArrayList<GlobalFunction> globalFunctionGroup;
	private ArrayList<Package> packageGroup;
	private ArrayList<Class> classGroup;
	private ArrayList<lrg.memoria.core.File> fileGroup;

	
	//initializate de createEntityHashMaps() --> <numeObj, Obj>
	private HashMap<String, Package> packageMap=new HashMap<String, Package>();
	private HashMap<String, Class> classMap=new HashMap<String, Class>();
	private HashMap<String, lrg.memoria.core.File> fileMap=new HashMap<String, lrg.memoria.core.File>();
	private HashMap<String, Function> methodMap=new HashMap<String, Function>();

	
    public BugReportsImporter() {
        super("Import Bug Reports", "Includes Information from Bug Reports to Model", "system");
    }

	private void initMembers(AbstractEntityInterface theSystem) {
		 currentSystem = (lrg.memoria.core.System) theSystem;
		 FilteringRule notInnerClass = new NotComposedFilteringRule(new IsInner());
	        

		packageGroup = currentSystem.getGroup("package group").applyFilter("model package").getElements();
		fileGroup = currentSystem.getGroup("package group").applyFilter("model package").getGroup("file group").applyFilter("model file").getElements();

		classGroup = currentSystem.getGroup("class group").applyFilter("model class").applyFilter(notInnerClass).getElements();

		methodGroup = currentSystem.getGroup("method group").applyFilter("model function").getElements();
		globalFunctionGroup = currentSystem.getGroup("global function group").applyFilter("model function").getElements();
		
		createEntityHashMaps();
	}

    private String truncateFilename(String fullFilename) {
    	if(fullFilename.indexOf("FILE:") < 0) return fullFilename;

    	String prefix = "FILE:" + currentSystem.getFullName() + java.io.File.separator;
    	
    	String newName = fullFilename.substring(prefix.length());
		    	
    	return  "." + java.io.File.separator + newName;    	
    }

    private  void createEntityHashMaps() {
    	for(Package aPackage : packageGroup) packageMap.put(aPackage.getFullName(), aPackage);

    	for(lrg.memoria.core.File aFile : fileGroup) fileMap.put(truncateFilename(aFile.getFullName()), aFile);

    	for(Class aClass : classGroup) classMap.put(aClass.getFullName(), aClass);

    	for(Method aMethod : methodGroup) methodMap.put(aMethod.getScope().getFullName() + "." + aMethod.getName(), aMethod);
    	for(GlobalFunction aMethod : globalFunctionGroup) methodMap.put(aMethod.getScope().getFullName() + "." + aMethod.getName(),aMethod);
    }

    
    public void run(AbstractEntityInterface theSystem, Object theToolParameters) {
    	ArrayList<String> params = (ArrayList<String>) theToolParameters;

    	initMembers(theSystem);
    	
    	System.out.println("Processing entity list...");
    	Entity entity = new Entity(packageMap.keySet(), fileMap.keySet(), classMap.keySet(), methodMap.keySet());
    	System.out.println("Finished processing entity list!");
    	
    	String bugFile = params.get(0);
    	StringAnalyzer analyzer=new StringAnalyzer(entity);
    	BugProcessor bugProcessor;
    	HashMap<Bug, ArrayList<HashSet<String>>> output = null;
    	try {
    		bugProcessor = new BugProcessor(bugFile, analyzer);
    		
    		//se returneaza output, o mapare intre un obiect bug (cheie), si lista entitatilor referite:
			//0 pozitie -- lista pachete gasite
			//1 pozitie -- lista clase gasite
			//2 pozitie -- lista metode gasite
			//3 pozitie -- lista fisiere gasite 
    		output = bugProcessor.route();
    		
    		if(output != null) doMapping(output);
    		System.out.println("Bug file processed");
    		
    		String initialCacheName = MetaModel.instance().getCacheFilename();
    		lrg.memoria.core.System.serializeToFile(new File(initialCacheName), (lrg.memoria.core.System)theSystem);
    		System.out.println("Model serialized to file " + initialCacheName);

    		
    	} catch (DocumentException e) {
    		e.printStackTrace();
    	} catch (SAXException e) {
    		e.printStackTrace();
    	} catch (InvalidBugFileNameException e) {
			e.getMessage();
			e.printStackTrace();
		}    	

    }

    
    private void doMapping(HashMap<Bug, ArrayList<HashSet<String>>> bugsWithEntityNames) {
		// pentru fiecare Bug iterez peste lista cu listele de entitati pentru fiecare tip de entitate in parte -> HashMap (numeEntitate, obiect Bug)
    	// incerc sa fac match exact intre numele entitatii din entitiesWithBugs si cel din package/file/classes/methodsMap
    	// daca prindExact -> ex. fileMap.get(numeEntitate) ==> refe. entitate respectiva ()
    	
    	// lrg.memoria.core.File aFile = fileMap.get("bubu");

    	// Bug bug = entitiesWithBugs("bubu")
    	// aFile.addBug(bug);
    	NamedModelElement entitate=null;
    	for (Bug bug:bugsWithEntityNames.keySet()){
    		
    		//mapping with packages
    		for (String numePachet:bugsWithEntityNames.get(bug).get(0))
    			if ((entitate=packageMap.get(numePachet))!=null) {
    				System.out.println("Added Bug to package!");
    				bug.add(entitate);
    			}
    		
    		//mapping with classes
    		for (String numeClasa:bugsWithEntityNames.get(bug).get(1))
    			if ((entitate=classMap.get(numeClasa))!=null) {
    				System.out.println("Added Bug to classes!");
    				bug.add(entitate);
    			}
    		
    		//mapping with methods
    		for (String numeMetoda:bugsWithEntityNames.get(bug).get(2))
    			if ((entitate=methodMap.get(numeMetoda))!=null) {
    				System.out.println("Added Bug to method!");
    				bug.add(entitate);
    			}
    		
    		//mapping with files
    		for (String numeFisier:bugsWithEntityNames.get(bug).get(3))
    			if ((entitate=packageMap.get(numeFisier))!=null) {
    				System.out.println("Added Bug to file!");
    				bug.add(entitate);
    			}	
    		
    	}
    	System.out.println("Bugs mapped.\nFinished");
	}

	public String getToolName() {
        return "Import Bug Reports";
    }

    public ArrayList<String> getParameterList() {
        ArrayList<String> parList = new ArrayList<String>();
        parList.add("Bug Report File (to import from)");
        return parList;
    }

    public ArrayList<String> getParameterExplanations() {
        ArrayList<String> parList = new ArrayList<String>();
        parList.add("Name of the bug report file from which the bug descriptions will be added to model");
        return parList;
    }

}
