package lrg.insider.plugins.tools;

import java.util.ArrayList;
import java.util.Iterator;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.AbstractPlugin;
import lrg.common.abstractions.plugins.properties.PropertyComputer;
import lrg.common.abstractions.plugins.tools.AbstractEntityTool;
import lrg.common.metamodel.Loader;

public class PluginsGenerator extends AbstractEntityTool {
    public PluginsGenerator() {
        super("PluginsGenerator","Plugins Generator", "system");
    }

    public void run(AbstractEntityInterface anEntity, Object theToolParameters) {
        Loader loader = new Loader("classes");
        Iterator it = loader.getNames().iterator();
        int counter = 0;

        while (it.hasNext()) {
            AbstractPlugin someCommand = loader.buildFrom((String) it.next());
            if(someCommand instanceof PropertyComputer == false) continue;
            PropertyComputer aComputer = (PropertyComputer) someCommand;
            System.out.println("public ResultEntity "+
                    aComputer.getDescriptorObject().getName()+
                    "() { return super.getProperty(\""+ aComputer.getDescriptorObject().getName()+
                    "\"); }");
        }
    }


    public String getToolName() {
          return "PluginsGenerator";
    }

    public ArrayList<String> getParameterList() {
        ArrayList<String> parList = new ArrayList<String>();
        // parList.add("Filename");
        return parList;
    }

    public ArrayList<String> getParameterExplanations() {
        ArrayList<String> parList = new ArrayList<String>();
        // parList.add("Filename");
        return parList;
    }
}
