package lrg.insider.plugins.tools.bugreports;
//clasa ce pastreaza pozitia de start si de stop al unui cuvant dintr-un string
	class StringLimits
		implements Comparable<StringLimits>{
		private int start;
		private int stop;
		private String word;
		
		public StringLimits(String x, int start, int stop){
			this.word=x;
			this.start=start;
			this.stop=stop;
		}
		
		//calculeaza distanta intre 2 cuvinte din string
		public int stringDistance(StringLimits sl){
			return this.stop<sl.start?sl.start-this.stop:this.start-sl.stop;
		}
		
		//elementul cel mai mare este elementul cel mai apropiat de inceputul stringului
		//a.compareTo(b)<0	a<b
		//a.compareTo(b)=0  a=b
		//a.compareTo(b)>0  a>b
		public int compareTo(StringLimits t) {
			if (this.start>t.start)
				return -1; 
			else 
				if (this.start<t.start)
					return 1;
			return 0;
		}

		public String getName() {
			return this.word;
		}
	}