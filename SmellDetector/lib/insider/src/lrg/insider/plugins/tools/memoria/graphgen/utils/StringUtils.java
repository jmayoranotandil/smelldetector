package lrg.insider.plugins.tools.memoria.graphgen.utils;

public final class StringUtils {
	public static final String noDots(String s)
	{
		return s.replace('.', '_');
	}
}
