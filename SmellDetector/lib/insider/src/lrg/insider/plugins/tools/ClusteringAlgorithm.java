package lrg.insider.plugins.tools;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import lrg.common.abstractions.entities.AbstractEntity;
import lrg.common.abstractions.entities.GroupEntity;



public class ClusteringAlgorithm {
	private HashMap<AbstractEntity, GroupEntity> entityToPartnersMapper;
	private HashMap<GroupEntity, GroupEntity> resultClustersMap;
	
	public ClusteringAlgorithm(HashMap<AbstractEntity, GroupEntity> e2pm) {
		entityToPartnersMapper = new HashMap<AbstractEntity, GroupEntity>(e2pm);
		resultClustersMap = new HashMap<GroupEntity, GroupEntity>();

		GroupEntity crtCluster;

		for (Iterator<AbstractEntity> iterator = entityToPartnersMapper.keySet().iterator(); iterator.hasNext();) {
			AbstractEntity crtEntity = iterator.next();
			crtCluster = new GroupEntity("cluster", new ArrayList()); 
			crtCluster.add(crtEntity);
			resultClustersMap.put(crtCluster, entityToPartnersMapper.get(crtEntity));
		}
		
	}
	
	public HashMap<GroupEntity, GroupEntity> cluster() {
		GroupEntity firstCluster, firstInvokerClasses;
    	HashMap<GroupEntity, GroupEntity> newClusterMap; 
		int found = 0;
		
    	do {
    		found = 0;
    		newClusterMap =  new HashMap<GroupEntity, GroupEntity>(resultClustersMap);
	
    		Iterator<AbstractEntity> methodsIt = entityToPartnersMapper.keySet().iterator(); 
    		while(methodsIt.hasNext()) {
				int newfound = 0;
    			AbstractEntity crtMethod = methodsIt.next();
    			newClusterMap =  new HashMap<GroupEntity, GroupEntity>(resultClustersMap);
				for (Iterator<GroupEntity> clusterIt = resultClustersMap.keySet().iterator(); clusterIt.hasNext();) {
	    			GroupEntity crtCluster = clusterIt.next();
	    			GroupEntity crtInvokerClasses = resultClustersMap.get(crtCluster);
	    			if(crtCluster.isInGroup(crtMethod) == false) newfound += findAndUpdateClusters(newClusterMap, crtCluster, crtInvokerClasses, crtMethod, entityToPartnersMapper.get(crtMethod));    					
	    		}
				found += newfound;				
	    		resultClustersMap = newClusterMap;
	    		// if(newfound > 0) methodsIt = entityToPartnersMapper.keySet().iterator();
			} 
    		resultClustersMap = newClusterMap;
    	} while(found > 0);
		System.out.println(resultClustersMap.size() + " >> " + resultClustersMap);
		return resultClustersMap;
	}
	
    private int findAndUpdateClusters(HashMap<GroupEntity,GroupEntity> map, GroupEntity firstCluster, GroupEntity firstInvokerClasses, AbstractEntity crtMethod, GroupEntity secondInvokerClasses) {
    	GroupEntity commonInvokerClasses = firstInvokerClasses.intersect(secondInvokerClasses);  
    	ArrayList<AbstractEntity> firstClusterElements = firstCluster.getElements();
    	if(commonInvokerClasses.size() > 0) {    	
    		GroupEntity newCluster = new GroupEntity("cluster", new ArrayList(firstClusterElements));
    		newCluster.add(crtMethod);
    		if(shouldRemove(map, firstCluster, firstInvokerClasses, commonInvokerClasses)) {    			
    			map.remove(firstCluster);
    		}
    		if(isPresent(newCluster, map) == false) { 
    			map.put(newCluster, commonInvokerClasses); 
    			return 1; 
    		}
           	return 0;
    	}    	
		return 0;
    }


    private boolean isPresent(GroupEntity anEntity, HashMap<GroupEntity,GroupEntity> aMap) {
    	Iterator<GroupEntity> it = aMap.keySet().iterator();
    	
    	while(it.hasNext()) {
    		GroupEntity crtKey = it.next();
    		if(areEqual(crtKey, anEntity) || isContained(anEntity, crtKey)) return true;
    	}
    	return false;
    }
 	
    private boolean shouldRemove(HashMap<GroupEntity,GroupEntity> map, GroupEntity toRemove, GroupEntity toRemoveInvokers, GroupEntity commonInvokers) {
    	if(toRemoveInvokers.size() == commonInvokers.size()) return true;
    	GroupEntity containedInvokers = new GroupEntity("clustere", new ArrayList());
    	for (Iterator<GroupEntity> iterator = map.keySet().iterator(); iterator.hasNext();) {
			GroupEntity crtKey = iterator.next();
			if(isContained(toRemove, crtKey)) containedInvokers.addAll(map.get(crtKey));			
		}
    	if(toRemoveInvokers.size() == containedInvokers.distinct().size()) { 
    		return true;
    	}
    	return false;
    }

    private boolean areEqual(GroupEntity first, GroupEntity second) {
    	return ((first.exclude(second).size() == 0) && (second.exclude(first).size() == 0));
    }
    
    private boolean isContained(GroupEntity small, GroupEntity large) {
    	if(small.size() >= large.size()) return false;
    	
    	if(small.intersect(large).size() == small.size()) return true;
    	return false;
    }

	
}