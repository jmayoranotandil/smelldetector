package lrg.insider.plugins.tools.dat;

import java.util.ArrayList;
import java.util.Iterator;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.AbstractPlugin;
import lrg.common.abstractions.plugins.tools.AbstractEntityTool;
import lrg.common.metamodel.Loader;

public class DatesTool extends AbstractEntityTool {

    public DatesTool() {
        super("DATES", "Design Analysis Tool for Enterprise Systems", "system");
    }

    public void run(AbstractEntityInterface abstractEntityInterface, Object o) {
        if(abstractEntityInterface instanceof lrg.memoria.core.System == false) return;

        System.out.println("../dates/classes");
        Loader loader = new Loader("../dates/classes");
        Iterator it = loader.getNames().iterator();
        while (it.hasNext())
        {
            AbstractPlugin someCommand = loader.buildFrom((String) it.next(),"tools");
            if (someCommand != null)   {
                ((AbstractEntityTool)someCommand).run(abstractEntityInterface,o);
            }
        }
    }

    public String getToolName() {
        return "DATES";
    }

   public ArrayList<String> getParameterList() {
        ArrayList<String> parList = new ArrayList<String>();
        parList.add("DataSource Library  ");
        parList.add("Presentation Library");
        parList.add("Database name");
        return parList;
    }

    public ArrayList<String> getParameterExplanations() {
        ArrayList<String> parList = new ArrayList<String>();
        parList.add("DataSource Library  ");
        parList.add("Presentation Library");
        parList.add("Database name");
        return parList;
    }

    public ArrayList<String> getParameterInitialValue() {
        ArrayList<String> parList = new ArrayList<String>();
        parList.add("sql");
        parList.add("swing");
        parList.add("");
        return parList;
    }
}
