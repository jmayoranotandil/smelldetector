package lrg.insider.plugins.tools;

import java.io.FileOutputStream;
import java.io.PrintStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;

import lrg.common.abstractions.entities.AbstractEntity;
import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.tools.AbstractEntityTool;

// the strategy with how to get the group of other classes that I "cast" my vote for, i.e.
// the classes that USE ME!

abstract class AbstractCAListBuilder {
    protected abstract GroupEntity getCAGroup(AbstractEntityInterface aClass);

    public ArrayList<AbstractEntityInterface> buildCAListFor(AbstractEntityInterface aClass) {
        return getCAGroup(aClass).exclude((AbstractEntity)aClass).applyFilter("model class").distinct().getElements();
    }
}


class CALisIncludeInheritance extends AbstractCAListBuilder {
    protected GroupEntity getCAGroup(AbstractEntityInterface aClass) {
        return aClass.getGroup("all descendants").applyFilter("model class");
    }
}

class CAListIncludeAttributes extends AbstractCAListBuilder {
    protected GroupEntity getCAGroup(AbstractEntityInterface aClass) {
        return ((GroupEntity) aClass.getGroup("group of variables of this type").
                                     applyFilter("is attribute").belongsTo("class")).
                                     applyFilter("model class");
    }
}


class CAListIncludeParameters extends AbstractCAListBuilder {
    protected GroupEntity getCAGroup(AbstractEntityInterface aClass) {
       return ((GroupEntity) aClass.getGroup("group of variables of this type").
                                    applyFilter("is parameter").belongsTo("class")).
                                    applyFilter("model class");
    }
}


class ComposedCAListBuilder extends AbstractCAListBuilder {
    private ArrayList<AbstractCAListBuilder> listOfBuilders;
    public ComposedCAListBuilder(ArrayList<AbstractCAListBuilder> aListOfBuilders) {
        listOfBuilders = aListOfBuilders;
    }

    protected GroupEntity getCAGroup(AbstractEntityInterface aClass) {
       GroupEntity resultGroup = new GroupEntity("a group", new ArrayList());
       for(AbstractCAListBuilder crtBuilder : listOfBuilders) {
            resultGroup = resultGroup.union(crtBuilder.getCAGroup(aClass));
       }
       return resultGroup;
    }
}


public class PageRankComputer extends AbstractEntityTool {
    private static int NO_ITERATIONS = 40;
    private static double DAMPING_FACTOR = 0.85;
    private AbstractCAListBuilder caListBuilder;

    private HashMap<AbstractEntityInterface, PRStructure>  resultsTable;

    private ArrayList<AbstractCAListBuilder> prepareCollection() {
       ArrayList<AbstractCAListBuilder> theCollection = new ArrayList<AbstractCAListBuilder>();

        theCollection.add(new CALisIncludeInheritance());
//        theCollection.add(new CAListIncludeAttributes());
//        theCollection.add(new CAListIncludeParameters());
        return theCollection;
    }

    public PageRankComputer() {
        super("PageRankComputer","PageRankComputer", "system");
        resultsTable = new HashMap<AbstractEntityInterface, PRStructure> ();
        caListBuilder = new ComposedCAListBuilder(prepareCollection());
    }

    public void run(AbstractEntityInterface anEntity, Object theToolParameters) {
        try {
            PrintStream out_stream = new PrintStream(new FileOutputStream("D:\\temp\\"+anEntity.getName()+"-pagerank_"+NO_ITERATIONS+".txt"));
            out_stream.print(computePageRank(anEntity));
            out_stream.close();
        } catch (Exception ex) {}


        /*
        if (params.get(0).equals(""))
            System.out.println(computePageRank(anEntity));
        else {
        }
        */
    }



    private String computePageRank(AbstractEntityInterface theSystem) {
        ArrayList<AbstractEntityInterface> allModelClasses;
        allModelClasses = getAllModelClasses(theSystem);
        initPageRank(allModelClasses);
        for (int i = 0; i < NO_ITERATIONS; i++) {
            computeIteration(allModelClasses);
            updatePRForAllClasses(allModelClasses);
        }

        return printPRTable();
    }

    private ArrayList<AbstractEntityInterface> getAllModelClasses(AbstractEntityInterface theSystem) {
        return theSystem.getGroup("class group").applyFilter("model class").getElements();
    }




    private void initPageRank(ArrayList<AbstractEntityInterface> allModelClasses) {
        for (AbstractEntityInterface crtClass : allModelClasses) {
            // resultsTable.put(crtClass, new PRStructure(crtClass, initCAList(crtClass)));
            resultsTable.put(crtClass, new PRStructure(crtClass, caListBuilder.buildCAListFor(crtClass), 0));
        }

        for(AbstractEntityInterface crtClass : resultsTable.keySet()) {
            PRStructure crtPRStructure = resultsTable.get(crtClass);
            for(AbstractEntityInterface caClass : crtPRStructure.CAList) {
                PRStructure linkedPRStructure = resultsTable.get(caClass);
                if(linkedPRStructure != null)
                    linkedPRStructure.TList.add(crtPRStructure);
            }
        }
    }

    /*
    private ArrayList<AbstractEntityInterface> initCAList(AbstractEntityInterface crtClass) {
        GroupEntity typesOfAttributes = crtClass.getGroup("attribute group").getGroup("type of variable").applyFilter("model class");
        // GroupEntity typesOfParameters = crtClass.getGroup("parameter group").getGroup("type of variable");
        GroupEntity modelAncestors = crtClass.getGroup("all ancestors").applyFilter("model class");
        return typesOfAttributes.union(modelAncestors).distinct().exclude((AbstractEntity)crtClass).applyFilter("model class").getElements();
    }
    */

    private String printPRTable() {
        String resultString = "";
        DecimalFormat twoDecimals = new DecimalFormat("#0.00");
        for(AbstractEntityInterface crtClass : resultsTable.keySet()) {
            PRStructure crtPRStructure = resultsTable.get(crtClass);
            crtClass.addProperty("#PAGERANK#" , new ResultEntity(crtPRStructure));
            resultString += crtClass.getName() + "\t" + twoDecimals.format(crtPRStructure.getPR()) + "\n";
        }
        return resultString;
    }


    private void computeIteration(ArrayList<AbstractEntityInterface> allModelClasses) {
        for(AbstractEntityInterface crtClass : allModelClasses) {
            PRStructure crtPRStruct = resultsTable.get(crtClass);
            PRStructure newPRStruct = computeNewPRValue(crtPRStruct);

            resultsTable.put(crtClass, newPRStruct);
        }
    }

    private PRStructure computeNewPRValue(PRStructure crtPRStructure) {
        double newPR = 1 - DAMPING_FACTOR;

        for(PRStructure crtTLink : crtPRStructure.TList)
            if(crtTLink.CA != 0)
             newPR += DAMPING_FACTOR * (crtTLink.getPR() / crtTLink.CA);

        return new PRStructure(crtPRStructure, newPR);
    }


    private void updatePRForAllClasses(ArrayList<AbstractEntityInterface> allModelClasses) {
        for(AbstractEntityInterface crtClass : allModelClasses) {
            PRStructure crtPRStructure = resultsTable.get(crtClass);
            for(PRStructure crtTLink : crtPRStructure.TList) {
                PRStructure temp = resultsTable.get(crtTLink.theClass);
                if(temp != null) crtTLink.PR = temp.getPR();
            }
        }
    }

    public String getToolName() {
          return "PageRankComputer";
    }

    public ArrayList<String> getParameterList() {
        ArrayList<String> parList = new ArrayList<String>();
        // parList.add("Filename");
        return parList;
    }

    public ArrayList<String> getParameterExplanations() {
        ArrayList<String> parList = new ArrayList<String>();
        // parList.add("Filename");
        return parList;
    }
}
