package lrg.insider.plugins.tools.memoria.graphgen;

public final class EdgeConstants {
	public static final String PROPERTY_COLOUR = "colour";
	public static final String PROPERTY_LINKSTOFRONTIER = "linksToFrontier";
	public static final String PROPERTY_MULTIPLICITY = "multiplicity";
	public static final String PROPERTY_INHERITANCE = "inheritance";
}
