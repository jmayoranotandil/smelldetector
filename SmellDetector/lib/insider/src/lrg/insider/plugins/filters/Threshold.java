package lrg.insider.plugins.filters;


/**
 * Created by IntelliJ IDEA.
 * User: marinescu
 * Date: 20.04.2005
 * Time: 13:31:43
 * To change this template use File | Settings | File Templates.
 */
public class Threshold {
    public static final double NONE = 0;
    public static final double SHALLOW = 1;
    public static final double FEW = 2;
    public static final double DEEP = 3;
    public static final double MANY = 4;
    public static final double SMemCap = 7;

    public static final double ONE_QUARTER = 0.25;
    public static final double ONE_THIRD = 0.333;
    public static final double HALF = 0.5;
    public static final double TWO_THIRDS = 0.666;
    private static final double OUTLIER_FACTOR = 1.5;

    public static double WMC_AVG =
            StatisticalThresholds.CYCLO_LOC_AVG *
            StatisticalThresholds.LOC_NOM_AVG *
            StatisticalThresholds.NOM_NOC_AVG;

    public static double WMC_HIGH =
            StatisticalThresholds.CYCLO_LOC_HIGH *
            StatisticalThresholds.LOC_NOM_HIGH *
            StatisticalThresholds.NOM_NOC_HIGH;

    public static double WMC_VERYHIGH =
            WMC_HIGH * OUTLIER_FACTOR;

    public static double NOM_AVG =
            StatisticalThresholds.NOM_NOC_AVG;

    public static double NOM_HIGH =
            StatisticalThresholds.NOM_NOC_HIGH;

    public static double LOC_VERYHIGH =
            StatisticalThresholds.LOC_NOM_HIGH *
            StatisticalThresholds.NOM_NOC_HIGH *
            (OUTLIER_FACTOR - 1.0);

    public static double LOC_CLASS_VERYHIGH =
            StatisticalThresholds.LOC_NOM_HIGH *
            StatisticalThresholds.NOM_NOC_HIGH *
            OUTLIER_FACTOR;

    public static double AMW_AVG =
            StatisticalThresholds.CYCLO_LOC_AVG *
            StatisticalThresholds.LOC_NOM_AVG;
}