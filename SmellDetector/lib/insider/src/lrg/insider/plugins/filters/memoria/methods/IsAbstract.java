package lrg.insider.plugins.filters.memoria.methods;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.Descriptor;
import lrg.common.abstractions.plugins.filters.FilteringRule;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 19.02.2005
 * Time: 12:37:43
 * To change this template use File | Settings | File Templates.
 */
public class IsAbstract extends FilteringRule {
    public IsAbstract() {
        super(new Descriptor("is abstract", "method"));
    }

    public boolean applyFilter(AbstractEntityInterface anEntity) {
        if (anEntity instanceof lrg.memoria.core.Method == false) return false;

        return ((lrg.memoria.core.Method) anEntity).isAbstract();
    }
}

