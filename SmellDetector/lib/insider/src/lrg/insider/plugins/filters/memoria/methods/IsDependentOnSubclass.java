package lrg.insider.plugins.filters.memoria.methods;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.Descriptor;
import lrg.common.abstractions.plugins.filters.FilteringRule;

class IsDependentOnSubclass extends FilteringRule {
    public IsDependentOnSubclass() {
        super(new Descriptor("is dependent on subclasses", "method"));
    }

    public boolean applyFilter(AbstractEntityInterface aMethod) {
        return aMethod.getGroup("subclasses dependencies").size() > 0;
    }

}
