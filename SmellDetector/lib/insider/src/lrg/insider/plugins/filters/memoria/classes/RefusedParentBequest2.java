package lrg.insider.plugins.filters.memoria.classes;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.plugins.Descriptor;
import lrg.common.abstractions.plugins.filters.FilteringRule;
import lrg.common.abstractions.plugins.filters.composed.NotComposedFilteringRule;
import lrg.insider.plugins.filters.Threshold;
import lrg.insider.plugins.filters.memoria.methods.IsConstructor;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 05.11.2004
 * Time: 18:48:19
 * To change this template use File | Settings | File Templates.
 */
public class RefusedParentBequest2 extends FilteringRule {
    public RefusedParentBequest2() {
        super(new Descriptor("Refused Parent Bequest 2", "class"));
    }

    private double NOM = 0.0;

    private double BOvR(AbstractEntityInterface derivedClass) {
        GroupEntity baseclassMethods = derivedClass.uses("base classes").applyFilter("model class").
                contains("method group");
        double nrOfOverridenMethods = derivedClass.uses("methods overriden").intersect(baseclassMethods).size();
        return (nrOfOverridenMethods / NOM);

    }

    private double NumberOfProtectedMembersinBaseClass(AbstractEntityInterface derivedClass) {
        GroupEntity baseClasses = derivedClass.uses("base classes").applyFilter("model class");
        baseClasses.applyFilter(new NotComposedFilteringRule(new IsInterface()));
        if (baseClasses.size() == 0) return 0;

        GroupEntity protectedMethods = baseClasses.contains("method group").applyFilter("is protected");
        protectedMethods.applyFilter(new NotComposedFilteringRule(new IsConstructor()));
        GroupEntity protectedAttributes = baseClasses.contains("attribute group").applyFilter("is protected");

        return protectedAttributes.size() + protectedMethods.size();
    }

    private boolean childClassIgnoresBequest(AbstractEntityInterface derivedClass) {
        double NProtMInBase = NumberOfProtectedMembersinBaseClass(derivedClass);
        boolean lowUsageRatio = new FilteringRule("BUR", "<", "class", Threshold.ONE_THIRD).
                applyFilter(derivedClass);
        boolean lowOverridingRatio = BOvR(derivedClass) < Threshold.ONE_THIRD;

        GroupEntity calledClasses = ((GroupEntity) derivedClass.getGroup("operations called").belongsTo("class"));

        boolean usesbaseclass = calledClasses.applyFilter("model class").intersect(derivedClass.getGroup("base classes")).size() > 0;
        // return ((NProtMInBase > Threshold.FEW) && lowUsageRatio) && lowOverridingRatio;
        return usesbaseclass && (BOvR(derivedClass) == 0);
        // double bur = ((Double)derivedClass.getProperty("BUR").getValue()).doubleValue();
        // return (bur >= Threshold.ONE_THIRD);
    }

    private boolean childClassIsNotDwarf(AbstractEntityInterface derivedClass) {
        double amw = ((Double) derivedClass.getProperty("AMW").getValue()).doubleValue();
        double wmc = ((Double) derivedClass.getProperty("WMC").getValue()).doubleValue();

        return (amw > Threshold.AMW_AVG) &&
                (wmc > Threshold.WMC_AVG) &&
                (NOM > Threshold.NOM_AVG);
    }

    public boolean applyFilter(AbstractEntityInterface derivedClass) {
        FilteringRule notConstructors = new NotComposedFilteringRule(new IsConstructor());
        NOM = derivedClass.contains("method group").applyFilter(notConstructors).size();
        if (NOM == 0.0) return false;

        return (!new IsRootClass().applyFilter(derivedClass)) &&
                childClassIgnoresBequest(derivedClass) &&
                childClassIsNotDwarf(derivedClass);
    }
}
