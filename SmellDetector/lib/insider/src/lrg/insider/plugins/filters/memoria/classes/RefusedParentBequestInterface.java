package lrg.insider.plugins.filters.memoria.classes;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.Descriptor;
import lrg.common.abstractions.plugins.filters.FilteringRule;
import lrg.common.abstractions.plugins.filters.composed.NotComposedFilteringRule;
import lrg.insider.plugins.filters.Threshold;
import lrg.insider.plugins.filters.memoria.methods.IsConstructor;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 05.11.2004
 * Time: 18:48:19
 * To change this template use File | Settings | File Templates.
 */
public class RefusedParentBequestInterface extends FilteringRule {
    public RefusedParentBequestInterface() {
        super(new Descriptor("Refused Parent Bequest Interface", "class"));
    }

    private double NOM = 0.0;

    private boolean childClassIgnoresBequest(AbstractEntityInterface derivedClass) {
        FilteringRule manyOverwritingMethods = new FilteringRule("BOvM", ">", "class", Threshold.MANY);
        FilteringRule fewSpecializingMethods = new FilteringRule("NSPECM", "<", "class", 1);

        return manyOverwritingMethods.applyFilter(derivedClass) &&
                fewSpecializingMethods.applyFilter(derivedClass);
    }

    private boolean childClassIsNotDwarf(AbstractEntityInterface derivedClass) {
        double amw = ((Double) derivedClass.getProperty("AMW").getValue()).doubleValue();
        double wmc = ((Double) derivedClass.getProperty("WMC").getValue()).doubleValue();

        return (amw > Threshold.AMW_AVG) &&
                (wmc > Threshold.WMC_AVG) &&
                (NOM > Threshold.NOM_AVG);
    }

    public boolean applyFilter(AbstractEntityInterface derivedClass) {
        FilteringRule notConstructors = new NotComposedFilteringRule(new IsConstructor());
        NOM = derivedClass.contains("method group").applyFilter(notConstructors).size();
        if (NOM == 0.0) return false;

        return childClassIgnoresBequest(derivedClass) && childClassIsNotDwarf(derivedClass);
    }
}
