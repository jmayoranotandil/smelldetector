package lrg.insider.plugins.filters.memoria.methods;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.plugins.Descriptor;
import lrg.common.abstractions.plugins.filters.FilteringRule;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 07.02.2005
 * Time: 21:36:51
 * To change this template use File | Settings | File Templates.
 */
class EncapsulationTraitors extends FilteringRule {
    public EncapsulationTraitors() {
        super(new Descriptor("Encapsulation Traitors", "method"));
    }

    public boolean applyFilter(AbstractEntityInterface aMethod) {
        // FilteringRule isAccessor = new FilteringRule("IsAccessor", "IsTrue", "method");

        if (new IsAccessor().applyFilter(aMethod) == false) return false;

        GroupEntity attributesAccessed = aMethod.getGroup("variables accessed").applyFilter("is attribute").applyFilter("is protected");

        GroupEntity scopesOfAccessedAttributes = (GroupEntity) attributesAccessed.belongsTo("class");

        return scopesOfAccessedAttributes.intersect(aMethod.belongsTo("class").uses("all ancestors")).size() > 0;
    }
}