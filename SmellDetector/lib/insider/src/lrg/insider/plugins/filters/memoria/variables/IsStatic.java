package lrg.insider.plugins.filters.memoria.variables;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.Descriptor;
import lrg.common.abstractions.plugins.filters.FilteringRule;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 04.02.2005
 * Time: 18:54:46
 * To change this template use File | Settings | File Templates.
 */
public class IsStatic extends FilteringRule {
    public IsStatic() {
        // super(new Descriptor("is static", new String[]{"global variable", "attribute", "local variable", "parameter"}));
        super(new Descriptor("is static", "attribute"));
    }

    public boolean applyFilter(AbstractEntityInterface anEntity) {
        if (anEntity instanceof lrg.memoria.core.Attribute) return ((lrg.memoria.core.Attribute) anEntity).isStatic();

        if (anEntity instanceof lrg.memoria.core.GlobalVariable) return ((lrg.memoria.core.GlobalVariable) anEntity).isStatic();
        if (anEntity instanceof lrg.memoria.core.LocalVariable)
            return ((lrg.memoria.core.LocalVariable) anEntity).isStatic();

        else
            return false;
    }
}