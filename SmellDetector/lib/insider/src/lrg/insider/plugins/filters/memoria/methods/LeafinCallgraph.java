package lrg.insider.plugins.filters.memoria.methods;


import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.Descriptor;
import lrg.common.abstractions.plugins.filters.FilteringRule;

public class LeafinCallgraph extends FilteringRule {
    public LeafinCallgraph() {
        super(new Descriptor("leaf in callgraph", "method"));
    }

    public boolean applyFilter(AbstractEntityInterface aMethod) {
        return aMethod.uses("operations called").applyFilter("model function").size() == 0;
    }

}
