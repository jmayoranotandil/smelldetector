package lrg.insider.plugins.filters.memoria.variables;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.Descriptor;
import lrg.common.abstractions.plugins.filters.FilteringRule;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 05.02.2005
 * Time: 15:42:51
 * To change this template use File | Settings | File Templates.
 */
public class IsPublic extends FilteringRule {
    public IsPublic() {
        super(new Descriptor("not encapsulated", "attribute"));
    }

    public boolean applyFilter(AbstractEntityInterface anEntity) {
        if (anEntity instanceof lrg.memoria.core.Attribute == false) return false;

        return (((lrg.memoria.core.Attribute) anEntity).isPublic() ||
                ((lrg.memoria.core.Attribute) anEntity).isPackage());
    }
}
