package lrg.insider.plugins.filters.memoria.classes;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.plugins.Descriptor;
import lrg.common.abstractions.plugins.filters.FilteringRule;
import lrg.common.abstractions.plugins.filters.composed.NotComposedFilteringRule;
import lrg.insider.plugins.core.filters.memoria.ModelClassFilter;
import lrg.insider.plugins.filters.Threshold;


public class FutileHierarchy extends FilteringRule {
    public FutileHierarchy() {
        super(new Descriptor("Futile Hierarchy", "class"));
    }

    private boolean isSpecializingBaseclass(AbstractEntityInterface derivedClass, AbstractEntityInterface baseClass) {
        GroupEntity specializationMethods = derivedClass.getGroup("method group").applyFilter("is specialization");

        return (specializationMethods.size() > 0);
    }


    private boolean classIsNotInterfaceAndBelongsToModel(AbstractEntityInterface baseClass) {
        FilteringRule notInterface = new NotComposedFilteringRule(new IsInterface());
        return (new IsInterface().applyFilter(baseClass) == false) &&
                (new ModelClassFilter().applyFilter(baseClass));
    }

    private boolean classHasOneSingleChild(AbstractEntityInterface baseClass) {
        FilteringRule notInterface = new NotComposedFilteringRule(new IsInterface());
        if (baseClass.uses("all ancestors").applyFilter("model class").applyFilter(notInterface).size() > 0)
            return false;

        return (baseClass.isUsed("all descendants").size() == 1);
    }

    private boolean baseClassAbstractOrSignificantBehavior(AbstractEntityInterface baseClass) {
        double NAbsM = ((Double) baseClass.getProperty("NAbsM").getValue()).doubleValue();
        double AMW = ((Double) baseClass.getProperty("AMW").getValue()).doubleValue();
        double NOM = ((Double) baseClass.getProperty("NOM").getValue()).doubleValue();

        return (NAbsM > Threshold.NONE) ||
                ((AMW > Threshold.AMW_AVG) && (NOM > Threshold.NOM_AVG));
    }


    private boolean childDefinesSignificantBehaviourWithoutSpecialization(AbstractEntityInterface baseClass) {
        AbstractEntityInterface derivedClass = baseClass.isUsed("all descendants").getElementAt(0);

        boolean noSpecializationFromBaseClass = isSpecializingBaseclass(derivedClass, baseClass);

        double AMWDerived = ((Double) derivedClass.getProperty("AMW").getValue()).doubleValue();
        double PNASDerived = ((Double) derivedClass.getProperty("PNAS").getValue()).doubleValue();
        double NOMDerived = ((Double) derivedClass.getProperty("NOM").getValue()).doubleValue();

        return noSpecializationFromBaseClass &&
                (NOMDerived > Threshold.NOM_AVG) &&
                (PNASDerived > Threshold.HALF) || (AMWDerived > Threshold.AMW_AVG);
    }

    public boolean applyFilter(AbstractEntityInterface baseClass) {
        return classIsNotInterfaceAndBelongsToModel(baseClass) &&
                classHasOneSingleChild(baseClass) &&
                baseClassAbstractOrSignificantBehavior(baseClass) &&
                childDefinesSignificantBehaviourWithoutSpecialization(baseClass);
    }
}
