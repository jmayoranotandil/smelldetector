package lrg.insider.plugins.filters.memoria.packages;

import java.util.ArrayList;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.Descriptor;
import lrg.common.abstractions.plugins.filters.FilteringRule;

public class SDPViolationSubsystem extends FilteringRule {
    public SDPViolationSubsystem() {
        super(new Descriptor("SDP Violation (subsystem)", "subsystem"));
    }
    
    public static ArrayList<AbstractEntityInterface> SDPBreakers(AbstractEntityInterface aSubsystem) {
    	ArrayList<AbstractEntityInterface> sdpBreakers = new ArrayList<AbstractEntityInterface>();
    	
       	ArrayList<AbstractEntityInterface> temp = aSubsystem.getGroup("fanout class group").belongsTo("package").getProperty("Subsystem").getValueAsCollection();

       	if(temp == null) return sdpBreakers;
       	ArrayList<AbstractEntityInterface> usedSubsystems = new GroupEntity("subsystems", temp).distinct().getElements();
       	
    	double referenceIFValue = (Double)aSubsystem.getProperty("IF").getValue();
    	
    	for(AbstractEntityInterface crtSubsystemResult : usedSubsystems) {
    		AbstractEntityInterface crtSubsystem = (AbstractEntityInterface) ((ResultEntity)crtSubsystemResult).getValue(); 
        	double crtIFValue = (Double)crtSubsystem.getProperty("IF").getValue();    		
    		if(referenceIFValue < crtIFValue*0.9) sdpBreakers.add(crtSubsystem); 
    	}
    	return sdpBreakers;
    }
    
    public boolean applyFilter(AbstractEntityInterface aPackage) {    	    
    	return SDPBreakers(aPackage).size() > 0;
    }
}