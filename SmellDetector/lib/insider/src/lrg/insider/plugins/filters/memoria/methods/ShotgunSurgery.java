package lrg.insider.plugins.filters.memoria.methods;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.Descriptor;
import lrg.common.abstractions.plugins.filters.FilteringRule;
import lrg.insider.plugins.core.filters.memoria.ModelFunctionFilter;
import lrg.insider.plugins.filters.Threshold;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 23.02.2005
 * Time: 20:01:33
 * To change this template use File | Settings | File Templates.
 */
public class ShotgunSurgery extends FilteringRule {
    public ShotgunSurgery() {
        super(new Descriptor("Shotgun Surgery", "method"));
    }

    private boolean preconditionsFulfilled(AbstractEntityInterface aMethod) {
        return (new ModelFunctionFilter().applyFilter(aMethod)) &&
                !(new IsConstructor().applyFilter(aMethod)) &&
                !(new IsStatic().applyFilter(aMethod));
    }

    public boolean applyFilter(AbstractEntityInterface aMethod) {
        FilteringRule highCM = new FilteringRule("CM", ">", "method", Threshold.SMemCap);
        FilteringRule highCC = new FilteringRule("CC", ">=", "method", (Threshold.SMemCap / 2));

        return preconditionsFulfilled(aMethod) &&
                highCM.applyFilter(aMethod) &&
                highCC.applyFilter(aMethod);
    }
}
