package lrg.insider.plugins.filters.memoria.classes;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.Descriptor;
import lrg.common.abstractions.plugins.filters.FilteringRule;
import lrg.insider.plugins.filters.Threshold;


public class DataClass extends FilteringRule {
    public DataClass() {
        super(new Descriptor("Data Class", "class"));
    }

    public boolean applyFilter(AbstractEntityInterface anEntity) {
        if (new FilteringRule("WOC", "<", "class", Threshold.ONE_THIRD).
                applyFilter(anEntity) == false)
            return false;

        double totalNumberOfPublicData =
                ((Double) anEntity.getProperty("NOPA").getValue()).doubleValue() +
                (((Double) anEntity.getProperty("NOAM").getValue()).doubleValue());

        double wmc = ((Double) anEntity.getProperty("WMC").getValue()).doubleValue();


        boolean smallandDataClasss = (totalNumberOfPublicData > Threshold.FEW) &&
                (wmc < Threshold.WMC_HIGH);
        boolean largerButDataClass = (totalNumberOfPublicData > Threshold.MANY) &&
                (wmc < Threshold.WMC_VERYHIGH);

        return (smallandDataClasss || largerButDataClass);
    }
}
