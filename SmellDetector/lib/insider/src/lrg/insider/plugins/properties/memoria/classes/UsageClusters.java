package lrg.insider.plugins.properties.memoria.classes;

import java.util.ArrayList;
import java.util.HashMap;

import lrg.common.abstractions.entities.AbstractEntity;
import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.properties.PropertyComputer;
import lrg.insider.plugins.tools.ClusteringAlgorithm;


public class UsageClusters extends PropertyComputer {
 
	public UsageClusters() {
        super("Usage Clusters", "Usage Clusters", "class", "numerical");
    }

    private HashMap<AbstractEntity, GroupEntity> createInitialMap(AbstractEntityInterface measuredClass) {
		HashMap<AbstractEntity, GroupEntity> entity2Partners = new HashMap<AbstractEntity, GroupEntity>();
		GroupEntity externalInvokerClasses; 
		ArrayList<AbstractEntity> methods = measuredClass.contains("method group").applyFilter("is public").getElements();
    	for(AbstractEntity crtMethod : methods) {
    		externalInvokerClasses = ((GroupEntity)((AbstractEntity)crtMethod).getGroup("operations calling me").belongsTo("class")).distinct().exclude((AbstractEntity) measuredClass);
    		if(externalInvokerClasses.size() > 0) 
    			entity2Partners.put(crtMethod, externalInvokerClasses);
    	}
		
		return entity2Partners;
    }
    
    public ResultEntity compute(AbstractEntityInterface measuredClass) {
		HashMap<AbstractEntity, GroupEntity> initialMapping = createInitialMap(measuredClass);
		return new ResultEntity(new ClusteringAlgorithm(initialMapping).cluster());
    }        
 }

