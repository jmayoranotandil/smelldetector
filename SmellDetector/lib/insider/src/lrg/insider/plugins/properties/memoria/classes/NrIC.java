package lrg.insider.plugins.properties.memoria.classes;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.properties.PropertyComputer;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 03.03.2005
 * Time: 20:41:43
 * To change this template use File | Settings | File Templates.
 */
public class NrIC extends PropertyComputer {
    public NrIC() {
        super("NrIC", "", "class", "numerical");
    }

    public ResultEntity compute(AbstractEntityInterface measuredClass) {
        GroupEntity aGroup = measuredClass.getGroup("method group");

        return new ResultEntity(aGroup.applyFilter("Intensive Coupling").size());
    }
}
