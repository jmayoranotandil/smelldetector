package lrg.insider.plugins.properties.memoria.classes;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.properties.PropertyComputer;

public class NODD extends PropertyComputer {
    public NODD() {
        super("NODD", "Number of Direct Descendants", "class", "numerical");
    }

    public ResultEntity compute(AbstractEntityInterface measuredClass) {
        return new ResultEntity(measuredClass.getGroup("derived classes").size());
    }
}
