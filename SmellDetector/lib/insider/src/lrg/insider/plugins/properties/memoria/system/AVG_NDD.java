package lrg.insider.plugins.properties.memoria.system;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.filters.FilteringRule;
import lrg.common.abstractions.plugins.filters.composed.NotComposedFilteringRule;
import lrg.common.abstractions.plugins.properties.PropertyComputer;
import lrg.insider.plugins.filters.memoria.classes.IsInterface;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 12.01.2005
 * Time: 20:08:43
 * To change this template use File | Settings | File Templates.
 */
public class AVG_NDD extends PropertyComputer {
    public AVG_NDD() {
        super("AVG_NDD", "Average Number of Children for model classes", "system", "numerical");
    }

    public ResultEntity compute(AbstractEntityInterface anEntity) {
        FilteringRule notInterface = new NotComposedFilteringRule(new IsInterface());
        return anEntity.getGroup("class group").applyFilter("model class").applyFilter(notInterface).getProperty("NODD").aggregate("avg");
    }
}

