package lrg.insider.plugins.properties.memoria.packages;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.properties.PropertyComputer;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 26.01.2005
 * Time: 16:08:43
 * To change this template use File | Settings | File Templates.
 */
public class AbstractRatio extends PropertyComputer {
    public AbstractRatio() {
        super("AR", "Abstract Ratio", "package", "numerical");
    }

    public ResultEntity compute(AbstractEntityInterface anEntity) {
        double noc = ((Double) anEntity.getProperty("NOC").getValue()).doubleValue();

        if (noc == 0) return new ResultEntity(1);
        double abstractClasses = new ModelClassesInPackage().buildGroupEntity(anEntity).applyFilter("is abstract").size();

        return new ResultEntity(abstractClasses / noc);
    }
}