package lrg.insider.plugins.properties.memoria.classes;

import java.util.Iterator;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.properties.PropertyComputer;
import lrg.dude.duplication.Duplication;
import lrg.dude.duplication.DuplicationList;
import lrg.dude.duplication.MethodEntity;
import lrg.memoria.core.DataAbstraction;

public class IDUPLINES extends PropertyComputer {
    public IDUPLINES() {
        super("IDUPLINES", "Duplication Level Inside Class", "class", "numerical");
    }

    public ResultEntity compute(AbstractEntityInterface anEntity) {
        int result = 0;
        if (anEntity instanceof lrg.memoria.core.Class == false) return null;

        DataAbstraction theClass = (DataAbstraction) anEntity;
        lrg.memoria.core.Method aMethod;
        Iterator it = theClass.getMethodList().iterator();
        while (it.hasNext()) {
            aMethod = (lrg.memoria.core.Method) it.next();
            ResultEntity aResult = aMethod.getProperty("#DUPLICATION#");
            if (aResult == null || !(aResult.getValue() instanceof DuplicationList)) return new ResultEntity(0);
            DuplicationList aDuplicationList = (DuplicationList) aResult.getValue();
            if (aDuplicationList.size() > 0)
                result += countDuplicationLength(aDuplicationList);
        }

        return new ResultEntity(result);
    }

    private int countDuplicationLength(DuplicationList aDuplicationList) {
        int result = 0;
        MethodEntity reference = (MethodEntity) aDuplicationList.get(0).getReferenceCode().getEntity();

        for (int i = 0; i < aDuplicationList.size(); i++) {
            Duplication aDuplication = aDuplicationList.get(i);
            MethodEntity duplicator = (MethodEntity) aDuplication.getDuplicateCode().getEntity();
            if (reference.getMethod().belongsTo("class").equals(duplicator.getMethod().belongsTo("class")))
                if (aDuplication.isSelfDuplication())
                    result += aDuplication.copiedLength();
                else
                    result += aDuplication.copiedLength() / 2;
        }
        return result;
    }
}
