package lrg.insider.plugins.properties.memoria.methods;

import java.util.ArrayList;
import java.util.Iterator;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.properties.PropertyComputer;
import lrg.dude.duplication.Duplication;
import lrg.dude.duplication.DuplicationList;
import lrg.insider.plugins.groups.memoria.MethodsWithExternalDuplication;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 07.02.2005
 * Time: 16:18:37
 * To change this template use File | Settings | File Templates.
 */

public class EDUPLINES extends PropertyComputer {
    public EDUPLINES() {
        super("EDUPLINES", "Duplication level with other classes", "method", "numerical");
    }

    public ResultEntity compute(AbstractEntityInterface aMethod) {
        double result = 0.0;
        ResultEntity aResult = aMethod.getProperty("#DUPLICATION#");
        if (aResult == null) return new ResultEntity(result);

        // so here we have a little problem....
        // it is possible for  aMethod.getPropety()  to not always return a DuplicationList
        // in some cases it may return an ArrayList, for example when a property is
        // computed like AbstractEntity.getPropertyGroupFromSubparts(String)
        // (called from AbstractEntity.getProperty(String))

        // this little bug manifested itself when running Membrain Extension without
        // the DUDE tool and trying to see the detailes of a method

        // as a quick_fix some if (X instanceof DuplicationList) were placed in
        // EDUPLINES.compute(AbstractEntityInterface)
        // HDUPLINES.compute(AbstractEntityInterface)
        // IDUPLINES.compute(AbstractEntityInterface)
        // Detail.printDuplication(Function)
        if (aResult.getValue() instanceof DuplicationList) {
            ArrayList duplicators = MethodsWithExternalDuplication.getUnrelatedMethodsWithDuplication((DuplicationList) aResult.getValue());

            for (Iterator it = duplicators.iterator(); it.hasNext();)
                result += ((Duplication) it.next()).copiedLength();
        }

        return new ResultEntity(result);
    }
}
