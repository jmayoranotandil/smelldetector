package lrg.insider.plugins.properties.memoria.methods;

import lrg.common.abstractions.plugins.properties.PropertyComputer;
import lrg.insider.plugins.groups.memoria.MaxCallPath;


/**
 * Created by IntelliJ IDEA.
 * User: marinescu
 * Date: 19.05.2006
 * Time: 14:00:27
 * To change this template use File | Settings | File Templates.
 */
public class CallChainLength extends PropertyComputer {
    public CallChainLength() {
        super("CCL", "Call Chain Length", "method", "numerical");
        basedOnGroup(new MaxCallPath());
    }
/*
    public ResultEntity compute(AbstractEntityInterface aMethod) {

        if(new LeafinCallgraph().applyFilter(aMethod) == true) return new ResultEntity(0);

        GroupEntity calledMethods = aMethod.getGroup("operations called").distinct().applyFilter("model function");

        Double tmp = (Double)calledMethods.getProperty("CCL").aggregate("max").getValue();
        return new ResultEntity(tmp.doubleValue()+1);
    }
*/
}