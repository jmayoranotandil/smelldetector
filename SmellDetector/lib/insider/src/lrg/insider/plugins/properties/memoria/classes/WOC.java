package lrg.insider.plugins.properties.memoria.classes;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.filters.FilteringRule;
import lrg.common.abstractions.plugins.filters.composed.NotComposedFilteringRule;
import lrg.common.abstractions.plugins.properties.PropertyComputer;
import lrg.insider.plugins.filters.memoria.methods.IsConstructor;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 31.08.2004
 * Time: 20:54:44
 * To change this template use File | Settings | File Templates.
 */
public class WOC extends PropertyComputer {
    public WOC() {
        super("WOC", "Weight of a Class", "class", "numerical");
    }

    public ResultEntity compute(AbstractEntityInterface anEntity) {
        if (anEntity instanceof lrg.memoria.core.Class == false)
            return null;

        // NumericalResult aResult = (NumericalResult) new lrg.metrics.classes.WeightOfAClass().measure((lrg.memoria.core.Class) anEntity);

        FilteringRule notConstructor = new NotComposedFilteringRule(new IsConstructor());
        // FilteringRule isAccessor = new FilteringRule("IsAccessor", "IsTrue", "method");
        GroupEntity publicMethods = anEntity.contains("method group").applyFilter("is public").applyFilter(notConstructor);
        GroupEntity publicAttr = anEntity.contains("attribute group").applyFilter("not encapsulated");


        GroupEntity accessorMethods = publicMethods.applyFilter("is accessor");

        double accessorM = accessorMethods.size() + publicAttr.size() + 0.0;
        double publicM = publicMethods.size() + publicAttr.size() + 0.0;


        if (publicM == 0) return new ResultEntity(0);


        return new ResultEntity(1.00 - (accessorM / publicM));
    }
}
