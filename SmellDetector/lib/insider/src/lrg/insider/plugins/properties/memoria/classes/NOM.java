package lrg.insider.plugins.properties.memoria.classes;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.properties.PropertyComputer;
import lrg.memoria.core.DataAbstraction;

public class NOM extends PropertyComputer
{
    public NOM()
    {
        super("NOM", "Number of Methods", "class", "numerical");
    }

    public ResultEntity compute(AbstractEntityInterface anEntity)
    {
        if (anEntity instanceof lrg.memoria.core.Class == false)
            return null;
   
        return new ResultEntity(((DataAbstraction) anEntity).getMethodList().size());
    }
}
