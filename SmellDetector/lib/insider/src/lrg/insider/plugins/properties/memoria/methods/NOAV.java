package lrg.insider.plugins.properties.memoria.methods;

import lrg.common.abstractions.plugins.properties.PropertyComputer;
import lrg.insider.plugins.core.groups.memoria.uses.OperationAccesses;

public class NOAV extends PropertyComputer {
    public NOAV() {
        super("NOAV", "Number of Accessed Variables", new String[]{"method", "global function"}, "numerical");
        basedOnDistinctGroup(new OperationAccesses());
    }
}
