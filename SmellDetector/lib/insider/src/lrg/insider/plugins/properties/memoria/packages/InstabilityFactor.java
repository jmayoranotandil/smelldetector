package lrg.insider.plugins.properties.memoria.packages;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.properties.PropertyComputer;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 26.01.2005
 * Time: 16:15:08
 * To change this template use File | Settings | File Templates.
 */
public class InstabilityFactor extends PropertyComputer {
    public InstabilityFactor() {
        super("IF", "Instability Factor", "package", "numerical");
    }


    public ResultEntity compute(AbstractEntityInterface anEntity) {
        double fanout = anEntity.getGroup("fanout class group").size();
        double fanin = anEntity.getGroup("fanin class group").size();
        
        double totalDependencies = fanin + fanout;
        if (totalDependencies == 0) return new ResultEntity(2);

        return new ResultEntity(fanout / totalDependencies);
    }
}