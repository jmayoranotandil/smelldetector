package lrg.insider.plugins.properties.memoria.classes;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.properties.PropertyComputer;
import lrg.memoria.core.DataAbstraction;

public class InterfaceReuseIntention extends PropertyComputer {

    public InterfaceReuseIntention() {
        super("InterfaceReuseIntention", "Interface Reuse Intention", "class", "numerical");
    }

    public ResultEntity compute(AbstractEntityInterface aClass) {

        DataAbstraction theClass = (DataAbstraction)aClass;
        if((Double)theClass.getProperty("#AVG_TotalUniform#").getValue() < 0 || (Double)theClass.getProperty("#AVG_PartialUniform#").getValue() < 0) {
            return new ResultEntity(-1);
        }
        return new ResultEntity((Double)aClass.getProperty("#AVG_TotalUniform#").getValue() + 0.5 * (Double)aClass.getProperty("#AVG_PartialUniform#").getValue());
    }

}
