package lrg.insider.plugins.properties.memoria.system;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.filters.FilteringRule;
import lrg.common.abstractions.plugins.filters.composed.NotComposedFilteringRule;
import lrg.common.abstractions.plugins.properties.PropertyComputer;
import lrg.insider.plugins.filters.memoria.classes.IsInterface;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 14.01.2005
 * Time: 10:20:13
 * To change this template use File | Settings | File Templates.
 */
public class AVG_HIT extends PropertyComputer {
    public AVG_HIT() {
        super("AVG_HIT", "Average Height of Inheritance for model classes", "system", "numerical");
    }

    public ResultEntity compute(AbstractEntityInterface anEntity) {
        FilteringRule notInterface = new NotComposedFilteringRule(new IsInterface());
        GroupEntity allClasses = anEntity.getGroup("class group");
        GroupEntity rootClasses = allClasses.applyFilter("is root-class"); 
        ResultEntity hitResults = rootClasses.getProperty("HIT"); 
        return hitResults.aggregate("avg");
    }
}

