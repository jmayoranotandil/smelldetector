package lrg.insider.plugins.properties.memoria.methods;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.filters.FilteringRule;
import lrg.common.abstractions.plugins.filters.composed.NotComposedFilteringRule;
import lrg.common.abstractions.plugins.properties.PropertyComputer;
import lrg.memoria.core.Method;

public class PureCodeReuse extends PropertyComputer {

    public PureCodeReuse() {
        super("PCR", "Pure Code Reuse", "method", "numerical");
    }

    public ResultEntity compute(AbstractEntityInterface m) {

        if(!(m instanceof Method)) {
            return new ResultEntity(-1);
        }

        Method theMethod = (Method)m;

        GroupEntity allConcreteDescendants = theMethod.belongsTo("class").getGroup("all descendants").applyFilter(
                new NotComposedFilteringRule(
                            new FilteringRule("is abstract","is true", "class", null)));

        GroupEntity allConcretePure = theMethod.getGroup("applies-to group").applyFilter(
                new NotComposedFilteringRule(
                            new FilteringRule("is abstract","is true", "class", null)));
        allConcretePure = allConcretePure.exclude(theMethod.belongsTo("class"));

        if(allConcreteDescendants.size() == 0) {
            return new ResultEntity(-1);
        }

        if(theMethod.isConstructor() || theMethod.isPrivate() || theMethod.isStatic() || theMethod.isProtected() || theMethod.isPackage()) {
            return new ResultEntity(-1);
        }

        if(theMethod.isAbstract()) {
            return new ResultEntity(0);
        }

        return new ResultEntity(((double)allConcretePure.size()) / allConcreteDescendants.size());

    }
    
}
