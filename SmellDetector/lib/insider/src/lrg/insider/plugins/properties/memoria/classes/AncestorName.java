package lrg.insider.plugins.properties.memoria.classes;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.properties.PropertyComputer;

public class AncestorName extends PropertyComputer {
    public AncestorName() {
        super("ancestor name", "The name of the (first) ancestor of the class", "class", "string");
    }

    public ResultEntity compute(AbstractEntityInterface anEntity) {
        if (anEntity instanceof lrg.memoria.core.Class == false)
            return null;
        
        GroupEntity grp = anEntity.getGroup("base classes");
        if(grp.size() == 0)
        	return new ResultEntity("");
        return new ResultEntity(grp.getElementAt(0).getName());
    }
}
