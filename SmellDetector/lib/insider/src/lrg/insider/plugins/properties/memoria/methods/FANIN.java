package lrg.insider.plugins.properties.memoria.methods;

import lrg.common.abstractions.plugins.properties.PropertyComputer;
import lrg.insider.plugins.core.groups.memoria.uses.OperationIsCalled;

public class FANIN extends PropertyComputer {
    public FANIN() {
        super("FANIN", "Export Coupling", new String[]{"method", "global function"}, "numerical");
        basedOnGroup(new OperationIsCalled());
    }
}
