package lrg.insider.plugins.properties.memoria.classes;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.properties.PropertyComputer;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 05.02.2005
 * Time: 15:23:27
 * To change this template use File | Settings | File Templates.
 */
class NrDCUsages extends PropertyComputer {
    public NrDCUsages() {
        super("NrDCUsages", "Number of Usages of Data from Data Classes", "class", "numerical");
    }

    public ResultEntity compute(AbstractEntityInterface measuredClass) {
        GroupEntity scopeOfForeignData = (GroupEntity) measuredClass.getGroup("foreign data").belongsTo("class");
        return new ResultEntity(scopeOfForeignData.applyFilter("Data Class").size());
    }
}
