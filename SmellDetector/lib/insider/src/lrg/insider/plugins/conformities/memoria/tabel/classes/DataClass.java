package lrg.insider.plugins.conformities.memoria.tabel.classes;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.Descriptor;
import lrg.common.abstractions.plugins.conformities.ConformityRule;
import lrg.common.abstractions.plugins.conformities.composed.AddComposedConformityRule;
import lrg.common.abstractions.plugins.conformities.composed.ComposedMetricConformityRule;
import lrg.common.abstractions.plugins.conformities.thresholds.Threshold;

/**
 * Created by Horia Radu. User: horia Date: 21.09.2010 Time: 18:07:08 To change
 * this template use File | Settings | File Templates.
 */
public class DataClass extends ConformityRule {
	public DataClass() {
		super(new Descriptor("_Data Class_", "class"));
	}

	public Double applyConformity(AbstractEntityInterface anEntity) {
		ConformityRule cwRule = new ConformityRule("CW", "<", "class", Threshold.ONE_THIRD);
		String[] metrics = new String[2];
		metrics[0] = "NOPA";
		metrics[1] = "NOAM";
		ConformityRule nrData = new ComposedMetricConformityRule(metrics, ">", "class", Threshold.MANY); 
		
		ConformityRule result = new AddComposedConformityRule(cwRule, nrData);
		return result.applyConformity(anEntity) / result.getWeight();
	}
}