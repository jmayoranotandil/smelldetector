package lrg.insider.plugins.conformities.memoria.classes;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.Descriptor;
import lrg.common.abstractions.plugins.conformities.ConformityRule;
import lrg.common.abstractions.plugins.conformities.composed.AddComposedConformityRule;
import lrg.common.abstractions.plugins.conformities.weights.Weight;
import lrg.common.abstractions.plugins.filters.FilteringRule;
import lrg.common.abstractions.plugins.filters.composed.NotComposedFilteringRule;
import lrg.insider.plugins.filters.Threshold;
import lrg.insider.plugins.filters.memoria.methods.IsConstructor;

/**
 * Created by IntelliJ IDEA. User: user Date: 05.11.2004 Time: 18:48:19 To
 * change this template use File | Settings | File Templates.
 */
public class RefusedParentBequestInterface extends ConformityRule {
	public RefusedParentBequestInterface() {
		super(new Descriptor("__Refused Parent Bequest Interface", "class"));
	}

	private double NOM = 0.0;

	private ConformityRule childClassIgnoresBequest() {
		ConformityRule manyOverwritingMethods = new ConformityRule("BOvM", ">", "class", Threshold.MANY);
		manyOverwritingMethods.setWeight(Weight.HIGH.value());
		ConformityRule fewSpecializingMethods = new ConformityRule("NSPECM", "<", "class", 1);
		fewSpecializingMethods.setWeight(Weight.HIGH.value());

		return new AddComposedConformityRule(manyOverwritingMethods, fewSpecializingMethods);
	}

	private ConformityRule childClassIsNotDwarf() {
		ConformityRule amwRule = new ConformityRule("AMW", ">", "class", Threshold.AMW_AVG);
		amwRule.setWeight(Weight.LOW.value());
		ConformityRule wmcRule = new ConformityRule("WMC", ">", "class", Threshold.WMC_AVG);
		wmcRule.setWeight(Weight.LOW.value());
		ConformityRule nomRule = new ConformityRule("NOM", ">", "class", Threshold.NOM_AVG);
		nomRule.setWeight(Weight.LOW.value());
		ConformityRule result =  new AddComposedConformityRule(new AddComposedConformityRule(amwRule, wmcRule), nomRule);
		return result;
	}

	public Double applyConformity(AbstractEntityInterface derivedClass) {
		FilteringRule notConstructors = new NotComposedFilteringRule(new IsConstructor());
		NOM = derivedClass.contains("method group").applyFilter(notConstructors).size();
		if (NOM == 0.0)
			return 0.0;

		ConformityRule result = new AddComposedConformityRule(childClassIgnoresBequest(), childClassIsNotDwarf());
		return result.applyConformity(derivedClass) / result.getWeight();
	}
}
