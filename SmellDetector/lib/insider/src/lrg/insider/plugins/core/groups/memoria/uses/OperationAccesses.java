package lrg.insider.plugins.core.groups.memoria.uses;

import java.util.ArrayList;
import java.util.Iterator;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.groups.GroupBuilder;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 14.10.2004
 * Time: 17:57:21
 * To change this template use File | Settings | File Templates.
 */
public class OperationAccesses extends GroupBuilder {
    public OperationAccesses() {
        super("variables accessed", "", new String[]{"method", "global function"});
    }

    public ArrayList buildGroup(AbstractEntityInterface anEntity) {
        ArrayList resultList = new ArrayList();

        if (anEntity instanceof lrg.memoria.core.Function == false)
            return resultList;

        lrg.memoria.core.Function aMethod = (lrg.memoria.core.Function) anEntity;
        if (aMethod.getBody() == null) return resultList;

        if (aMethod.getBody().getAccessList() == null) return resultList;

        Iterator it = aMethod.getBody().getAccessList().iterator();
        while (it.hasNext()) {
            lrg.memoria.core.Access theAccess = (lrg.memoria.core.Access) it.next();
            resultList.add(theAccess.getVariable());
        }
        return resultList;
    }
}
