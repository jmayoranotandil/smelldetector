package lrg.insider.plugins.core.properties.memoria.inheritance;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.properties.PropertyComputer;
import lrg.memoria.core.InheritanceRelation;

public class Address extends PropertyComputer {

    public Address() {
        super("Address", "The address of the inheritance relation", "inheritance relation", "string");
    }

    public ResultEntity compute(AbstractEntityInterface anEntity) {

        if (!(anEntity instanceof InheritanceRelation)) {
            return null;
        }

        InheritanceRelation rel = (InheritanceRelation)anEntity;
        return new ResultEntity(lrg.insider.metamodel.Address.buildFor(rel.getSuperClass())+"<->"+lrg.insider.metamodel.Address.buildFor(rel.getSubClass()));

    }

}
