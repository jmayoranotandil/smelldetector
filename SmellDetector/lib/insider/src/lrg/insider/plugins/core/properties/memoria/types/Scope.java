package lrg.insider.plugins.core.properties.memoria.types;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.properties.PropertyComputer;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 14.10.2004
 * Time: 12:33:36
 * To change this template use File | Settings | File Templates.
 */
public class Scope extends PropertyComputer {
    public Scope() {
        super("scope", "The scope of the type", "type", "entity");
    }

    public ResultEntity compute(AbstractEntityInterface anEntity) {
        if (anEntity instanceof lrg.memoria.core.Type == false)
            return null;

        return new ResultEntity(((lrg.memoria.core.Type) anEntity).getScope());
    }
}
