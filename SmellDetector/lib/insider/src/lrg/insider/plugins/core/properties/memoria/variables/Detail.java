package lrg.insider.plugins.core.properties.memoria.variables;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.insider.plugins.core.properties.AbstractDetail;
import lrg.memoria.core.Attribute;
import lrg.memoria.core.GlobalVariable;
import lrg.memoria.core.Variable;

public class Detail extends AbstractDetail {
    public Detail() {
        super("Detail", "HTML Detail", new String[]{"global variable", "attribute", "local variable", "parameter"}, "string");
    }

    public ResultEntity compute(AbstractEntityInterface anEntity) {
        if (anEntity instanceof Variable == false) return null;

        Variable aVariable = (Variable) anEntity;

        String text = "<big>" + printKind(aVariable) + " ";
        if (aVariable instanceof Attribute) {
            Attribute aAttr = (Attribute) aVariable;
            text += "</big><b>" + getAccessModeHTML(aAttr.getAccessMode()) + "</b><big> ";
        }

        String fullTypeName = aVariable.getType().getFullName();
        int openBracketIndex = fullTypeName.indexOf("<");
        int closeBracketIndex = fullTypeName.indexOf(">");
        if ((openBracketIndex > 0) && (closeBracketIndex > 0)) {
            text += linkTo(fullTypeName.substring(0, openBracketIndex)) + "&lt;" +
                    linkTo(fullTypeName.substring(openBracketIndex + 1, closeBracketIndex - 1)) + "&gt;";
        } else
            text += linkTo(fullTypeName);

        text += " " + linkTo(aVariable.getScope()) + " :: " + linkTo(aVariable) + "</big><br>";
        if (aVariable instanceof lrg.memoria.core.Attribute)
            text += buildModifiers((lrg.memoria.core.Attribute) aVariable) + "<hr><br>";

        return new ResultEntity(text);
    }

    protected String printKind(Variable aVariable) {
        if (aVariable instanceof lrg.memoria.core.GlobalVariable)
            return "Global Variable";
        else if (aVariable instanceof lrg.memoria.core.Attribute)
            return "Attribute";
        else
            return "Variable";
    }

    private String buildModifiers(Variable v) {
        String text = "";
        String c = MethodDecorations.propertyColor;
        String d = MethodDecorations.endFont;

        if (v instanceof Attribute) {
            Attribute anAttribute = (Attribute) v;
            if (anAttribute.isPackage()) text += c + "package" + d;
            if (anAttribute.isStatic()) text += c + "static" + d;
        } else if (v instanceof GlobalVariable) {
            GlobalVariable gv = (GlobalVariable) v;
            if (gv.isStatic()) text += c + "static" + d;
        }

        if (v.isFinal()) text += c + "final" + d;
        return text;
    }
}
