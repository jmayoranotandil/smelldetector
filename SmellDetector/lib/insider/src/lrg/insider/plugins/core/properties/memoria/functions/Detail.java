package lrg.insider.plugins.core.properties.memoria.functions;

import java.util.ArrayList;
import java.util.Iterator;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.dude.duplication.Duplication;
import lrg.dude.duplication.DuplicationList;
import lrg.dude.duplication.MethodEntity;
import lrg.insider.plugins.core.details.HTMLDetail;
import lrg.insider.plugins.filters.memoria.methods.IsAccessor;
import lrg.insider.plugins.groups.memoria.MethodsWithExternalDuplication;
import lrg.insider.plugins.groups.memoria.MethodsWithHierarchyDuplication;
import lrg.memoria.core.Function;
import lrg.memoria.core.GlobalFunction;
import lrg.memoria.core.Method;
import lrg.memoria.core.Parameter;

public class Detail extends HTMLDetail {
    public Detail() {
        super("Detail", "Constructs a detailed HTML String to be shown in the browser.", new String[]{"method", "global function"});
    }

    private String buildModifiers(Method aMethod) {
        String c = MethodDecorations.propertyColor;
        String d = MethodDecorations.endFont;
        String text = "";
        if (aMethod.isAbstract()) text += c + "abstract" + d;
        if (new IsAccessor().applyFilter(aMethod)) text += c + "accessor" + d;
        if (aMethod.isConstructor()) text += c + "constructor" + d;
        if (aMethod.isFinal()) text += c + "final" + d;
        if (aMethod.isPackage()) text += c + "package" + d;
        if (aMethod.isStatic()) text += c + "static" + d;
        return text;
    }

    private String printDuplication(Function aFunction) {
        String htmlText = "";
        ResultEntity aResult = aFunction.getProperty("#DUPLICATION#");
        if (aResult == null) return "";

        // @see class EDUPLINES for the reason that this instanceof is here
        if (!(aResult.getValue() instanceof DuplicationList)) return "";

        ArrayList htmlLines = new ArrayList();

        ArrayList duplicators = lrg.insider.plugins.properties.memoria.methods.IDUPLINES.
                getMethodsWithIntraClassDuplication((DuplicationList) aResult.getValue());

        if (duplicators.iterator().hasNext())
            htmlText += "<b>" + MethodDecorations.flawColor + "Intraclass Duplication" + MethodDecorations.endFont + "</b><br>";

        htmlText += "<ul>";
        for (Iterator it = duplicators.iterator(); it.hasNext();) {
            MethodEntity duplicator = (MethodEntity) ((Duplication) it.next()).getDuplicateCode().getEntity();
            htmlText += "<li>"+"&nbsp;" + linkTo(duplicator.getMethod()) + "  [" + duplicator.getNoOfRelevantLines() + "]";
        }        
        htmlText += "</ul>";
        htmlText += bulletedList(htmlLines);

        duplicators = MethodsWithHierarchyDuplication.
                getMethodsWithHierarchyDuplication((DuplicationList) aResult.getValue());

        if (duplicators.iterator().hasNext())
            htmlText += "<b>" + MethodDecorations.flawColor + "Hierarchy Duplication" + MethodDecorations.endFont + "</b><br>";

        htmlText += "<ul>";        
        for (Iterator it = duplicators.iterator(); it.hasNext();) {
            MethodEntity duplicator = (MethodEntity) ((Duplication) it.next()).getDuplicateCode().getEntity();
            htmlText += "<li> &nbsp;" + linkTo(duplicator.getMethod().belongsTo("class")) + " :: " + linkTo(duplicator.getMethod()) + "  [" + duplicator.getNoOfRelevantLines() + "]";
        }
        htmlText += "</ul>";        


        duplicators = MethodsWithExternalDuplication.
                getUnrelatedMethodsWithDuplication((DuplicationList) aResult.getValue());

        if (duplicators.iterator().hasNext())
            htmlText += "<b>" + MethodDecorations.flawColor + "External Duplication" + MethodDecorations.endFont + "</b><br>";

        htmlText += "<ul>";        
        for (Iterator it = duplicators.iterator(); it.hasNext();) {
            MethodEntity duplicator = (MethodEntity) ((Duplication) it.next()).getDuplicateCode().getEntity();
            htmlText += "<li>&nbsp;" + linkTo(duplicator.getMethod().belongsTo("class")) + " :: " + linkTo(duplicator.getMethod()) + "  [" + duplicator.getNoOfRelevantLines() + "]";

        }
        htmlText += "</ul>";        

        return htmlText;
    }

    public ResultEntity compute(AbstractEntityInterface anEntity) {

        if (anEntity instanceof lrg.memoria.core.Function == false) return null;
/*
        FunctionViewer viewer = new FunctionViewer(((Function)anEntity).getBody().getLocation().getStartLine(),((Function)anEntity).getBody().getLocation().getStartChar(),((Function)anEntity).getBody().getSourceCode());
        BasicBlock[] tmp = ((XFunctionBody)((Function)anEntity).getBody()).getControlFlowGraph().getBasicBlocks();
        for(int i = 0; i < tmp.length; i++) {
            if(tmp[i].getLineSourceStartAnchor() != -1 && tmp[i].getColumnSourceStartAnchor() != -1) {
                viewer.markPosition(tmp[i].getLineSourceStartAnchor(),tmp[i].getColumnSourceStartAnchor(),"<font color=#800000><font bgcolor=#FFE0E0>");
            }
            if(tmp[i].getLineSourceEndAnchor() != -1 && tmp[i].getColumnSourceEndAnchor() != -1) {
                viewer.markPosition(tmp[i].getLineSourceStartAnchor(),tmp[i].getColumnSourceStartAnchor(),"</font>");
            }
        }
        return new ResultEntity(viewer.getHtmlCode());*/

        lrg.memoria.core.Function aFunction = (lrg.memoria.core.Function) anEntity;
        String returnType = "";
        if (aFunction.getReturnType() != null)
            returnType = linkTo(aFunction.getReturnType().getFullName());

        String text = "<big></big>";
        if (anEntity instanceof Method)
            text += getAccessModeHTML(((Method) anEntity).getAccessMode());
        
        
        String packageFullName = aFunction.getScope().getFullName();
        if(aFunction instanceof GlobalFunction) {
        	packageFullName = ((GlobalFunction)aFunction).getPackage().getFullName();
        }
        text += "<big> " + returnType + "  " + linkTo(packageFullName) + " :: " + linkTo(aFunction) + " ( ";

        Iterator paramIt = aFunction.getParameterList().iterator();
        // ArrayList list = new ArrayList();
        while (paramIt.hasNext()) {
            Parameter p = (Parameter) paramIt.next();
            String theParameter = linkTo(p.getType().getFullName()) + " " + linkTo(p.getFullName());
            // list.add(theParameter);
            text += theParameter;
            if (paramIt.hasNext()) text += ", ";
        }
        text += " )</big><br>";

        if (aFunction instanceof lrg.memoria.core.Method)
            text += buildModifiers((lrg.memoria.core.Method) anEntity) + "<br>";

        text += "<b>" + new MethodDecorations().getAfterDecoration(anEntity) + "</b><br><hr>";
        text += printDuplication(aFunction) + "<hr><br>";
        /*
        GroupEntity gp = new MethodOverrides().buildGroupEntity(aFunction), gp1, gp2, gp3;
        text += "<b>Methods overridden: </b>" + linkToNumber(gp) + "<hr><br>";


        GroupEntity accClasses = aFunction.getGroup("accessed model classes");
        GroupEntity accData = aFunction.getGroup("accessed model data");
        GroupEntity classAndAncestors = aFunction.getGroup("current class and ancestors");
        GroupEntity allMethodsAttributes = classAndAncestors.getGroup("method group").union(classAndAncestors.getGroup("attribute group"));


        text += "<b>Access to Local Data(ALD):</b> " + linkToNumber(accData.intersect(allMethodsAttributes)) + "<br>";

        text += "<b>Accesses to Foreign Data(ATFD):</b> " + linkToNumber(accData.exclude(allMethodsAttributes)) + " <br>";
        text += "<b> Foreign Data Providers(FDP):</b> " + linkToNumber(accClasses.exclude(classAndAncestors).distinct()) + " classes<hr><br>";


        gp = new ExternalServiceProviders().buildGroupEntity(aFunction);
        text += "<b>Coupling Intensity(CINT):</b> " + linkToNumber(gp) + "<br>";

        gp = ((GroupEntity) gp.belongsTo("class")).distinct().applyFilter("model class");
        text += "<b>Coupling Extent(CEXT):</b> " + linkToNumber(gp) + "<hr><br>";

        gp = new ChangingMethods().buildGroupEntity(aFunction);
        text += "<b>Changing Methods(CM):</b> " + linkToNumber(gp) + "<br>";

        gp = new ChangingClasses().buildGroupEntity(aFunction);
        text += "<b>Changing Classes(CC):</b> " + linkToNumber(gp) + "<br>";
        */
        return new ResultEntity(text);
    }
}
