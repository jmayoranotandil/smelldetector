package lrg.insider.plugins.core.properties.memoria.subsystems;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.properties.PropertyComputer;
import lrg.memoria.core.Subsystem;

/**
 * Created by IntelliJ IDEA.
 * User: marinescu
 * Date: 22.11.2006
 * Time: 12:57:29
 * To change this template use File | Settings | File Templates.
 */
public class Address extends PropertyComputer {
    public Address() {
        super("Address", "The address of the subsystem", "subsystem", "string");
    }

    public ResultEntity compute(AbstractEntityInterface anEntity) {
    	return new ResultEntity(lrg.insider.metamodel.Address.buildFor((Subsystem)anEntity));
    }
}
