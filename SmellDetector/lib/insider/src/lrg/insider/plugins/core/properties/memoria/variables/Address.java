package lrg.insider.plugins.core.properties.memoria.variables;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.properties.PropertyComputer;

public class Address extends PropertyComputer {
    public Address() {
        super("Address", "The address of the variable", new String[]{"global variable", "attribute", "local variable", "parameter"}, "string");
    }

    public ResultEntity compute(AbstractEntityInterface anEntity) {
        if (anEntity instanceof lrg.memoria.core.Variable == false)
            return null;

        return new ResultEntity(lrg.insider.metamodel.Address.buildFor((lrg.memoria.core.Variable) anEntity));
    }
}
