package lrg.insider.plugins.core.groups.memoria.containment;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.groups.GroupBuilder;
import lrg.memoria.core.ModelElementList;
import lrg.memoria.core.Type;

public class NamespaceHasTypes  extends GroupBuilder {

    public NamespaceHasTypes()
    {
        super("type group", "", "namespace");
    }

    public ModelElementList<Type> buildGroup(AbstractEntityInterface anEntity)
    {
        if (anEntity instanceof lrg.memoria.core.Namespace == false)
            return new ModelElementList<Type>();

        return ((lrg.memoria.core.Namespace)anEntity).getTypesList();
    }
}
