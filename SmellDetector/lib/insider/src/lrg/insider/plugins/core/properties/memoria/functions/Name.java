package lrg.insider.plugins.core.properties.memoria.functions;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.properties.PropertyComputer;

public class Name extends PropertyComputer
{
    public Name() {
        super("Name", "", new String[]{"method", "global function"}, "string");
    }

    public ResultEntity compute(AbstractEntityInterface anEntity)
    {
        if (anEntity instanceof lrg.memoria.core.Function == false)
            return null;

        return new ResultEntity(((lrg.memoria.core.Function) anEntity).getName());
    }
}
