package lrg.insider.plugins.core.groups.memoria.uses;

import java.util.ArrayList;
import java.util.Iterator;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.groups.GroupBuilder;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 14.10.2004
 * Time: 18:10:27
 * To change this template use File | Settings | File Templates.
 */
public class VariableIsAccessed extends GroupBuilder {
    public VariableIsAccessed() {
        super("methods accessing variable", "", new String[]{"global variable", "attribute", "local variable", "parameter"});
    }

    public ArrayList buildGroup(AbstractEntityInterface anEntity) {
        ArrayList resultList = new ArrayList();

        if (anEntity instanceof lrg.memoria.core.Variable == false)
            return resultList;

        lrg.memoria.core.Variable aVariable = (lrg.memoria.core.Variable) anEntity;
        if (aVariable.getAccessList() == null) return resultList;

        Iterator it = aVariable.getAccessList().iterator();
        while (it.hasNext()) {
            lrg.memoria.core.Access theAccess = (lrg.memoria.core.Access) it.next();
            resultList.add(theAccess.getScope().getScope());
        }
        return resultList;
    }

}
