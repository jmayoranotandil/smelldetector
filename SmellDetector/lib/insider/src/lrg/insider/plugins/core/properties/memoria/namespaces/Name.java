package lrg.insider.plugins.core.properties.memoria.namespaces;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.properties.PropertyComputer;

public class Name extends PropertyComputer
{
    public Name()
    {
        super("Name", "The name of the namespace", "namespace", "string");
    }

    public ResultEntity compute(AbstractEntityInterface anEntity)
    {
        if (anEntity instanceof lrg.memoria.core.Namespace == false)
            return null;

        return new ResultEntity(((lrg.memoria.core.Namespace) anEntity).getName());
    }
}