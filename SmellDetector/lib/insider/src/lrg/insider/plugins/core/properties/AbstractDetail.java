package lrg.insider.plugins.core.properties;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.metamodel.MetaModel;
import lrg.insider.plugins.filters.memoria.methods.BrainMethod;
import lrg.insider.plugins.filters.memoria.methods.ExtensiveCoupling;
import lrg.insider.plugins.filters.memoria.methods.FeatureEnvy;
import lrg.insider.plugins.filters.memoria.methods.IntensiveCoupling;
import lrg.insider.plugins.filters.memoria.methods.IsAccessor;
import lrg.insider.plugins.filters.memoria.methods.ShotgunSurgery;
import lrg.memoria.core.AccessMode;
import lrg.memoria.core.Attribute;
import lrg.memoria.core.Method;

public abstract class AbstractDetail extends lrg.common.abstractions.plugins.details.AbstractDetail {

    protected static interface ListDecorator {

        String getBeforeDecoration(AbstractEntityInterface enEntity);

        String getAfterDecoration(AbstractEntityInterface enEntity);
    }

    private static String htmlAble(String s) {
        return s.replaceAll(">","&gt;").replaceAll("<","&lt;");
    }
    public AbstractDetail(String name, String longName, String entityType, String resultEntityTypeName) {
        super(name, longName, entityType);
    }

    public AbstractDetail(String name, String longName, String[] entityTypes, String resultEntityTypeName) {
        super(name, longName, entityTypes);
    }


    public static String linkToNumber(GroupEntity anEntity) {
        return "<a href=\"" + anEntity.getProperty("Address") + "\" style=\"text-decoration:none\">" + anEntity.size() + "</a>";
    }

    public static String linkTo(AbstractEntityInterface anEntity) {
        return
            "<a href=\"" + anEntity.getProperty("Address") + "\" " +
               "style=\"text-decoration:none\">" +
                    htmlAble(anEntity.getProperty("Name")+"") +
            "</a>";
    }

    protected String linkTo(String name, String address) {
        return "<a href=\"" + address + "\" style=\"text-decoration:none\">" + htmlAble(name) + "</a>";
    }

    protected String linkTo(String address) {
        AbstractEntityInterface anEntity = MetaModel.instance().findEntityByAddress(address);

        if (anEntity != null)
            return linkTo(anEntity);
        else
            return address;
    }

    protected String bulletedLinkList(Collection listOfEntities, ListDecorator ld) {
        if (listOfEntities.size() < 1) return "";
        String text = "<ul>";
        Iterator it = listOfEntities.iterator();
        while (it.hasNext()) {
            AbstractEntityInterface theEntity = (AbstractEntityInterface) it.next();
            text += "<li>" + ((ld != null) ? ld.getBeforeDecoration(theEntity) : "") +
                    " " + linkTo(theEntity) + " " +
                    ((ld != null) ? ld.getAfterDecoration(theEntity) : "") + "</li>";
        }
        text += "</ul>";
        return text;
    }

    protected String bulletedLinkList(Collection listOfEntities) {
        return bulletedLinkList(listOfEntities, null);
    }

    protected String bulletedList(ArrayList listOfStrings) {
        if (listOfStrings.size() < 1) return "";
        String text = "<ul>";
        Iterator it = listOfStrings.iterator();
        while (it.hasNext())
            text += "<li>" + htmlAble(it.next()+"") + "</li>";
        text += "</ul>";
        return text;
    }

    protected String commaLinkList(ArrayList list) {
        String text = "";
        Iterator it = list.iterator();
        while (it.hasNext())
            text += linkTo((AbstractEntityInterface) it.next()) + ", ";
        text = text.substring(0, text.lastIndexOf(","));
        return text;
    }

    protected String image(String relativePath) {
        String workDir = java.lang.System.getProperty("user.dir");
        return "<img src=file://" + workDir + java.lang.System.getProperty("file.separator") + relativePath;
    }

    protected static String getAccessModeHTML(int mode) {
        switch (mode) {
            case AccessMode.PUBLIC:
                return "<font bgcolor=#C0F0C0>public</font>";
            case AccessMode.PROTECTED:
                return "<font bgcolor=#E0FFA0>protected</font>";
            case AccessMode.PACKAGE:
                return "<font bgcolor=#FFE0A0></font>";
            case AccessMode.PRIVATE:
                return "<font bgcolor=#FFC0C0>private</font>";
            default:
                return "<font bgcolor=#D0D0D0>unknown access mode</font>";
        }
    }

    public static class MethodDecorations implements AbstractDetail.ListDecorator {
        public static final String flawColor = "<font color=#800000><font bgcolor=#FFE0E0>";
        public static final String endFont = "</font></font>";
        public static final String propertyColor = "<font color=#606020><font bgcolor=#E0E0FF>";

        public String getBeforeDecoration(AbstractEntityInterface anEntity) {
            if ((anEntity instanceof Method) == false) return "";
            String c = propertyColor;
            String d = endFont;

            String txt = "<small>";
            txt += AbstractDetail.getAccessModeHTML(((Method) anEntity).getAccessMode());
            if (new IsAccessor().applyFilter(anEntity) == true) txt += "  " + c + "accessor" + d;
            txt += "</small>";
            return txt;
        }

        public String getAfterDecoration(AbstractEntityInterface anEntity) {
            if ((anEntity instanceof Method) == false) return "";
            String a = flawColor;
            String b = endFont + "&nbsp;";

            String sik = "";
            double edup = ((Double) anEntity.getProperty("EDUPLINES").getValue()).doubleValue();
            double hdup = ((Double) anEntity.getProperty("HDUPLINES").getValue()).doubleValue();
            double idup = ((Double) anEntity.getProperty("IDUPLINES").getValue()).doubleValue();

            if (edup + hdup + idup > 0) sik += a + "Duplication" + b;
            if (new BrainMethod().applyFilter(anEntity) == true) sik += a + "BrainMethod" + b;
            if (new FeatureEnvy().applyFilter(anEntity) == true) sik += a + "FeatureEnvy" + b;
            if (new IntensiveCoupling().applyFilter(anEntity) == true) sik += a + "IntensiveCoupling" + b;
            if (new ExtensiveCoupling().applyFilter(anEntity) == true) sik += a + "ExtensiveCoupling" + b;
            if (new ShotgunSurgery().applyFilter(anEntity) == true) sik += a + "ShotgunSurgery" + b;

            return sik;
        }
    }

    public static class AttributeDecorations implements AbstractDetail.ListDecorator {

        public String getBeforeDecoration(AbstractEntityInterface anEntity) {
            if ((anEntity instanceof Attribute) == false) return "";
            String txt = "<small>";
            txt += AbstractDetail.getAccessModeHTML(((Attribute) anEntity).getAccessMode());
            txt += "</small>";
            txt += ((Attribute) anEntity).getType().getName();
            return txt;
        }

        public String getAfterDecoration(AbstractEntityInterface anEntity) {
            return "";
        }
    }

}


