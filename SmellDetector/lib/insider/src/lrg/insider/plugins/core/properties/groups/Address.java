package lrg.insider.plugins.core.properties.groups;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.properties.PropertyComputer;

/**
 * Created by IntelliJ IDEA.
 * User: marinescu
 * Date: 05.04.2005
 * Time: 19:34:25
 * To change this template use File | Settings | File Templates.
 */
public class Address extends PropertyComputer {
    public Address() {
        super("Address", "The address of the group", "group", "string");
    }

    public ResultEntity compute(AbstractEntityInterface anEntity) {
        if (anEntity instanceof GroupEntity == false)
            return null;

        String tmp = anEntity.getName();

        tmp = tmp.replaceAll(" ", ".");
        return new ResultEntity(tmp);
    }
}
