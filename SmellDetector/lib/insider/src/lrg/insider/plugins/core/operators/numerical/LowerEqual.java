package lrg.insider.plugins.core.operators.numerical;

import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.operators.FilteringOperatorWithThresholds;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 04.11.2004
 * Time: 19:16:17
 * To change this template use File | Settings | File Templates.
 */
public class LowerEqual extends FilteringOperatorWithThresholds {
    public LowerEqual() {
        super("<=", "numerical");
    }

    public boolean apply(ResultEntity theResult, Object threshold) {
        if (theResult.getValue() instanceof Double == false) return false;
        if (threshold instanceof Double == false) return false;

        return ((Double) theResult.getValue()).compareTo((Double) threshold) <= 0;
    }
}

