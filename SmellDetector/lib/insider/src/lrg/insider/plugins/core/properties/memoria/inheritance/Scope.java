package lrg.insider.plugins.core.properties.memoria.inheritance;

import java.util.ArrayList;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.properties.PropertyComputer;
import lrg.memoria.core.InheritanceRelation;
import lrg.memoria.core.System;

public class Scope extends PropertyComputer {

    private static System currentSystem;
    private static ArrayList<InheritanceRelation> relation = new ArrayList<InheritanceRelation>();

    public static void clear() {
        relation.clear();
    }

    public static void registerScopeProperty(System s, InheritanceRelation rel) {
        currentSystem = s;
        relation.add(rel);
    }

    public static ArrayList getElements() {
        ArrayList tmp = new ArrayList();
        tmp.addAll(relation);
        return tmp;
    }

    public Scope() {
        super("scope", "The scope of the inheritance relation", "inheritance relation", "entity");
    }

    public ResultEntity compute(AbstractEntityInterface anEntity) {
        if (anEntity instanceof InheritanceRelation) {
            return new ResultEntity(currentSystem);
        } else {
            return null;
        }
    }

}

