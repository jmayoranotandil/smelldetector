package lrg.insider.plugins.core.filters.memoria;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.Descriptor;
import lrg.common.abstractions.plugins.filters.FilteringRule;
import lrg.memoria.core.Statute;


public class ModelFunctionFilter extends FilteringRule
{
    public ModelFunctionFilter()
    {
        super(new Descriptor("model function", "", new String[]{"method", "global function"}));
    }

    public boolean applyFilter(AbstractEntityInterface anEntity)
    {
        if (anEntity instanceof lrg.memoria.core.Function == false) return false;

        return ((lrg.memoria.core.Function) anEntity).getStatute() == Statute.NORMAL;
    }
}
