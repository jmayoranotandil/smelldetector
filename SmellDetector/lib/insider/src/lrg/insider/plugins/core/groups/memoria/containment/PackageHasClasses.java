package lrg.insider.plugins.core.groups.memoria.containment;

import java.util.ArrayList;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.groups.GroupBuilder;

public class PackageHasClasses extends GroupBuilder {
    public PackageHasClasses()
    {
        super("class group", "", "package");
    }

    public ArrayList buildGroup(AbstractEntityInterface anEntity)
    {
        if (anEntity instanceof lrg.memoria.core.Package == false)
            return new ArrayList();

        return ((lrg.memoria.core.Package)anEntity).getAbstractDataTypes();
    }
}
