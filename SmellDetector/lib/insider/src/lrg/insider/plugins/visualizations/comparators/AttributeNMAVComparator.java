package lrg.insider.plugins.visualizations.comparators;

import java.util.Comparator;

import lrg.common.abstractions.entities.AbstractEntity;

public class AttributeNMAVComparator implements Comparator {
	public int compare(Object o1, Object o2) {
		Double value1 = (Double) ((AbstractEntity)o1).getProperty("NMAV").getValue();
		Double value2 = (Double) ((AbstractEntity)o2).getProperty("NMAV").getValue();
		return (int) (value2 - value1);
	}	
}