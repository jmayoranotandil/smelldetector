package lrg.insider.plugins.visualizations;

import java.awt.Color;

public class OurColors {
	public static final double ORANGE = new Color(255,204, 0).getRGB();
	public static final double CYAN = new Color(0,255,255).getRGB();
	public static final double PINK = new Color(255,102,153).getRGB();
	public static final double RED = new Color(255,0,0).getRGB();
	public static final double ACCESSOR_BLUE = new Color(51, 51, 153).getRGB();
	public static final double ABSTRACT_BLUE = new Color(153, 153, 255).getRGB();
	public static final double BLUE = new Color(0,102,255).getRGB();
	public static final double GREEN = new Color(102,153,51).getRGB();
//	public static final double LILA = new Color(153,0,204).getRGB();
	public static final double LILA = new Color(102,51,204).getRGB();
	public static final double LIGHT_GRAY = new Color(230,230,230).getRGB();
	public static final double VERY_LIGHT_GRAY = new Color(245,245,245).getRGB();
///	public static final double LIGHT_LILA = new Color(240,220,255).getRGB();
	public static final double LIGHT_LILA = new Color(200,225,255).getRGB();
	public static final double LIGHT_RED = new Color(255,220,225).getRGB();
	public static final double BLACK = new Color(0,0,0).getRGB();

}