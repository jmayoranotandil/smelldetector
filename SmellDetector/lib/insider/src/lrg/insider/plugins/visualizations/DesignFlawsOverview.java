package lrg.insider.plugins.visualizations;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;

import lrg.common.abstractions.entities.AbstractEntity;
import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.plugins.filters.FilteringRule;
import lrg.common.abstractions.plugins.filters.composed.NotComposedFilteringRule;
import lrg.common.abstractions.plugins.visualization.AbstractVisualization;
import lrg.insider.plugins.filters.memoria.classes.DataClass;
import lrg.insider.plugins.filters.memoria.classes.GodClass;
import lrg.insider.plugins.filters.memoria.methods.FeatureEnvy;
import lrg.insider.plugins.groups.memoria.InheritanceRelation;
import lrg.insider.util.Visualization;
import lrg.jMondrian.commands.AbstractFigureDescriptionCommand;
import lrg.jMondrian.commands.AbstractNumericalCommand;
import lrg.jMondrian.figures.Figure;
import lrg.jMondrian.layouts.FlowLayout;
import lrg.jMondrian.layouts.TreeLayout;
import lrg.jMondrian.painters.LineEdgePainter;
import lrg.jMondrian.painters.RectangleNodePainter;
import lrg.jMondrian.view.ViewRenderer;

class ClassComparator implements Comparator {
	public int compare(Object o1, Object o2) {
		AbstractEntityInterface e1 = (AbstractEntityInterface) o1;
		AbstractEntityInterface e2 = (AbstractEntityInterface) o2;
		
		double e1wmc = (Double)e1.getProperty("WMC").getValue() + 3;
		double e2wmc = (Double)e2.getProperty("WMC").getValue() + 3 ;
		
		return (int) (e2wmc - e1wmc);
	}
}

class HierarchyComparator implements Comparator {
	public int compare(Object o1, Object o2) {
		return (int)  ((GroupEntity)o1).size() - ((GroupEntity)o2).size();
	}	
}

public class DesignFlawsOverview extends AbstractVisualization {
	private Figure theFigure;

	public DesignFlawsOverview()  {
        super("Design Flaws Overview", "System Complexity", new String[]{"system", "package"});
    }

	public void view( AbstractEntityInterface entity) {
		final GroupEntity allClasses = entity.getGroup("class group").applyFilter("model class").applyFilter(new NotComposedFilteringRule(new FilteringRule("is interface","is true","class")));

		GroupEntity packageGroup = new GroupEntity("package group", new ArrayList());
		if (entity.getEntityType().getName().compareTo("package")==0){
			packageGroup.add(entity);
		}
		else{
			packageGroup.addAll(entity.getGroup("package group"));
		}
		final GroupEntity allEdges = new GroupEntity("all inheritance relations", new ArrayList());
		Iterator<AbstractEntity> iterator = packageGroup.iterator();
		while (iterator.hasNext()){
			AbstractEntity packageEntity = iterator.next();
			allEdges.addAll(packageEntity.getGroup("all inheritance relations"));
			//allEdges = entity.getGroup("all inheritance relations");
		}



		ArrayList theLayers = new ArrayList();

		GroupEntity standaloneClasses = new GroupEntity("standalone", new ArrayList());
		ArrayList<AbstractEntityInterface> theRoots = allClasses.getElements();
		for (AbstractEntityInterface aRoot : theRoots) {
			if(aRoot.getGroup("all ancestors").intersect(allClasses).size() > 0) continue;
			GroupEntity descendants = aRoot.getGroup("all descendants").intersect(allClasses);
			if(descendants.size() == 0) standaloneClasses.add(aRoot);
			else {
				GroupEntity aTree = new GroupEntity("tree", new ArrayList());
				aTree.add(aRoot); aTree.addAll(descendants);
				theLayers.add(aTree);
			}		
		}

		Collections.sort(theLayers, new HierarchyComparator());			
		Collections.sort(standaloneClasses.getElements(), new ClassComparator());
		theLayers.add(standaloneClasses);			

		theFigure = new Figure();

		theFigure.nodesUsingForEach(theLayers, new RectangleNodePainter(false),  new AbstractFigureDescriptionCommand(){
			public Figure describe(){
				Figure f = new Figure();	
				GroupEntity aLayer = (GroupEntity) receiver;
				f.nodesUsing(aLayer.getElements(), new RectangleNodePainter(true)
				.width(Visualization.metricCommand("NOA", 5))
				.height(Visualization.metricCommand("NOM", 5))
				.name(Visualization.stringCommand("Name"))
				.color(new AbstractNumericalCommand() {
					public double execute() {
						if(hasClassDesignFlaw((AbstractEntity) receiver)){ return OurColors.RED;}
						else if(hasMethodDesignFlaw((AbstractEntity) receiver)) { return Color.YELLOW.getRGB();}
						else return OurColors.GREEN;
					}

					private boolean hasClassDesignFlaw(AbstractEntity aClass) {
						if(new GodClass().applyFilter(aClass) ||
								new DataClass().applyFilter(aClass)) return true;						
						return false;
					}

					private boolean hasMethodDesignFlaw(AbstractEntity aClass) {
						if(aClass.contains("method group").applyFilter(new FeatureEnvy()).size() > 0) return true;
						return false;
					}
				} ));
				if(aLayer.getName().equals("tree")) {
					Iterator<InheritanceRelation> it = allEdges.iterator();
					ArrayList edges = new ArrayList();
					while(it.hasNext()) {
						InheritanceRelation rel = it.next();
						if(aLayer.isInGroup(rel.getSubClass()) && aLayer.isInGroup(rel.getSuperClass())) {
							edges.add(rel);
						}
					}
					f.edgesUsing(edges, new LineEdgePainter(
							Visualization.entityCommand("getSubClass"),
							Visualization.entityCommand("getSuperClass"))
					.color( new AbstractNumericalCommand(){ 
						public double execute(){return Color.GRAY.getRGB();} 
					}));                    							
					f.layout(new TreeLayout(5,10));
				}
				else f.layout(new FlowLayout(10,10,300));
				return f;
			}
		});
		ViewRenderer r = new ViewRenderer("Design Flaws Overview on " + entity.getName());
		theFigure.layout(new FlowLayout(20,20, 500));
		theFigure.renderOn(r);
        r.open();
	}
}
