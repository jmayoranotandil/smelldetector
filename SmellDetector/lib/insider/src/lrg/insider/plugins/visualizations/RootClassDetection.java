package lrg.insider.plugins.visualizations;

import java.util.List;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.visualization.AbstractVisualization;
import lrg.insider.util.Visualization;
import lrg.jMondrian.figures.Figure;
import lrg.jMondrian.layouts.ScatterPlotLayout;
import lrg.jMondrian.painters.RectangleNodePainter;
import lrg.jMondrian.view.ViewRenderer;

public class RootClassDetection extends AbstractVisualization {

    public RootClassDetection() {
        super("Root Class Detection", "Root Class Detection", "system");
    }

    public void view(AbstractEntityInterface entity) {

        List set = entity.getGroup("class group").applyFilter("model classes").getElements();

        Figure v = new Figure();
        v.nodesUsing(set,new RectangleNodePainter(10,10,true).x(Visualization.metricCommand("NOD")).y(Visualization.metricCommand("NODD")));
        v.layout(new ScatterPlotLayout());

        ViewRenderer r = new ViewRenderer("RootClassDetection");
        v.renderOn(r);
        r.open();

    }
}
