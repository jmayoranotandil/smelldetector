package lrg.insider.plugins.visualizations;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Collections;

import lrg.common.abstractions.entities.AbstractEntity;
import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.EntityType;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.plugins.visualization.AbstractVisualization;
import lrg.insider.plugins.filters.memoria.methods.IsAbstract;
import lrg.insider.plugins.filters.memoria.methods.IsAccessor;
import lrg.insider.plugins.filters.memoria.variables.IsAttribute;
import lrg.insider.plugins.filters.memoria.variables.IsConstant;
import lrg.insider.plugins.groups.memoria.CallRelation;
import lrg.insider.plugins.visualizations.comparators.AttributeNMAVComparator;
import lrg.insider.plugins.visualizations.comparators.ClassLOCComparator;
import lrg.insider.plugins.visualizations.comparators.MethodLOCComparator;
import lrg.insider.util.Visualization;
import lrg.jMondrian.commands.AbstractFigureDescriptionCommand;
import lrg.jMondrian.commands.AbstractNumericalCommand;
import lrg.jMondrian.figures.Figure;
import lrg.jMondrian.layouts.FlowLayout;
import lrg.jMondrian.layouts.TreeLayout;
import lrg.jMondrian.painters.LineEdgePainter;
import lrg.jMondrian.painters.RectangleNodePainter;
import lrg.jMondrian.util.CommandColor;
import lrg.jMondrian.view.ViewRenderer;


public class MethodInteraction extends AbstractVisualization {
	private Figure f;

    public MethodInteraction() {
        super("Method Interaction", "Method Interaction", "method");
    }
    
	public void view(final AbstractEntityInterface theMethod) {
		final GroupEntity clientClassesLayer = new GroupEntity("Client Classes", new EntityType("")); 
		final GroupEntity serverClassesLayer = new GroupEntity("Server Classes", new EntityType("")); 
		final GroupEntity methodsClassLayer = new GroupEntity("Method's Class", new EntityType("")); 
		GroupEntity allLayers = new GroupEntity(theMethod.getName() + " layer", new EntityType("")); 

		final AbstractEntity containerClass = theMethod.belongsTo("class"); 
		final GroupEntity iCallThem = theMethod.getGroup("operations called").applyFilter("model function")
										.union(theMethod.getGroup("variables accessed").applyFilter("is attribute"));
		final GroupEntity overridenMethodCalls = theMethod.getGroup("methods overriden").getGroup("operations calling me");

		final GroupEntity theyCallMe = theMethod.getGroup("operations calling me").union(overridenMethodCalls).applyFilter("model function");
        GroupEntity classesThatCallMe = ((GroupEntity)theyCallMe.belongsTo("class")).distinct().exclude(containerClass);
        GroupEntity classesThatICall = ((GroupEntity)iCallThem.belongsTo("class")).distinct().exclude(containerClass);
        final GroupEntity allInvolvedMethods = iCallThem.union(theyCallMe).union((AbstractEntity) theMethod);        
        ArrayList<AbstractEntityInterface> allClasses = classesThatCallMe.union(classesThatICall).getElements();
        for (AbstractEntityInterface crtClass : allClasses) {
        	GroupEntity members = crtClass.contains("method group").union(crtClass.contains("attribute group"));
			if(members.intersect(iCallThem).size() > members.intersect(theyCallMe).size())
				serverClassesLayer.add(crtClass);
			else clientClassesLayer.add(crtClass);
		}
 		methodsClassLayer.add(containerClass);
		if(clientClassesLayer.size()>0) allLayers.add(clientClassesLayer); 
		allLayers.add(methodsClassLayer); 
		if(serverClassesLayer.size()>0) allLayers.add(serverClassesLayer);
		ArrayList<AbstractEntityInterface> globalLayer = new ArrayList<AbstractEntityInterface>();
		globalLayer.add(allLayers);
					
        f = new Figure();

        f.nodesUsingForEach(globalLayer, 
       		 	new RectangleNodePainter(false),
                      new AbstractFigureDescriptionCommand(){
                            public Figure describe(){
                                        Figure fig = new Figure();
                                        GroupEntity receiverLayer = (GroupEntity) receiver;
                                        fig.nodesUsingForEach(receiverLayer.getElements(), 
                                        		new RectangleNodePainter(false)
                                        			.color(CommandColor.WHITE), 
                                        		new AbstractFigureDescriptionCommand() {
                                        			public Figure describe() {
                                        				Figure classFig = new Figure();
                                        				GroupEntity classList = (GroupEntity) receiver;
            	                            			Collections.sort(classList.getElements(), new ClassLOCComparator(allInvolvedMethods));                                
                                        				classFig.nodesUsingForEach(classList.getElements(), 
                                        				new RectangleNodePainter(20,30, true)
                                        				.name(Visualization.stringCommand("Name"))
                                            			.frameColor(CommandColor.LIGHT_GRAY)
                                        				.color(new AbstractNumericalCommand() { 
                                							public double execute() {
                                								if(receiver.equals(containerClass)) return OurColors.VERY_LIGHT_GRAY;
                                								if(containerClass.getGroup("all descendants").intersect((AbstractEntity) receiver).size() > 0)
                                									return OurColors.LIGHT_RED;
                                								if(containerClass.getGroup("all ancestors").intersect((AbstractEntity) receiver).size() > 0)
                                									return OurColors.LIGHT_LILA;
                                								return Color.WHITE.getRGB(); 
                                							} }), 
                                        						new AbstractFigureDescriptionCommand() {
                                        							public Figure describe() {
                                        								Figure methodFig = new Figure();
                                                        				AbstractEntityInterface aClass  = (AbstractEntityInterface) receiver;	                                                        				
                                                        				GroupEntity interestingElements = new GroupEntity("group", new ArrayList());
                                                        				GroupEntity methods = aClass.getGroup("method group");
                                                        				GroupEntity usedMethods = methods.exclude((AbstractEntity) theMethod).intersect(allInvolvedMethods);
                                                        				interestingElements.addAll(usedMethods);
                                                        				Collections.sort(interestingElements.getElements(), new MethodLOCComparator());
                                                        				GroupEntity usedAttributes = aClass.getGroup("attribute group").intersect(allInvolvedMethods); 
                                                        				Collections.sort(usedAttributes.getElements(), new AttributeNMAVComparator());
                                                        				interestingElements.addAll(usedAttributes);
                                                        				if(methods.intersect(theMethod).size() > 0) interestingElements.add(theMethod); 	                                                        				
                                                        				methodFig.nodesUsing(interestingElements.getElements(), 
                                        							new RectangleNodePainter(false)
                                                        				.name(Visualization.stringCommand("Name"))
                                										.width(new AbstractNumericalCommand(){
                                											public double execute(){
                                												double value ;
                                												double offset = 7;
                                												AbstractEntityInterface receiverEntity = (AbstractEntityInterface) receiver;
                                												if(theMethod.equals(receiverEntity)) value = 300;
                                												else if(new IsAttribute().applyFilter(receiverEntity)) value =  (Double) receiverEntity.getProperty("NMAV").getValue() + offset;
                                												else if(new IsAbstract().applyFilter(receiverEntity)) value = (Double) receiverEntity.getProperty("CM").getValue() + offset;
                                												else if(new IsAccessor().applyFilter(receiverEntity)) value = (Double) receiverEntity.getProperty("CM").getValue() + offset;
                                												else value = (Double) receiverEntity.getProperty("CM").getValue() + offset;
                                												
                               												return value;
                                											}
                                										})
                                										.height(new AbstractNumericalCommand(){
                                											public double execute(){
                                												double value;
                                												double offset = 7;
                                												AbstractEntityInterface receiverEntity = (AbstractEntityInterface) receiver;
                                												if(theMethod.equals(receiverEntity)) value = 10;
                                												else if(new IsAttribute().applyFilter(receiverEntity)) value =  (Double) receiverEntity.getProperty("NMAV").getValue() + offset;
                                												else if(new IsAbstract().applyFilter(receiverEntity)) value = receiverEntity.getGroup("methods overriding").size() + offset;
                                												else if(new IsAccessor().applyFilter(receiverEntity)) value = (Double) receiverEntity.getProperty("CM").getValue() + offset;
                                												else value = (Double) receiverEntity.getProperty("LOC").getValue() + offset;
	                               												return value;
                                											}
                                										})
                                										.color(new AbstractNumericalCommand(){ 
                                											public double execute(){ 
                                                                				AbstractEntityInterface receiverEntity  = (AbstractEntityInterface) receiver;
                                                                				if(receiverEntity.equals(theMethod)) return OurColors.VERY_LIGHT_GRAY;
                                                                				if(new IsAttribute().applyFilter(receiverEntity)) 
                                                                					if(new IsConstant().applyFilter(receiverEntity)) return OurColors.CYAN; 
                                                                					else return OurColors.BLUE; 
                                                                				if(theyCallMe.intersect(receiverEntity).size() > 0) return OurColors.RED; 
                                                                				if(overridenMethodCalls.intersect(receiverEntity).size() > 0) return OurColors.PINK;	                                              				
                                                                				if(iCallThem.intersect(receiverEntity).size() > 0) {
                                                                					if(new IsAccessor().applyFilter(receiverEntity))  return OurColors.ACCESSOR_BLUE;
                                                                					if(new IsAbstract().applyFilter(receiverEntity)) return OurColors.ABSTRACT_BLUE;
                                                                					return OurColors.LILA;
                                                                				}
                                                                				return OurColors.LILA; 
                                											} }));
   								
                                        								methodFig.layout(new FlowLayout(5,5,300));
                                        								return methodFig;
                                        							}
                                        						});
                                        				classFig.layout(new FlowLayout(20,5,400));
                                        				return classFig;
                                        			}
                                        		}
                                        );
                            			ArrayList<CallRelation> layerEdges = new ArrayList<CallRelation>();
                            			if(clientClassesLayer.size()>0) layerEdges.add(new CallRelation(clientClassesLayer, methodsClassLayer));
                            			if(serverClassesLayer.size()>0) layerEdges.add(new CallRelation(methodsClassLayer, serverClassesLayer));

                            			fig.edgesUsing(layerEdges,
                            					new LineEdgePainter
                            					(Visualization.entityCommand("getIsCalledNode"),
                            					 Visualization.entityCommand("getCallsNode")
                            					).color(CommandColor.INVISIBLE));


                                        fig.layout(new TreeLayout(2,45));
                                        return fig;
                            }
        			});
        
        ArrayList<CallRelation> outEdges = new ArrayList<CallRelation>();

		ArrayList<AbstractEntity> callThemMethods = iCallThem.getElements();
		for (AbstractEntity calledMth : callThemMethods) {
			if(!calledMth.equals(theMethod)) outEdges.add(new CallRelation((AbstractEntity) theMethod, calledMth));
		}

		f.edgesUsing(outEdges,
				new LineEdgePainter
				(Visualization.entityCommand("getIsCalledNode"),
				 Visualization.entityCommand("getCallsNode")
				).color(CommandColor.INVISIBLE));

		ArrayList<CallRelation> inEdges = new ArrayList<CallRelation>();
		ArrayList<AbstractEntity> callMeMethods = theyCallMe.getElements();
		for (AbstractEntity callerdMth : callMeMethods) {
			if(!callerdMth.equals(theMethod)) inEdges.add(new CallRelation(callerdMth, (AbstractEntity) theMethod));
		}


		f.edgesUsing(inEdges,
			new LineEdgePainter
			(Visualization.entityCommand("getIsCalledNode"),
			 Visualization.entityCommand("getCallsNode")
			).color(CommandColor.INVISIBLE));
      
        f.layout(new FlowLayout());


        ViewRenderer r = new ViewRenderer("Method Interaction on " + theMethod.getName() );
        f.renderOn(r);
        r.open();        
}
	
}
