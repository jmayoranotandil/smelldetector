package lrg.insider.plugins.visualizations;

import java.util.ArrayList;
import java.util.Iterator;

import lrg.common.abstractions.entities.AbstractEntity;
import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.plugins.filters.FilteringRule;
import lrg.common.abstractions.plugins.filters.composed.NotComposedFilteringRule;
import lrg.common.abstractions.plugins.visualization.AbstractVisualization;
import lrg.insider.util.Visualization;
import lrg.jMondrian.commands.AbstractFigureDescriptionCommand;
import lrg.jMondrian.figures.Figure;
import lrg.jMondrian.layouts.FlowLayout;
import lrg.jMondrian.layouts.TreeLayout;
import lrg.jMondrian.painters.LineEdgePainter;
import lrg.jMondrian.painters.RectangleNodePainter;
import lrg.jMondrian.util.CommandColor;
import lrg.jMondrian.view.ViewRenderer;
import lrg.memoria.core.Call;
import lrg.memoria.core.Function;
import lrg.memoria.core.InheritanceRelation;
import lrg.memoria.core.Method;

public class ClassInteraction extends AbstractVisualization {

    public ClassInteraction() {
        super("Class Interaction", "Class Interaction", "class");
    }

    public void view(AbstractEntityInterface entity) {

        GroupEntity allCalee = entity.uses("operations calling me").applyFilter("model function").applyFilter(new NotComposedFilteringRule(new FilteringRule("is global","is true","method")));
        GroupEntity allCalled = entity.uses("operations called").applyFilter("model function").applyFilter(new NotComposedFilteringRule(new FilteringRule("is global","is true","method")));
        GroupEntity myMethods = entity.getGroup("method group").applyFilter("model function");
        GroupEntity allRepresentedMethods = allCalee.union(allCalled).union(myMethods).distinct();

        GroupEntity allCaleeClasses = ((GroupEntity)allCalee.belongsTo("class")).distinct();
        GroupEntity allCalledClasses = ((GroupEntity)allCalled.belongsTo("class")).distinct();
        allCalledClasses = allCalledClasses.union(allCalledClasses.getGroup("all descendants")).distinct();
        GroupEntity allClasses = allCaleeClasses.union(allCalledClasses).union((AbstractEntity)entity).union(entity.getGroup("all descendants")).distinct();

        GroupEntity allEdges = entity.belongsTo("system").getGroup("all inheritance relations");
        ArrayList edges = new ArrayList();
        Iterator<InheritanceRelation> it = allEdges.iterator();
        while(it.hasNext()) {
            InheritanceRelation rel = it.next();
            if(allClasses.isInGroup(rel.getSubClass()) && allClasses.isInGroup(rel.getSuperClass())) {
                edges.add(rel);
            }
        }

        //Search all the calls to other classes
        GroupEntity methods = entity.getGroup("method group");
        ArrayList calls = new ArrayList();
        Iterator it1 = methods.iterator();
        while(it1.hasNext()) {
            ArrayList<Call> tmp = ((Function)it1.next()).getBody().getCallList();
            for(int i = 0; i < tmp.size(); i++) {
                Call x = tmp.get(i);
                if(allRepresentedMethods.isInGroup(x.getScope().getScope()) && allCalled.isInGroup(x.getFunction())) {
                    calls.add(tmp.get(i));
                }
            }
        }

        //Search all the calls to the class
        methods = allClasses.getGroup("method group");
        Iterator it2 = methods.iterator();
        while(it2.hasNext()) {
            ArrayList<Call> tmp = ((Method)it2.next()).getCallList();
            for(int i = 0; i < tmp.size(); i++) {
                Call x = tmp.get(i);
                if(allRepresentedMethods.isInGroup(x.getScope().getScope()) && entity.getGroup("method group").isInGroup(x.getFunction())) {
                    calls.add(tmp.get(i));
                }
            }
        }

        Figure f = new Figure();
        f.nodesUsingForEach(allClasses.getElements(), new RectangleNodePainter(true), new AbstractFigureDescriptionCommand() {

            public Figure describe() {
                Figure fig = new Figure();
                fig.nodesUsing(((AbstractEntityInterface)receiver).getGroup("method group").getElements(), new RectangleNodePainter(10,10,true));
                fig.layout(new FlowLayout(50));
                return fig;
            }

        });
        f.edgesUsing(edges, new LineEdgePainter(Visualization.entityCommand("getSubClass"),Visualization.entityCommand("getSuperClass")));
        f.edgesUsing(calls, new LineEdgePainter(Visualization.indirectionCommand("getScope",Visualization.entityCommand("getScope")),Visualization.entityCommand("getFunction"),true).color(CommandColor.RED));
        f.layout(new TreeLayout(20,50));

        ViewRenderer r = new ViewRenderer("ClassInteraction");
        f.renderOn(r);
        r.open();

    }

}
