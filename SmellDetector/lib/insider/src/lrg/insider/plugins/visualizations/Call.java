package lrg.insider.plugins.visualizations;

import lrg.common.abstractions.entities.AbstractEntityInterface;

public class Call{
    private AbstractEntityInterface caller;
    private AbstractEntityInterface callee;

    public Call(AbstractEntityInterface caller, AbstractEntityInterface callee){
        this.caller = caller;
        this.callee = callee;
    }


    public AbstractEntityInterface getCaller() {
        return caller;
    }

    public AbstractEntityInterface getCallee() {
        return callee;
    }
}
