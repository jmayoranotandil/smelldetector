package lrg.insider.plugins.groups.memoria;

import java.util.ArrayList;
import java.util.Iterator;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.plugins.groups.GroupBuilder;

/**
 * Created by IntelliJ IDEA.
 * User: marinescu
 * Date: 19.05.2006
 * Time: 14:45:34
 * To change this template use File | Settings | File Templates.
 */
public class MaxCallPath extends GroupBuilder {
    public MaxCallPath() {
        super("max call path", "", "method");
    }

    private GroupEntity getDistinctCalledModelMethods(AbstractEntityInterface aMethod) {
        return aMethod.getGroup("operations called").distinct().applyFilter("model function");
    }



/*
    private GroupEntity buildCallPath(GroupEntity theMethods, ArrayList visited) {
        if(theMethods.size() == 0) return theMethods;
        GroupEntity calledMth = getDistinctCalledModelMethods(theMethods).exclude(new GroupEntity("some name", visited));

        visited.addAll(theMethods.getElements());
        return theMethods.union(buildCallPath(calledMth, visited));
    }
*/


    private ArrayList buildCallPath(AbstractEntityInterface currentMethod, ArrayList alreadyVisited) {
        GroupEntity calledMethods = getDistinctCalledModelMethods(currentMethod);
        calledMethods = calledMethods.exclude(new GroupEntity("name", alreadyVisited));

        ArrayList thePath;
        ArrayList crtList;
        ArrayList maxList = new ArrayList();

        if(calledMethods.size() == 0) {
            thePath = new ArrayList();
            thePath.add(currentMethod);
            return thePath;
        }

        Iterator it = calledMethods.getElements().iterator();

        while(it.hasNext())     {
            AbstractEntityInterface crt = (AbstractEntityInterface) it.next();
            alreadyVisited.add(currentMethod);
            crtList = buildCallPath(crt, alreadyVisited);
            if(crtList.size() > maxList.size()) maxList = crtList;
        }

        alreadyVisited.remove(currentMethod);
        maxList.add(0,currentMethod);
        return maxList;
    }


    public ArrayList buildGroup(AbstractEntityInterface aMethod) {
          ArrayList visited = new ArrayList();
          visited.add(aMethod);
   /*
          Method meth = new Method("cucu");
          Class cls;

        Call call; call.getFunction()
        meth.addCall();
     */   
          return buildCallPath(aMethod, visited);
    }

}
