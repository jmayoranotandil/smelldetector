package lrg.insider.plugins.groups.memoria;

import java.util.ArrayList;

import lrg.common.abstractions.entities.AbstractEntity;
import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.plugins.groups.GroupBuilder;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 26.01.2005
 * Time: 12:06:07
 * To change this template use File | Settings | File Templates.
 */
public class ClassExternalServiceProviders extends GroupBuilder {
    public ClassExternalServiceProviders() {
        super("external service providers for class", "", "class");
    }

    public ArrayList buildGroup(AbstractEntityInterface aClass) {
        GroupEntity callees = (GroupEntity) aClass.uses("operations called").distinct().belongsTo("class");
        GroupEntity accessed = (GroupEntity) aClass.uses("variables accessed").distinct().belongsTo("class");

        callees.addAll(accessed);
        callees = callees.applyFilter("model class");

        GroupEntity relatedClasses = aClass.getGroup("all ancestors").union((AbstractEntity) aClass);

        return callees.exclude(relatedClasses).getElements();
    }

}