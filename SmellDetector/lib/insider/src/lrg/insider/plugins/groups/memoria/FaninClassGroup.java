package lrg.insider.plugins.groups.memoria;

import java.util.ArrayList;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.plugins.groups.GroupBuilder;

public class FaninClassGroup extends GroupBuilder {
    public FaninClassGroup() {
        super("fanin class group", "", new String[]{"package", "subsystem"});
    }

    public ArrayList buildGroup(AbstractEntityInterface aPackageOrSubsystem) {

        GroupEntity descendantClasses = aPackageOrSubsystem.isUsed("all descendants");
        GroupEntity classesOfOperationsCallers = (GroupEntity) aPackageOrSubsystem.uses("operations calling me").belongsTo("class");
        GroupEntity classesOfVariableAccessors = (GroupEntity) aPackageOrSubsystem.uses("methods accessing variable").belongsTo("class");

        GroupEntity userClasses = descendantClasses.union(classesOfOperationsCallers).union(classesOfVariableAccessors).distinct().applyFilter("model class");        
        
        return userClasses.exclude(aPackageOrSubsystem.contains("class group")).getElements();    
    }

}

