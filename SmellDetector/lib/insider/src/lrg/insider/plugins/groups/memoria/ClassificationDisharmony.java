package lrg.insider.plugins.groups.memoria;

import java.util.ArrayList;
import java.util.Iterator;

import lrg.common.abstractions.entities.AbstractEntity;
import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.plugins.filters.FilteringRule;
import lrg.common.abstractions.plugins.filters.composed.AndComposedFilteringRule;
import lrg.common.abstractions.plugins.filters.composed.NotComposedFilteringRule;
import lrg.common.abstractions.plugins.groups.GroupBuilder;
import lrg.insider.plugins.core.filters.memoria.ModelClassFilter;
import lrg.insider.plugins.filters.memoria.classes.IsInterface;

/**
 * Created by IntelliJ IDEA.
 * User: radum
 * Date: 13.02.2004
 * Time: 21:05:33
 * To change this template use Options | File Templates.
 */
public class ClassificationDisharmony extends GroupBuilder {
    private GroupEntity badRoots;
    private GroupEntity flawedClasses;
    private GroupEntity modelClasses;

    public ClassificationDisharmony() {
        super("Classification Disharmony", "", "system");
        flawedClasses = new GroupEntity("classification disharmonies", new ArrayList());
        badRoots = new GroupEntity("classification disharmony roots", new ArrayList());
    }

    private GroupEntity getBaseClasses(AbstractEntity initialGroup) {
        FilteringRule notInterface = new NotComposedFilteringRule(new IsInterface());
        return initialGroup.getGroup("base classes").applyFilter("model class").applyFilter(notInterface);
    }

    private void processDesignFlawInDerived(String designFlaw, String annotationName) {
        GroupEntity currentFlawGroup = modelClasses.applyFilter(designFlaw);
        currentFlawGroup.putAnnotation(annotationName, designFlaw);

        flawedClasses = flawedClasses.union(currentFlawGroup).distinct();
        badRoots = badRoots.union(getBaseClasses(currentFlawGroup)).distinct();

        System.out.println(designFlaw + " processed:" + badRoots.size() + " " + flawedClasses.size());
    }


    private void processDesignFlawInBase(String designFlaw, String annotationName) {
        GroupEntity currentFlawGroup = modelClasses.applyFilter(designFlaw);
        currentFlawGroup.putAnnotation(annotationName, designFlaw);

        badRoots = badRoots.union(currentFlawGroup).distinct();
        flawedClasses = flawedClasses.union(currentFlawGroup.getGroup("derived classes")).distinct();

        System.out.println(designFlaw + " processed:" + badRoots.size() + " " + flawedClasses.size());
    }

    private void processIdentityDesignFlaw(String designFlaw, String annotationName) {
        FilteringRule modelNotInterface = new AndComposedFilteringRule(new ModelClassFilter(), new NotComposedFilteringRule(new IsInterface()));
        FilteringRule hasDescendants = new FilteringRule("NODD", ">", "class", 0);
        GroupEntity identityFlawedClasses = modelClasses.applyFilter(designFlaw);

        AbstractEntity crtBadClass;
        for (Iterator it = identityFlawedClasses.iterator(); it.hasNext();) {
            crtBadClass = (AbstractEntity) it.next();
            GroupEntity modelBaseClasses = crtBadClass.getGroup("base classes").applyFilter(modelNotInterface);
            if (modelBaseClasses.size() > 0) {
                crtBadClass.putAnnotation(annotationName, designFlaw);
                flawedClasses = flawedClasses.union(crtBadClass);
                badRoots = badRoots.union(modelBaseClasses);
            }
            if (hasDescendants.applyFilter(crtBadClass)) {
                crtBadClass.putAnnotation(annotationName, designFlaw);
                flawedClasses = flawedClasses.union(crtBadClass.getGroup("derived classes"));
                badRoots = badRoots.union(crtBadClass);
            }
        }

        badRoots = badRoots.distinct();
        flawedClasses = flawedClasses.distinct();

        System.out.println(designFlaw + " processed:" + badRoots.size() + " " + flawedClasses.size());
    }

    private void annotateWithMethodFlaw(String metricName, String designFlaw, String annotationName) {
        FilteringRule aFilter = new FilteringRule(metricName, ">", "class", 0);
        GroupEntity thefilteredGroup = badRoots.union(flawedClasses).applyFilter(aFilter);
        AbstractEntity crtClass;

        for (Iterator it = thefilteredGroup.iterator(); it.hasNext();) {
            crtClass = (AbstractEntity) it.next();
            int metricValue = ((Double) crtClass.getProperty(metricName).getValue()).intValue();
            crtClass.putAnnotation(annotationName + metricValue, designFlaw);
        }
    }

    private void annotateWithOtherFlaws() {
        System.out.println("Start additional annotation");

        badRoots.applyFilter("God Class").putAnnotation("GC", "God Class");
        flawedClasses.applyFilter("God Class").putAnnotation("GC", "God Class");

        badRoots.applyFilter("Brain Class").putAnnotation("BC", "Brain Class");
        flawedClasses.applyFilter("Brain Class").putAnnotation("BC", "Brain Class");

        badRoots.applyFilter("Data Class").putAnnotation("DC", "Data Class");
        flawedClasses.applyFilter("Data Class").putAnnotation("DC", "Data Class");


        annotateWithMethodFlaw("NrBM", "Brain Method", "BM");
        annotateWithMethodFlaw("NrFE", "Feature Envy", "FE");
        annotateWithMethodFlaw("NrIC", "Intensive Coupling", "IC");
        annotateWithMethodFlaw("NrEC", "Extensive Coupling", "EC");
        annotateWithMethodFlaw("NrSS", "Shotgun Surgery", "SS");
        annotateWithMethodFlaw("EDUPLCLS", "External Duplication", "EDUP");
        annotateWithMethodFlaw("IDUPLINES", "Internal Duplication", "IDUP");

        System.out.println("Stop additional annotation " + badRoots.size() + " " + flawedClasses.size());
    }


    private String printFlawedDescendants(AbstractEntity aRoot, int depth) {
        GroupEntity derivedClasses = aRoot.getGroup("derived classes").intersect(flawedClasses.union(badRoots));
        AbstractEntity crtDerived;
        String indentation = "";
        for (int i = 0; i < depth; i++) indentation += "\t";
        String result = "";

        for (Iterator itDerived = derivedClasses.iterator(); itDerived.hasNext();) {
            crtDerived = (AbstractEntity) itDerived.next();
            result += indentation + crtDerived.getName() + "\t[" + crtDerived.annotationsToString() + "]\n";
            if (isLeaf(crtDerived) == false) result += printFlawedDescendants(crtDerived, ++depth);
        }
        return result;
    }


    private boolean isRoot(AbstractEntity aNode) {
        GroupEntity ancestors = getBaseClasses(aNode);
        return (ancestors.intersect(badRoots).size() == 0);
    }

    private boolean isLeaf(AbstractEntity aNode) {
        return (badRoots.isInGroup(aNode) == false);
    }


    private ArrayList printResults() {
        AbstractEntity crtRoot, crtDerived;
        ArrayList theRoots = new ArrayList();

        for (Iterator it = badRoots.iterator(); it.hasNext();)
            System.out.print(((AbstractEntity) it.next()).getName() + "  ");
        System.out.println("\n");

        for (Iterator it = flawedClasses.iterator(); it.hasNext();)
            System.out.print(((AbstractEntity) it.next()).getName() + "  ");
        System.out.println("\n");


        for (Iterator it = badRoots.iterator(); it.hasNext();) {
            crtRoot = (AbstractEntity) it.next();
            if (isRoot(crtRoot)) {
                System.out.println(crtRoot.getName() + "\t[" + crtRoot.annotationsToString() + "]");
                System.out.println(printFlawedDescendants(crtRoot, 1));
                theRoots.add(crtRoot);
            }
        }
        return theRoots;
    }


    public ArrayList buildGroup(AbstractEntityInterface theSystem) {
        modelClasses = theSystem.contains("class group").applyFilter("model class");

        processIdentityDesignFlaw("God Class", "GC");
        processIdentityDesignFlaw("Brain Class", "BC");

        processDesignFlawInDerived("Hierarchy Duplication", "HDUP");
        processDesignFlawInDerived("Refused Parent Bequest", "RPB");
        processDesignFlawInDerived("Tradition Breaker", "TB");

        processDesignFlawInBase("Futile Hierarchy", "FH");

        processDesignFlawInBase("Futile Abstract Pipeline", "FH");
        processDesignFlawInDerived("Futile Abstract Pipeline", "FH");

        annotateWithOtherFlaws();

        return printResults();
    }
}
