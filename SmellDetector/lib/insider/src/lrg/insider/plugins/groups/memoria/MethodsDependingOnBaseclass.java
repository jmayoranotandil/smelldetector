package lrg.insider.plugins.groups.memoria;

import java.util.ArrayList;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.filters.FilteringRule;
import lrg.common.abstractions.plugins.groups.GroupBuilder;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 16.02.2005
 * Time: 17:49:58
 * To change this template use File | Settings | File Templates.
 */
public class MethodsDependingOnBaseclass extends GroupBuilder {
    public MethodsDependingOnBaseclass() {
        super("methods depending on base class", "", "class");
    }

    public ArrayList buildGroup(AbstractEntityInterface aClass) {
        FilteringRule aRule = new FilteringRule("BCD", ">", "method", 0);

        return aClass.contains("method group").applyFilter(aRule).getElements();
    }

}
