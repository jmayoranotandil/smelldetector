package lrg.insider.plugins.groups.memoria;

import java.util.ArrayList;
import java.util.Iterator;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.plugins.filters.composed.NotComposedFilteringRule;
import lrg.common.abstractions.plugins.groups.GroupBuilder;
import lrg.insider.plugins.filters.memoria.classes.IsInterface;
import lrg.insider.plugins.filters.memoria.methods.IsConstructor;

public class RefusedAncestors extends GroupBuilder {
    public RefusedAncestors() {
        super("refused ancestors", "", "class");
    }

    private double usageRatio(GroupEntity usedprotectedMethods, GroupEntity usedprotectedAttributes, AbstractEntityInterface ancestorClass) {
        GroupEntity protectedMethods = ancestorClass.contains("method group").applyFilter("is protected");
        protectedMethods.applyFilter(new NotComposedFilteringRule(new IsConstructor()));
        GroupEntity protectedAttributes = ancestorClass.contains("attribute group").applyFilter("is protected");

        double totalProtected = protectedAttributes.size() + protectedMethods.size();
        if (totalProtected < 3) return 1.0;

        double usedProtected = protectedAttributes.intersect(usedprotectedAttributes).size() +
                protectedMethods.intersect(usedprotectedMethods).size();

        return (usedProtected / totalProtected);
    }

    private double countOverridenMethods(GroupEntity overridenMethods, AbstractEntityInterface ancestorClass) {
        return overridenMethods.intersect(ancestorClass.contains("method group")).size();
    }

    public ArrayList buildGroup(AbstractEntityInterface aClass) {
        ArrayList resultList = new ArrayList();
        GroupEntity usedprotectedMethods = aClass.uses("operations called").distinct().applyFilter("is protected");
        GroupEntity usedprotectedAttributes = aClass.uses("variables accessed").distinct().applyFilter("is protected");
        GroupEntity overridenMethods = aClass.uses("methods overriden");

        GroupEntity allAncestors = aClass.uses("all ancestors").applyFilter("model class");
        allAncestors.applyFilter(new NotComposedFilteringRule(new IsInterface()));
        if (allAncestors.size() == 0) return resultList;

        for (Iterator it = allAncestors.iterator(); it.hasNext();) {
            AbstractEntityInterface crtAncestor = (AbstractEntityInterface) it.next();
            double ur = usageRatio(usedprotectedMethods, usedprotectedAttributes, crtAncestor);
            double ovr = countOverridenMethods(overridenMethods, crtAncestor);
            if ((ur < 0.33) && (ovr < 1)) resultList.add(crtAncestor);
        }
        return resultList;
    }
}
