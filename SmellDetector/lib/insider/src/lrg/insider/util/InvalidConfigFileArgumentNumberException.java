package lrg.insider.util;

public class InvalidConfigFileArgumentNumberException extends Exception {
	
	public InvalidConfigFileArgumentNumberException(){
		super("Invalid number of arguments in config file!");
	}

}
