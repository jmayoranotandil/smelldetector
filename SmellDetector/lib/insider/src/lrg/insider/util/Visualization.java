package lrg.insider.util;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.jMondrian.commands.AbstractEntityCommand;
import lrg.jMondrian.commands.AbstractNumericalCommand;
import lrg.jMondrian.commands.AbstractStringCommand;

public class Visualization {

    public static AbstractNumericalCommand metricCommand(final String prop, final double offset) {
        return new AbstractNumericalCommand(prop) {
            public double execute() {
                return ((Double)((AbstractEntityInterface)receiver).getProperty(prop).getValue()).doubleValue() + offset;
            }
        };
    }

    public static AbstractNumericalCommand metricCommand(final String prop) {
        return new AbstractNumericalCommand(prop) {
            public double execute() {
                return ((Double)((AbstractEntityInterface)receiver).getProperty(prop).getValue()).doubleValue();
            }
        };
    }

    public static AbstractEntityCommand entityCommand(final String prop) {
        return new AbstractEntityCommand(prop) {
            public AbstractEntityInterface execute() {
                try {
                    return (AbstractEntityInterface)receiver.getClass().getMethod(prop,new Class[] {}).invoke(receiver);
                } catch(Exception e) {
                    System.out.println(e);
                    throw new RuntimeException(prop + " method does not exist or it is improperly invoked");
                }
            }
        };
    }

    public static AbstractStringCommand stringCommand(final String prop) {
        return new AbstractStringCommand(prop) {
            public String execute() {
                return ((String)((AbstractEntityInterface)receiver).getProperty(prop).getValue());
            }
        };
    }
    
    public static AbstractEntityCommand indirectionCommand(final String prop, final AbstractEntityCommand command) {
        return new AbstractEntityCommand(prop) {
            public AbstractEntityInterface execute() {
                try {
                    command.setReceiver(receiver);
                    AbstractEntityInterface tmp = (AbstractEntityInterface)command.execute();
                    return (AbstractEntityInterface)tmp.getClass().getMethod(prop,new Class[] {}).invoke(tmp);
                } catch(Exception e) {
                    System.out.println(e);
                    throw new RuntimeException(prop + " method does not exist or it is improperly invoked");
                }
            }
        };
    }
}
