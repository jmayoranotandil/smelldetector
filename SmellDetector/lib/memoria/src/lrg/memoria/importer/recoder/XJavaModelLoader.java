package lrg.memoria.importer.recoder;

import lrg.memoria.importer.recoder.recoder.MeMoJCCrossReferenceServiceConfiguration;
import lrg.memoria.importer.recoder.recoder.service.FailedDepErrorHandler;
import lrg.common.utils.ProgressObserver;
import lrg.membrain.translation.RecoderConfiguration;
import recoder.service.CrossReferenceSourceInfo;
import recoder.service.NameInfo;
import recoder.ParserException;
import recoder.convenience.ASTIterator;
import recoder.java.CompilationUnit;
import recoder.list.CompilationUnitList;
import recoder.io.PropertyNames;
import recoder.io.SourceFileRepository;
import recoder.util.NaturalHashTable;

import java.io.File;
import java.util.StringTokenizer;

public class XJavaModelLoader extends JavaModelLoader {

    public XJavaModelLoader(String sourcePathList, String cachePath, ProgressObserver observer) throws Exception {
        super(sourcePathList,cachePath,observer);
    }


    protected lrg.memoria.core.System loadModelFromCacheOrFromSources(String cachePath, String sourcePathList) throws Exception {

        try {
            java.lang.System.setProperty("memoria.extended.model","yes");
            java.lang.System.out.println("Loading the EXTENDED model from source files ... ");
            java.lang.System.out.println("The source path list is: " + sourcePathList);
            loadModelFromSources(sourcePathList);
            resolveParticularNamespacesAndPackages();
            return system;
        }catch(Exception e) {
            throw e;
        }finally{
            java.lang.System.setProperty("memoria.extended.model","no");            
        }

    }

    protected void loadModelFromSources(String pathList) throws ParserException {

        NaturalHashTable prefixes = new NaturalHashTable();
        StringTokenizer st = new StringTokenizer(pathList, File.pathSeparator);
        String currentClassPath = java.lang.System.getProperty("java.class.path");
        String path = "";
        while (st.hasMoreTokens()) {
            path = st.nextToken();
            prefixes.put(path, null);
            currentClassPath = currentClassPath + File.pathSeparator + path;
        }
        java.lang.System.setProperty("java.class.path", currentClassPath);
        java.lang.System.out.println("CLASSPATH=" + currentClassPath);
        XJavaModelLoader.JavaFilenameFilter jfltr = new XJavaModelLoader.JavaFilenameFilter(prefixes);

        ModelRepository mr;
        mr = DefaultModelRepository.getModelRepository(pathList);
        system = mr.getSystem();

        crsc = new MeMoJCCrossReferenceServiceConfiguration();
        RecoderConfiguration.setServiceConfiguration(crsc);
        crsc.getProjectSettings().setErrorHandler(new FailedDepErrorHandler());
        crsc.getProjectSettings().setProperty(PropertyNames.OVERWRITE_PARSE_POSITIONS, "false");
        crsc.getProjectSettings().setProperty(PropertyNames.OVERWRITE_INDENTATION, "false");

        SourceFileRepository sfr = crsc.getSourceFileRepository();
        sourceInfo = (CrossReferenceSourceInfo) crsc.getSourceInfo();

        CompilationUnitList cul = sfr.getAllCompilationUnitsFromPath(jfltr);
        if (cul.size() == 0) {
            java.lang.System.err.println("ERROR: The size of compilation units list is 0.");
            java.lang.System.err.println("The model could not be loaded !");
            java.lang.System.exit(1);
        }

        NameInfo nameInfo = crsc.getNameInfo();
        lrg.memoria.core.Class hierarchyRootClass = mr.addClass(nameInfo.getJavaLangObject(), "Object");
        lrg.memoria.core.Class.setHierarchyRootClass(hierarchyRootClass);

        CompilationUnit cu;
        ASTIterator asti = new ASTIterator();
        asti.setListener(new ModelConstructor());
        int size = cul.size();
        if (loadingProgressObserver != null)
            loadingProgressObserver.setMaxValue(size);
        for (int i = 0; i < size; i++) {
            cu = cul.getCompilationUnit(i);
            java.lang.System.out.println("File " + i + " - building model from " + cu.getDataLocation().toString().substring(5));
            cu.toSource();
            asti.iterate(cu);
            if (loadingProgressObserver != null) {
                loadingProgressObserver.increment();
            }
        }
        cleanUp();
    }

}
