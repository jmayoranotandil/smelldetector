package lrg.memoria.hismo.core;


public class PackageHistoryGroup extends AbstractHistoryGroup {

    public AbstractHistory createHistory(VersionsList versionsList) {
        return new PackageHistory(versionsList);
    }

    public AbstractHistoryGroup createHistoryGroup() {
        return new PackageHistoryGroup();
    }
}
