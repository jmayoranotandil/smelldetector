package lrg.memoria.hismo.core;

public class LocalVariableHistoryGroup extends AbstractHistoryGroup {

    public AbstractHistory createHistory(VersionsList versionsList) {
        return new LocalVariableHistory(versionsList);
    }

    public AbstractHistoryGroup createHistoryGroup() {
        return new LocalVariableHistoryGroup();
    }
}
