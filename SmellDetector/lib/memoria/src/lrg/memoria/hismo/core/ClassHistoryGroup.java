package lrg.memoria.hismo.core;


public class ClassHistoryGroup extends AbstractHistoryGroup {

    public AbstractHistory createHistory(VersionsList versionsList) {
        return new ClassHistory(versionsList);
    }

    public AbstractHistoryGroup createHistoryGroup() {
        return new ClassHistoryGroup();
    }
}
