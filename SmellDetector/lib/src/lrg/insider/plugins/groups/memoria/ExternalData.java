package lrg.insider.plugins.groups.memoria;

import java.util.ArrayList;
import java.util.Iterator;

import lrg.common.abstractions.entities.AbstractEntity;
import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.plugins.filters.FilteringRule;
import lrg.common.abstractions.plugins.filters.composed.NotComposedFilteringRule;
import lrg.common.abstractions.plugins.groups.GroupBuilder;
import lrg.insider.plugins.core.filters.memoria.ModelClassFilter;
import lrg.insider.plugins.filters.memoria.variables.IsConstant;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 08.02.2005
 * Time: 11:37:19
 * To change this template use File | Settings | File Templates.
 */
public class ExternalData extends GroupBuilder {
    public ExternalData() {
        super("foreign data", "", "class");
    }

    public ArrayList buildGroup(AbstractEntityInterface measuredClass) {
        GroupEntity allAccesses;
        // FilteringRule isAccessor = new FilteringRule("IsAccessor", "IsTrue", "method");
        FilteringRule notConstant = new NotComposedFilteringRule(new IsConstant());

        GroupEntity currentClassAndAncestors = measuredClass.uses("all ancestors").union((AbstractEntity) measuredClass);

        allAccesses = measuredClass.uses("variables accessed").applyFilter("is attribute").applyFilter(notConstant);
        allAccesses = allAccesses.union(measuredClass.uses("operations called").applyFilter("is accessor")).distinct();

        if (allAccesses.size() == 0) return new ArrayList();

        Iterator it = allAccesses.iterator();
        AbstractEntity crtEntity, scope;
        while (it.hasNext()) {
            crtEntity = ((AbstractEntity) it.next());
            scope = crtEntity.belongsTo("class");
            if (new ModelClassFilter().applyFilter(scope) == false)
                allAccesses = allAccesses.exclude(crtEntity);
            else if (currentClassAndAncestors.isInGroup(scope)) allAccesses = allAccesses.exclude(crtEntity);
        }

        return allAccesses.getElements();
    }

}

