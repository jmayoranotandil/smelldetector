package lrg.insider.plugins.groups.memoria;

import lrg.common.abstractions.entities.AbstractEntity;

public class CallRelation {
	private AbstractEntity callsNode;
	private AbstractEntity isCalledNode;
	
	public CallRelation(AbstractEntity calls, AbstractEntity isCalled) {
		callsNode = calls; isCalledNode = isCalled;
	}
	public AbstractEntity getCallsNode() { return callsNode; }
	
	public AbstractEntity getIsCalledNode() { return isCalledNode; }
}
