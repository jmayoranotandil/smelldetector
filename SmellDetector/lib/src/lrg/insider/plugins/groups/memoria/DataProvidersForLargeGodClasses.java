package lrg.insider.plugins.groups.memoria;

import java.util.ArrayList;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.plugins.groups.GroupBuilder;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 08.02.2005
 * Time: 15:38:22
 * To change this template use File | Settings | File Templates.
 */
public class DataProvidersForLargeGodClasses extends GroupBuilder {
    public DataProvidersForLargeGodClasses() {
        super("Data Providers For Large-God Classes", "", "system");
    }

    public ArrayList buildGroup(AbstractEntityInterface aSystem) {
        GroupEntity allClasses = aSystem.getGroup("class group").applyFilter("model class");
        GroupEntity largeGodClasses = allClasses.applyFilter("God Class");

        largeGodClasses = largeGodClasses.union(allClasses.applyFilter("Large Classes"));

        return largeGodClasses.getGroup("foreign data providers").distinct().getElements();
    }

}
