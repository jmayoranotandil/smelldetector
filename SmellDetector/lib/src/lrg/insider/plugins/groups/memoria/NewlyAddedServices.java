package lrg.insider.plugins.groups.memoria;

import java.util.ArrayList;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.plugins.filters.FilteringRule;
import lrg.common.abstractions.plugins.filters.composed.NotComposedFilteringRule;
import lrg.common.abstractions.plugins.groups.GroupBuilder;
import lrg.insider.plugins.filters.memoria.methods.IsAccessor;
import lrg.insider.plugins.filters.memoria.methods.IsConstructor;
import lrg.insider.plugins.filters.memoria.methods.IsOverriden;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 16.02.2005
 * Time: 18:06:36
 * To change this template use File | Settings | File Templates.
 */
public class NewlyAddedServices extends GroupBuilder {
    public NewlyAddedServices() {
        super("Newly Added Methods (NAS)", "", "class");
    }

    public ArrayList buildGroup(AbstractEntityInterface measuredClass) {
        GroupEntity allAncestors = measuredClass.uses("all ancestors").applyFilter("model class");
        if (allAncestors.size() == 0) return new ArrayList();

        FilteringRule notOverriden = new NotComposedFilteringRule(new IsOverriden());
        FilteringRule notConstructor = new NotComposedFilteringRule(new IsConstructor());
        FilteringRule notAccessor = new NotComposedFilteringRule(new IsAccessor());


        GroupEntity myPublicMethods = measuredClass.contains("method group").applyFilter("is public");

        // System.out.println(myPublicMethods.size());
        return myPublicMethods.applyFilter(notConstructor).applyFilter(notOverriden).applyFilter(notAccessor).getElements();
    }

}
