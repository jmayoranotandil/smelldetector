package lrg.insider.plugins.core.properties.memoria.packages;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.insider.plugins.core.groups.memoria.containment.PackageHasClasses;
import lrg.insider.plugins.core.properties.AbstractDetail;
import lrg.memoria.core.Package;

public class Detail extends AbstractDetail {
    public Detail() {
        super("Detail", "Constructs a detailed HTML String to be shown in the browser.", "package", "string");
    }

    public ResultEntity compute(AbstractEntityInterface anEntity) {
        if (anEntity instanceof lrg.memoria.core.Package) {
            Package aPackage = (Package) anEntity;
            String text = "<h1>" + "Package " + linkTo(aPackage) + "</h1><hr><br>";

            GroupEntity classes = new PackageHasClasses().buildGroupEntity(aPackage),
                    methods = aPackage.contains("method group"),
                    gp = aPackage.contains("global function group");
            methods.union(gp);

            text += "<b>Package Summary</b><br>";
            text += "Classes (" + linkToNumber(classes) + ") <br><br>";
            //"Global functions ("+linkToNumber(gp)+"), " +
            //"Global variables ("+linkToNumber(aPackage.contains("global variable group"))+")<br><br>";
            text += "<font bgcolor=#FFE0E0>";

            gp = classes.applyFilter("Brain Class");
            if (gp.size() > 0) text += "Brain Class: " + linkToNumber(gp) + "<br>";

            gp = classes.applyFilter("Data Class");
            if (gp.size() > 0) text += "Data Class: " + linkToNumber(gp) + "<br>";

            gp = classes.applyFilter("God Class");
            if (gp.size() > 0) text += "God Class: " + linkToNumber(gp) + "<br>";

            gp = classes.applyFilter("Tradition Breaker");
            if (gp.size() > 0) text += "Tradition Breaker: " + linkToNumber(gp) + " (classes)<br>";

            gp = classes.applyFilter("Refused Parent Bequest");
            if (gp.size() > 0) text += "Refusing Parent Bequest: " + linkToNumber(gp) + " (classes)<br>";

            gp = classes.applyFilter("Futile Hierarchy");
            if (gp.size() > 0) text += "Futile Hierarchy: " + linkToNumber(gp) + " (classes)<br>";


            gp = methods.applyFilter("Brain Method");
            if (gp.size() > 0) text += "Brain Method: " + linkToNumber(gp) + " <br>";

            gp = methods.applyFilter("Feature Envy");
            if (gp.size() > 0) text += "Feature Envy: " + linkToNumber(gp) + " (methods)<br>";

            gp = methods.applyFilter("Intensive Coupling");
            if (gp.size() > 0) text += "Intensive Coupling: " + linkToNumber(gp) + " (methods)<br>";

            gp = methods.applyFilter("Extensive Coupling");
            if (gp.size() > 0) text += "Extensive Coupling: " + linkToNumber(gp) + " (methods)<br>";

            gp = methods.applyFilter("Shotgun Surgery");
            if (gp.size() > 0) text += "Shotgun Surgery: " + linkToNumber(gp) + " (methods)<br>";

            text += "</font>";
            //text += bulletedLinkList(aPackage.getAllTypesList());

            return new ResultEntity(text);
        } else
            return null;
    }
}