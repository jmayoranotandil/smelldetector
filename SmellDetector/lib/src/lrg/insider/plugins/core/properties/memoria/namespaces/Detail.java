package lrg.insider.plugins.core.properties.memoria.namespaces;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.insider.plugins.core.properties.AbstractDetail;
import lrg.memoria.core.Namespace;
import lrg.memoria.core.UnnamedNamespace;

public class Detail extends AbstractDetail
{
    public Detail()
    {
        super("Detail", "Constructs a detailed HTML String to be shown in the browser.", "namespace", "string");
    }

    public ResultEntity compute(AbstractEntityInterface anEntity)
    {
        if (anEntity instanceof lrg.memoria.core.Namespace)
        {
            Namespace aNamespace = (Namespace) anEntity;
            String text = "<h1>" + "Namespace Detail: " + linkTo(aNamespace) + "</h1><hr><br>";
            if (aNamespace instanceof UnnamedNamespace)
                text += "Unnamed Namespace from file: " + ((UnnamedNamespace)aNamespace).getFile().getFullName() + "<br>";
            text += "Statute: " + aNamespace.getStatute() + "<br>";
            text += "Number of classes: " + aNamespace.getAbstractDataTypes().size() + "<br>";
            text += "Types: " + "<br>";
            text += bulletedLinkList(aNamespace.getAllTypesList());
            return new ResultEntity(text);
        }
        else
           return null;
    }
}