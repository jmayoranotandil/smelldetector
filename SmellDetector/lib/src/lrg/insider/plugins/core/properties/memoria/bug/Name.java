package lrg.insider.plugins.core.properties.memoria.bug;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.properties.PropertyComputer;

public class Name extends PropertyComputer
{
    public Name()
    {
        super("Name", "The name of the annotation", "bug", "string");
    }

    public ResultEntity compute(AbstractEntityInterface anEntity)
    {
    	   if(anEntity instanceof lrg.memoria.core.Bug == false)	return null;

        return new ResultEntity(anEntity.getName());
    }
}