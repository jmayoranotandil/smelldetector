package lrg.insider.plugins.core.properties.memoria.subsystems;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.properties.PropertyComputer;
import lrg.memoria.core.Subsystem;

/**
 * Created by IntelliJ IDEA.
 * User: marinescu
 * Date: 22.11.2006
 * Time: 12:57:45
 * To change this template use File | Settings | File Templates.
 */
public class Scope extends PropertyComputer {
    public Scope() {
        super("scope", "The scope of the component", "subsystem", "entity");
    }

    public ResultEntity compute(AbstractEntityInterface anEntity) {
    	lrg.memoria.core.Package firstPackage = (lrg.memoria.core.Package) ((Subsystem)anEntity).getPackages().get(0);
        return new ResultEntity(firstPackage.getSystem());
    }
}
