package lrg.insider.plugins.core.operators.aggregation;

import java.util.ArrayList;
import java.util.Iterator;

import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.operators.AggregationOperator;

/**
 * Created by IntelliJ IDEA.
 * User: radum
 * Date: Nov 1, 2004
 * Time: 6:40:25 PM
 * To change this template use Options | File Templates.
 */
public class LongestNameOperator extends AggregationOperator {
    public LongestNameOperator()
    {
        super("maxlen", "string");
    }

    public ResultEntity aggregate(ArrayList resultGroup)
    {
        int size = resultGroup.size();
        if (size == 0) return new ResultEntity(0);
        double maxlen = -1;

        Iterator it = resultGroup.iterator();

        while (it.hasNext())  {
             int len = ((String) (((ResultEntity) it.next()).getValue())).length();

             if(len > maxlen) maxlen = len;
        }
        return new ResultEntity(maxlen);
    }

}
