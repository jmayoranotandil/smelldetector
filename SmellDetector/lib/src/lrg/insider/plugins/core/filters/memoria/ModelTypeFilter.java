package lrg.insider.plugins.core.filters.memoria;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.Descriptor;
import lrg.common.abstractions.plugins.filters.FilteringRule;

public class ModelTypeFilter extends FilteringRule {
    public ModelTypeFilter() {
        super(new Descriptor("model type", "type"));
    }

    public boolean applyFilter(AbstractEntityInterface anEntity) {
        return (anEntity instanceof lrg.memoria.core.Class);
    }
}

