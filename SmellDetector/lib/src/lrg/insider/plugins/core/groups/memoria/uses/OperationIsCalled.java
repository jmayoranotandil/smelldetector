package lrg.insider.plugins.core.groups.memoria.uses;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.groups.GroupBuilder;
import lrg.memoria.core.Call;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 29.10.2004
 * Time: 17:50:12
 * To change this template use File | Settings | File Templates.
 */
public class OperationIsCalled extends GroupBuilder {
    public OperationIsCalled() {
        super("operations calling me", "", new String[]{"global function", "method"});
    }

    public ArrayList buildGroup(AbstractEntityInterface anEntity) {
        HashSet resultList = new HashSet();
        if (anEntity instanceof lrg.memoria.core.Function == false) return new ArrayList(resultList);

        lrg.memoria.core.Function aFunction = (lrg.memoria.core.Function) anEntity;

        Iterator it = aFunction.getCallList().iterator();
        while (it.hasNext())
            resultList.add(((Call) it.next()).getScope().getScope());

        return new ArrayList(resultList);
    }
}
