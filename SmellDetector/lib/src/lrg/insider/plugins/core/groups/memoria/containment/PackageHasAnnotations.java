package lrg.insider.plugins.core.groups.memoria.containment;

import java.util.ArrayList;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.groups.GroupBuilder;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 03.06.2004
 * Time: 10:31:46
 * To change this template use File | Settings | File Templates.
 */
public class PackageHasAnnotations extends GroupBuilder {
    public PackageHasAnnotations() {
        super("annotations group", "the annotations", "package");
    }

    public ArrayList buildGroup(AbstractEntityInterface anEntity) {
        ArrayList annotationsList = new ArrayList();
        if (anEntity instanceof lrg.memoria.core.Package == false) return annotationsList;
        annotationsList = ((lrg.memoria.core.Package)anEntity).getAnnotationsList();
        return annotationsList;
    }
}
