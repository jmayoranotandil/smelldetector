package lrg.insider.plugins.core.groups.memoria.uses;

import java.util.ArrayList;
import java.util.HashSet;

import lrg.common.abstractions.entities.AbstractEntity;
import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.plugins.groups.GroupBuilder;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 15.10.2004
 * Time: 19:39:37
 * To change this template use File | Settings | File Templates.
 */
public class ClassUsedByDescendants extends GroupBuilder {
    public ClassUsedByDescendants() {
        super("all descendants", "", "class");
    }

    private boolean noCycles(AbstractEntityInterface parent, AbstractEntity child) {
        return child.getGroup("derived classes").intersect(parent).size() == 0;
    }

    public ArrayList buildGroup(AbstractEntityInterface anEntity) {
        HashSet<AbstractEntity> setOfDerivedClasses = new HashSet<AbstractEntity>();
        HashSet<AbstractEntity> tmp = new HashSet<AbstractEntity>();

        if (anEntity instanceof lrg.memoria.core.Class == false)
            return new ArrayList();

        setOfDerivedClasses.addAll(anEntity.getGroup("derived classes").getElements());
        if (setOfDerivedClasses.isEmpty()) return new ArrayList();

        tmp.addAll(setOfDerivedClasses);

        for(AbstractEntity crtDerivedClass : setOfDerivedClasses)
            if(noCycles(anEntity, crtDerivedClass)) {
               GroupEntity allDescendants = crtDerivedClass.getGroup("all descendants");
               tmp.addAll(allDescendants.getElements());
            }
            else System.out.println("CLASS-->DESCENDANT CYCLE: "+ anEntity.getName() + " --> " + crtDerivedClass.getName());

        return new ArrayList(tmp);
    }
}
