package lrg.insider.plugins.core.groups.memoria.containment;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.groups.GroupBuilder;
import lrg.memoria.core.ModelElementList;
import lrg.memoria.core.Package;

class SystemHasPackages extends GroupBuilder
{
    public SystemHasPackages()
    {
        super("package group", "", "system");
    }

    public ModelElementList<Package> buildGroup(AbstractEntityInterface anEntity)
    {
        if (anEntity instanceof lrg.memoria.core.System == false)
            return new ModelElementList<Package>();

        return ((lrg.memoria.core.System) anEntity).getPackages();
    }
}
