package lrg.insider.plugins.filters.memoria.classes;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.Descriptor;
import lrg.common.abstractions.plugins.filters.FilteringRule;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 04.02.2005
 * Time: 18:45:03
 * To change this template use File | Settings | File Templates.
 */
public class TemporaryFieldHost extends FilteringRule {
    public TemporaryFieldHost() {
        super(new Descriptor("Temporary Field Host", "class"));
    }

    public boolean applyFilter(AbstractEntityInterface anEntity) {
        return new FilteringRule("NTempF", ">", "class", 0).applyFilter(anEntity);
    }
}
