package lrg.insider.plugins.filters.memoria.classes;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.Descriptor;
import lrg.common.abstractions.plugins.filters.FilteringRule;
import lrg.common.abstractions.plugins.filters.composed.AndComposedFilteringRule;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 04.11.2004
 * Time: 18:18:01
 * To change this template use File | Settings | File Templates.
 */
class ShotgunSurgery extends FilteringRule {
    public ShotgunSurgery() {
        super(new Descriptor("Shotgun Surgery", "class"));
    }

    public boolean applyFilter(AbstractEntityInterface anEntity) {
        FilteringRule highCM = new FilteringRule("CM", ">", "class", 20);   // double compared to SS in methods
        FilteringRule highCC = new FilteringRule("CC", ">", "class", 10);   // double compare to SS in methods

        return new AndComposedFilteringRule(highCM, highCC).applyFilter(anEntity);
    }
}
