package lrg.insider.plugins.filters.memoria.classes;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.Descriptor;
import lrg.common.abstractions.plugins.filters.FilteringRule;
import lrg.common.abstractions.plugins.filters.composed.AndComposedFilteringRule;
import lrg.insider.plugins.filters.Threshold;

public class GodClass extends FilteringRule {
    public GodClass() {
        super(new Descriptor("God Class", "class"));
    }

    public boolean applyFilter(AbstractEntityInterface anEntity) {
        FilteringRule highATFD = new FilteringRule("ATFD", ">", "class", Threshold.FEW);
        FilteringRule lowTCC = new FilteringRule("TCC", "<", "class", Threshold.ONE_THIRD);
        FilteringRule highWMC = new FilteringRule("WMC", ">=", "class", Threshold.WMC_VERYHIGH);

        return new AndComposedFilteringRule(highWMC,
                new AndComposedFilteringRule(highATFD, lowTCC)).applyFilter(anEntity);

    }
}
