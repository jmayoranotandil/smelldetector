package lrg.insider.plugins.filters.memoria.methods;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.Descriptor;
import lrg.common.abstractions.plugins.filters.FilteringRule;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 20.10.2004
 * Time: 17:16:23
 * To change this template use File | Settings | File Templates.
 */
public class IsConstructor extends FilteringRule {
    public IsConstructor() {
        super(new Descriptor("is constructor", "method"));
    }

    public boolean applyFilter(AbstractEntityInterface anEntity) {
        if (anEntity instanceof lrg.memoria.core.Method == false) return false;

        return ((lrg.memoria.core.Method) anEntity).isConstructor();
    }
}
