package lrg.insider.plugins.filters.memoria.classes;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.plugins.Descriptor;
import lrg.common.abstractions.plugins.filters.FilteringRule;
import lrg.common.abstractions.plugins.filters.composed.AndComposedFilteringRule;
import lrg.common.abstractions.plugins.filters.composed.NotComposedFilteringRule;


public class FutileAbstractPipeline extends FilteringRule {
    public FutileAbstractPipeline() {
        super(new Descriptor("Futile Abstract Pipeline", "class"));
    }

    public boolean applyFilter(AbstractEntityInterface aClass) {
        // applies only to ABSTRACT classes

        if (aClass instanceof lrg.memoria.core.Class == false) return false;
        if (((lrg.memoria.core.Class) aClass).isAbstract() == false) return false;

        FilteringRule isNotInterfaceAncestor = new NotComposedFilteringRule(new IsInterface());
        FilteringRule isAbstractNotInterfaceAncestor = new AndComposedFilteringRule(isNotInterfaceAncestor, new IsAbstract());

        GroupEntity modelAbstractAncestors = aClass.uses("all ancestors").applyFilter("model class").applyFilter(isAbstractNotInterfaceAncestor);
        if (modelAbstractAncestors.size() != 1) return false;
        return aClass.isUsed("all descendants").size() == 1;
    }
}
