package lrg.insider.plugins.filters.memoria.classes;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.Descriptor;
import lrg.common.abstractions.plugins.filters.FilteringRule;
import lrg.insider.plugins.filters.Threshold;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 08.02.2005
 * Time: 12:54:13
 * To change this template use File | Settings | File Templates.
 */
public class SchizofrenicClass extends FilteringRule {
    public SchizofrenicClass() {
        super(new Descriptor("Schizofrenic Class", "class"));
    }

    public boolean applyFilter(AbstractEntityInterface anEntity) {
        return new FilteringRule("SCHIZO", ">=", "class", Threshold.FEW).applyFilter(anEntity);
    }
}