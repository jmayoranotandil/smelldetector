package lrg.insider.plugins.filters.memoria.methods;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.Descriptor;
import lrg.common.abstractions.plugins.filters.FilteringRule;
import lrg.common.abstractions.plugins.filters.composed.AndComposedFilteringRule;
import lrg.common.abstractions.plugins.filters.composed.OrComposedFilteringRule;
import lrg.insider.plugins.filters.Threshold;

public class IntensiveCoupling extends FilteringRule {
    public IntensiveCoupling() {
        super(new Descriptor("Intensive Coupling", "method"));
    }


    private boolean functionCallsMannyFromUnrelatedClasses(AbstractEntityInterface aMethod) {
        FilteringRule veryhighIntensity = new FilteringRule("CINT", ">", "method", Threshold.SMemCap);
        FilteringRule lowDispersion = new FilteringRule("CDISP", "<", "method", Threshold.HALF);

        FilteringRule moreThanFewCalls = new FilteringRule("CINT", ">", "method", Threshold.FEW);
        FilteringRule veryFocused = new FilteringRule("CDISP", "<", "method", Threshold.ONE_QUARTER);

        return new OrComposedFilteringRule(new AndComposedFilteringRule(veryhighIntensity, lowDispersion),
                new AndComposedFilteringRule(moreThanFewCalls, veryFocused)).applyFilter(aMethod);
    }

    public boolean applyFilter(AbstractEntityInterface aMethod) {
        boolean deepNestingOfCalls = new FilteringRule("MAXNESTING", ">", "method", Threshold.SHALLOW).applyFilter(aMethod);

        return deepNestingOfCalls && functionCallsMannyFromUnrelatedClasses(aMethod);
    }
}
