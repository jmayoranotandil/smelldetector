package lrg.insider.plugins.filters.memoria.methods;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.Descriptor;
import lrg.common.abstractions.plugins.filters.FilteringRule;

public class IsOverriden extends FilteringRule {
    public IsOverriden() {
        super(new Descriptor("is overriding", "method"));
    }

    public boolean applyFilter(AbstractEntityInterface anEntity) {
        if (anEntity instanceof lrg.memoria.core.Method == false) return false;

        return anEntity.uses("methods overriden").size() > 0;
    }
}
