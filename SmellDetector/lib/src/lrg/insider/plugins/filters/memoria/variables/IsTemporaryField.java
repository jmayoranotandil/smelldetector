package lrg.insider.plugins.filters.memoria.variables;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.Descriptor;
import lrg.common.abstractions.plugins.filters.FilteringRule;
import lrg.common.abstractions.plugins.filters.composed.AndComposedFilteringRule;
import lrg.common.abstractions.plugins.filters.composed.NotComposedFilteringRule;
import lrg.insider.plugins.core.filters.memoria.ModelVariableFilter;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 04.02.2005
 * Time: 19:03:05
 * To change this template use File | Settings | File Templates.
 */
public class IsTemporaryField extends FilteringRule {
    public IsTemporaryField() {
        super(new Descriptor("temporary field", new String[]{"global variable", "attribute", "local variable", "parameter"}));
    }

    public boolean applyFilter(AbstractEntityInterface anEntity) {
        if (anEntity instanceof lrg.memoria.core.Attribute == false) return false;
        if (new ModelVariableFilter().applyFilter(anEntity) == false) return false;

        FilteringRule isTemporaryField = new FilteringRule("NMAV", "==", "attribute", 1);
        FilteringRule notStatic = new NotComposedFilteringRule(new IsStatic());

        return new AndComposedFilteringRule(isTemporaryField, notStatic).applyFilter(anEntity);
    }
}
