package lrg.insider.plugins.filters.memoria.methods;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.Descriptor;
import lrg.common.abstractions.plugins.filters.FilteringRule;
import lrg.memoria.core.AccessMode;

/**
 * Created by IntelliJ IDEA.
 * User: user
 * Date: 19.02.2005
 * Time: 12:37:43
 * To change this template use File | Settings | File Templates.
 */
public class IsAccessor extends FilteringRule {
    public IsAccessor() {
        super(new Descriptor("is accessor", "method"));
    }

    public boolean applyFilter(AbstractEntityInterface anEntity) {
        if (anEntity instanceof lrg.memoria.core.Method == false)
            return false;

        if ((!anEntity.getName().startsWith("get")) && (!anEntity.getName().startsWith("Get")) &&
                (!anEntity.getName().startsWith("set")) && (!anEntity.getName().startsWith("Set")))
            return false;

        if (anEntity.getGroup("operations called").size() > 0) return false;
        lrg.memoria.core.Method aMethod = (lrg.memoria.core.Method) anEntity;

        if (aMethod.getAccessMode() == AccessMode.PRIVATE) return false;
        if (aMethod.isStatic() == true) return false;

        if (aMethod.getBody() == null) return false;

        return (aMethod.getBody().getCyclomaticNumber() == 1);
    }
}

