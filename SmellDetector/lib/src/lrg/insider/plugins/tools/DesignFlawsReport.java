package lrg.insider.plugins.tools;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Iterator;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.tools.AbstractEntityTool;

public class DesignFlawsReport extends AbstractEntityTool {

    public DesignFlawsReport() {
        super("Complex Design Flaw Report", "Building Report", "system");
    }

	public String getToolName() {
		return "OCEReport";
	}

	private void writeFile(String foldername, AbstractEntityInterface anEntity) {
		if(anEntity.getEntityType().getName().compareTo("system") == 0) {
	        try {
	        	String filename = foldername+"/__index.html";
	        	PrintStream out_stream = new PrintStream(new FileOutputStream(filename));
	        	out_stream.print(buildReport(anEntity));
	        	out_stream.close();
	        } catch (Exception ex) { }					
		}

        try {
        	String filename = foldername+"/"+anEntity.getProperty("Address").getValue();
        	PrintStream out_stream = new PrintStream(new FileOutputStream(filename));
        	out_stream.print(buildReport(anEntity));
        	out_stream.close();
        } catch (Exception ex) { }		
	}
	public void run(AbstractEntityInterface aSystem, Object theToolParameters) {
        ArrayList<String> params = (ArrayList<String>) theToolParameters;
        String foldername = "../../"; 
        if(params.get(0).equals("")) foldername += "DesignFlaw-Report";
        else foldername += params.get(0);
        new File(foldername).mkdir();
        
        
        writeFile(foldername, aSystem);    

        
        for (Iterator iterator = aSystem.contains("class group").getElements().iterator(); iterator.hasNext();)
			writeFile(foldername, (AbstractEntityInterface) iterator.next());			
        
        for (Iterator iterator = aSystem.contains("method group").getElements().iterator(); iterator.hasNext();)
			writeFile(foldername, (AbstractEntityInterface) iterator.next());			
	}

    private String buildReport(AbstractEntityInterface anEntity) {
       	if(anEntity.getEntityType().getName().compareTo("system") == 0)
    		return (String) new lrg.insider.plugins.details.OverviewPyramid().compute(anEntity).getValue();   
    	if(anEntity.getEntityType().getName().compareTo("class") == 0)
    		return (String) new lrg.insider.plugins.details.DesignFlawsClassDetail().compute(anEntity).getValue();
    	if(anEntity.getEntityType().getName().compareTo("method") == 0)
    		return (String) new lrg.insider.plugins.details.DesignFlawsMethodDetail().compute(anEntity).getValue();
    	return "";
    }

	public ArrayList<String> getParameterList() {
        ArrayList<String> parList = new ArrayList<String>();
        parList.add("Folder name (relative to working directory): ");
        return parList;
    }

    public ArrayList<String> getParameterExplanations() {
        ArrayList<String> parList = new ArrayList<String>();
        parList.add("Name of the folder to stort HTML files");
        return parList;
    }
	
}
