package lrg.insider.plugins.tools;

import java.util.ArrayList;

import lrg.common.abstractions.entities.AbstractEntityInterface;

/**
 * Created by IntelliJ IDEA.
 * User: marinescu
 * Date: 03.11.2006
 * Time: 21:56:12
 * To change this template use File | Settings | File Templates.
 */

public class PRStructure {
    AbstractEntityInterface theClass;
    double PR;
    double CA;
    ArrayList<AbstractEntityInterface> CAList;
    ArrayList<PRStructure> TList;
    PRStructure(AbstractEntityInterface aClass,
                ArrayList<AbstractEntityInterface> aCAList,
                double pr) {
        theClass = aClass;
        CAList = aCAList;
        CA = CAList.size();
        TList = new ArrayList<PRStructure>();
        PR = pr;

    }

    PRStructure(PRStructure anotherStructure, double newPR) {
        theClass = anotherStructure.theClass;
        CA = anotherStructure.CA;
        CAList = anotherStructure.CAList;
        TList = anotherStructure.TList;

        PR = newPR;
    }

    public double getPR() {
        return PR;
    }
}
