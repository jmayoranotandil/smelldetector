/*package lrg.insider.plugins.tools.bugreports;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;


class OutputWriter {
	private Document outDoc;
	private String filename="";
	private String projectName="";
	private Element root;
	
	public OutputWriter(String projectName){
		outDoc=DocumentHelper.createDocument();
		if (projectName.startsWith("\"")&&projectName.endsWith("\""))
			this.projectName=projectName.split("\\W")[1];
		else
			this.projectName=projectName;
//		System.out.println("Project Name \n"+this.projectName);
		filename="Output_"+this.projectName+".xml";
		this.root=outDoc.addElement("root");
		this.root.addAttribute("project_name", projectName);
	}
	
	//adauga un bugreport in lista
	public void addBug(ArrayList<String> bugTexts, ArrayList<HashSet<String>> output){
		Element intermediar=root.addElement("bug");
		
		Element texts=intermediar.addElement("texts");
		for (String bugText:bugTexts)
			texts.addElement("text").addText(bugText);
		
		Element packages=intermediar.addElement("packages");
		for (String pachet:output.get(0))
			packages.addElement("package").addText(pachet);
		
		Element classes=intermediar.addElement("classes");
		for (String clasa:output.get(1))
			classes.addElement("class").addText(clasa);
		
		Element methods=intermediar.addElement("functions");
		for (String metoda:output.get(2))
			methods.addElement("class").addText(metoda);
		
		Element files=intermediar.addElement("files");
		for (String file:output.get(3))
			files.addElement("file").addText(file);		
	}

	
	//scrie in fisier ce a acumulat pana acum, sa speram...
public void finish(){
	
	
	try{
		OutputFormat format = OutputFormat.createPrettyPrint();
		format.setTrimText(false);
		XMLWriter output = new XMLWriter(new FileWriter(new File(filename)),format);
	        output.write(outDoc);
	        output.close();
	        }
	     catch(IOException e){
	    	 System.out.println("***"+e.getMessage());
	    	 }

	
}
}


*/