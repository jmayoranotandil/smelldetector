package lrg.insider.plugins.tools;

import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.plugins.filters.FilteringRule;
import lrg.common.abstractions.plugins.tools.AbstractEntityTool;
import lrg.memoria.core.Function;

public class HippoReportGenerator extends AbstractEntityTool {

    public HippoReportGenerator() {
        super("HippoReportGenerator","ReportGenerator", "system");
    }

    public String getToolName() {
        return "ReportGenerator";
    }

    public void run(AbstractEntityInterface abstractEntityInterface, Object o) {

        if (abstractEntityInterface instanceof lrg.memoria.core.System == false) return;
        AbstractEntityInterface theSystem = (lrg.memoria.core.System)abstractEntityInterface;
        ArrayList<String> params = (ArrayList<String>) o;
        String prefix = params.get(0);

        //Create initial groups
        GroupEntity allClassGroup = theSystem.getGroup("class group").applyFilter("model class");
        GroupEntity allMethodGroup = theSystem.getGroup("method group").applyFilter("model function");

        GroupEntity classGroup,methodGroup;
        classGroup = allClassGroup.applyFilter(new FilteringRule("Name","starts as","class",prefix));
        methodGroup = allMethodGroup.applyFilter(new FilteringRule("scope name","starts as","method",prefix));

        GroupEntity godClasses;
        GroupEntity brainClasses;
        GroupEntity shotgunMethods;
        GroupEntity intensiveMethods;
        GroupEntity extensiveMethods;
        GroupEntity traditionClasses;
        GroupEntity refusedClasses;

        //Compute the numbers
        int noGodClasses        = (godClasses = classGroup.applyFilter("God Class")).size();
        int noBrainClasses      = (brainClasses = classGroup.applyFilter("Brain Class")).size();
        int noShutgunSurgery    = (shotgunMethods = methodGroup.applyFilter("Shotgun Surgery")).size();
        int noIntensive         = (intensiveMethods = methodGroup.applyFilter("Intensive Coupling")).size();
        int noExtensive         = (extensiveMethods = methodGroup.applyFilter("Extensive Coupling")).size();
        int noTraditionBreakers = (traditionClasses = classGroup.applyFilter("Tradition Breaker")).size();
        int noRefusedBequest    = (refusedClasses = classGroup.applyFilter("Refused Parent Bequest 2")).size();

        String text = "";

        text+="GodClasses\n";
        for(int i = 0; i < godClasses.size(); i++) {
            AbstractEntityInterface tmp = godClasses.getElementAt(i);
            text+=tmp.getName() + "\t" + tmp.getProperty("ATFD") + "\t" + tmp.getProperty("WMC") + "\t" + tmp.getProperty("TCC") + "\n";
        }

        text+="BrainClasses\n";
        for(int i = 0; i < brainClasses.size(); i++) {
            AbstractEntityInterface tmp = brainClasses.getElementAt(i);
            text+=tmp.getName() + "\t" + tmp.getProperty("WMC") + "\n";
        }

        text+="ShutgunSurgery\n";
        for(int i = 0; i < shotgunMethods.size(); i++) {
            AbstractEntityInterface tmp = shotgunMethods.getElementAt(i);
            text+=((Function)tmp).getScope().getName() + "::" + tmp.getName() + "\t" + tmp.getProperty("CM") + "\t" + tmp.getProperty("CC") + "\n";
        }

        text+="IntensiveCoupling\n";
        for(int i = 0; i < intensiveMethods.size(); i++) {
            AbstractEntityInterface tmp = intensiveMethods.getElementAt(i);
            text+=((Function)tmp).getScope().getName() + "::" + tmp.getName() + "\t" + tmp.getProperty("CINT") + "\t" + tmp.getProperty("CDISP") + "\t" + tmp.getProperty("MAXNESTING") + "\n";
        }

        text+="ExtensiveCoupling\n";
        for(int i = 0; i < extensiveMethods.size(); i++) {
            AbstractEntityInterface tmp = extensiveMethods.getElementAt(i);
            text+=((Function)tmp).getScope().getName() + "::" + tmp.getName() + "\t" + tmp.getProperty("CINT") + "\t" + tmp.getProperty("CDISP") + "\t" + tmp.getProperty("MAXNESTING") + "\n";
        }

        text+="TraditionBrakers\n";
        for(int i = 0; i < traditionClasses.size(); i++) {
            AbstractEntityInterface tmp = traditionClasses.getElementAt(i);
            text+=tmp.getName() + "\n";
        }

        text+="RefusedBequest\n";
        for(int i = 0; i < refusedClasses.size(); i++) {
            AbstractEntityInterface tmp = refusedClasses.getElementAt(i);
            text+=tmp.getName() + "\n";
        }

        //Print results
        try {
            PrintStream out_stream = new PrintStream(new FileOutputStream(prefix));
            out_stream.print(noGodClasses + "\t" + noBrainClasses + "\t" + noShutgunSurgery + "\t" + noIntensive + "\t" + noExtensive + "\t" + noTraditionBreakers + "\t" + noRefusedBequest + "\n\n");
            out_stream.print(text);
            out_stream.close();
        } catch (Exception ex) {
          System.out.println("Error writing the result!");
        }
    }

    public ArrayList<String> getParameterList() {
        ArrayList<String> parList = new ArrayList<String>();
        parList.add("Prefix");
        return parList;
    }

    public ArrayList<String> getParameterExplanations() {
        ArrayList<String> parList = new ArrayList<String>();
        parList.add("Prefix");
        return parList;
    }

}
