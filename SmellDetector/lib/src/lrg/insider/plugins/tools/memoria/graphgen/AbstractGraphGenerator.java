/**
 * 
 */
package lrg.insider.plugins.tools.memoria.graphgen;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.Iterator;

import lrg.common.abstractions.entities.AbstractEntity;
import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.plugins.filters.FilteringRule;
import lrg.common.abstractions.plugins.filters.composed.NotComposedFilteringRule;
import lrg.common.abstractions.plugins.tools.AbstractEntityTool;
import lrg.insider.plugins.core.properties.AbstractDetail;
import lrg.insider.plugins.tools.memoria.graphgen.rules.AbstractGraphBuildRule;
import lrg.insider.plugins.tools.memoria.graphgen.rules.StrategyDefinition;
import lrg.insider.plugins.tools.memoria.graphgen.strategies.FactoryOfInterestingStuff;
import lrg.insider.plugins.tools.memoria.graphgen.strategies.InterestingMembersProvider;
import lrg.memoria.core.DataAbstraction;

/**
 * @author danc
 *
 * Generates three files containing the dependency trees for the current class. The files
 * are in the graphviz dot format, and show the tree flavours of dependency trees:
 * 1. "UsersOfAllMembers" - showing the most general dependency
 * 2. "UsersOfAllMembersByFieldType" - showing only the classes that indirectly refer 
 * a specific field type. All type names that include a given string will be taken into 
 * consideration.
 * 3. "UsersOfAllMembersBalanced" - a balanced approach between the above two
 * 
 * Tool parameters:
 *   params[0]: The string contained by the field type to look for (approaches 2 and 3)
 *   params[1]: The name of the directory where the files will be created
 *   params[2]: The maximum depth of the call tree
 * Tool parameter defaults:
 *   params[0]: "Socket"
 *   params[1]: "d:/prj/graphs"
 *	 params[2]: "7"
 */
public abstract class AbstractGraphGenerator extends AbstractEntityTool {

	private ArrayList<AbstractGraphBuildRule> buildRules;
	
	/**
	 *	Defines the build strategies involved by the current generator.
	 *	This is mandatory, and this method should simply use addStrategy()
	 *	for each necessary build strategy.
	 *
	 *  Note: some build strategies need parameters. It is safe to use the
	 *  tool parameters in this method, as it is called no earlier than
	 *  at the beginning of the run() method.
	 */
	protected abstract void defineBuildStrategies(Object toolParameters);
	
	protected String toolParameter0Name()
	{
		return "String contained by type: ";
	}
	
	protected String toolParameter0Default()
	{
		return "Socket";
	}
	protected String toolParameter0Description()
	{
		return "The string to look for when considering the root field/class types";
	}
	
	public AbstractGraphGenerator(String name, String description, String entity)
	{
		super(name, description, entity);
		buildRules = new ArrayList<AbstractGraphBuildRule>();
	}

	protected void addRule(AbstractGraphBuildRule rule)
	{
		buildRules.add(rule);
	}
	
	@Override
	public ArrayList<String> getParameterExplanations()
	{
		ArrayList<String> list = new ArrayList<String>();
		
		list.add(toolParameter0Description());
		list.add("The folder where the files will be saved");
		list.add("The maximum depth of the call chain");
		return list;
	}

	@Override
	public ArrayList<String> getParameterInitialValue()
	{
		ArrayList<String> list = new ArrayList<String>();
		
		list.add(toolParameter0Default());
		list.add("d:/prj/graphs");
		list.add("7");
		return list;
	}
	
	@Override
	public ArrayList<String> getParameterList()
	{
		ArrayList<String> list = new ArrayList<String>();
		
		list.add(toolParameter0Name());
		list.add("Directory to save files: ");
		list.add("Maximum call depth: ");
		return list;
	}

	
	/* (non-Javadoc)
	 * @see lrg.common.abstractions.plugins.tools.AbstractEntityTool#getToolName()
	 */
	@Override
	public String getToolName()
	{
		// TODO Auto-generated method stub
		return "GraphGenerator";
	}

	private void writeDotHeaders(String graphName)
	{
		String text = "// Generated at " + (new GregorianCalendar()).getTime().toString() +" by GraphGenerator, \n// (c) Dan Cosma & LRG, 2006\n\n";
		text += "digraph " + graphName + "_graph\n{\n"; //append _graph because graph name and node names must be distinct
		text += "node [color=green];\n";
		
		for(int i=0; i< buildRules.size(); i++)
			buildRules.get(i).writeToFile(text);
	}
	
	private void writeDotFooters()
	{
		for(int i=0; i< buildRules.size(); i++)
			buildRules.get(i).writeToFile("}\n");
	}
	
	/**
	 * Gets the GroupEntity containing the classes that will be taken as roots for building
	 * the dependency graphs.
	 * @param entity The root entity for which the tool was called 
	 * @param attributeType The string contained by the attribute type some strategies (fieldType, balanced) search for 
	 * @return an entity group containing only classes
	 */
	protected abstract GroupEntity getGroupOfRootClasses(AbstractEntityInterface entity, String attributeType);
	
	
	/**
	 * Specifies whether a single file should be generated for all dependencies detected with
	 * a strategy.  
	 * 
	 * This method must return true if the results must occur in only three files, one for
	 * each strategy (all, fieldtype, balanced).
	 * 
	 * If it returns false, a separate file will be generated for each root class and strategies.
	 * @param nameTemplate
	 */
	protected abstract boolean useCommonFiles();
	
	/*
	 * (non-Javadoc)
	 * @see lrg.common.abstractions.plugins.tools.AbstractEntityTool#run(lrg.common.abstractions.entities.AbstractEntityInterface, java.lang.Object)
	 */
	@Override
	public void run(AbstractEntityInterface anEntity, Object theToolParameters)
	{
		/* define the build strategies to work with. This must be called here because
		 * some strategies may need the tool parameters, therefore we cannot call this 
		 * in the constructor :( */
		defineBuildStrategies(theToolParameters);
		
		if( !(anEntity instanceof lrg.memoria.core.Class) && !(anEntity instanceof lrg.memoria.core.System)) return;

		ArrayList<String> params = (ArrayList<String>)theToolParameters;
		
		String type = getParameterInitialValue().get(0), dir = getParameterInitialValue().get(1);
        int maxDepth;
        try{
        	maxDepth = Integer.parseInt(getParameterInitialValue().get(2));
        }catch (NumberFormatException e) {
			maxDepth = 7;
		}
        
        if(params.size() > 0)
        	type = params.get(0);
        if(params.size() > 1)
        	dir = params.get(1);
        if(params.size() > 2)
        	try{
            	maxDepth = Integer.parseInt(params.get(2));
            }catch (NumberFormatException e) {
    			maxDepth = 7;
    		}
            
		GroupEntity group = getGroupOfRootClasses(anEntity, type);
		
		boolean commonFiles = useCommonFiles();
		
		if(commonFiles)
			createGlobalFiles(dir);
		
		for(int i=0; i<group.size(); i++)
		{
			DataAbstraction rootClass = (DataAbstraction) group.getElementAt(i);
	
			if(!commonFiles)
				createLocalFiles(dir, rootClass);
			
			generateDotFiles(rootClass, type, dir, maxDepth);
		
			if(!commonFiles)
				closeFiles();
		}
		
		if(commonFiles)
			closeFiles();
		
	}


	private void closeFiles()
	{
		writeDotFooters();
		for(int i=0; i< buildRules.size(); i++)
			buildRules.get(i).closeFile();
		
	}


	private void createLocalFiles(String dir, DataAbstraction rootClass)
	{
		String rootClassName = rootClass.getName();
		for(int i=0; i< buildRules.size(); i++)
			buildRules.get(i).createFile(dir, rootClassName);

		writeDotHeaders(rootClassName);
	}


	private void createGlobalFiles(String dir)
	{	
		for(int i=0; i< buildRules.size(); i++)
			buildRules.get(i).createFile(dir, "global");

		writeDotHeaders("global");
	}


	private void generateDotFiles(DataAbstraction rootClass, String type, String dir, int maxDepth)
	{
		String rootClassName = rootClass.getName();
		for(int i=0; i< buildRules.size(); i++)
			buildRules.get(i).writeToFile(rootClassName + " [color=red];\n");
		
		for(int i=0; i< buildRules.size(); i++)
		{
			AbstractGraphBuildRule rule = buildRules.get(i);
			ArrayList<StrategyDefinition> strategies = rule.getStrategies(); 
			for(int j=0; j<strategies.size(); j++)
			{
				StrategyDefinition strategy = strategies.get(j);
				buildDependencyTree(rootClass, null, 0, maxDepth, 
						strategy.getInitialStrategy().getName(), strategy.getInitialStrategyParameter(), 
						strategy.getLevelTwoStrategy().getName(),
						rule.getWriter(), strategy.getEdgeColor(), strategy.isEdgeDirectedOutward());
			}
		}
	}

	private Component buildDependencyTree(
			AbstractEntityInterface theClass, 
			GroupEntity previouslyInterestingMethods, 
			int crtDepth, int maxDepth, 
			String initialStrategyClass, String initialStrategyParameters, 
			String levelTwoStrategyHelperClass,
			FileWriter writer, String edgeColor, boolean edgeDirection)
	{
	    if(crtDepth == maxDepth) 
	    	return new Component(theClass,crtDepth);

	    //compute the classes related to me (classes calling me or classes called by me)
	    InterestingMembersProvider provider = null;
	    if(crtDepth == 0) {
	        if(initialStrategyParameters == null)
	            provider = FactoryOfInterestingStuff.create(initialStrategyClass, theClass);
	        else
	            provider = FactoryOfInterestingStuff.create(initialStrategyClass, theClass, initialStrategyParameters);
	    }
	    else
	    	provider = FactoryOfInterestingStuff.create(levelTwoStrategyHelperClass, previouslyInterestingMethods.intersect(theClass.contains("method group")));
		    
	
	    GroupEntity interestingMethods = provider.methodRelated();// methods calling my methods or methods caled by my methods
	    GroupEntity classesWithInterestingMethods = ((GroupEntity) interestingMethods.belongsTo("class")).applyFilter("model class");
	
	    GroupEntity methodsAccessingMyAttributes = provider.attributeRelated();//methods accessing my attributes or attributes accessed by my methods
	    GroupEntity classesAccessingMyAttributes = ((GroupEntity) methodsAccessingMyAttributes.belongsTo("class")).applyFilter("model class");
	    FilteringRule notInnerClass = new NotComposedFilteringRule(new FilteringRule("is inner","is true","class"));
	
	    GroupEntity classesRelatedToMe = classesWithInterestingMethods.union(classesAccessingMyAttributes).distinct().exclude((AbstractEntity)theClass).applyFilter(notInnerClass);
	    
	    //the set is computed, go on, recursively
	    if(classesRelatedToMe.size() == 0) return new Component(theClass,crtDepth);
	
	    Iterator it = classesRelatedToMe.getElements().iterator();
	    Composite composedComponent = new Composite(theClass,crtDepth);
	
	    while(it.hasNext()) {
	        AbstractEntityInterface crtClass = (AbstractEntityInterface)it.next();
	        composedComponent.addComponentAndExportDot(
	        		buildDependencyTree(crtClass, interestingMethods, 
	        				crtDepth+1, maxDepth, 
	        				initialStrategyClass,initialStrategyParameters, 
	        				levelTwoStrategyHelperClass,
	        				writer, edgeColor, edgeDirection),
	        		writer, edgeColor, edgeDirection);
	        //note: only methods are considered important for the next step. Therefore, when detecting the "methods called by me" relation, all 
	        //the methods in the related classes must be considered: the attributesRelated() in level 2+ in this case should return 
	        // all methods in the classes with attributes referred by the current class.
	    }
	
	    return composedComponent;
	}

	}

class Component {
    protected AbstractEntityInterface theEntity;
    protected int depth;

    public String HTMLExport() {
        return AbstractDetail.linkTo(theEntity)+"\n";
    }

    /*
     * Export in graphwiz dot format for later graph visualisation.
     */
    public void dotFormatExport(FileWriter writer, String edgeColor, boolean isEdgeDirectedOutward) 
    {
    	return; 
    }

    public Component(AbstractEntityInterface anEntity, int depth) {
        theEntity = anEntity;
        this.depth = depth;
    }
}

/*
class Leaf extends Component {
    public Leaf(AbstractEntityInterface anEntity) {
        super(anEntity);
    }

    public String HTMLExport() {
        return AbstractDetail.linkTo(theEntity)+"\n";
    }
}
*/


class Composite extends Component {
    ArrayList theComponents;

    public Composite(AbstractEntityInterface anEntity, int depth) {
        super(anEntity,depth);
        theComponents = new ArrayList();
    }


    public void addComponentAndExportDot(Component aComponent, FileWriter writer, String edgeColor, boolean isEdgeDirectedOutward) {
        theComponents.add(aComponent);
        try
		{
			if(isEdgeDirectedOutward)
				writer.write(theEntity.getName() + " -> " + aComponent.theEntity.getName() + " [color=" + edgeColor +"];\n");
			else
				writer.write(aComponent.theEntity.getName() + " -> " + theEntity.getName() + " [color=" + edgeColor +"];\n");
		} catch (IOException e)
		{
			e.printStackTrace();
			return;
		}
    }

    /*
     * Export in graphwiz dot format for later graph visualizing.
     * @see lrg.insider.plugins.core.properties.memoria.classes.Component#dotFormatExport()
     */
    public void dotFormatExport(FileWriter writer, String edgeColor, boolean isEdgeDirectedOutward) { 
        Iterator it = theComponents.iterator();
        while(it.hasNext()) {
            Component crtComponent = (Component) it.next();
            try
			{
				if(isEdgeDirectedOutward)
					writer.write(theEntity.getName() + " -> " + crtComponent.theEntity.getName() + " [color=" + edgeColor +"];\n");
				else
					writer.write(crtComponent.theEntity.getName() + " -> " + theEntity.getName() + " [color=" + edgeColor +"];\n");
			} catch (IOException e)
			{
				e.printStackTrace();
				return;
			}
            crtComponent.dotFormatExport(writer, edgeColor, isEdgeDirectedOutward);
        }
    }

    public String HTMLExport() {
        String tmp = super.HTMLExport();
        Iterator it = theComponents.iterator();

        while(it.hasNext()) {
            Component crtComponent = (Component) it.next();
            for(int i = 0; i < depth; i++) tmp += "\t";
            tmp += crtComponent.HTMLExport();
        }
        return tmp;
    }

}
