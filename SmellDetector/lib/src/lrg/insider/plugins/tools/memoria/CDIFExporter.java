package lrg.insider.plugins.tools.memoria;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.tools.AbstractEntityTool;
import lrg.memoria.exporter.cdif.MooseCDIFExporter;

public class CDIFExporter extends AbstractEntityTool {

    public CDIFExporter() {
        super("CDIF Exporter", "Exports the model into CDIF", "system");
    }

    public void run(AbstractEntityInterface abstractEntityInterface, Object o) {
        if(abstractEntityInterface instanceof lrg.memoria.core.System == false) return;

        lrg.memoria.core.System aSystem = (lrg.memoria.core.System) abstractEntityInterface;

        ArrayList<String> params = (ArrayList<String>)o;
        File outputFile = new File(params.get(0));
        try {
            new MooseCDIFExporter(aSystem).exportToStream(new PrintStream(new FileOutputStream(outputFile)));
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    public String getToolName() {
        return "CDIF Exporter";
    }

    public ArrayList<String> getParameterList() {
        ArrayList<String> parList = new ArrayList<String>();
        parList.add("Output File ");
        return parList;
    }

    public ArrayList<String> getParameterExplanations() {
        ArrayList<String> parList = new ArrayList<String>();
        parList.add("The CDIF file name where the model will be exported");
        return parList;
    }
}
