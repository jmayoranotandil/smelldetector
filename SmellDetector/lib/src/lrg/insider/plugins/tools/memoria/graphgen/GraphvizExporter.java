package lrg.insider.plugins.tools.memoria.graphgen;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import lrg.insider.plugins.tools.memoria.graphgen.utils.StringUtils;
import lrg.memoria.core.DataAbstraction;

import org.jgrapht.Graph;

import cdc.clusters.ExtendedWeightedEdge;
import cdc.clusters.Node;

public class GraphvizExporter {
	
	//colors for component background
	private static final double minHue = 0.15, maxHue = 1, hueStep = 0.2;
	private static double crtHue = minHue;
	//private static final double fixedSat = 0.13, fixedLum = 1.0;
	private static final double fixedSat = 0.941176, fixedLum = 0.9375;
	
	public static void exportGraph(Graph<Node, ExtendedWeightedEdge> graph, String graphName, String dirName, String fileName)
		{
			try
			{
				FileWriter writer = new FileWriter(dirName+"/"+fileName);
				
				writeHeader(graphName, writer);
				
				writeGraphContent(graph, writer);
				
				writeFooter(writer);
				
				writer.close();
			} catch (IOException e)
			{
				e.printStackTrace();
			}
	
		}

	private static void writeFooter(FileWriter writer) throws IOException
	{
		writer.write("}\n");		
	}

	private static void writeHeader(String graphName, FileWriter writer) throws IOException
	{
		String text = "// Generated at " + (new GregorianCalendar()).getTime().toString() +" by GraphGenerator, \n// (c) Dan Cosma & LRG, 2006\n\n";
		text += "digraph " + graphName + "\n{\n"; 
		text += "node [color=green];\n";
		writer.write(text);
	}
	
	public static void exportClusters(SystemGraph<Node, ExtendedWeightedEdge> graph, List<SystemSubgraph<Node, ExtendedWeightedEdge>> clusters, String graphName, String dirName, String fileName, boolean colouredClusters)
	{
		try
		{
			FileWriter writer = new FileWriter(dirName+"/"+fileName);
			
			writeHeader(graphName, writer);
			
			String text = "";
			text += "compound=true;\n";
			writer.write(text);
			
			for(SystemSubgraph<Node, ExtendedWeightedEdge> subgraph : clusters)
			{
				text = "subgraph " + "cluster_"+subgraph.getName() + "{\n";//graphviz needs names starting with "cluster_" to draw the clusters!...
				
				if(colouredClusters)
				{
					text += "bgcolor =\"#" + hslToRGB(crtHue, fixedSat, fixedLum) +"\"";
				}
				text += "label=\""+subgraph.getName()+"\";\n";
				text += "fontsize=24;\n";

				writer.write(text);
				
				writeGraphContent(subgraph, writer);
				
				text = "}\n";
				writer.write(text);
				
				crtHue += hueStep;
				if(crtHue > maxHue)
					crtHue = minHue;
			}
			
			for(SystemSubgraph<Node, ExtendedWeightedEdge> subgraph : clusters)
			{
				lrg.memoria.core.Component component = subgraph.getCorrespondingComponent();
				ArrayList<ExtendedWeightedEdge> remoteEdges =(ArrayList<ExtendedWeightedEdge>) component.getAnnotation(AnnotationConstants.COMPONENT_REMOTEEDGES);
				
				for(ExtendedWeightedEdge edge : remoteEdges)
				{
					writeEdge(graph, writer, edge);
				}
			}
			
			writeFooter(writer);
			
			writer.close();
			
		} catch (IOException e)
		{
			e.printStackTrace();
		}
		
	}

	private static void writeGraphContent(Graph<Node, ExtendedWeightedEdge> graph, FileWriter writer) throws IOException
	{
		Set<Node> nodes = graph.vertexSet();
		Iterator<Node> it1=nodes.iterator();
		while(it1.hasNext())
		{
			Node node = it1.next();
			
			String color = "blue";
			
			if(((DataAbstraction)node.getObject()).getAnnotation(AnnotationConstants.CLASS_ISFRONTIER)!=null)
				color = "red";
			
			String name = node.getName();
			String label = name.substring(name.lastIndexOf('.')+1);
			
			String comment = "";
/*				if(((lrg.memoria.core.Class)node.getObject()).isInterface())
				comment="<<i>> ";*/
			
			String fontcolor = "black";
			if(((DataAbstraction)node.getObject()).isInterface())
				fontcolor = "forestgreen";

			writer.write(StringUtils.noDots(name) + " [color=" + color +", label=\"" + comment + label +"\", fontcolor="+fontcolor+"];\n");
		}
		
		Set<ExtendedWeightedEdge> edges = graph.edgeSet();
		
		Iterator<ExtendedWeightedEdge> it2=edges.iterator();
		while(it2.hasNext())
		{
			ExtendedWeightedEdge edge = it2.next();
			writeEdge(graph, writer, edge);
		}
	}

	private static void writeEdge(Graph<Node, ExtendedWeightedEdge> graph, FileWriter writer, ExtendedWeightedEdge edge) throws IOException
	{
		String color = edge.getProperty(EdgeConstants.PROPERTY_COLOUR);
		if(color == null)
			color = "green";
		
		String label = "";
		if((edge.getProperty(EdgeConstants.PROPERTY_LINKSTOFRONTIER)!=null)
				&&(edge.getProperty(EdgeConstants.PROPERTY_INHERITANCE)==null))
		{
			label = "remote";
			color = "red";
		}

		/*
		String s = edge.getProperty(EdgeConstants.PROPERTY_MULTIPLICITY);
		if(s!=null)
			label += "[" + s + "]";
		*/

		String arrow = "normal";
		if(edge.getProperty(EdgeConstants.PROPERTY_INHERITANCE)!=null)
			arrow = "empty";
			 
		writer.write(StringUtils.noDots(graph.getEdgeSource(edge).getName()) + " -> " + StringUtils.noDots(graph.getEdgeTarget(edge).getName()) + " [color=" + color + ", label=\"" + label + "\", arrowhead="+ arrow + "];\n");
	}

	private static String hslToRGB(double h, double s, double l)
	{
		double color[] = new double[3]; // r,g,b;

		if(s==0)
		{
			for(int i=0; i<3; i++)
				color[i]=l;
		}
		else
		{

			double temp2;
			//three variables for R, G, B at indices 0, 1, 2 respectively
			double temp3[] = new double[3];

			if(l<0.5)
				temp2=l*(1.0+s);
			else
				temp2=l+s - (l*s);

			double temp1 = 2.0 * l - temp2;

			temp3[0] = h+1.0/3.0; //R 
			temp3[1]=h; //G
			temp3[2]=h-1.0/3.0; //B

			for(int i=0; i<3; i++)
			{
				if(temp3[i]<0) temp3[i]+=1.0;
				if(temp3[i]>1.0)  temp3[i]-=1.0;
			}

			for(int i=0; i<3; i++)
			{
				if(temp3[i]<1.0/6.0)
					color[i] = temp1 + ((temp2 - temp1) * 6.0 * temp3[i]);
				else
					if((temp3[i]>=1.0/6.0)&&(temp3[i]<1.0/2.0))
						color[i] = temp2;
					else
						if((temp3[i]>=1.0/2.0)&&(temp3[i]<2.0/3.0))
							color[i] = temp1 + ((temp2-temp1)*(2.0/3.0-temp3[i])*6.0);
						else
							color[i] = temp1;
			}
		}
		
		String hex = ""; 
		for(int i=0; i<3; i++)
		{
			int c = (int)(255.0*color[i]);
			hex += Integer.toHexString(c);
		}
		
		return hex;
	}

}
