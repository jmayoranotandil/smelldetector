package lrg.insider.plugins.tools.memoria.graphgen.strategies;

import java.util.ArrayList;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;

public class UsedMembersLevelTwo extends InterestingMembersProvider {

	public UsedMembersLevelTwo(AbstractEntityInterface aei)
	{
		super(aei);
	}

	@Override
	public GroupEntity attributeRelated()
	{	//returns the methods in the classes with attributes referred by the current methods
		return new GroupEntity("empty group", new ArrayList());
		/*
		AbstractEntityInterface entity = theCurrentEntity;
		
		if(theCurrentEntity instanceof lrg.memoria.core.Class)
		{
			lrg.memoria.core.Class theClass = (lrg.memoria.core.Class)theCurrentEntity;
			
			if(theClass.isInterface())
				entity = theClass.getGroup("all descendants");
		}
		return entity.uses("accessed model classes").exclude(theCurrentEntity.belongsTo("class")).contains("method group");
		*/
	}

	@Override
	public GroupEntity methodRelated()
	{
		AbstractEntityInterface entity = theCurrentEntity;
		
		return entity.uses("operations called");
	}

}
