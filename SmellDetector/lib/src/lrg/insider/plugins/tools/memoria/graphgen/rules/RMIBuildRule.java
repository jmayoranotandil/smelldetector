package lrg.insider.plugins.tools.memoria.graphgen.rules;

import lrg.insider.plugins.tools.memoria.graphgen.strategies.AllUsedMembers;
import lrg.insider.plugins.tools.memoria.graphgen.strategies.UsedMembersLevelTwo;
import lrg.insider.plugins.tools.memoria.graphgen.strategies.UsersOfAllMembers;
import lrg.insider.plugins.tools.memoria.graphgen.strategies.UsersOfMembersLevelTwo;

public class RMIBuildRule extends AbstractGraphBuildRule {

	public RMIBuildRule()
	{
		super();
		
		strategies.add(new StrategyDefinition("blue", false, UsersOfAllMembers.class, null, 
											  UsersOfMembersLevelTwo.class, null));
		strategies.add(new StrategyDefinition("orange", true, AllUsedMembers.class, null, 
				  UsedMembersLevelTwo.class, null));
	}
	
	@Override
	protected String fileNameSuffix()
	{
		return "-rmi-all";
	}

}
