package lrg.insider.plugins.tools.memoria.graphgen;

public class SystemOneFileGraphGenerator extends SystemGraphGenerator {

	public SystemOneFileGraphGenerator()
	{
		super("One-File GraphGenerator", "Creates class dependency graphs as GraphViz dot format files", "system");
		// TODO Auto-generated constructor stub
	}

	@Override
	protected boolean useCommonFiles()
	{
		return true;
	}

}
