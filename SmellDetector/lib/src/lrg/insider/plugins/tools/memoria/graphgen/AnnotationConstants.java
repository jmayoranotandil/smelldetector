package lrg.insider.plugins.tools.memoria.graphgen;

import java.util.ArrayList;

import cdc.clusters.ExtendedWeightedEdge;

public final class AnnotationConstants {
	//for classes
	public static final String CLASS_ISFRONTIER = "class_isFrontier"; 
	public static final String CLASS_ISROOT = "class_isRoot"; //classes generated as the root elemens for building the graph
	public static final String CLASS_ISANCESTORTO = "class_isAncestorTo";
	public static final String CLASS_JGRAPHTNODE = "jgraphtnode";
	
	//for system
	/**
	 * all components as ModelElementList<Component>
	 */
	public static final String SYSTEM_ALLCOMPONENTS = "system_allComponents";
	public static final String SYSTEM_JGRAPHTGRAPH = "system_jgraphtgraph";
	
	//for components
	/**
	 * The system this component belongs to.
	 */
	public static final String COMPONENT_SYSTEM = "component_system";
	/**
	 * The subgraph corresponding to this component. Type: {@link SystemSubgraph}
	 */
	public static final String COMPONENT_JGRAPHTSUBGRAPH = "component_jgraphtsubgraph"; //SystemSubgraph
	/**
	 * The edges that start in this component and link to other components.
	 * Type: {@link ArrayList} of {@link ExtendedWeightedEdge}
	 */
	public static final String COMPONENT_REMOTEEDGES = "component_remoteEdges"; // ArrayList of ExtendedWeightedEdge
	
	//generic
	public static final String CLASS_COMPONENT = "component";
}
