package lrg.insider.plugins.tools.memoria.graphgen.strategies;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.plugins.filters.FilteringRule;

public class UsersOfMembersByFieldType extends InterestingMembersProvider {
    protected String nameOfFieldType;
    private GroupEntity attributesWithSpecificTypeName;
    public UsersOfMembersByFieldType(AbstractEntityInterface aei, String fieldType) {
        super(aei);
        nameOfFieldType = fieldType;
        attributesWithSpecificTypeName = null;
    }

    protected GroupEntity getAttributesWithSpecificTypeName() {
        if(attributesWithSpecificTypeName == null) {
            FilteringRule hasTypeName = new FilteringRule("Type", "contain", "attribute", nameOfFieldType);
            attributesWithSpecificTypeName = theCurrentEntity.contains("attribute group").applyFilter(hasTypeName);
        }
        return attributesWithSpecificTypeName;

    }

    public GroupEntity methodRelated() {
        return getAttributesWithSpecificTypeName().isUsed("methods accessing variable").
                intersect(theCurrentEntity.contains("method group")).
                    isUsed("operations calling me");

        /* BALANCED VERSION --- > codul de mai sus DISPARE SE INLOC. cu cel de mai jos
        // myInterestingMethods se califica direct
       GroupEntity myInterestingMethods = getAttributesWithSpecificTypeName().isUsed("methods accessing variable").
                intersect(theCurrentEntity.contains("method group"));

        // ownCallers se califica "la baraj" ca le apeleaza pe alea calificate direct
       GroupEntity ownCallers = myInterestingMethods.isUsed("operations calling me").intersect(theCurrentEntity.contains("method group"));
       return myInterestingMethods.union(ownCallers);
       */
    }

    public GroupEntity attributeRelated() {
        return getAttributesWithSpecificTypeName().isUsed("methods accessing variable").
                 exclude(theCurrentEntity.contains("method group"));
    }
}
