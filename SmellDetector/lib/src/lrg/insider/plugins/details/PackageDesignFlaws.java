package lrg.insider.plugins.details;

import java.text.DecimalFormat;
import java.util.ArrayList;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.insider.plugins.core.details.HTMLDetail;
import lrg.insider.plugins.filters.memoria.packages.SDPViolation;

public class PackageDesignFlaws extends HTMLDetail {
	
	public PackageDesignFlaws() {
        super("Details of Package Design Flaws", "Package Design Flaws", "package");
	}
	public ResultEntity compute(AbstractEntityInterface aPackage) {
        String text = "<h1>" + "Package " + linkTo(aPackage) + "</h1><hr><br>";
        
        GroupEntity faninClassGroup =  aPackage.getGroup("fanin class group");
        double fanin = faninClassGroup.size();
        double faninP = ((GroupEntity)faninClassGroup.belongsTo("package")).distinct().size();
        
        GroupEntity fanoutClassGroup =  aPackage.getGroup("fanout class group");
        double fanout= fanoutClassGroup.size();
        double fanoutP = ((GroupEntity)fanoutClassGroup.belongsTo("package")).distinct().size();

        double IF = (Double) aPackage.getProperty("IF").getValue();
        DecimalFormat twoDecimals = new DecimalFormat("#0.00");
        
        text += "<p>The package is used by " + fanin + " classes from " + faninP + "packages.</p>";
        text += "<p>The package uses " + fanout + " classes from " + fanoutP + "packages.</p>";        
        text += "<p>The package has an Instability Factor of " + twoDecimals.format(IF) + "</p>";
        
        ArrayList<AbstractEntityInterface> sdpBreakers = SDPViolation.SDPBreakers(aPackage);
        
        if(sdpBreakers.size() == 0) return new ResultEntity(text);

        text += "<p>The package breaks the Stable Dependencies Principle(SDP), by depending on following packages, which are more instable than the current package:</p>";
        
		text += "<ul>";
		for (AbstractEntityInterface crtBreaker : sdpBreakers) {
			text += "<li>" + linkTo(crtBreaker) + "(InstabilityFactor = " + twoDecimals.format(crtBreaker.getProperty("IF").getValue()) + ")"; 
		}
		text += "</ul>";

    	return new ResultEntity(text);
	}

}
