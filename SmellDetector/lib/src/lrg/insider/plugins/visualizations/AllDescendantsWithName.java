package lrg.insider.plugins.visualizations;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Iterator;

import lrg.common.abstractions.entities.AbstractEntity;
import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.plugins.visualization.AbstractVisualization;
import lrg.insider.util.Visualization;
import lrg.jMondrian.commands.AbstractNumericalCommand;
import lrg.jMondrian.figures.Figure;
import lrg.jMondrian.layouts.CrossReductionTreeLayout;
import lrg.jMondrian.painters.LineEdgePainter;
import lrg.jMondrian.painters.RectangleNodePainter;
import lrg.jMondrian.view.ViewRenderer;
import lrg.memoria.core.InheritanceRelation;

public class AllDescendantsWithName extends AbstractVisualization {

    public AllDescendantsWithName() {
        super("All Descendants With Name", "All Descendants With Name", "class");
    }

    public void view(AbstractEntityInterface entity) {

        GroupEntity allClasses = entity.getGroup("all descendants");
        allClasses = allClasses.union((AbstractEntity)entity).distinct();

        GroupEntity allEdges = entity.belongsTo("system").getGroup("all inheritance relations");
        ArrayList edges = new ArrayList();
        Iterator<InheritanceRelation> it = allEdges.iterator();
        while(it.hasNext()) {
            InheritanceRelation rel = it.next();
            if(allClasses.isInGroup(rel.getSubClass()) && allClasses.isInGroup(rel.getSuperClass())) {
                edges.add(rel);
            }
        }
        Figure f = new Figure();
        f.nodesUsing(allClasses.getElements(), new RectangleNodePainter(20,20,true).color(
                new AbstractNumericalCommand() {
                    public double execute() {
                        if((Boolean)((AbstractEntityInterface)receiver).getProperty("is interface").getValue()) {
                            return Color.LIGHT_GRAY.getRGB();
                        } else if((Boolean)((AbstractEntityInterface)receiver).getProperty("is abstract").getValue()) {
                                return Color.DARK_GRAY.getRGB();
                        } else {
                            return Color.WHITE.getRGB();
                        }
                    }
                }
        ).name(Visualization.stringCommand("Name")));
        f.edgesUsing(edges, new LineEdgePainter(Visualization.entityCommand("getSubClass"),Visualization.entityCommand("getSuperClass")));
        f.layout(new CrossReductionTreeLayout(20,50,false));

        ViewRenderer r = new ViewRenderer("All Descendants");
        f.renderOn(r);
        r.open();

    }

}
