package lrg.insider.plugins.properties.memoria.methods;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.plugins.filters.FilteringRule;
import lrg.common.abstractions.plugins.filters.composed.NotComposedFilteringRule;
import lrg.common.abstractions.plugins.groups.GroupEntityBuilder;
import lrg.insider.plugins.filters.memoria.methods.IsAccessor;
import lrg.insider.plugins.filters.memoria.variables.IsConstant;

public class AccessedModelData extends GroupEntityBuilder {

    public AccessedModelData(String name, String descr) {
        super(name,descr,"method");
    }

    public AccessedModelData() {
        this("accessed model data","used attributes or called accessor methods");
    }

// WARNING this creates a mixed group!!!
    public GroupEntity buildGroupEntity(AbstractEntityInterface aMethod) {
        FilteringRule isAccessor = new IsAccessor();
        FilteringRule notConstant = new NotComposedFilteringRule(new IsConstant());

        GroupEntity allAccesses = aMethod.uses("variables accessed").applyFilter("is attribute").applyFilter(notConstant);

        allAccesses = allAccesses.union(aMethod.uses("operations called").applyFilter(isAccessor));

        //GroupEntity anc=new CurrentClassAndAncestors().buildGroupEntity(aMethod);
        //anc.getGroup("method group").union(anc.getGroup("attribute group")).intersect(allAccesses);
        return allAccesses;
    }
}
