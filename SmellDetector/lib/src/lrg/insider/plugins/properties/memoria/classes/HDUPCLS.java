package lrg.insider.plugins.properties.memoria.classes;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.properties.PropertyComputer;

public class HDUPCLS extends PropertyComputer {
    public HDUPCLS() {
        super("HDUPCLS", "Number of Classes in Same Hierarchy containing Common Duplication", "class", "numerical");
    }

    public ResultEntity compute(AbstractEntityInterface anEntity) {
        GroupEntity siblingClasses = (GroupEntity) anEntity.contains("method group").getGroup("same hierarchy duplicated methods").belongsTo("class");
        return new ResultEntity(siblingClasses.distinct().size());
    }
}
