package lrg.insider.plugins.properties.memoria.methods;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.properties.PropertyComputer;
import lrg.dude.duplication.DuplicationList;

public class DUPSTRING extends PropertyComputer {
    public DUPSTRING() {
        super("DUPSTRING", "", "method", "string");
    }

    public ResultEntity compute(AbstractEntityInterface anEntity) {
        if (anEntity instanceof lrg.memoria.core.Method == false) return null;

        ResultEntity aResult = anEntity.getProperty("#DUPLICATION#");

        if (aResult == null) return new ResultEntity("");

        DuplicationList aDudeList = (DuplicationList) aResult.getValue();

        return new ResultEntity(aDudeList.toString());
    }
}
