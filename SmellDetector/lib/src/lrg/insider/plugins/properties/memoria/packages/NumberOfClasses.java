package lrg.insider.plugins.properties.memoria.packages;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.plugins.groups.GroupEntityBuilder;
import lrg.common.abstractions.plugins.properties.PropertyComputer;

class ModelClassesInPackage extends GroupEntityBuilder {
    public ModelClassesInPackage() {
        super("model classes in package", "model classes in package", "package");
    }

    public GroupEntity buildGroupEntity(AbstractEntityInterface aPackage) {
        return aPackage.contains("class group").applyFilter("model class");
    }
}

public class NumberOfClasses extends PropertyComputer {
    public NumberOfClasses() {
        super("NOC", "Number of Classes", "package", "numerical");
        basedOnGroup(new ModelClassesInPackage());
    }
}
