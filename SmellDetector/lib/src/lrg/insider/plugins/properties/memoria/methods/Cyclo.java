package lrg.insider.plugins.properties.memoria.methods;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.properties.PropertyComputer;
import lrg.memoria.core.Body;
import lrg.memoria.core.Function;

public class Cyclo extends PropertyComputer {
    public Cyclo() {
        super("CYCLO", "Cyclomatic complexity", new String[]{"method", "global function"}, "numerical");

    }

    public lrg.common.abstractions.entities.ResultEntity compute(AbstractEntityInterface anEntity) {
        if (anEntity instanceof lrg.memoria.core.Function == false)
            return null;

        Function aFunction = (Function) anEntity;

        Body aBody = aFunction.getBody();

        if (aBody == null) return new ResultEntity(0);
        return new ResultEntity(aBody.getCyclomaticNumber());

        // return new ResultEntity(((NumericalResult) new MaximumNumberOfBranches().measure((lrg.memoria.core.Method) anEntity)).getValue());
    }
}
