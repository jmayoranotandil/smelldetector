package lrg.insider.plugins.properties.memoria.packages;

import lrg.common.abstractions.plugins.properties.PropertyComputer;
import lrg.insider.plugins.groups.memoria.EfferentCoupling;

public class NOEC extends PropertyComputer {
    public NOEC() {
        super("NOEC", "Number of Efferent Coupling", "package", "numerical");
        basedOnGroup(new EfferentCoupling());
    }
}

