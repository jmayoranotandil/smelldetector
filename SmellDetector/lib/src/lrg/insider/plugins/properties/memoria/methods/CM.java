package lrg.insider.plugins.properties.memoria.methods;

import lrg.common.abstractions.plugins.properties.PropertyComputer;

public class CM extends PropertyComputer {
    public CM() {
        super("CM", "Changing Methods", "method", "numerical");
        basedOnDistinctGroup(new ChangingMethods());
    }
}