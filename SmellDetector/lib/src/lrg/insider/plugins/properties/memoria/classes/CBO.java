package lrg.insider.plugins.properties.memoria.classes;

import lrg.common.abstractions.plugins.properties.PropertyComputer;
import lrg.insider.plugins.groups.memoria.ClassExternalServiceProviders;


public class CBO extends PropertyComputer {
    public CBO() {
        super("CBO", "Number of Coupled Classes", "class", "numerical");
        basedOnDistinctGroup(new ClassExternalServiceProviders());
    }
}
