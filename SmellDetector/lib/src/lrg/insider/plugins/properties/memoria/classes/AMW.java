package lrg.insider.plugins.properties.memoria.classes;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.properties.PropertyComputer;

public class AMW extends PropertyComputer {
    public AMW() {
        super("AMW", "Average Method Weight (complexity)", "class", "numerical");
    }

    public ResultEntity compute(AbstractEntityInterface measuredClass) {
        double nom = ((Double) measuredClass.getProperty("NOM").getValue()).doubleValue();
        double wmc = ((Double) measuredClass.getProperty("WMC").getValue()).doubleValue();

        if (nom == 0) return new ResultEntity(0);
        return new ResultEntity(wmc / nom);
    }
}
