package lrg.insider.plugins.properties.memoria.classes;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.plugins.groups.GroupEntityBuilder;
import lrg.common.abstractions.plugins.properties.PropertyComputer;

class ChangingClasses extends GroupEntityBuilder {
    public ChangingClasses() {
        super("group of changing classes",
                "classes that would be potentiall affected by a change in analyzed class", "class");
    }

    public GroupEntity buildGroupEntity(AbstractEntityInterface measuredClass) {
        GroupEntity clientMethods = new ChangingMethods().buildGroupEntity(measuredClass);
        return ((GroupEntity) clientMethods.belongsTo("class"));
    }
}

public class CC extends PropertyComputer {
    public CC() {
        super("CC", "Changing Classes", "class", "numerical");
        basedOnDistinctGroup(new ChangingClasses());
    }
}
