package lrg.insider.plugins.properties.memoria.methods;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;

public class AccessedModelClasses extends AccessedModelData {

    public AccessedModelClasses() {
        super("accessed model classes",
                "model classes accessed by using attributes or calling accessor methods");
    }

    public GroupEntity buildGroupEntity(AbstractEntityInterface aMethod) {
        return ((GroupEntity)super.buildGroupEntity(aMethod).
                belongsTo("class")).applyFilter("model class");
    }
}
