package lrg.insider.plugins.properties.memoria.methods;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.plugins.groups.GroupEntityBuilder;

public class CurrentClassAndAncestors extends GroupEntityBuilder {
    public CurrentClassAndAncestors() {
        super("current class and ancestors", "a class and its ancestors", "method");
    }

    public GroupEntity buildGroupEntity(AbstractEntityInterface aMethod) {
        return aMethod.belongsTo("class").uses("all ancestors").union(aMethod.belongsTo("class"));
    }
}
