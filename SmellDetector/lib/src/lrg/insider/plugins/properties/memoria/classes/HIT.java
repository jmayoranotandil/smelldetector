package lrg.insider.plugins.properties.memoria.classes;

import java.util.ArrayList;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.entities.GroupEntity;
import lrg.common.abstractions.entities.ResultEntity;
import lrg.common.abstractions.plugins.properties.PropertyComputer;
import lrg.memoria.core.DataAbstraction;

public class HIT extends PropertyComputer {
    public HIT() {
        super("HIT", "Height of Inheritance Tree", "class", "numerical");
    }

    private int level(DataAbstraction actcls, DataAbstraction parent) {
        int i, max = 0, aux;
        ArrayList descs = actcls.getDescendants();
        DataAbstraction crt;

        //we consider that the height of the current class is 0
        if (descs.size() == 0) return 0;

        for (i = 0; i < descs.size(); i++) {
            try {
                crt = (DataAbstraction) descs.get(i);
                if (parent == crt) continue;
            } catch (ClassCastException e) {
                continue;
            }
            aux = level(crt, actcls);
            if (aux > max) max = aux;
        }

        return max + 1;
    }

    public ResultEntity compute(AbstractEntityInterface anEntity) {
        if (anEntity instanceof lrg.memoria.core.Class == false)
            return null;

        GroupEntity derivedClasses = anEntity.getGroup("derived classes").getGroup("derived classes");

        // NumericalResult aResult = (NumericalResult) new HeightOfInheritanceTree().measure((lrg.memoria.core.Class) anEntity);
        return new ResultEntity(level((DataAbstraction) anEntity, null));
    }
}
