package lrg.insider.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;

import lrg.common.metamodel.MetaModel;
import lrg.insider.metamodel.MemoriaJavaModelBuilder;
import lrg.insider.metamodel.MemoriaModelBuilder;

import org.tmatesoft.svn.core.SVNCommitInfo;
import org.tmatesoft.svn.core.SVNErrorCode;
import org.tmatesoft.svn.core.SVNErrorMessage;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.SVNNodeKind;
import org.tmatesoft.svn.core.SVNURL;
import org.tmatesoft.svn.core.auth.ISVNAuthenticationManager;
import org.tmatesoft.svn.core.internal.io.dav.DAVRepositoryFactory;
import org.tmatesoft.svn.core.io.ISVNEditor;
import org.tmatesoft.svn.core.io.ISVNReporter;
import org.tmatesoft.svn.core.io.ISVNReporterBaton;
import org.tmatesoft.svn.core.io.SVNRepository;
import org.tmatesoft.svn.core.io.SVNRepositoryFactory;
import org.tmatesoft.svn.core.io.diff.SVNDeltaProcessor;
import org.tmatesoft.svn.core.io.diff.SVNDiffWindow;
import org.tmatesoft.svn.core.wc.SVNWCUtil;

// STORY: Sa municm voiosi cu spor
class ExportReporterBaton implements ISVNReporterBaton {
    private long exportRevision;

    public ExportReporterBaton( long revision ){
        exportRevision = revision;
    }

    public void report( ISVNReporter reporter ) throws SVNException {
        try {
            reporter.setPath( "" , null , exportRevision , true );
            reporter.finishReport( );
        } catch( SVNException svne ) {
            reporter.abortReport( );
            System.out.println( "Report failed" );
        }
    }
}


class ExportEditor implements ISVNEditor {

    private File myRootDirectory;
    private SVNDeltaProcessor myDeltaProcessor;

    public ExportEditor( File root ) {
        myRootDirectory = root;

        /*
        * Utility class that will help us to transform 'deltas' sent by the
        * server to the new file contents.
        */
        myDeltaProcessor = new SVNDeltaProcessor( );
    }

    public void targetRevision( long revision ) throws SVNException {
    }

    public void openRoot( long revision ) throws SVNException {
    }

    public void addDir( String path , String copyFromPath , long copyFromRevision ) throws SVNException {
        File newDir = new File( myRootDirectory , path );
        if ( !newDir.exists( ) ) {
            if ( !newDir.mkdirs( ) ) {
                SVNErrorMessage err = SVNErrorMessage.create( SVNErrorCode.IO_ERROR , "error: failed to add the directory ''{0}''." , newDir );
                throw new SVNException( err );
            }
        }
        System.out.println( "dir added: " + path );
    }

    public void openDir( String path , long revision ) throws SVNException {
    }

    public void changeDirProperty( String name , String value ) throws SVNException {
    }

    public void addFile( String path , String copyFromPath , long copyFromRevision ) throws SVNException {
        File file = new File( myRootDirectory , path );
        if ( file.exists( ) ) {
            SVNErrorMessage err = SVNErrorMessage.create( SVNErrorCode.IO_ERROR , "error: exported file ''{0}'' already exists!" , file );
            throw new SVNException( err );
        }

        try {
            file.createNewFile( );
        } catch ( IOException e ) {
            SVNErrorMessage err = SVNErrorMessage.create( SVNErrorCode.IO_ERROR , "error: cannot create new  file ''{0}''" , file );
            throw new SVNException( err );
        }
    }

    public void openFile( String path , long revision ) throws SVNException {
    }

    public void changeFileProperty( String path , String name , String value ) throws SVNException {
    }
    public void applyTextDelta( String path , String baseChecksum ) throws SVNException {
        myDeltaProcessor.applyTextDelta( null , new File( myRootDirectory , path ) , false );
    }

    public OutputStream textDeltaChunk( String path , SVNDiffWindow diffWindow )   throws SVNException {
        return myDeltaProcessor.textDeltaChunk( diffWindow );
    }

    public void textDeltaEnd(String path) throws SVNException {
        myDeltaProcessor.textDeltaEnd( );
    }

    public void closeFile( String path , String textChecksum ) throws SVNException {
        System.out.println( "file added: " + path );
    }

    public void closeDir( ) throws SVNException {
    }

    public void deleteEntry( String path , long revision ) throws SVNException {
    }

    public void absentDir( String path ) throws SVNException {
    }

    public void absentFile( String path ) throws SVNException {
    }

    public SVNCommitInfo closeEdit( ) throws SVNException {
        return null;
    }

    public void abortEdit( ) throws SVNException {
    }
}


public class SVNExport {
   	private static int endDay = 31;
	private static int beginDay = 1;	
	private static int beginYear = 2004;
	private static int endYear = 2006;
	private static int beginMonth = 1;
	private static int endMonth = 12;
	private static String folder = "../../jedit";
	private static String SVNURI = "https://jedit.svn.sourceforge.net/svnroot/jedit/jEdit/trunk/org/gjt/sp/jedit/gui";
	private static int stepInDays = 240;
   
	// Deletes all files and subdirectories under dir.
    // Returns true if all deletions were successful.
    // If a deletion fails, the method stops attempting to delete and returns false.
	
	public static boolean deleteDir(File dir) {
        if (dir.isDirectory()) {
            String[] children = dir.list();
            for (int i=0; i<children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }

        // The directory is now empty so delete it
        return dir.delete();
    }


    public static void main( String[] args )  throws Exception {
        Date date;
        String configFileName = "./svnconfig.txt";
        
        if((args.length != 0) && (args.length != 1)) {
        	System.err.println("Usage: svn2mse [configfile]");
        	System.exit(1);
        }
        
        if(args.length == 1) configFileName = args[0];
        
        processConfigFile(configFileName);
        Calendar calendar = Calendar.getInstance();
        Calendar endCalendar = Calendar.getInstance();
        endCalendar.set(endYear, endMonth - 1, endDay);
        for(calendar.set(beginYear, beginMonth - 1, beginDay); calendar.before(endCalendar); calendar.add(Calendar.DAY_OF_YEAR, stepInDays )) {
        	date = calendar.getTime();
        	int month = calendar.get(Calendar.MONTH) + 1;
        	int day = calendar.get(Calendar.DAY_OF_MONTH);
        	long revisionNumber = exportVersionFromSVN(date);
            String dateLiniute = calendar.get(Calendar.YEAR)+"-"+
            						(month<9?"0":"")+month+"-"+
            						(day>9?"":"0")+ day+
            						"-r"+revisionNumber;
            MemoriaModelBuilder theModel = new MemoriaJavaModelBuilder("../../svn-export", "model-"+dateLiniute+".dat", "", null);
            MetaModel.createFrom(theModel, folder);
            File outputFile = new File(folder+"/"+dateLiniute+".mse");
            try {
                new MooseMSEExporter(theModel.getCurrentSystem()).exportToStream(new PrintStream(new FileOutputStream(outputFile)));
             } catch (FileNotFoundException e) { throw new RuntimeException(e.getMessage()); }
            MetaModel.closeMetaModel();
        }
    }


	private static void processConfigFile(String configFileName) {
		try { 
			Properties svnConfig = new Properties();
			svnConfig.load(new FileInputStream(configFileName));
			SVNURI = svnConfig.getProperty("SVNURL");
			beginYear = Integer.parseInt(svnConfig.getProperty("BEGIN_YEAR"));
			beginMonth = Integer.parseInt(svnConfig.getProperty("BEGIN_MONTH"));
			beginDay = Integer.parseInt(svnConfig.getProperty("BEGIN_DAY"));

			endYear = Integer.parseInt(svnConfig.getProperty("END_YEAR"));
			endMonth = Integer.parseInt(svnConfig.getProperty("END_MONTH"));
			endDay = Integer.parseInt(svnConfig.getProperty("END_DAY"));

			stepInDays = Integer.parseInt(svnConfig.getProperty("DAYS_BETWEEN_SAMPLES"));
			folder = svnConfig.getProperty("EXPORT_FOLDER");			
		} catch (IOException e) { 
			e.printStackTrace();
		}
	}


	public static long exportVersionFromSVN(Date versionDate) throws Exception {
        DAVRepositoryFactory.setup( );
        // SVNURL url = SVNURL.parseURIEncoded( "http://argouml.tigris.org/svn/argouml/trunk/src_new" );
        // SVNURL url = SVNURL.parseURIEncoded( "https://jedit.svn.sourceforge.net/svnroot/jedit/jEdit/trunk/org" );
        SVNURL url = SVNURL.parseURIEncoded(SVNURI);
        String userName = "guest";
        String userPassword = "";

        //Prepare filesystem directory (export destination).
        File exportDir = new File( "../../svn-export" );
        if ( exportDir.exists( ) ) {
            System.out.println("Delete export folder");
            boolean deleted = deleteDir(exportDir);
            if(deleted)
                System.out.println("Export folder deleted");
            else {
                System.out.println("Export folder NOT deleted");
                SVNErrorMessage err = SVNErrorMessage.create( SVNErrorCode.IO_ERROR , "Path ''{0}'' already exists" , exportDir );
                throw new SVNException( err );
            }

        }
        exportDir.mkdirs( );

        SVNRepository repository = SVNRepositoryFactory.create( url );

        ISVNAuthenticationManager authManager = SVNWCUtil.createDefaultAuthenticationManager( userName, userPassword );
        repository.setAuthenticationManager( authManager );

        SVNNodeKind nodeKind = repository.checkPath( "" , -1 );
        if ( nodeKind == SVNNodeKind.NONE ) {
            SVNErrorMessage err = SVNErrorMessage.create( SVNErrorCode.UNKNOWN , "No entry at URL ''{0}''" , url );
            throw new SVNException( err );
        } else if ( nodeKind == SVNNodeKind.FILE ) {
            SVNErrorMessage err = SVNErrorMessage.create( SVNErrorCode.UNKNOWN , "Entry at URL ''{0}'' is a file while directory was expected" , url );
            throw new SVNException( err );
        }

        //Get latest repository revision. We will export repository contents at this very revision.
        long latestRevision = repository.getDatedRevision(versionDate);
        System.out.println(latestRevision + " " + versionDate);
        ISVNReporterBaton reporterBaton = new ExportReporterBaton( latestRevision );

        ISVNEditor exportEditor = new ExportEditor( exportDir );

        /*
        * Now ask SVNKit to perform generic 'update' operation using our reporter and editor.
        *                      
        * We are passing:
        *
        * - revision from which we would like to export
        * - null as "target" name, to perform export from the URL SVNRepository was created for,
        *   not from some child directory.
        * - reporterBaton
        * - exportEditor.
        */
        repository.update( latestRevision , null , true , reporterBaton , exportEditor );

        System.out.println( "Exported revision: " + latestRevision );
        return latestRevision;

    }
}
