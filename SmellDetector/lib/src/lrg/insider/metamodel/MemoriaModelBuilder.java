package lrg.insider.metamodel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.managers.EntityTypeManager;
import lrg.common.metamodel.ModelBuilder;
import lrg.common.utils.ProgressObserver;
import lrg.memoria.core.Annotation;
import lrg.memoria.core.AnnotationInstance;
import lrg.memoria.core.Attribute;
import lrg.memoria.core.Class;
import lrg.memoria.core.DataAbstraction;
import lrg.memoria.core.Function;
import lrg.memoria.core.GlobalFunction;
import lrg.memoria.core.GlobalVariable;
import lrg.memoria.core.InheritanceRelation;
import lrg.memoria.core.LocalVariable;
import lrg.memoria.core.Method;
import lrg.memoria.core.ModelElementList;
import lrg.memoria.core.Namespace;
import lrg.memoria.core.Package;
import lrg.memoria.core.Parameter;
import lrg.memoria.core.Statute;
import lrg.memoria.core.Subsystem;
import lrg.memoria.core.System;
import lrg.memoria.core.Type;
import lrg.memoria.core.TypedefDecorator;
import lrg.memoria.core.Variable;


public abstract class MemoriaModelBuilder extends ModelBuilder {
	protected String sourcePath;
	protected ProgressObserver progressObserver;
	protected System currentSystem;
	protected String programmingLanguage = "";
	
	public MemoriaModelBuilder(String sourcePath, String cachePath, ProgressObserver observer) {
		this.sourcePath = sourcePath;
		this.cachePath = cachePath;
		progressObserver = observer;
	}

	public MemoriaModelBuilder(String sourcePath, String cachePath, ProgressObserver observer, String language) {
		this.sourcePath = sourcePath;
		this.cachePath = cachePath;
		progressObserver = observer;
		programmingLanguage = language;
	}

	public System getCurrentSystem() {
		return currentSystem;
	}

	public void buildModel() throws Exception {
		loadModel();
		createEntityTypes();

		attachSpecificEntityTypes();
		 
		currentSystem.programmingLanguage = programmingLanguage;
	}

	public void cleanModel() {
		System.unloadSystemFromMemory(currentSystem);
		currentSystem = null;
	}

	protected abstract void loadModel() throws Exception;

	private void attachSpecificEntityTypes() {
		for (int i = 0; i < currentSystem.getNamespaces().size(); i++) {
			Namespace currentNamespace = (Namespace) currentSystem.getNamespaces().get(i);
			Function aFunction;
			for (int j = 0; j < currentNamespace.getGlobalFunctionsList().size(); j++) {
				aFunction = (Function) currentNamespace.getGlobalFunctionsList().get(j);
				if (aFunction instanceof GlobalFunction) {
					registerLocalVariables(aFunction);
					registerParameters(aFunction);
					aFunction.setEntityType(EntityTypeManager.getEntityTypeForName("global function"));
					addressMap.put(Address.buildFor(aFunction), aFunction);
				}
			}

			Variable aVariable;
			for (int j = 0; j < currentNamespace.getGlobalVariablesList().size(); j++) {
				aVariable = (Variable) currentNamespace.getGlobalVariablesList().get(j);
				if (aVariable instanceof GlobalVariable) {
					aVariable.setEntityType(EntityTypeManager.getEntityTypeForName("global variable"));
					addressMap.put(Address.buildFor(aVariable), aVariable);
				}
			}
			for (Type currentType : currentNamespace.getAllTypesList()) {
				if (currentType instanceof Class == false) {
					currentType.setEntityType(EntityTypeManager.getEntityTypeForName("type"));
					addressMap.put(Address.buildFor(currentType), currentType);
				}

			}

			currentNamespace.setEntityType(EntityTypeManager.getEntityTypeForName("namespace"));
			addressMap.put(Address.buildFor(currentNamespace), currentNamespace);
		}


		int i = 0;
		for (Package currentPackage : currentSystem.getPackages()) {
			int j = 0;
			for (Type currentType : currentPackage.getAllTypesList()) {
				if (currentType instanceof Class) {					
					currentType.setEntityType(EntityTypeManager.getEntityTypeForName("class"));
					addressMap.put(Address.buildFor(currentType), currentType);

					Class currentDataAbstraction = (Class) currentType;

					for(DataAbstraction aSuperType : currentDataAbstraction.getAncestorsList()) {
						aSuperType.setEntityType(EntityTypeManager.getEntityTypeForName("class"));
						addressMap.put(Address.buildFor(aSuperType), aSuperType);						
					}
					for (int k = 0; k < currentDataAbstraction.getMethodList().size(); k++) {
						Method currentMethod = (Method) currentDataAbstraction.getMethodList().get(k);
						registerLocalVariables(currentMethod);
						registerParameters(currentMethod);
						registerAnnotationInstances(currentMethod);                        
						currentMethod.setEntityType(EntityTypeManager.getEntityTypeForName("method"));
						addressMap.put(Address.buildFor(currentMethod), currentMethod);
					}
					for (int k = 0; k < currentDataAbstraction.getAttributeList().size(); k++) {
						Attribute currentAttribute = (Attribute) currentDataAbstraction.getAttributeList().get(k);

						for (int l = 0; l < currentAttribute.getAnnotations().size(); l++) {
							AnnotationInstance currentAnnotationInstance = (AnnotationInstance) currentAttribute.getAnnotations().get(l);
							currentAnnotationInstance.setEntityType(EntityTypeManager.getEntityTypeForName("annotation instance"));
							addressMap.put(Address.buildFor(currentAnnotationInstance), currentAnnotationInstance);
						}

						currentAttribute.setEntityType(EntityTypeManager.getEntityTypeForName("attribute"));
						addressMap.put(Address.buildFor(currentAttribute), currentAttribute);
					}


					for (int k = 0; k < currentDataAbstraction.getAnnotations().size(); k++) {
						AnnotationInstance currentAnnotationInstance = (AnnotationInstance) currentDataAbstraction.getAnnotations().get(k);
						currentAnnotationInstance.setEntityType(EntityTypeManager.getEntityTypeForName("annotation instance"));
						addressMap.put(Address.buildFor(currentAnnotationInstance), currentAnnotationInstance);
					}


				} else if ((currentType instanceof TypedefDecorator) || ((currentType instanceof DataAbstraction))) {
					currentType.setEntityType(EntityTypeManager.getEntityTypeForName("type"));
					addressMap.put(Address.buildFor(currentType), currentType);

				}
			}

			if(currentPackage.getAnnotationsList() != null) {
				for(Annotation currentAnnotation : currentPackage.getAnnotationsList()) {

					//added for annotation instances (meta-annotations)
					for (int l = 0; l < currentAnnotation.getAnnotations().size(); l++) {
						AnnotationInstance currentAnnotationInstance = (AnnotationInstance) currentAnnotation.getAnnotations().get(l);
						currentAnnotationInstance.setEntityType(EntityTypeManager.getEntityTypeForName("annotation instance"));
						addressMap.put(Address.buildFor(currentAnnotationInstance), currentAnnotationInstance);
					}

					currentAnnotation.setEntityType(EntityTypeManager.getEntityTypeForName("annotation"));
					addressMap.put(Address.buildFor(currentAnnotation), currentAnnotation);            	
				}
			}

			//added for annotation instances
			for (int k = 0; k < currentPackage.getAnnotations().size(); k++) {
				AnnotationInstance currentAnnotationInstance = (AnnotationInstance) currentPackage.getAnnotations().get(k);
				currentAnnotationInstance.setEntityType(EntityTypeManager.getEntityTypeForName("annotation instance"));
				addressMap.put(Address.buildFor(currentAnnotationInstance), currentAnnotationInstance);
			}

			currentPackage.setEntityType(EntityTypeManager.getEntityTypeForName("package"));
			addressMap.put(Address.buildFor(currentPackage), currentPackage);
		}

		currentSystem.setEntityType(EntityTypeManager.getEntityTypeForName("system"));
		String systemName = currentSystem.getName();
		currentSystem.setName(systemName.substring(systemName.lastIndexOf("\\") + 1));
		addressMap.put(Address.buildForRoot(), currentSystem);

		lrg.insider.plugins.core.properties.memoria.inheritance.Scope.clear();
		for (Package currentPackage : currentSystem.getPackages()) {
			for (Type currentType : currentPackage.getAllTypesList()) {
				if (currentType instanceof DataAbstraction) {
					DataAbstraction currentDataAbstraction = (DataAbstraction) currentType;
					ArrayList<InheritanceRelation> list = currentDataAbstraction.getRelationAsSuperClass();
					for(int k = 0; k < list.size(); k++) {
						InheritanceRelation rel = list.get(k);
						lrg.insider.plugins.core.properties.memoria.inheritance.Scope.registerScopeProperty(currentSystem,rel);
						rel.setEntityType(EntityTypeManager.getEntityTypeForName("inheritance relation"));
						addressMap.put(Address.buildFor(currentDataAbstraction)+"<->"+Address.buildFor(rel.getSubClass()), rel);
					}
				}
			}
		}
		registerSubsystems(currentSystem);
	}

	private void registerSubsystems(System currentSystem) {
		HashMap<String, ArrayList> theSubsystems = new HashMap<String, ArrayList>();
		int NESTING_LEVEL = 3;

		ModelElementList<Package> thePackages = currentSystem.getPackages();

		for (Package aPackage : thePackages) {
			if(aPackage.getStatute() != Statute.NORMAL) continue; 
			String subsystemName = getSubsystemName(aPackage.getName(), NESTING_LEVEL);
			addPackageToSubsystemMap(subsystemName, aPackage, theSubsystems);
		}

		Set<String> keys = theSubsystems.keySet();

		ArrayList<AbstractEntityInterface> groupOfSubsystems = new ArrayList<AbstractEntityInterface>();
		for (String subsystemName : keys) {
			Subsystem aSubsystem = new Subsystem(subsystemName, theSubsystems.get(subsystemName));
			aSubsystem.setEntityType(EntityTypeManager.getEntityTypeForName("subsystem"));
			currentSystem.addSubsystem(aSubsystem);
			addressMap.put(Address.buildFor(aSubsystem), aSubsystem);			
			groupOfSubsystems.add(aSubsystem);
		}
		
		for(Subsystem aSubsystem : currentSystem.getSubsystems()) 
			for(Package aPackage : aSubsystem.getPackages())
				aPackage.setSubsystem(aSubsystem);

	}

	private void addPackageToSubsystemMap(String subsystemName, AbstractEntityInterface aPackage, HashMap<String, ArrayList> theSubsystems) {
		ArrayList<AbstractEntityInterface> thePackages = theSubsystems.get(subsystemName);

		if (thePackages == null) {
			thePackages = new ArrayList<AbstractEntityInterface>();
			theSubsystems.put(subsystemName, thePackages);
		}

		thePackages.add(aPackage);
	}

	private  String getSubsystemName(String name, int nestingLevel) {
		int crtIndex = 0;

		for (int i = 0; i < nestingLevel; i++) {
			int crtDot = name.indexOf(".", crtIndex);
			if (crtDot == -1)
				return name;
			crtIndex = crtDot + 1;
		}

		return name.substring(0, crtIndex - 1);
	}



	private void registerAnnotationInstances(Method currentMethod) {
		for (int k = 0; k < currentMethod.getAnnotations().size(); k++) {
			AnnotationInstance currentAnnotationInstance = (AnnotationInstance) currentMethod.getAnnotations().get(k);
			currentAnnotationInstance.setEntityType(EntityTypeManager.getEntityTypeForName("annotation instance"));
			addressMap.put(Address.buildFor(currentAnnotationInstance), currentAnnotationInstance);
		}
	}

	private void registerLocalVariables(Function currentFunction) {
		for (LocalVariable lv : currentFunction.getBody().getLocalVarList()) {

			//added for annotation instances
			for (int l = 0; l < lv.getAnnotations().size(); l++) {
				AnnotationInstance currentAnnotationInstance = (AnnotationInstance) lv.getAnnotations().get(l);
				currentAnnotationInstance.setEntityType(EntityTypeManager.getEntityTypeForName("annotation instance"));
				addressMap.put(Address.buildFor(currentAnnotationInstance), currentAnnotationInstance);
			}

			lv.setEntityType(EntityTypeManager.getEntityTypeForName("local variable"));
			addressMap.put(Address.buildFor(lv), lv);
		}
	}

	private void registerParameters(Function currentFunction) {
		for (Parameter param : currentFunction.getParameterList()) {

			//added for annotation instances
			for (int l = 0; l < param.getAnnotations().size(); l++) {
				AnnotationInstance currentAnnotationInstance = (AnnotationInstance) param.getAnnotations().get(l);
				currentAnnotationInstance.setEntityType(EntityTypeManager.getEntityTypeForName("annotation instance"));
				addressMap.put(Address.buildFor(currentAnnotationInstance), currentAnnotationInstance);
			}

			param.setEntityType(EntityTypeManager.getEntityTypeForName("parameter"));
			addressMap.put(Address.buildFor(param), param);
		}
	}

	protected void registerEntityTypes() {
		EntityTypeManager.registerEntityType("result", "");
		EntityTypeManager.registerEntityType("numerical", "");
		EntityTypeManager.registerEntityType("boolean", "");
		EntityTypeManager.registerEntityType("string", "");

		EntityTypeManager.registerEntityType("group", "");

		EntityTypeManager.registerEntityType("system", "");
		EntityTypeManager.registerEntityType("subsystem", "system");
		EntityTypeManager.registerEntityType("package", "subsystem");
		// EntityTypeManager.registerEntityType("namespace", "system");
		EntityTypeManager.registerEntityType("class", "package");
		/*comment here*/ EntityTypeManager.registerEntityType("type", "namespace");
		EntityTypeManager.registerEntityType("method", "class");
		EntityTypeManager.registerEntityType("attribute", "class");
		EntityTypeManager.registerEntityType("local variable", "method");
		EntityTypeManager.registerEntityType("local variable", "global function");
		EntityTypeManager.registerEntityType("parameter", "method");
		EntityTypeManager.registerEntityType("parameter", "global function");

		EntityTypeManager.registerEntityType("global function", "package");
		EntityTypeManager.registerEntityType("global variable", "package");
		EntityTypeManager.registerEntityType("inheritance relation", "system");


		// added for Component
		EntityTypeManager.registerEntityType("component", "");

		EntityTypeManager.registerEntityType("bug", "");


		EntityTypeManager.registerEntityType("annotation", "package");
		EntityTypeManager.registerEntityType("annotation instance", "");

	}
}

