package lrg.insider.gui.ui.filter;

import lrg.common.abstractions.entities.AbstractEntityInterface;
import lrg.common.abstractions.plugins.Descriptor;
import lrg.common.abstractions.plugins.filters.FilteringRule;

/**
 * Created by IntelliJ IDEA.
 * User: cristic
 * Date: May 11, 2004
 * Time: 2:14:22 PM
 * To change this template use File | Settings | File Templates.
 */
public class WannabeFilteringRule extends FilteringRule
{
    public static WannabeFilteringRule instance()
    {
        if (theInstance == null)
            theInstance = new WannabeFilteringRule();


        return theInstance;
    }

    private static WannabeFilteringRule theInstance;

    private WannabeFilteringRule()
    {
        super(new Descriptor("<html><font color=#ff0000>wannabe filteringRule</font></html>", "?"));
        //super(new Descriptor("wannabe filteringRule", "?"));
    }

    public boolean applyFilter(AbstractEntityInterface anEntity)
    {
        // System.err.println("ERROR: WannabeFilteringRule detected in composed rule. Tried to apply on entity: " + anEntity.getName());
        return false;
    }
}
