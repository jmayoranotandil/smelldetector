package lrg.insider.gui.ui.utils;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JProgressBar;

import lrg.common.utils.ProgressObserver;

public class ProgressBar implements ProgressObserver {
    private JFrame frame;
    private JProgressBar insiderProgress = new JProgressBar();
    private int contor = 0;

    public ProgressBar(String label) {
        insiderProgress.setString(label);
        insiderProgress.setStringPainted(true);


        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout());
        panel.add(insiderProgress, BorderLayout.CENTER);

        frame = new JFrame();
        frame.getContentPane().add(panel, BorderLayout.CENTER);
        frame.setLocation(400, 300);
        frame.setSize(250, 60);
        frame.setVisible(true);
        frame.setResizable(false);
    }

    public void setMaxValue(int max) {
        insiderProgress.setMaximum(max);
    }

    public void increment() {
        contor++;
        insiderProgress.setValue(contor);
    }

    public void close() {
        frame.setVisible(false);
    }
}
