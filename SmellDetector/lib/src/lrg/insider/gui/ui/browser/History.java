package lrg.insider.gui.ui.browser;

public class History {
    private HistoryDetail[] historyDetails;
    private int currentHistoryIndex;
    private int lastHistoryIndex;

    public History(int maxAddressesToRemember) {
        historyDetails = new HistoryDetail[maxAddressesToRemember];
        currentHistoryIndex = -1;
        lastHistoryIndex = -1;
    }

    public int numberOfBackLocations() {
        return currentHistoryIndex;
    }

    public int numberOfForwardLocations() {
        return (lastHistoryIndex - currentHistoryIndex);
    }

    public HistoryDetail getBackDetail() {
        if (numberOfBackLocations() > 0) {
            currentHistoryIndex--;
            return historyDetails[currentHistoryIndex];
        }
        else {
            return null;
        }
    }

    public HistoryDetail getForwardDetail()
    {
        if (numberOfForwardLocations() > 0) {
            currentHistoryIndex++;
            return historyDetails[currentHistoryIndex];
        }
        else {
            return null;
        }
    }

    public void addNewDetail(String address, String detailName)
    {
        if (currentHistoryIndex == (historyDetails.length - 1)) {
            for (int i = 1; i < historyDetails.length; i++) {
                historyDetails[i - 1] = historyDetails[i];
            }
        } else {
            currentHistoryIndex++;
        }

        historyDetails[currentHistoryIndex] = new HistoryDetail(address, detailName);
        lastHistoryIndex = currentHistoryIndex;
    }
}
