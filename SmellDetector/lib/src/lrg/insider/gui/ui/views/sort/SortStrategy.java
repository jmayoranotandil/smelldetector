package lrg.insider.gui.ui.views.sort;

import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: cristic
 * Date: May 5, 2004
 * Time: 12:47:20 PM
 * To change this template use File | Settings | File Templates.
 */
abstract public class SortStrategy
{
    abstract public void sort(ArrayList unsortedColumnValues, int[] indexMap, boolean ascending);

    // lol , I'm so stupid...
    // this doesn't work because Java doesn't pass basic types parameters as references (it passes them by value)
    protected void swap(int i, int j)
    {
        int temp = i;
        i = j;
        j = temp;
    }
}
