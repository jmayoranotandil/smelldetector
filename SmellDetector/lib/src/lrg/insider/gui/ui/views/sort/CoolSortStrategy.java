package lrg.insider.gui.ui.views.sort;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import lrg.common.abstractions.entities.ResultEntity;

public class CoolSortStrategy extends SortStrategy {
    public void sort(ArrayList unsortedColumnValues, int[] indexMap, boolean ascending) {
        ArrayList indexList = new ArrayList();
        for (int i = 0; i < indexMap.length; i++) indexList.add(new Integer(indexMap[i]));
        Collections.sort(indexList, new ResultEntityComparator(unsortedColumnValues));
        if (ascending == false) Collections.reverse(indexList);

        for (int i = 0; i < indexList.size(); i++) {
            indexMap[i] = ((Integer) indexList.get(i)).intValue();
        }
    }
}


class ResultEntityComparator implements Comparator {
    private ArrayList unsortedColumnValues;

    public ResultEntityComparator(ArrayList columnValues) {
        unsortedColumnValues = columnValues;
    }

    public int compare(Object o, Object o1) {
        int i = ((Integer) o).intValue();
        int j = ((Integer) o1).intValue();

        ResultEntity ri = (ResultEntity) unsortedColumnValues.get(i);
        ResultEntity rj = (ResultEntity) unsortedColumnValues.get(j);

        if (ri == null) return 1;
        return -ri.compareTo(rj);
    }
}