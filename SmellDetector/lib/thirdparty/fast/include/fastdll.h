
/*
  ********
  ********   FAST
  ********
  ********

  Copyright Sema Group - 1998

  ********

Olivier Roth - 11/6/98
 - Creation

*/
#ifdef WIN32PC

#undef IMPORT
#if FAST_H_DLL == 1
#define IMPORT __declspec( dllexport )
#else
#define IMPORT __declspec( dllimport )
#endif

#endif /* WIN32PC */
