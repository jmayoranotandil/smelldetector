package lrg.dude.gui;

import lrg.dude.duplication.CodeFragment;

import javax.swing.*;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultHighlighter;
import javax.swing.text.Highlighter;
import java.awt.*;
import java.io.File;
import java.io.FileReader;

/**
 * Created by IntelliJ IDEA.
 * User: Richard
 * Date: 20.05.2004
 * Time: 20:55:05
 * To change this template use File | Settings | File Templates.
 */
public class FileViewer extends JPanel {
    private JTextArea file;

    private Highlighter highlighter = new DefaultHighlighter();;

    public FileViewer() {
        setLayout(new GridLayout(1, 1));
        file = new JTextArea();
        file.setEditable(false);
        JScrollPane file1Pane = new JScrollPane(file,
                ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
                ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        setBorder(BorderFactory.createTitledBorder("<empty>"));
        setMinimumSize(new Dimension(200, 75));
        setPreferredSize(new Dimension(400, 150));
        add(file1Pane);
    }

    public void viewFile(String startingPath, String fileName, CodeFragment code) {
        try {
            FileReader fr = new FileReader(startingPath + File.separator + code.getEntity().getName());
            file.read(fr, null);
            setBorder(BorderFactory.createTitledBorder(code.getEntity().getName()));
            fr.close();
        } catch (Exception ex) {
            System.err.println("Could not load page files");
        }
        try {
            int offsetStart = file.getLineStartOffset((int) code.getBeginLine() - 1);
            int offsetEnd = file.getLineEndOffset((int) code.getEndLine() - 1);

            highlighter.install(file);
            //LIGHT_GRAY, CYAN, GREEN, ORANGE, YELLOW, 16777164 (galben f deschis),16777113 (galben mai inchis)
            Highlighter.HighlightPainter painter =
                    new DefaultHighlighter.DefaultHighlightPainter(new Color(16777113));

            file.getHighlighter().removeAllHighlights();

            file.getHighlighter().addHighlight(offsetStart, offsetEnd, painter);
            file.setCaretPosition(offsetStart);
        } catch (BadLocationException e1) {
            e1.printStackTrace();
        }
    }

    public void clear() {
        file.setText("");
        setBorder(BorderFactory.createTitledBorder("<empty>"));
    }
}
